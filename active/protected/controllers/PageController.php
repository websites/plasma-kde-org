<?php
Class PageController Extends Controller {
	public function restrict() {
		if($_SESSION['admin'])
			return true;

		if($_POST) {
			$hashed = trim(file_get_contents('protected/password'));

			if($hashed === md5($_POST['password'])) {
				$_SESSION['admin'] = True;
				return True;
			}
		}

		$this->render('login');
		throw new CHttpException(403);
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionAdmin() {
		$this->restrict();
		$this->render('admin');
	}

	public function actionCreate() {
		$this->restrict();
		$model = New Page;

		if($_POST) {
			$model->title = $_POST['Page']['title'];
			$model->description = $_POST['Page']['description'];
			$model->line = $_POST['Page']['line'];
			$model->icon = CUploadedFile::getInstance($model, 'icon');

			if($model->save()) {
				$model->icon->saveAs(Yii::app()->runtimePath.'/icons/'.$model->icon->name);

				if($_POST['Link']) {
					$link = New Link;
					$link->attributes = $_POST['Link'];
					$link->page_id = $model->id;
					$link->save();
				}

				$image = CUploadedFile::getInstanceByName('Image');
				if($image)
					$image->saveAs($model->imagesPath.'/'.$image->name);

				$this->redirect(Array('/page/update', array('id' => $model->id)));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate() {
		$this->restrict();
		$model = Page::model()->findByPK($_GET['id']);

		if($_POST) {
			$model->title = $_POST['Page']['title'];
			$model->description = $_POST['Page']['description'];
			$model->line = $_POST['Page']['line'];

			$icon = CUploadedFile::getInstance($model, 'icon');

			if($icon)
				$model->icon = $icon;

			if($model->save()) {
				foreach($model->links as $link) {
					if(!$_POST['Link'][$link->id]) {
						$link->delete();
						continue;
					}

					$link->title = $_POST['Link'][$link->id]['title'];
					$link->url = $_POST['Link'][$link->id]['url'];

					$link->save();
				}

				if($_POST['Link']['title']) {
					$link = New Link;
					$link->attributes = $_POST['Link'];
					$link->page_id = $model->id;
					$link->save();
				}

				if($model->icon InstanceOf CUploadedFile)
					$model->icon->saveAs(Yii::app()->runtimePath.'/icons/'.$model->icon->name);

				$model->getRelated('links', True); //Refresh!

				$image = CUploadedFile::getInstanceByName('Image');
				if($image)
					$image->saveAs($model->imagesPath.'/'.$image->name);

				if(is_array($_POST['delete_image']))
					foreach($_POST['delete_image'] as $filename => $null)
						if($filename)
							unlink($model->imagesPath.'/'.$filename);
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionDelete() {
		$this->restrict();
		Page::model()->findByPK($_GET['id'])->delete();
	}

	public function actionJson() {
		$page = Page::model()->findByPK($_GET['id']);

		$path = Yii::app()->runtimePath.'/icons/'.$page->icon;
		Yii::app()->thumb->load($path);
		Yii::app()->thumb->resize(150, 150);
		Yii::app()->thumb->save($page->id.'_150.png', 'png');
		$url = Yii::app()->baseUrl.'/assets/'.$page->id.'_150.png';

		$links = array();
		foreach($page->links as $link)
			$links[] = array('title' => $link->title, 'url' => $link->url);

		$array['title'] = $page->title;
		$array['description'] = $page->description;
		$array['links'] = $links;
		$array['icon'] = $url;
		$array['images'] = array();

		foreach($page->images as $image) {
			Yii::app()->thumb->load($image);
			Yii::app()->thumb->resize(700, 400);
			$filename = explode('/', $image);
			$filename = $filename[count($filename) - 1];
			$filename = $model->id.'_700_400_'.$filename;

			Yii::app()->thumb->save($filename, end(explode('.', $filename)));
			$url = Yii::app()->baseUrl.'/assets/'.$filename;
			$array['images'][] = $url;
		}

		print json_encode($array);
	}

	public function actionSubscribe() {
		$subscriber = New Subscriber;
		$subscriber->email = $_POST['email'];
		print json_encode(array('result' => $subscriber->save(), 'error' => current($subscriber->errors)));
	}
}