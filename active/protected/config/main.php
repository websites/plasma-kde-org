<?php
return  array(
	'name' => 'Plasma Active',
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' ,

	'import'=>array(
			'application.models.*',
			'application.components.*',
			'application.controllers.*',
	),
	'components' => array(

		'errorHandler' => array(
			'class' => 'CErrorHandler',
			'discardOutput' => False
		),

		'db'=>array(
				'class' => 'CDbConnection',
				'connectionString' => 'mysql:host=localhost;dbname=active',
				'emulatePrepare' => true,
				'username' => 'root',
				'password' => 'root',
				'charset' => 'utf8',
				'tablePrefix' => 'tbl_',
				'enableProfiling' => true,
				'enableParamLogging' => true
		),
		'urlManager'=>array(

			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'' => 'page/index'
			),
		),

		'user' => array(
			'loginUrl' => array('/user/login')
		),

		'thumb' => array(
			'class' => 'ext.phpthumb.EasyPhpThumb',
			'thumbsDirectory' => '/assets'
		),
	)
);
?>
 
