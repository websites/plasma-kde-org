<?php
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/canvas.text.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/faces/optimer-normal-normal.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/cake.js');
$pages = Page::model()->findAll();
?>
<script type="text/javascript">
	function createCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function getParameter(url, name) {
		var urlparts = url.split('?');
		if (urlparts.length > 1) {
			var parameters = urlparts[1].split('&');
			for (var i = 0; i < parameters.length; i++) {
				var paramparts = parameters[i].split('=');
				if (paramparts.length > 1 && unescape(paramparts[0]) == name) {
					return unescape(paramparts[1]);
				}
			}
		}
		return null;
	}

	window.onload = function() {
		pages = new Array();
		<?php
		foreach(Page::model()->lines as $lineId => $lineName)
			echo "pages[{$lineId}] = new Array();";

		$linesCount = Array();
		foreach($pages = Page::model()->findAll() as $index => $page) {
			$path = Yii::app()->runtimePath.'/icons/'.$page->icon;
			Yii::app()->thumb->load($path);
			Yii::app()->thumb->resize(32, 32);
			Yii::app()->thumb->save($page->id.'.png', 'png');
			$url = Yii::app()->baseUrl.'/assets/'.$page->id.'.png';

			$index = (Int)$linesCount[$page->line]++;
			echo "pages[{$page->line}][{$index}] = {
				id:'{$page->id}',
				icon:'{$url}',
				title:'{$page->title}'
			};";
		}

		?>

		var map = new Array();
		map[0] = new Array();
		map[0]['color'] = [128,198,128,1];

		map[1] = new Array();
		map[1]['color'] = [245,185,152,1];

		map[2] = new Array();
		map[2]['color'] = [223,129,129,1];

		map[3] = new Array();
		map[3]['color'] = [249,225,128,1];

		map[4] = new Array();
		map[4]['color'] = [222,169,206,1];

		map[0][0] = [40, 40];
		map[0][1] = [80, 75];
		map[0][2] = [130, 75];
		map[0][3] = [170, 75];
		map[0][4] = [215, 75];
		map[0][5] = [260, 75];
		map[0][6] = [310, 75];
		map[0][7] = [360, 75];
		map[0][8] = [400, 75];
		map[0][9] = [450, 75];
		map[0][10] = [475, 105];
		map[0][11] = [475, 145];

		map[1][0] = [910, 32];
		map[1][1] = [840, 32];
		map[1][2] = [793, 50];
		map[1][3] = [793, 100];
		map[1][4] = [760, 150];
		map[1][5] = [690, 150];
		map[1][6] = [620, 150];

		map[2][0] = [30, 330];
		map[2][1] = [80, 330];
		map[2][2] = [130, 330];
		map[2][3] = [165, 325];
		map[2][4] = [180, 295];
		map[2][5] = [195, 265];
		map[2][6] = [245, 265];
		map[2][7] = [290, 265];
		map[2][8] = [335, 265];
		map[2][9] = [380, 265];
		map[2][10] = [420, 258];
	
		map[3][0] = [323, 363];
		map[3][1] = [390, 363];
		map[3][2] = [458, 363];
		map[3][3] = [540, 333];
		map[3][4] = [608, 315];

		map[4][0] = [919, 309];
		map[4][1] = [874, 309];
		map[4][2] = [829, 309];
		map[4][3] = [767, 309];
		map[4][4] = [722, 278];
		map[4][5] = [675, 278];
		map[4][6] = [630, 278];
		map[4][7] = [582, 278];
		map[4][8] = [537, 278];
		map[4][9] = [510, 249];

		drawTicker = function() {
			var links = new Array();
			links[0] = {
				title:'Foo bar bar bar do do d',
				line:0
			};

			links[1] = {
				title:'fweoi fwfwef',
				line:1
			};

			links[2] = {
				title:'Bang',
				line:3
			};

			links[3] = {
				title:'Dalang Doloong balang',
				line:2
			};

			var ticker = new Rectangle(550, 60, {x:230, stroke:'white', y:-10, rx:10});
			var items = new Rectangle(0, 60, {y:30, stroke:false});
			var scroll = new Timeline({repeat:true});
			scroll.addKeyframe(0, {x:0});
			var time = 0;

			links.forEach(function(link) {
				var circle = new Circle(7, {x:items.width, stroke:'black', strokeWidth:2.5, fill:map[link.line]['color']});
				var title = new TextNode(link.title, {x:circle.x + 15, fill:'black', y:7, font:'17px Optimer'});

				items.append(circle);
				items.append(title);

				items.width = items.width + 1000;

				time = time + 500;
				scroll.addKeyframe(time, {x:-circle.x+30});
				time = time + 5000;
				scroll.addKeyframe(time, {x:-circle.x+30});
			});

			scroll.addKeyframe(time, {x:0});
			items.addTimeline(scroll);

			items.width = ticker.width;

			ticker.append(items);
			Canvas.append(ticker);
		}

		loadingImage = function() {
			var loading = new Image();
			loading.src = '<?php echo Yii::app()->baseUrl;?>/css/images/loading.png';
			loading = new ImageNode(loading);
			var rotate = new Timeline({repeat:true});
			rotate.addKeyframe(0, {rotation:[0, 8, 8]});
			rotate.addKeyframe(500, {rotation:[2*Math.PI, 8, 8]});
			rotate.addKeyframe(500, {rotation:[0, 8, 8]});
			loading.addTimeline(rotate);
			return loading;
		}

		drawIcon = function(i, j) {
			var image = new Image();
			image.src = pages[i][j]['icon'];

			var node = new ImageNode(image);
			node.line = i;
			node.number = j;
			node.x = map[i][j][0];
			node.y = map[i][j][1];

			node.cursor = "pointer";

			Canvas.font = "15px Optimer";

			node.addEventListener('mouseover', function() {
				var grow = new Timeline();
				grow.addKeyframe(0, {scale:1}, 'sine');
				grow.addKeyframe(100, {scale:1.5}, 'sine');
				this.addTimeline(grow);

				this.title = new TextNode(pages[this.line][this.number]['title'], {x:-10, y:45});
				this.append(this.title);

				this.zIndex = 2;
			});

			node.addEventListener('mouseout', function() {
				this.removeChild(this.title);
				var shrink = new Timeline();
				shrink.addKeyframe(0, {scale:this.scale}, 'sine');
				shrink.addKeyframe(200, {scale:1});
				this.addTimeline(shrink);
				this.zIndex = 1;
			});

			node.showDetails = function() {
				hideTextbox();

				jQuery.ajax({
					url: '<?php echo $this->createAbsoluteUrl('/page/json/');?>/'+pages[node.line][node.number]['id'],
					dataType: 'json',
					success: function(page) {
						drawDetails(node, page);
					}
				});
			};

			node.addEventListener('click', node.showDetails);

			Canvas.append(node);

			return node;
		}

		drawPlaceholder = function(i, j) {
			var circle = new Circle(7, {x:map[i][j][0]+20, y:map[i][j][1]+20, stroke:'black', strokeWidth:2.5, fill:[255,255,255,1]});
			circle.line = i;
			circle.number = j;

			Canvas.append(circle);

			return circle;
		}

		Canvas = new Canvas(document.getElementById('pages'), 1000, 425);
		var routes = new Image();
		routes.src = '<?php echo Yii::app()->baseUrl;?>/css/images/routes.png';
		routes = new ImageNode(routes);
		Canvas.append(routes);

		routes.addEventListener('click', function(e) {
			if(e.target.tagName !== 'CANVAS')
				return ;
			hideTextbox();
		});

		for(var i=0; i< map.length; i++)
			for(var j=0; j<map[i].length; j++) {
				if(typeof(pages[i]) !== 'undefined' && typeof(pages[i][j]) !== 'undefined')
					var node = drawIcon(i, j);
				else
					var node = drawPlaceholder(i, j);

				map[i][j] = node;
			}

		var run = function() {
			var lineId = Math.floor(Math.random()*5);

			for(var i = 0;i<map[lineId].length;i++) {
				var circle = map[lineId][i];
				if(circle.image)
					continue;

				var blink = new Timeline();

				blink.addKeyframe((i*50)+100, {fill:[255,255,255,1]}, 'sine');
				blink.addKeyframe((i*50)+300, {fill:map[lineId]['color']}, 'sine');
				blink.addKeyframe((i*50)+600, {fill:[255,255,255,1]});
				circle.addTimeline(blink);
			}
		}

		setInterval(run, 2000);

// 		drawTicker();

		drawDetails = function(icon, pageInfo) {
			window.location.hash = icon.line+','+icon.number;

			Canvas.canvas.height = Canvas.height = Canvas.canvasContainer.style.height = 600;

			var overlay = createOverlay();

			var image = new Image();
			image.src = pageInfo.icon;
			var item = new ImageNode(image, {x:icon.x, y:icon.y});

			var t = new Timeline();
			t.addKeyframe(0, {x:item.x, y:item.y}, 'sine');
			t.addKeyframe(100, {x:0, y:0, scale:1});
			overlay.pane.append(item);
			item.addTimeline(t);

			var title = new TextNode(pages[icon.line][icon.number]['title'], {x:10, y:160, font: "15px optimer"});
			overlay.pane.append(title);

			pageInfo.description.split("\n").forEach(function(line, index) {
				var line = new TextNode(line, {x:170, y:30+(40*index), scale:2});
				overlay.pane.append(line);
			});

			var back = new Image();
			back.src = '<?php echo Yii::app()->baseUrl;?>/css/images/back.png';
			var back = new ImageNode(back, {x:Canvas.width - 24, y:Canvas.height - 24});
			buttonize(back);
			overlay.pane.append(back);

			back.addEventListener('click', function() {
				removeOverlay(overlay);
				Canvas.height = Canvas.canvas.height = Canvas.canvasContainer.style.height = 425;
				window.location.hash = '';
			});

			if(pageInfo.links.length > 0)
				overlay.pane.append(drawLinks(pageInfo.links));

			drawGallery(overlay, pageInfo);

			Canvas.append(overlay);
			Canvas.append(overlay.pane);
		}

		drawLinks = function(links) {
			Canvas.font = "14px Optimer";

			var box = new Rectangle(300, 40, {x:710, y:0, zIndex:1});
			box.fill = [192, 209, 218, 0.2];
			var title = new TextNode('More infortmation', {x:80, y:20, fill:'black'});
			box.append(title);

			links.forEach(function(link, i) {
				var domain = link.url.match(/:\/\/(.[^/]+)/)[1];

				if(domain == 'www.youtube.com' || domain == 'youtube.com')
					return ;

				var favicon = new Image();
				favicon.src = 'http://www.google.com/s2/favicons?domain='+domain;
				var image = new ImageNode(favicon, {x:10, y:box.height-15});

				var title = new TextNode(link.title, {x:30, y:box.height, fill:'blue'});

				image.cursor = title.cursor = 'pointer';
				image.href = title.href = link.url;

				var clicked = function() {
					window.open(this.href);
				}

				var mouseovered = function() {
					this.fill = 'black';
				}

				var mouseout = function() {
					this.fill = 'blue';
				}

				image.addEventListener('click', clicked);

				title.addEventListener('click', clicked);
				title.addEventListener('mouseover', mouseovered);
				title.addEventListener('mouseout', mouseout);

				box.append(image, title);

				box.height = box.height + 30;
			});

			return box;
		}

		drawGallery = function(overlay, pageInfo) {
			var images = new Array();

			var thumbnails = pageInfo.images;
			var youtubeUrls = {};

			pageInfo.links.forEach(function(link) {
				var domain = link.url.match(/:\/\/(.[^/]+)/)[1];

				if(domain !== 'www.youtube.com' && domain !== 'youtube.com')
					return ;

				var youtubeId = getParameter(link.url, 'v');
				var thumbnail = 'http://img.youtube.com/vi/'+youtubeId+'/0.jpg';
				youtubeUrls[thumbnail] = youtubeId;

				thumbnails.push(thumbnail);
			});

			if(thumbnails.length < 1)
				return ;

			overlay.pane.loadingImage = loadingImage();
			overlay.pane.loadingImage.y = 580;
			overlay.pane.append(overlay.pane.loadingImage);

			function loadingText(number) {
				Canvas.font = "13px Optimer"
				var node = new TextNode(number+'/'+thumbnails.length, {x:17, y:590});
				if(overlay.pane.loadingText)
					overlay.pane.removeChild(overlay.pane.loadingText);

				overlay.pane.loadingText = node;
				overlay.pane.append(node);

				if(number == thumbnails.length) {
					imagesContainer.move.addKeyframe(time, {x:0});
					var shrink = new Timeline();
					shrink.addKeyframe(0, {scale:1});
					shrink.addKeyframe(200, {scale:0.01});
					overlay.pane.loadingText.addTimeline(shrink);
					overlay.pane.loadingImage.addTimeline(shrink);
					overlay.pane.loadingText.after(200, function() {
						overlay.pane.removeChild(overlay.pane.loadingImage);
						overlay.pane.removeChild(overlay.pane.loadingText);
					});
				}
			}

			loadingText(0);

			var imagesContainer = new Rectangle(0, 300, {y:180});
			imagesContainer.move = new Timeline({repeat:true});
			imagesContainer.move.addKeyframe(0, {x:0});
			imagesContainer.addTimeline(imagesContainer.move);
			overlay.pane.append(imagesContainer);

			time = 0;
			var updateLoading = function() {
				var node = new ImageNode(this, {x:imagesContainer.width + ((images.length)*500)});

				if(youtubeUrls[this.src]) {
					var imageOverlay = new Rectangle(this.width, this.height, {fill:'#D7EAF5', opacity:0.6});
					node.append(imageOverlay);

					var playButton = new Image();
					playButton.src = '<?php echo Yii::app()->baseUrl;?>/css/images/play.png';
					playButton = new ImageNode(playButton, {x:216, y:156});
					node.append(playButton);

					playButton.cursor = imageOverlay.cursor = 'pointer';
					playButton.youtube = imageOverlay.youtube = youtubeUrls[this.src];

					var play = function() {
						imagesContainer.removeTimeline(imagesContainer.move);

						var div = E('div', '<iframe width=480 height=390 src="http://www.youtube.com/embed/'+this.youtube+'?autoplay=1&showinfo=0" />');
						var div = new ElementNode(div, {opacity:1, zIndex:2});
						node.append(div);

						var close = new Image();
						close.src = '<?php echo Yii::app()->baseUrl;?>/css/images/close.png';
						var close = new ImageNode(close, {x:480, y:-20, cursor:'pointer'});

						close.addEventListener('click', function() {
							node.removeChild(div);
							node.removeChild(close);
							imagesContainer.addTimeline(imagesContainer.move);
						});

						node.append(close);
					}

					imageOverlay.addEventListener('click', play);
					playButton.addEventListener('click', play);

					var hover = function() {
						imageOverlay.animateTo('opacity', 0.1, 200);
						playButton.animateTo('opacity', 0.4, 200);
					};
					imageOverlay.addEventListener('mouseover',hover);
					playButton.addEventListener('mouseover', hover);

					imageOverlay.addEventListener('mouseout', function() {
						this.animateTo('opacity', 0.6, 200);
						playButton.animateTo('opacity', 1, 200);
					});
				}

				imagesContainer.width = imagesContainer.width + this.width;
				imagesContainer.append(node);
				images.push(node);

				var x = (710 - this.width) / 2;
				imagesContainer.move.addKeyframe(time, {x:(-node.x + x)});
				time += 5000;

				imagesContainer.move.addKeyframe(time, {x:(-node.x + x)});
				time += 500;

				loadingText(images.length);
			}

			thumbnails.forEach(function(url) {
				var image = new Image();
				image.onload = image.load = updateLoading;
				image.src = url;
			});
		}

		drawSubscribe = function() {
			Canvas.font = "30px Optimer";

			var date = new Rectangle(235, 35, {rx:14, stroke:[0,0,0,1], strokeWidth:1.5, fill:[255,255,255,1], cx:430, cy:210, cursor:'pointer'});
			date.text = new TextNode('9.10.11', {cx:500, y:237, fill:[0,0,0,1]});

			Canvas.append(date);
			Canvas.append(date.text);

			hideTextbox = function() {
				if(!date.email || !date.email.active)
					return ;

				date.email.active = false;
				date.removeChild(date.email);
				date.removeChild(date.save);
				date.opacity = date.text.opacity = date.visible = date.text.visible = 1;
			}

			var showTextbox = function(e) {
				if(date.email && date.email.active)
					return ;

				date.opacity = date.text.opacity = 0;
				date.text.visible = 0;

				date.email = new E('input', {type:'email'});
				date.email.style.height = date.height;
				date.email.style.width = date.width;
				date.email = new ElementNode(date.email, {x:date.cx, y:date.cy, width:date.width, height:date.height, opacity:1});
				date.email.active = true;

				date.save = new E('input', {type:'button', value:'Save'});
				date.save.style.height = date.height;
				date.save = new ElementNode(date.save, {x:date.cx+date.width, y:date.cy, height:date.height, opacity:1});

				var saveEmail = function() {
					var image = loadingImage();
					image.opacity = 1;

					image.x = date.save.x + 55;
					image.y = date.save.y + 10;
					Canvas.append(image);
					if(date.save.result)
						Canvas.removeChild(date.save.result);

					jQuery.ajax({
						url: '<?php echo $this->createAbsoluteUrl('/page/subscribe/');?>',
						dataType: 'json',
						data:{email:date.email.content.value},
						type:'post',
						success: function(result) {
							Canvas.removeChild(image);

							if(result.result) {
								hideTextbox();

								var text = 'Thanks for subscribing!';
								var color = [0,140,0,1];
							} else {
								var text = result.error[0];
								var color = [255,0,0,1];
							}

							date.save.result = new TextNode(text, {x:date.save.x, y:date.save.y+50, fill:color, font:"13px Optimer"});
							Canvas.append(date.save.result);

							date.save.result.animateTo('fill', [255,255,255,0], 10000);
						}
					});
				}
				date.save.element.onclick = saveEmail;

				date.append(date.email);
				date.append(date.save);
				date.email.content.focus();
			};

			date.addEventListener('mouseover', showTextbox);

			var subscribe = new Image();
			subscribe.src = '<?php echo Yii::app()->baseUrl;?>/css/images/subscribe.png';
			subscribe.onload = function() {
				subscribe = new ImageNode(this, {x:520, y:110});
				var t = new Timeline({repeat:true});
				t.addKeyframe(0, {opacity:0});
				t.addKeyframe(3000, {opacity:1});
				t.addKeyframe(3500, {opacity:1});
				t.addKeyframe(6500, {opacity:0});
				subscribe.addTimeline(t);

				Canvas.append(subscribe);
			}
		}

		drawSubscribe();

		buttonize = function(button) {
			button.cursor = 'pointer';

			button.addEventListener('mouseover', function() {
				this.stroke = 'blue';
				this.strokeWidth = 0.5;
			});
			button.addEventListener('mouseout', function() {
				this.stroke = false;
			});
		}

		createOverlay = function() {
			var overlay = new Rectangle(Canvas.width, Canvas.height, {fill:[216,234,245,0.95], zIndex:3});
			overlay.pane = new Rectangle(Canvas.width, Canvas.height, {opacity:1, zIndex:4});
			return overlay;
		};

		removeOverlay = function(overlay) {
			var fadeOut = new Timeline();
			fadeOut.addKeyframe(0, {opacity:1});
			fadeOut.addKeyframe(300, {opacity: 0});
			overlay.after(300, function() {
				Canvas.remove(overlay.pane);
				Canvas.remove(overlay);
			});
			overlay.addTimeline(fadeOut);
			overlay.pane.addTimeline(fadeOut);
		}

		drawIntroduction = function() {
			createCookie('introduction_shown', true);

			var overlay = createOverlay();
			Canvas.append(overlay);
			Canvas.append(overlay.pane);

			var desc = "Plasma Active aims at creating a cross-device user experience for emerging devices such as tablet computers, media centers, smartphones, and more.\n Its being developed in five different aspects. You can track the progress of each aspect as they reach new milestones.";

			desc.split("\n").forEach(function(line, index) {
				var l = new TextNode(line, {font:'16px Optimer', x:10, y:30+(40*index)});
				overlay.pane.append(l);
			});

			var intro = new Image();
			intro.src = '<?php echo Yii::app()->baseUrl;?>/css/images/introduction.png';
			intro = new ImageNode(intro, {x:30, y:120});
			overlay.pane.append(intro);

			var go = new Rectangle(210, 80, {rx:20, x:500, y:250, fill:'white'});
			overlay.pane.append(go);

			go.text = new TextNode('Show me more!', {font:'30px Optimer', fill:'black', x:5, y:45});
			go.append(go.text);

			go.addEventListener('click', function() {
				removeOverlay(overlay);
			});

			buttonize(go);
		};

		var about = new TextNode('?', {fill:'black', font:"40px Optimer", y:35, x:1});
		var aboutContainer = new Rectangle(20, 40, {fill:'white', stroke:'black', x:970, y:380, cursor:'pointer'});
		aboutContainer.append(about);
		buttonize(aboutContainer);
		Canvas.append(aboutContainer);
		aboutContainer.addEventListener('click', drawIntroduction);

		if(window.location.hash) {
			var nums = window.location.hash.split(',');
			map[nums[0].substr(1,1)][nums[1]].showDetails();
		} else {
			if(!readCookie('introduction_shown'))
				drawIntroduction();
		}
	};
</script>

<div id="pages"></div>