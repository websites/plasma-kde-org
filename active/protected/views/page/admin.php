<a href="<?php echo $this->createUrl('/page/create');?>">
	<?php echo Yii::t('active', 'Add new page'); ?>
</a>

<?php
$dataProvider = new CActiveDataProvider('Page', array(
	'pagination' => False
));

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns' => array(
		'title',
		array(
			'class' => 'CButtonColumn',
			'template' => '{delete}{update}'
		)
	)
));
?>
<br />
<?php
echo Yii::t('active', 'Number of subscriptions: :num', array(':num' =>Subscriber::model()->count()));