<?php
Yii::app()->clientScript->registerCoreScript('jquery');

$form = $this->beginWidget('CActiveForm', array('htmlOptions' => array(
	'enctype' => 'multipart/form-data'
)));
?>

<div class="row">
	<?php echo $form->labelEx($model, 'title'); ?>
	<br />
	<?php echo $form->textField($model, 'title'); ?>
	<br />
	<?php echo $form->error($model, 'title'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'icon'); ?>
	<br />
	<?php echo $form->fileField($model, 'icon'); ?>
	<br />
	<?php echo $form->error($model, 'icon'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'line'); ?>
	<br />
	<?php echo $form->dropDownList($model, 'line', $model->lines); ?>
	<br />
	<?php echo $form->error($model, 'line'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model, 'description'); ?>
	<br />
	<?php echo $form->textArea($model, 'description'); ?>
	<br />
	<?php echo $form->error($model, 'description'); ?>
</div>

<fieldset>
	<legend>
		<?php echo Yii::t('active', 'New link'); ?>
	</legend>
	<div class="column">
		<?php
		$link = New Link;
		echo $form->labelEx($link, 'title');
		?>
		<br />
		<?php echo $form->textField($link, 'title'); ?>
	</div>
	<div class="column">
		<?php echo $form->labelEx($link, 'url'); ?>
		<br />
		<?php echo $form->textField($link, 'url');?>
	</div>
</fieldset>

<?php foreach($model->links as $link) { ?>
	<fieldset>
		<legend>
			<?php echo $link->title; ?>
		</legend>
		<div class="column">
			<?php
			echo $form->labelEx($link, 'title');
			?>
			<br />
			<?php echo $form->textField($link, '['.$link->id.']title'); ?>
		</div>
		<div class="column">
			<?php echo $form->labelEx($link, 'url'); ?>
			<br />
			<?php echo $form->textField($link, '['.$link->id.']url'); ?>
		</div>

		<input type="button" value="<?php echo Yii::t('active', 'Delete'); ?>" onclick="$(this).parents('fieldset').detach();" />
	</fieldset>
<?php } ?>
<fieldset>
	<legend>
		<?php echo Yii::t('active', 'New image'); ?>
	</legend>
	<div>
		<?php echo CHtml::fileField('Image'); ?>
	</div>
</fieldset>

<?php
foreach($model->images as $image) {
	Yii::app()->thumb->load($image);
	Yii::app()->thumb->resize(100, 70);
	$filename = explode('/', $image);
	$filename = $filename[count($filename) - 1];
	$thumbname = $model->id.'_100_700_'.$filename;

	Yii::app()->thumb->save($thumbname, end(explode('.', $thumbname)));
	$url = Yii::app()->baseUrl.'/assets/'.$thumbname;
	echo CHtml::image($url);
	echo '<br />';
	echo 'Delete: '.CHtml::checkBox('delete_image['.$filename.']');
	echo '<br />';
}
echo CHtml::submitButton();
$this->endWidget();
?>