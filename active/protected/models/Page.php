<?php
Class Page Extends CActiveRecord {
	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function rules() {
		return array(
			array('title, description, line', 'safe'),
			array('title, description, line', 'required'),
			array('icon', 'file', 'types' => 'png', 'allowEmpty' => True),
			array('icon', 'file', 'types' => 'png', 'allowEmpty' => False, 'on' => 'insert'), //on create, icon is required
		);
	}

	public function getLines() {
		return Array(
			0 => Yii::t('active', 'Active Apps'),
			1 => Yii::t('active', 'Plasma Quick'),
			2 => Yii::t('active', 'Contour'),
			3 => Yii::t('active', 'OS Platforms'),
			4 => Yii::t('active', 'Vendor Interaction'),
		);
	}

	public function relations() {
		return array(
			'links' => array(self::HAS_MANY, 'Link', 'page_id')
		);
	}

	public function getImagesPath() {
		$path = Yii::app()->runtimePath.'/images/'.$this->id;
		if(!is_dir($path))
			mkdir($path);

		return $path;
	}

	public function getImages() {
		if($this->isNewRecord)
			return Array();

		$images = Array();
		$dir = opendir($this->imagesPath);
		while($image = readdir($dir))
			if($image{0} != '.')
				$images[] = $this->imagesPath.'/'.$image;

		return $images;
	}
}