<?php
Class Link Extends CActiveRecord {
	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function rules() {
		return array(
			array('title, url', 'required'),
			array('title, url', 'safe')
		);
	}

	public function relations() {
		return array(
			'links' => array(self::HAS_MANY, 'Link', 'page_id')
		);
	}
}