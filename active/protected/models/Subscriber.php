<?php
Class Subscriber Extends CActiveRecord {
	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function rules() {
		return array(
			array('email', 'required'),
			array('email', 'unique'),
			array('email', 'email')
		);
	}
}