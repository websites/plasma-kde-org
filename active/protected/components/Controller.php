<?php
Class Controller Extends CController {
	protected $_menu = Array();
	public $breadcrumbs = Array();

	public function beforeAction() {
		$this->_menu[] = array(
			'label' => Yii::t('sprint', 'Upcoming sprints'),
			'url' => array('/sprint')
		);

		return True;
	}
}