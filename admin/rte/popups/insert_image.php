<!-- based on insimage.dlg -->
<?php

include_once("../../include/common.php");
include_once("$phpfwkIncludePath/dmsCommon.php");

/*
 * Create a Form Select obj that contains a list of Images in a given directory
 * $sysPath, the directory you wish to search e.g. "/var/www/public/images/"
 * $pubPath, "http://www.domain.com/images/"
 * $attributes, "ID=txtFileName size=4 style=\"left: 8.54em; top: 1.0647em; width: 21.5em; height: 6.3294em; \" tabIndex=10";
 * requires a javascript function showImage();
 */
function createImageSelector($sysPath, $pubPath, $attributes)
{
    global $dms_imageFolderID;
    $db = db_connect();
    $images = db_query($db, "select documentID, filename, title from dmsDocuments where folderID = $dms_imageFolderID order by displayOrder;");
    $numImages = db_numRows($images);
    print "<SELECT $attributes onChange=\"showImage(this.value); \">";
    for ($i = 0 ; $i < $numImages; ++$i)
    {
        list ($documentID, $filename, $title) = db_row($images, $i);
        $filename = dmsFileURL($dms_imageFolderID, $documentID, $filename);
        print "<option value=\"$documentID::$filename\">$title</option>";
    }
    print '</SELECT>';
}

$system_cms_images = dmsFolderPath($dms_imageFolderID);
$public_cms_images = dmsFolderURL($dms_imageFolderID);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD W3 HTML 3.2//EN">
<HTML  id=dlgImage STYLE="width: 432px; height: 220px; ">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="MSThemeCompatible" content="Yes">
<TITLE>Insert Image</TITLE>
<style>
  html, body, button, div, input, select, fieldset { font-family: MS Shell Dlg; font-size: 12pt; };
</style>

<script LANGUAGE="JavaScript">

function _isValidNumber(txtBox) {
  var val = parseInt(txtBox);
  if (isNaN(val) || val < 0 || val > 999) { return false; }
  return true;
}

/*
 *
 */
 var timer = 0;
function showImage(imgSrc)
{
    var input = new String(imgSrc);
    var colons = input.search("::");
    if (colons == -1)
    {
         return;
    }
    colons += 2;
    var imageURL = imgSrc.substr(colons, input.length);
    var span = document.getElementById("foo");
    span.innerHTML = '<img id="idPreview" src="select_image.gif">';
    var img = document.getElementById("idPreview");
    img.src = imageURL;
    var w = img.width;
    var h = img.height;
    var dw = 1;
    var dh = 1;

    if (w > 300)
    {
        dw = 300/w;
        dh = dw;
    }

    h = h*dh;
    w = w*dw;

    if (h > 250)
    {
        dh = 250/h;
        dw = dh;
    }
    img.width = w*dw;
    img.height = h*dh;

}

// Send gathered data to the dialog window object's returnedValue property
function transferData()
{
    var input = document.getElementById("imagePath").value;
    var colons = input.search("::");
    var rv = new Array(input.substr(0, colons), input.substr(colons + 2, input.length));
    if (top.opener && !top.opener.closed)
    {
        top.opener.dialogWin.returnedValue = rv;
    }
}
</SCRIPT>
</SCRIPT>
</HEAD>
<BODY id=bdy style="background: threedface;" scroll=no>
<table width="100%">
<tr>
    <th>Images</th>
    <th>Preview</th>
</tr>
<tr>
    <td valign=top>
<?php
   $selectAttributes = "ID=imagePath name=imagePath size=10 10";
   createImageSelector($system_cms_images, $public_cms_images, $selectAttributes);
?>
    </td>
    <td align=center width="100%">
    <span id=foo><img id="idPreview" src="select_image.gif"></foo>
    </td>
</tr>
</table>
</BODY>
</HTML>
