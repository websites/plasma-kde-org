<!-- based on insimage.dlg -->
<?php

include_once("../../include/common.php");
include_once("$phpfwkIncludePath/dmsCommon.php");
$thisPage = "$common_baseURL/include/cmsrte/popups/insert_dms.php";

/*
 * Create a Form Select obj that contains a list of Images in a given directory
 * $sysPath, the directory you wish to search e.g. "/var/www/public/images/"
 * $pubPath, "http://www.domain.com/images/"
 * $attributes, "ID=txtFileName size=4 style=\"left: 8.54em; top: 1.0647em; width: 21.5em; height: 6.3294em; \" tabIndex=10";
 * requires a javascript function showImage();
 */
function createDMSSelector($currentfolderID)
{
    global $thisPage;
    $db = db_connect();
    $currentFolderID = intval($currentfolderID);

    $folders = db_query($db, "select folderID, name from dmsFolders where folderID > 1 order by lower(name);");
    $numFolders = db_numRows($folders);

    print '<table>';

    print '<tr><th>Folders</th>';
    if ($currentFolderID > 0)
    {
        print '<th>Documents</th>';
    }
    else
    {
        print '<th></th>';
    }
    print '</tr>';
    print '<tr><td valign=top nowrap>';

    for ($i = 0 ; $i < $numFolders; ++$i)
    {
        list ($folderID, $name) = db_row($folders, $i);
        print "<a href=\"$thisPage?folderID=$folderID\">$name</a><br>";
    }

    print '</td><td valign=top>';

    if ($currentFolderID > 0)
    {
        $documents = db_query($db, "select documentID, filename, title from dmsDocuments where folderID = $currentFolderID order by displayOrder;");
        $numDocuments = db_numRows($documents);
        for ($i = 0 ; $i < $numDocuments; ++$i)
        {
            list ($documentID, $filename, $title) = db_row($documents, $i);
            $filename = dmsFileURL($currentFolderID, $documentID, $filename);
            print "<a href=\"javascript: return true;\" onClick=\"transferData('$filename');\">$title</option>";
        }
    }

    print '</td></tr></table>';
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD W3 HTML 3.2//EN">
<HTML  id=dlgImage STYLE="width: 432px; height: 220px; ">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="MSThemeCompatible" content="Yes">
<TITLE>Insert Image</TITLE>
<style>
  html, body, button, div, input, select, fieldset { font-family: MS Shell Dlg; font-size: 12pt; };
</style>

<script LANGUAGE="JavaScript">
// Send gathered data to the dialog window object's returnedValue property
function transferData(input)
{
    if (opener && !opener.closed)
    {
        opener.dialogWin.returnedValue = input;
        opener.dialogWin.returnFunc();
    }
    window.close();
}
</SCRIPT>
</SCRIPT>
</HEAD>
<BODY id=bdy style="background: threedface;" scroll=no>
<?php
   createDMSSelector($folderID);
?>
</BODY>
</HTML>
