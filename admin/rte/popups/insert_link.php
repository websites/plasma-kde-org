<!-- based on insimage.dlg -->
<?php

include_once("../../include/common.php");
include_once("$phpfwkIncludePath/cmsCommon.php");

/*
 * Create a Form Select obj that contains a list of Images in a given directory
 * $sysPath, the directory you wish to search e.g. "/var/www/public/images/"
 * $pubPath, "http://www.domain.com/images/"
 * $attributes, "ID=txtFileName size=4 style=\"left: 8.54em; top: 1.0647em; width: 21.5em; height: 6.3294em; \" tabIndex=10";
 * requires a javascript function showImage();
 */
function createLinkSelector()
{
    global $common_baseURL, $thisPage, $currentPageID, $currentPagePath, $auth_userGID;
    global $cms_complexityLevel;
    $db = db_connect();

    $query = "select meta.pageID, meta.pubRevision, meta.title from cmsPagesMetadata meta
                  where meta.parentID is null order by displayOrder;";
    $pages = db_query($db, $query);
    $numPages = db_numRows($pages);

    print '<table border=0 cellpadding=3 cellspacing=0 height="100%">';
    for ($i = 0; $i < $numPages; ++$i)
    {
        list($pageID, $pubRevision, $title) = db_row($pages, $i);

        if (!trim($title))
        {
            $title = 'Unnamed';
        }
        print '<tr><td class="largeGreyHeader" style="border-top: 1px solid; border-bottom: none;" colspan=2>';
        $title = str_replace(' ', '&nbsp;', $title);
        $privs = new cmsPagePriveleges($pageID);
        print $title;

        if ($cms_complexityLevel == CMS_BASIC || isSuper())
        {
            $query = "select meta.pageID, meta.typeID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID order by displayOrder;";
        }
        else
        {
            $query = "select meta.pageID, meta.typeID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID and (meta.editGroup = $auth_userGID or meta.adminGroup = $auth_userGID) order by displayOrder;";
        }

        $subPages = db_query($db, $query);
        $numSubPages = db_numRows($subPages);

//         print '<tr><td nowrap>';

        $odd = true;
        for ($j = 0; $j < $numSubPages; ++$j)
        {
            list($subPageID, $subPageType, $subPubRevision, $subTitle) = db_row($subPages, $j);
            print_CMSTreeRecursive($subPageID, $subPageType, $selectedPageID, $subPubRevision, $subTitle, $odd);
        }

//         print '</td></tr>';
    }
    print '<tr><td class="largeGreyHeader" height="100%" colspan=3></td></tr>';
    print '</table>';

    return $path;
}

function print_CMSTreeRecursive($pageID, $pageType, $currentPageID, $pubRevision, $title, &$odd, $level = 0)
{
    global $common_baseURL, $thisPage, $cms_complexityLevel, $auth_userGID;
    $pageID = intval($pageID);
    if (!$pageID)
    {
        return;
    }

    if (!trim($title))
    {
        $title = 'Unnamed';
    }

    $db = db_connect();
    if ($cms_complexityLevel == CMS_BASIC || isSuper())
    {
        $query = "select meta.pageID, meta.typeID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID and pubrevision is not null order by displayOrder;";
    }
    else
    {
        $query = "select meta.pageID, meta.typeID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID and (meta.editGroup = $auth_userGID or meta.adminGroup = $auth_userGID) and pubrevision is not null order by displayOrder;";
    }
    $subPages = db_query($db, $query);
    $numSubPages = db_numRows($subPages);

    $normalTitle = $title;
    if (strlen($title) > 23)
    {
        $title = substr($title, 0, 20) . '...';
    }
    $title = str_replace(' ', '&nbsp;', $title);

    $privs = new cmsPagePriveleges($pageID);
    if ($currentPageID == $pageID)
    {
        $title = "<u>$title</u>";
    }

    $cssClass = $odd ? 'oddRow' : 'evenRow';
    $odd = !$odd;
    //print "offset calc for $title: $offset = 3 + ($level * 5);<br>";
    print "<tr>";
    if ($level > 0)
    {
        print "<td style=\"padding-left: 8px;\">&nbsp;</td>";
        $firstColumnColspan = 1;
        if ($level > 1)
        {
            $offset = 3 + (($level + 1) * 5);
        }
    }
    else
    {
        $firstColumnColspan = 2;
    }
    print "<td style=\"padding-left: {$offset}px;\" class=\"$cssClass\" colspan=\"$firstColumnColspan\" valign=top nowrap>";
    print_radio('cmsID', $title, "$pageType::$pageID", -1, 'onClick="document.getElementById(\'cmsPageID\').value = \'' . "$pageType::$pageID" . '\';"');
    print "<td class=\"$cssClass\" valign=top align=right>";
    print "</td></tr>";

    if ($numSubPages > 0)
    {
        for ($i = 0; $i < $numSubPages; ++$i)
        {
            list($subPageID, $subPageType, $subPubRevision, $subTitle) = db_row($subPages, $i);
            print_CMSTreeRecursive($subPageID, $subPageType, $currentPageID, $subPubRevision, $subTitle, $odd, $level + 1);
        }
    }
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD W3 HTML 3.2//EN">
<HTML  id=dlgImage STYLE="width: 432px; height: 220px; ">
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="MSThemeCompatible" content="Yes">
<TITLE>Insert Image</TITLE>
<style>
  html, body, button, div, input, select, fieldset { font-family: MS Shell Dlg; font-size: 12pt; };
</style>

<script LANGUAGE="JavaScript">

// Send gathered data to the dialog window object's returnedValue property
function transferData()
{
    if (top.opener && !top.opener.closed)
    {
        top.opener.dialogWin.returnedValue = document.getElementById("cmsPageID").value;
    }
}
</SCRIPT>
</SCRIPT>
</HEAD>
<BODY id=bdy style="background: threedface;" scroll=no>
<form style="margin: 0px;">
<input type="hidden" name="cmsPageID" id="cmsPageID" value="-1">
<?php
   createLinkSelector();
?>
</form>
</BODY>
</HTML>
