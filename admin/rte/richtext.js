// Cross-Browser Rich Text Editor
// http://www.kevinroth.com/rte/demo.htm
// Written by Kevin Roth (kevin@NOSPAMkevinroth.com - remove NOSPAM)

//**********************************************************************
//  BEGIN MODAL DIALOG CODE
//***********************************************************************/
// Global for brower version branching.
var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) == 4))

// One object tracks the current modal dialog opened from this window.
var dialogWin = new Object()

// Generate a modal dialog.
// Parameters:
//    url -- URL of the page/frameset to be loaded into dialog
//    width -- pixel width of the dialog window
//    height -- pixel height of the dialog window
//    returnFunc -- reference to the function (on this page)
//                  that is to act on the data returned from the dialog
//    args -- [optional] any data you need to pass to the dialog
function openDGDialog(url, width, height, returnFunc, args) {
        if (!dialogWin.win || (dialogWin.win && dialogWin.win.closed)) {
                // Initialize properties of the modal dialog object.
                dialogWin.returnFunc = returnFunc
                dialogWin.returnedValue = ""
                dialogWin.args = args
                dialogWin.url = url
                dialogWin.width = width
                dialogWin.height = height
                // Keep name unique so Navigator doesn't overwrite an existing dialog.
                dialogWin.name = (new Date()).getSeconds().toString()
                // Assemble window attributes and try to center the dialog.
                if (Nav4) {
                        // Center on the main window.
                        dialogWin.left = window.screenX +
                           ((window.outerWidth - dialogWin.width) / 2)
                        dialogWin.top = window.screenY +
                           ((window.outerHeight - dialogWin.height) / 2)
                        var attr = "screenX=" + dialogWin.left +
                           ",screenY=" + dialogWin.top + ",resizable=no,scrollbars=yes,width=" +
                           dialogWin.width + ",height=" + dialogWin.height
                } else {
                        // The best we can do is center in screen.
                        dialogWin.left = (screen.width - dialogWin.width) / 2
                        dialogWin.top = (screen.height - dialogWin.height) / 2
                        var attr = "left=" + dialogWin.left + ",top=" +
                           dialogWin.top + ",resizable=no,scrollbars=yes,width=" + dialogWin.width +
                           ",height=" + dialogWin.height
                }

                // Generate the dialog and make sure it has focus.
                dialogWin.win=window.open(dialogWin.url, dialogWin.name, attr)
                dialogWin.win.focus()
        } else {
                dialogWin.win.focus()
        }
}

// Event handler to inhibit Navigator form element
// and IE link activity when dialog window is active.
function deadend() {
        if (dialogWin.win && !dialogWin.win.closed) {
                dialogWin.win.focus()
                return false
        }
}

// Since links in IE4 cannot be disabled, preserve
// IE link onclick event handlers while they're "disabled."
// Restore when re-enabling the main window.
var IELinkClicks

// Disable form elements and links in all frames for IE.
function disableForms() {
        IELinkClicks = new Array()
        for (var h = 0; h < frames.length; h++) {
                for (var i = 0; i < frames[h].document.forms.length; i++) {
                        for (var j = 0; j < frames[h].document.forms[i].elements.length; j++) {
                                frames[h].document.forms[i].elements[j].disabled = true
                        }
                }
                IELinkClicks[h] = new Array()
                for (i = 0; i < frames[h].document.links.length; i++) {
                        IELinkClicks[h][i] = frames[h].document.links[i].onclick
                        frames[h].document.links[i].onclick = deadend
                }
                frames[h].window.onfocus = checkModal
            frames[h].document.onclick = checkModal
        }
}

// Restore IE form elements and links to normal behavior.
function enableForms() {
        for (var h = 0; h < frames.length; h++) {
                for (var i = 0; i < frames[h].document.forms.length; i++) {
                        for (var j = 0; j < frames[h].document.forms[i].elements.length; j++) {
                                frames[h].document.forms[i].elements[j].disabled = false
                        }
                }
                for (i = 0; i < frames[h].document.links.length; i++) {
                        frames[h].document.links[i].onclick = IELinkClicks[h][i]
                }
        }
}

// Grab all Navigator events that might get through to form
// elements while dialog is open. For IE, disable form elements.
function blockEvents() {
        if (Nav4) {
                window.captureEvents(Event.CLICK | Event.MOUSEDOWN | Event.MOUSEUP | Event.FOCUS)
                window.onclick = deadend
        } else {
                disableForms()
        }
        window.onfocus = checkModal
}
// As dialog closes, restore the main window's original
// event mechanisms.
function unblockEvents() {
        if (Nav4) {
                window.releaseEvents(Event.CLICK | Event.MOUSEDOWN | Event.MOUSEUP | Event.FOCUS)
                window.onclick = null
                window.onfocus = null
        } else {
                enableForms()
        }
}

// Invoked by onFocus event handler of EVERY frame,
// return focus to dialog window if it's open.
function checkModal() {
        setTimeout("finishChecking()", 50)
        return true
}

function finishChecking() {
        if (dialogWin.win && !dialogWin.win.closed) {
                dialogWin.win.focus()
        }
}
//**************************
//  END MODAL DIALOG CODE
//**************************/


//init variables
var isRichText = false;
var rng;
var currentRTE;
var allRTEs = "";

var isIE;
var isGecko;

var imagesPath;
var includesPath;
var cssFile;


function initRTE(imgPath, incPath, css) {
        //check to see if designMode mode is available
        if (document.getElementById && document.designMode) isRichText = true;

        //set browser vars
        var ua = navigator.userAgent.toLowerCase();
        isIE = ((ua.indexOf("msie") != -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1));
        isGecko = (ua.indexOf("gecko") != -1 && ua.indexOf("safari") == -1);

        //set paths vars
        imagesPath = imgPath;
        includesPath = incPath;
        cssFile = css;

        //for testing standard textarea, uncomment the following line
        //isRichText = false;
}

function writeRichText(rte, html, width, height, buttons, readOnly) {
        if (isRichText) {
                if (allRTEs.length > 0) allRTEs += ";";
                allRTEs += rte;
                writeRTE(rte, html, width, height, buttons, readOnly);
        } else {
                writeDefault(rte, html, width, height, buttons, readOnly);
        }
}

function writeDefault(rte, html, width, height, buttons, readOnly) {
        if (!readOnly) {
                document.writeln('<textarea name="' + rte + '" id="' + rte + '" style="width: ' + width + 'px; height: ' + height + 'px;">' + html + '</textarea>');
        } else {
                document.writeln('<textarea name="' + rte + '" id="' + rte + '" style="width: ' + width + 'px; height: ' + height + 'px;" readonly>' + html + '</textarea>');
        }
}

function writeRTE(rte, html, width, height, buttons, readOnly) {
        if (readOnly) buttons = false;
        if (buttons == true) {
                document.writeln('<style type="text/css">');
                document.writeln('.btnImage {cursor: pointer; cursor: hand;}');
                document.writeln('</style>');
                document.writeln('<table id="Buttons1_' + rte + '">');
                document.writeln('        <tr>');
                document.writeln('                <td>');
                document.writeln('                        <select id="formatblock_' + rte + '" onchange="Select(\'' + rte + '\', this.id);">');
                document.writeln('                                <option value="<p>">Normal</option>');
                document.writeln('                                <option value="<p>">Paragraph</option>');
                document.writeln('                                <option value="<h1>">Heading 1 <h1></option>');
                document.writeln('                                <option value="<h2>">Heading 2 <h2></option>');
                document.writeln('                                <option value="<h3>">Heading 3 <h3></option>');
                document.writeln('                                <option value="<h4>">Heading 4 <h4></option>');
                document.writeln('                                <option value="<h5>">Heading 5 <h5></option>');
                document.writeln('                                <option value="<h6>">Heading 6 <h6></option>');
                document.writeln('                                <option value="<address>">Address <ADDR></option>');
                document.writeln('                                <option value="<pre>">Formatted <pre></option>');
                document.writeln('                        </select>');
                document.writeln('                </td>');
                document.writeln('                <td>');
                document.writeln('                        <select id="fontname_' + rte + '" onchange="Select(\'' + rte + '\', this.id)">');
                document.writeln('                                <option value="Font" selected>Font</option>');
                document.writeln('                                <option value="Arial, Helvetica, sans-serif">Arial</option>');
                document.writeln('                                <option value="Courier New, Courier, mono">Courier New</option>');
                document.writeln('                                <option value="Times New Roman, Times, serif">Times New Roman</option>');
                document.writeln('                                <option value="Verdana, Arial, Helvetica, sans-serif">Verdana</option>');
                document.writeln('                        </select>');
                document.writeln('                </td>');
                document.writeln('                <td>');
                document.writeln('                        <select unselectable="on" id="fontsize_' + rte + '" onchange="Select(\'' + rte + '\', this.id);">');
                document.writeln('                                <option value="Size">Size</option>');
                document.writeln('                                <option value="1">1</option>');
                document.writeln('                                <option value="2">2</option>');
                document.writeln('                                <option value="3">3</option>');
                document.writeln('                                <option value="4">4</option>');
                document.writeln('                                <option value="5">5</option>');
                document.writeln('                                <option value="6">6</option>');
                document.writeln('                                <option value="7">7</option>');
                document.writeln('                        </select>');
                document.writeln('                </td>');
                document.writeln('        </tr>');
                document.writeln('</table>');
                document.writeln('<table id="Buttons2_' + rte + '" cellpadding="1" cellspacing="0">');
                document.writeln('        <tr>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_bold.gif" width="25" height="24" alt="Bold" title="Bold" onClick="FormatText(\'' + rte + '\', \'bold\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_italic.gif" width="25" height="24" alt="Italic" title="Italic" onClick="FormatText(\'' + rte + '\', \'italic\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_underline.gif" width="25" height="24" alt="Underline" title="Underline" onClick="FormatText(\'' + rte + '\', \'underline\', \'\')"></td>');
                document.writeln('                <td>&nbsp;</td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_left_just.gif" width="25" height="24" alt="Align Left" title="Align Left" onClick="FormatText(\'' + rte + '\', \'justifyleft\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_centre.gif" width="25" height="24" alt="Center" title="Center" onClick="FormatText(\'' + rte + '\', \'justifycenter\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_right_just.gif" width="25" height="24" alt="Align Right" title="Align Right" onClick="FormatText(\'' + rte + '\', \'justifyright\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_justifyfull.gif" width="25" height="24" alt="Justify Full" title="Justify Full" onclick="FormatText(\'' + rte + '\', \'justifyfull\', \'\')"></td>');
                document.writeln('                <td>&nbsp;</td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_numbered_list.gif" width="25" height="24" alt="Ordered List" title="Ordered List" onClick="FormatText(\'' + rte + '\', \'insertorderedlist\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_list.gif" width="25" height="24" alt="Unordered List" title="Unordered List" onClick="FormatText(\'' + rte + '\', \'insertunorderedlist\', \'\')"></td>');
                document.writeln('                <td>&nbsp;</td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_outdent.gif" width="25" height="24" alt="Outdent" title="Outdent" onClick="FormatText(\'' + rte + '\', \'outdent\', \'\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_indent.gif" width="25" height="24" alt="Indent" title="Indent" onClick="FormatText(\'' + rte + '\', \'indent\', \'\')"></td>');
                document.writeln('                <td><div id="forecolor_' + rte + '"><img class="btnImage" src="' + imagesPath + 'post_button_textcolor.gif" width="25" height="24" alt="Text Color" title="Text Color" onClick="FormatText(\'' + rte + '\', \'forecolor\', \'\')"></div></td>');
                document.writeln('                <td><div id="hilitecolor_' + rte + '"><img class="btnImage" src="' + imagesPath + 'post_button_bgcolor.gif" width="25" height="24" alt="Background Color" title="Background Color" onClick="FormatText(\'' + rte + '\', \'hilitecolor\', \'\')"></div></td>');
                if (isIE) document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_spellcheck.gif" width="25" height="24" alt="Spell Check" title="Spell Check" onClick="checkspell()"></td>');
                document.writeln('                <td>Internal:</td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_hyperlink.gif" width="25" height="24" alt="Add Link to Internal Internal" title="Add Link To Internal Page" onClick="AddCMSLink(\'' + rte + '\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_image.gif" width="25" height="24" alt="Add Image" title="Add Image" onClick="AddCMSImage(\'' + rte + '\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_doc.gif" width="25" height="24" alt="Add Link to Document" title="Add Link To Document" onClick="AddDMSLink(\'' + rte + '\')"></td>');
                document.writeln('                <td>External:</td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_hyperlink.gif" width="25" height="24" alt="Insert Link" title="Insert Link" onClick="FormatText(\'' + rte + '\', \'createlink\')"></td>');
                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_image.gif" width="25" height="24" alt="Add Image" title="Add Image" onClick="AddImage(\'' + rte + '\')"></td>');
//                document.writeln('                <td>&nbsp;</td>');
//                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_cut.gif" width="25" height="24" alt="Cut" title="Cut" onClick="FormatText(\'' + rte + '\', \'cut\')"></td>');
//                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_copy.gif" width="25" height="24" alt="Copy" title="Copy" onClick="FormatText(\'' + rte + '\', \'copy\')"></td>');
//                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_paste.gif" width="25" height="24" alt="Paste" title="Paste" onClick="FormatText(\'' + rte + '\', \'paste\')"></td>');
//                document.writeln('                <td>&nbsp;</td>');
//                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_undo.gif" width="25" height="24" alt="Undo" title="Undo" onClick="FormatText(\'' + rte + '\', \'undo\')"></td>');
//                document.writeln('                <td><img class="btnImage" src="' + imagesPath + 'post_button_redo.gif" width="25" height="24" alt="Redo" title="Redo" onClick="FormatText(\'' + rte + '\', \'redo\')"></td>');
                document.writeln('        </tr>');
                document.writeln('</table>');
        }
        document.writeln('<iframe id="' + rte + '" name="' + rte + '" width="' + width + 'px" height="' + height + 'px"></iframe>');
        if (!readOnly) document.writeln('<br /><input type="checkbox" id="chkSrc' + rte + '" onclick="toggleHTMLSrc(\'' + rte + '\');" />&nbsp;View Source');
        document.writeln('<iframe width="254" height="174" id="cp' + rte + '" src="' + includesPath + 'palette.htm" marginwidth="0" marginheight="0" scrolling="no" style="visibility:hidden; display: none; position: absolute;"></iframe>');
        document.writeln('<input type="hidden" id="hdn' + rte + '" name="' + rte + '" value="">');
        document.getElementById('hdn' + rte).value = html;
        enableDesignMode(rte, html, readOnly);
}

function enableDesignMode(rte, html, readOnly) {
        var frameHtml = "<html id=\"" + rte + "\">\n";
        frameHtml += "<head>\n";
        //to reference your stylesheet, set href property below to your stylesheet path and uncomment
        if (cssFile.length > 0) {
                frameHtml += "<link media=\"all\" type=\"text/css\" href=\"" + cssFile + "\" rel=\"stylesheet\">\n";
        }
        frameHtml += "<style>\n";
        frameHtml += "body {\n";
        frameHtml += "        background: #FFFFFF;\n";
        frameHtml += "        margin: 0px;\n";
        frameHtml += "        padding: 0px;\n";
        frameHtml += "}\n";
        frameHtml += "</style>\n";
        frameHtml += "</head>\n";
        frameHtml += "<body>\n";
        frameHtml += html + "\n";
        frameHtml += "</body>\n";
        frameHtml += "</html>";

        if (document.all) {
                var oRTE = frames[rte].document;
                oRTE.open();
                oRTE.write(frameHtml);
                oRTE.close();
                if (!readOnly) oRTE.designMode = "On";
        } else {
                try {
                        if (!readOnly) document.getElementById(rte).contentDocument.designMode = "on";
                        try {
                                var oRTE = document.getElementById(rte).contentWindow.document;
                                oRTE.open();
                                oRTE.write(frameHtml);
                                oRTE.close();
                                //oRTE.addEventListener("blur", updateRTE(rte), true);
                                if (isGecko && !readOnly) {
                                        //attach a keyboard handler for gecko browsers to make keyboard shortcuts work
                                        oRTE.addEventListener("keypress", kb_handler, true);
                                }
                        } catch (e) {
                                alert("Error preloading content.");
                        }
                } catch (e) {
                        //gecko may take some time to enable design mode.
                        //Keep looping until able to set.
                        if (isGecko) {
                                setTimeout("enableDesignMode('" + rte + "', '" + html + "');", 10);
                        } else {
                                return false;
                        }
                }
        }
}

function updateRTEs() {
        var vRTEs = allRTEs.split(";");
        for (var i = 0; i < vRTEs.length; i++) {
                updateRTE(vRTEs[i]);
        }
}

function updateRTE(rte) {
        //set message value
        var oHdnMessage = document.getElementById('hdn' + rte);
        var oRTE = document.getElementById(rte);
        var readOnly = false;

        //check for readOnly mode
        if (document.all) {
                if (frames[rte] && frames[rte].document.designMode && frames[rte].document.designMode != "On") readOnly = true;
        } else {
                if (document.getElementById(rte).contentDocument.designMode != "on") readOnly = true;
        }

        if (isRichText && !readOnly) {
                //if viewing source, switch back to design view
                if (document.getElementById("chkSrc" + rte).checked) {
                        document.getElementById("chkSrc" + rte).checked = false;
                        toggleHTMLSrc(rte);
                }

                if (oHdnMessage.value == null) oHdnMessage.value = "";
                if (document.all) {
                        oHdnMessage.value = frames[rte].document.body.innerHTML;
                } else {
                        oHdnMessage.value = oRTE.contentWindow.document.body.innerHTML;
                }
                //if there is no content (other than formatting) set value to nothing
                if (stripHTML(oHdnMessage.value.replace("&nbsp;", " ")) == "") oHdnMessage.value = "";
        }
}

function toggleHTMLSrc(rte) {
        //contributed by Bob Hutzel (thanks Bob!)
        var oRTE;
        if (document.all) {
                oRTE = frames[rte].document;
        } else {
                oRTE = document.getElementById(rte).contentWindow.document;
        }

        if (document.getElementById("chkSrc" + rte).checked) {
                document.getElementById("Buttons1_" + rte).style.visibility = "hidden";
                document.getElementById("Buttons2_" + rte).style.visibility = "hidden";
                if (document.all) {
                        oRTE.body.innerText = oRTE.body.innerHTML;
                } else {
                        var htmlSrc = oRTE.createTextNode(oRTE.body.innerHTML);
                        oRTE.body.innerHTML = "";
                        oRTE.body.appendChild(htmlSrc);
                }
        } else {
                document.getElementById("Buttons1_" + rte).style.visibility = "visible";
                document.getElementById("Buttons2_" + rte).style.visibility = "visible";
                if (document.all) {
                        oRTE.body.innerHTML = oRTE.body.innerText;
                } else {
                        var htmlSrc = oRTE.body.ownerDocument.createRange();
                        htmlSrc.selectNodeContents(oRTE.body);
                        oRTE.body.innerHTML = htmlSrc.toString();
                }
        }
}

//Function to format text in the text box
function FormatText(rte, command, option) {
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];

                //get current selected range
                var selection = oRTE.document.selection;
                if (selection != null) {
                        rng = selection.createRange();
                }
        } else {
                oRTE = document.getElementById(rte).contentWindow;

                //get currently selected range
                var selection = oRTE.getSelection();
                rng = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        }

        try {
                if ((command == "forecolor") || (command == "hilitecolor")) {
                        //save current values
                        parent.command = command;
                        currentRTE = rte;

                        //position and show color palette
                        buttonElement = document.getElementById(command + '_' + rte);
                        document.getElementById('cp' + rte).style.left = getOffsetLeft(buttonElement) + "px";
                        document.getElementById('cp' + rte).style.top = (getOffsetTop(buttonElement) + buttonElement.offsetHeight) + "px";
                        if (document.getElementById('cp' + rte).style.visibility == "hidden") {
                                document.getElementById('cp' + rte).style.visibility = "visible";
                                document.getElementById('cp' + rte).style.display = "inline";
                        } else {
                                document.getElementById('cp' + rte).style.visibility = "hidden";
                                document.getElementById('cp' + rte).style.display = "none";
                        }
                } else if (command == "createlink") {
                        var szURL = prompt("Enter a URL:", "");
                        try {
                                //ignore error for blank urls
                                oRTE.document.execCommand("Unlink", false, null);
                                oRTE.document.execCommand("CreateLink", false, szURL);
                        } catch (e) {
                                //do nothing
                        }
                } else {
                        //oRTE.focus();
                          oRTE.document.execCommand(command, false, option);
                        //oRTE.focus();
                }
        } catch (e) {
                alert(e);
        }
}

//Function to set color
function setColor(color) {
        var rte = currentRTE;
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];
        } else {
                oRTE = document.getElementById(rte).contentWindow;
        }

        var parentCommand = parent.command;
        if (document.all) {
                //retrieve selected range
                var sel = oRTE.document.selection;
                if (parentCommand == "hilitecolor") parentCommand = "backcolor";
                if (sel != null) {
                        var newRng = sel.createRange();
                        newRng = rng;
                        newRng.select();
                }
        } else {
                //oRTE.focus();
        }
        oRTE.document.execCommand(parentCommand, false, color);
        //oRTE.focus();
        document.getElementById('cp' + rte).style.visibility = "hidden";
        document.getElementById('cp' + rte).style.display = "none";
}

//Function to add image
function AddImage(rte) {
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];

                //get current selected range
                var selection = oRTE.document.selection;
                if (selection != null) {
                        rng = selection.createRange();
                }
        } else {
                oRTE = document.getElementById(rte).contentWindow;

                //get currently selected range
                var selection = oRTE.getSelection();
                rng = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        }

        imagePath = prompt('Enter Image URL:', 'http://');
        if ((imagePath != null) && (imagePath != "")) {
                //oRTE.focus();
                oRTE.document.execCommand('InsertImage', false, imagePath);
        }
        //oRTE.focus();
}

//Function to add image from CMS
function AddCMSImage(rte) {
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];

                //get current selected range
                var selection = oRTE.document.selection;
                if (selection != null) {
                        rng = selection.createRange();
                }
        } else {
                oRTE = document.getElementById(rte).contentWindow;

                //get currently selected range
                var selection = oRTE.getSelection();
                rng = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        }

        currentRTE = oRTE;
        imagePath = openDGDialog(includesPath + "popups/insert_image_dialog.php", 650, 400, AddCMSImagePost);
}

function AddCMSImagePost()
{
    if ((dialogWin.returnedValue != null) && (dialogWin.returnedValue[1] != "")) {
        //oRTE.focus();
        currentRTE.document.execCommand('InsertImage', false, dialogWin.returnedValue[1]);
    }
    //oRTE.focus();
}
function AddCMSLink(rte) {
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];

                //get current selected range
                var selection = oRTE.document.selection;
                if (selection != null) {
                        rng = selection.createRange();
                }
        } else {
                oRTE = document.getElementById(rte).contentWindow;

                //get currently selected range
                var selection = oRTE.getSelection();
                rng = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        }

        currentRTE = oRTE;
        imagePath = openDGDialog(includesPath + "popups/insert_link_dialog.php", 650, 400, AddCMSLinkPost);
}

function AddCMSLinkPost()
{
//            alert(dialogWin.returnedValue);
    if ((dialogWin.returnedValue != null) && (dialogWin.returnedValue != -1)) {
        //oRTE.focus();
        try {
//            alert(dialogWin.returnedValue);
            //ignore error for blank urls
            currentRTE.document.execCommand("Unlink", false, null);
            currentRTE.document.execCommand("CreateLink", false, dialogWin.returnedValue);
        } catch (e) {
                //do nothing
        }
    }
    //oRTE.focus();
}

function AddDMSLink(rte) {
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];

                //get current selected range
                var selection = oRTE.document.selection;
                if (selection != null) {
                        rng = selection.createRange();
                }
        } else {
                oRTE = document.getElementById(rte).contentWindow;

                //get currently selected range
                var selection = oRTE.getSelection();
                rng = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        }

        currentRTE = oRTE;
        imagePath = openDGDialog(includesPath + "popups/insert_dms.php", 650, 400, AddDMSLinkPost);
}

function AddDMSLinkPost()
{
//            alert(dialogWin.returnedValue);
    if ((dialogWin.returnedValue != null) && (dialogWin.returnedValue != -1)) {
        //oRTE.focus();
        try {
//            alert(dialogWin.returnedValue);
            //ignore error for blank urls
            currentRTE.document.execCommand("Unlink", false, null);
            currentRTE.document.execCommand("CreateLink", false, dialogWin.returnedValue);
        } catch (e) {
                //do nothing
        }
    }
    //oRTE.focus();
}

//function to perform spell check
function checkspell() {
        try {
                var tmpis = new ActiveXObject("ieSpell.ieSpellExtension");
                tmpis.CheckAllLinkedDocuments(document);
        }
        catch(exception) {
                if(exception.number==-2146827859) {
                        if (confirm("ieSpell not detected.  Click Ok to go to download page."))
                                window.open("http://www.iespell.com/download.php","DownLoad");
                } else {
                        alert("Error Loading ieSpell: Exception " + exception.number);
                }
        }
}

function getOffsetTop(elm) {
        var mOffsetTop = elm.offsetTop;
        var mOffsetParent = elm.offsetParent;

        while(mOffsetParent){
                mOffsetTop += mOffsetParent.offsetTop;
                mOffsetParent = mOffsetParent.offsetParent;
        }

        return mOffsetTop;
}

function getOffsetLeft(elm) {
        var mOffsetLeft = elm.offsetLeft;
        var mOffsetParent = elm.offsetParent;

        while(mOffsetParent) {
                mOffsetLeft += mOffsetParent.offsetLeft;
                mOffsetParent = mOffsetParent.offsetParent;
        }

        return mOffsetLeft;
}

function Select(rte, selectname) {
        var oRTE;
        if (document.all) {
                oRTE = frames[rte];

                //get current selected range
                var selection = oRTE.document.selection;
                if (selection != null) {
                        rng = selection.createRange();
                }
        } else {
                oRTE = document.getElementById(rte).contentWindow;

                //get currently selected range
                var selection = oRTE.getSelection();
                rng = selection.getRangeAt(selection.rangeCount - 1).cloneRange();
        }

        var idx = document.getElementById(selectname).selectedIndex;
        // First one is always a label
        if (idx != 0) {
                var selected = document.getElementById(selectname).options[idx].value;
                var cmd = selectname.replace('_' + rte, '');
                oRTE.document.execCommand(cmd, false, selected);
                document.getElementById(selectname).selectedIndex = 0;
        }
        //oRTE.focus();
}

function kb_handler(evt) {
        var rte = evt.target.id;

        //contributed by Anti Veeranna (thanks Anti!)
        if (evt.ctrlKey) {
                var key = String.fromCharCode(evt.charCode).toLowerCase();
                var cmd = '';
                switch (key) {
                        case 'b': cmd = "bold"; break;
                        case 'i': cmd = "italic"; break;
                        case 'u': cmd = "underline"; break;
                };

                if (cmd) {
                        FormatText(rte, cmd, true);
                        //evt.target.ownerDocument.execCommand(cmd, false, true);
                        // stop the event bubble
                        evt.preventDefault();
                        evt.stopPropagation();
                }
         }
}

function docChanged (evt) {
        alert('changed');
}

function stripHTML(oldString) {
        var newString = oldString.replace(/(<([^>]+)>)/ig,"");

        //replace carriage returns and line feeds
        newString = escape(newString);
        newString = newString.replace("%0D%0A"," ");
        newString = newString.replace("%0A"," ");
        newString = newString.replace("%0D"," ");
        newString = unescape(newString);

        //trim string
        newString = trim(newString);

        return newString;
}

function trim(inputString) {
   // Removes leading and trailing spaces from the passed string. Also removes
   // consecutive spaces and replaces it with one space. If something besides
   // a string is passed in (null, custom object, etc.) then return the input.
   if (typeof inputString != "string") return inputString;
   var retValue = inputString;
   var ch = retValue.substring(0, 1);

   while (ch == " ") { // Check for spaces at the beginning of the string
      retValue = retValue.substring(1, retValue.length);
      ch = retValue.substring(0, 1);
   }
   ch = retValue.substring(retValue.length-1, retValue.length);

   while (ch == " ") { // Check for spaces at the end of the string
      retValue = retValue.substring(0, retValue.length-1);
      ch = retValue.substring(retValue.length-1, retValue.length);
   }

        // Note that there are two spaces in the string - look for multiple spaces within the string
   while (retValue.indexOf("  ") != -1) {
                // Again, there are two spaces in each of the strings
      retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length);
   }
   return retValue; // Return the trimmed string back to the user
}
