<?php

include_once('phpfwk_config.php');
include_once("$phpfwkIncludePath/db.php");
include_once("$phpfwkIncludePath/util.php");
include_once("$phpfwkIncludePath/cmsCommon.php");
include_once("$phpfwkIncludePath/html-ui.php");

global $common_pageTitle;
$common_pageTitle = '';

/*
 * print_header
 *
 * $title -> The page title.
 * $cmsPage -> boolean, true if this is a cmspage, false otherwise
 * $noCache -> pass in true to ensure the browser does not cache this page
 * $bodyClass -> the class from the css
 * $refresh -> autorefresh after N seconds, or never if 0
 */

function print_header($title = '', $cmsPage = true, $noCache = false, $bodyClass = 'main', $refresh = 0)
{
    global $common_siteName, $common_baseURL, $common_bizName;
    global $common_templates, $common_header, $common_baseImagesURL;
    global $cms_homepageID;
    global $common_bizName, $common_title;
    global $common_pageTitle;
    global $common_printStyleSheet, $common_styleSheet, $pf; // pf == printer friendly

    $common_pageTitle = $title;
    static $alreadyBeenHere = 0;
    if ($alreadyBeenHere)
    {
        return;
    }

    $alreadyBeenHere = 1;

    if ($cmsPage)
    {
        $cmsPage = cms_init();
        if ($cmsPage)
        {
            ob_start();
        }
    }

    print '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
    print '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">';
    print '<head><title>';
    if ($common_pageTitle)
    {
        print "$common_pageTitle: ";
    }
    print "$common_siteName</title>";
    print '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
    print '<meta name="description" content="Plasma for KDE Desktop" />';
    print '<link rel="stylesheet" type="text/css" href="/style.css" />';
    print '<script type="text/javascript" src="/behaviour.js"></script>';
//    print '<html><head><title>';

//    include_once($pf ? $common_printStyleSheet : $common_styleSheet);

    if ($noCache)
    {
        print '<meta http-equiv="Pragma" content="no-cache">';
    }

    if ($refresh)
    {
        print "<meta http-equiv=\"refresh\" content=\"$refresh\">";
    }

    print '</head><body>';
//    print "</head><body class=\"$bodyClass\">";
    if ($common_header && is_readable("$common_templates/$common_header"))
    {
        include_once("$common_templates/$common_header");
    }
    else
    {
        if ($pf)
        {
            print "<table border=0 cellpadding=0px cellspacing=0px bgcolor=\"white\"  height=\"98%\">";
        }
        else
        {
            print '<div align="center">';
            print "<table border=0 cellpadding=0 cellspacing=0 width=780 bgcolor=\"white\" style=\"border: solid 1px #00346A; border-right: solid 1px #00346A;\" height=\"99%\">";
        }

        print "<tr><td height=100% valign=top>";
    }

    if ($cmsPage)
    {
        cms_printPage(cms_currentPageID());
        ob_end_flush();
    }
    else
    {
        print '&nbsp;';
    }
}

/*
 * print_footer
 */
function print_footer()
{
    global $common_footer, $common_templates, $pf;

    if ($common_footer && is_readable("$common_templates/$common_footer"))
    {
        include_once("$common_templates/$common_footer");
    }
    else
    {
        print '</td></tr></table>';
        if (!$pf)
        {
            print '</div>';
        }
    }

    print '</body></html>';
}

?>
