<?php

// MAIN PHPFWK CONFIGURATION
global $common_siteHost, $common_sitePath, $common_siteName;
global $common_contactPerson, $common_contactEmail;
global $common_bizName, $common_pageTitle;
$common_siteHost = "http://plasma.kde.org";
$common_sitePath = "";
$common_siteName = "Plasma for KDE";
$common_contactPerson = "Aaron J. Seigo";
$common_contactEmail = "aseigo@kde.org";
$common_bizName = "Plasma Project";

// BASE PATHS AND URLS
global $common_baseURL, $common_htmlPath, $common_fwIncludesPath;
global $common_styleSheet, $common_printStyleSheet;
global $phpfwkIncludePath;
$common_baseURL = $common_siteHost . $common_sitePath;
$common_htmlPath = '/var/www/sites/plasma.kde.org';
$common_includesPath = '/var/www/sites/plasma.kde.org/include';
$phpfwkIncludePath = '/home/aseigo/phpfwk2';
$common_styleSheet =  "$common_htmlPath/style.css";
$common_printStyleSheet =  "$common_htmlPath/printStyle.css";

// DATABASE CONNECTION
global $dbName, $dbHost, $dbPort, $dbUsername, $dbPassword, $dbLocalOnly, $dbSingleDatabaseOnly;
$dbName = "plasma";
$dbHost = "localhost";
$dbPort = "5432";
$dbUsername = "plasma";
$dbPassword = "plasma";
$dbLocalOnly = true;
$dbSingleDatabaseOnly = true;

// DOCUMENT MANAGEMENT SYSTEM (DMS)
global $dms_useACLS, $dms_imageFolderID, $dmsFilesystemRoot, $dms_baseURL;
global $dms_notificationObject, $dms_notificationsTable, $dms_notificationsEmail;
$dms_useACLS = false;
$dms_imageFolderID = 1;
$dmsFilesystemRoot = "$common_htmlPath/dms";
$dms_baseURL = "$common_siteHost/dms";
$dms_notificationObject = 'dms_notification';
$dms_notificationsTable = 'dms_notifications';
$dms_notificationsEmail = 'dms@localhost';

// CONTENT MANAGEMENT SYSTEM (CMS)
define(CMS_BASIC, 0);
define(CMS_FLEXIBLE, 1);
define(CMS_COMPLETE, 2);

global $cms_homepageID;
$cms_homepageID = 1238;

global $cms_complexityLevel, $cms_fullTextSearch, $cms_emailNotifications;
global $cms_baseURL, $cms_objectFilePath, $cms_customObjectFilePath;
global $cms_searchEngineFrendlyLinks;
$cms_complexityLevel = CMS_BASIC;
$cms_fullTextSearch = false;
$cms_emailNotifications = false;

$cms_baseURL = $common_baseURL;
$cms_objectFilePath = "$phpfwkIncludePath/cmsObjects";
$cms_customObjectFilePath = "$common_includesPath/cmsObjects";
$cms_searchEngineFrendlyLinks = true;

// PHPFWK DB EXTRACTION
global $phpfwk_dbErrorMsg, $phpfwk_dbDebug, $phpfwk_dbProfile;
$phpfwk_dbErrorMsg = 1;
$phpfwk_dbDebug = 0;
$phpfwk_dbProfile = 0;

// CMS INTEGRATION
global $common_templates, $common_header, $common_footer, $common_mnemonic;
$common_templates = "$common_htmlPath/templates";
$common_header = 'defaultHeader.php';
$common_footer = 'defaultFooter.php';
$common_mnemonic = 'phpfwk';

// SYSTEM CONFIGURATION
global $phpfwk_sendmailPath, $phpfwk_sendmailArgs;
$phpfwk_sendmailPath = '/usr/sbin/sendmail';
$phpfwk_sendmailArgs = '';

// IMAGES
global $common_baseImagesURL, $common_baseImagesPath;
$common_baseImagesURL = "$common_baseURL/images";
$common_baseImagesPath = "$common_htmlPath/images";

// SHOPPING
global $common_baseCatalogImagesURL, $common_baseCatalogImagesPath;
global $shopping_defaultCurrency, $shopping_currencyFullList, $shopping_customObjectFilePath;
global $shopping_secureCart, $shopping_shippingClerk, $shopping_taxman;
$shopping_customObjectFilePath = "$common_includesPath/shoppingObjects";
$common_baseCatalogImagesPath = "$common_baseImagesPath/catalogs";
$common_baseCatalogImagesURL = "$common_baseImagesURL/catalogs";
$shopping_defaultCurrency = 'CAD';
$shopping_currencyFullList = false;
$shopping_secureCart = true;
$shopping_shippingClerk = '';
$shopping_taxman = '';

// ALBUMS
global $common_albumsImagesPath, $common_albumsImagesURL;
$common_albumsImagesPath = "$common_baseImagesPath/albums";
$common_albumsImagesURL = "$common_baseImagesURL/albums";
