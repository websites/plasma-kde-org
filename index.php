<?php

/*
 * This is a default page for placement in directories
 * so virtual directory viewing does not happen
 * in a case where that option is left on.
 *
 * Copy this file to index.php in the chosen directory (ies).
 */

include_once("include/common.php");

print_header();
print_footer();

?>
