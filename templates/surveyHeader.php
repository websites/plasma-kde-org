<?php
global $common_baseImagesURL, $common_baseImagesURL, $common_baseURL;
global $cms_TopPageID;

function printTopNavLink($pageID, $title, $last = false, $hasChildren = false, $depth = 0)
{
	return;
    static $first = true;
    $url = cms_currentBaseURL(false);
    $ancestry = cms_currentPageAncestry();

    if ($depth > 0)
    {
        return;
    }

    if ($pageID == cms_defaultHomePageID())
    {
        print "<a href=\"$url\">$title</a>";
    }
    else
    {
        print "<a href=\"$url" . cms_renderer::link($pageID) . "\">$title</a>";
    }

    return;
    $padding = $first ? '2px' : '2px';
    $mouseOvers = ''; //"onMouseOver=\"style.backgroundColor='#00346A';\" onMouseOut=\"style.backgroundColor='#737FB9';\"";
    $currentPageID = cms_currentPageID();
    if ($currentPageID != cms_defaultHomepageID() &&
        ($currentPageID == $pageID || $ancestry[1] == $pageID ))
    {
        $background = 'text-decoration: underline;';
        unset($mouseOvers);
    }

    if ($pageID == cms_defaultHomePageID())
    {
        print "<td class='navStripe' $mouseOvers nowrap style=\"padding-right: 8px; padding-left: $padding; $background;\" ><a class='nav' style=\"text-decoration: none; \" href=\"$url\">$title</a></td>";
    }
    else
    {
        print "<td class='navStripe'$mouseOvers nowrap style=\"padding-right: 8px; padding-left: $padding; $background;\" ><a class='nav' style=\"text-decoration: none; \" href=\"$url" . cms_renderer::link($pageID) . "\">$title</a></td>";
    }

    $first = false;
}

function plasma_basicNavTDPrinter($pageID, $title, $last = false, $hasChildren = false, $depth = 0)
{
    if ($depth > 0)
    {
        return;
    }

    global $cms_currentPage, $mouseOvers;
    $url = cms_currentBaseURL();

    if ($pageID == cms_currentPageID())
    {
        print "<br>" . str_repeat('&nbsp;', $depth*2)  . "$title";
    }
    else
    {
        print "<br>" . str_repeat('&nbsp;', $depth*2)  . "<a class='subnav' href=\"$url" . cms_renderer::link($pageID) . "\">$title</a>";
    }
}

function print_sideNav()
{
    global $ancestry, $sideNaved;
    if (count($ancestry) < 3)
    {
        return;
    }
   
    $pageID = $ancestry[1];
    while ($ancestry[0] != $pageID)
    {
        array_shift($ancestry);
    }
    array_shift($ancestry);

    //print $pageID;
    list($hasSubPages) = db_row(db_query(db_connect(), "select count(pageID) > 0 from cmsPagesPublished where parentID = $pageID and showInNav;"), 0);

    $sideNaved = db_boolean($hasSubPages);
    if ($sideNaved)
    {
        print '<table width=100% cellpadding=0px cellspacing=0px>';
        print '<tr>';
        print '<td valign=top nowrap style="border-right: solid 1px #e5e4e5; padding-right: 6px;"';
        cms_printPage($pageID);
        cms_printNav('plasma_basicNavTDPrinter', $pageID, $ancestry);
        print '</td>';
        print '<td valign=top width=100% style="padding-left: 6px;">';
    }
}

global $ancestry, $sideNaved, $common_baseURL;
$sideNaved = false;
$ancestry = array();
$path = cms_pathByPageID(cms_currentPageID(), $ancestry);
$isHomepage = (cms_currentPageID() == cms_defaultHomepageID());
?>
<div id="wrap">
	<div id="header">
	<h3 style="position: relative; top: 58px; left: 33px;">Open Source Desktops @ Work</h3>
	</div>
	<div id="nav" style="text-align: left; padding-top: 5px; padding-left: 13px;">A survey of Open Source desktop deployments in the workplace
	</div>
	<div id="content">
	
