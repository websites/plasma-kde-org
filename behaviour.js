window.onload = function()
{
	d=document;
	// Add "top" links to level 2 headings
	hh=d.getElementById('content').getElementsByTagName('h2');	
	for (i=0; i < hh.length; i++) {
		hh[i].innerHTML = "<a class='top' href='#'>top</a>"+hh[i].innerHTML;
	}
	// Highlight link to current page in menu
	mi=d.getElementById('nav').getElementsByTagName('a');
	for (i=0; i < mi.length; i++) {
		if (mi[i].href == d.location) {
			mi[i].setAttribute('class', 'selected');
			break;
		}
	}
}