<?php

include_once('include/common.php');
include_once("$phpfwkIncludePath/address.php");

$common_header = 'surveyHeader.php';
$common_footer = 'surveyFooter.php';
$thisPage = "$common_baseURL/osd_at_work.php";

$uses = Array(10 => "Basic office tasks",
              20 => "General/complete office tasks",
              30 => "Kiosk booth",
              40 => "Public computing",
              40 => "Single function GUI",
              50 => "Industrial control station",
              1000 => "Other");

$coTypes = Array(10 => "Home Office",
                 20 => "Small/Medium Size Business",
		 30 => "Small/Medium Size Enterprise",
		 40 => "Enterprise",
		 50 => "Government",
		 60 => "Education",
		 70 => "Non-profit/Charity",
		 80 => "NGO",
		 1000 => "Other");

$envs = Array(10 => "KDE",
              20 => "GNOME",
	      30 => "XFCE",
	      40 => "Window manager only",
	      1000 => "Other");

function printForm()
{
    global $thisPage, $uses, $envs, $coTypes;
    global $companyName, $companyType, $contactName, $contactEmail;
    global $envsUsed, $os;
    global $numberOfSystems, $numberOfOpenSystems, $numberOfUsers, $usedFor, $usedForOther;
    global $usedForSecondary, $usedForOtherSecondary;
    global $city, $province, $country, $feedback;

    if (!dataPresent())
    {
?>
    <p>Does your place of work have a deployment of computers that
     run an open source desktop environment such as <a href="http://www.kde.org">KDE</a> or <a href="http://www.gnome.org">GNOME</a>?
     If so, please fill out the information below to help us get a better idea of
     what such deployments look like so that we can better address
     the needs of our professional user base.</p>

    <p>Items with a star (*) and marked in red must be filled in
    to complete the survey.</p>

    <p>None of the specifics collected will be shared with any third party though the data
    may be used as part of a summary report that is anonymized and agregated into summary
    reports. No personal or identifiable information specific to you or your company will
    be shared.</p>
<?
    }

    print_form($thisPage);
    print '<div align="center"><table  cellspacing=0px cellpadding=3px class="box">';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Organization Name*</th><td width="400px">';
    print_textbox('companyName', $companyName);
    print '</td></tr>';
    
    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Organization Type*</th><td width="400px">';
    print_selectArray('companyType', $coTypes, $companyType);
    print '</td></tr>';


    print '<tr><th colspan=2 align=left style="font-size: 12pt;">Deployment Description</th></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap># of Desktops*</th><td width="200px">';
    print_multiRangeSelect('numberOfSystems', $numberOfSystems,
                           Array(1, 29, 1, 30, 100, 5, 105, 1000, 10));
    print '<br><span class="explanatoryText">This is the total number of desktop system in your organization, including both open source and proprietary systems.</span>';
    print '</td></tr>';
    
    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap># of Open Source Desktops*</th><td width="200px">';
    print_multiRangeSelect('numberOfOpenSystems', $numberOfOpenSystems,
                           Array(1, 29, 1, 30, 100, 5, 105, 1000, 10));
    print '<br><span class="explanatoryText">This is the number of desktops that run an open source desktop in your organization.</span>';
    print '</td></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap># of Users*</th><td width="200px">';
    print_multiRangeSelect('numberOfUsers', $numberOfUsers,
                           Array(1, 29, 1, 30, 100, 5, 105, 1000, 10, 1050, 10000, 50));
    print '<br><span class="explanatoryText">This is the average number of users serviced by your deployment. If it is a public installation, estimate the number in an average month.</span>';
    print '</td></tr>';
    
    print '<tr><th valign=top class="formDataHeader" nowrap>Environments</th><td width="200px"><table cellpadding=0 cellspacing=0 style="margin: 0px; padding: 0px;" border=0px>';
    print_checkboxTable('envsUsed', $envs, $envsUsed, 2, true);
    print '</table>';
    print '<br><span class="explanatoryText">Select the open source desktop environments used at your place of work above.</span></div>';
    print '</td></tr>';


    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Primary Use*</th><td width="200px">';
    print_selectArray('usedFor', $uses, $usedFor);
    print '<div style="padding-bottom: 0em; padding-top: .25em;">If other: ';
    print_textBox('usedForOther', $usedForOther);
    print '<br><span class="explanatoryText">Select what the primary use is of the systems in this deployment from the list above. If you can not find an appropriate selection in the list above, please select other and fill in the Other box.</span></div>';
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Secondary Use</th><td width="200px">';
    print_selectArray('usedForSecondary', $uses, $usedForSecondary, false);
    print '<div style="padding-bottom: 0em; padding-top: .25em;">If other: ';
    print_textBox('usedForOtherSecondary', $usedForOtherSecondary);
    print '<br><span class="explanatoryText">Select what the primary use is of the systems in this deployment from the list above. If you can not find an appropriate selection in the list above, please select other and fill in the Other box.</span></div>';
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Operating Systems</th><td width="200px">';
    print_textArea('os', $os, 3, 50);
    print '<br><span class="explanatoryText">List the open source operating systems used on the desktop at your place of work above.</span>';
    print '</td></tr>';

    print '<tr><th colspan=2 align=left style="font-size: 12pt; padding-top: 1.5em;">Location</th></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>City</th><td width="200px">';
    print_textBox('city', $city);
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Province/State</th><td width="200px">';
    print_textBox('province', $province);
    print '</td></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Country*</th><td width="200px">';
    print_countrySelect('country', $country, false);
    print '</td></tr>';

    print '<tr><th colspan=2 align=left style="font-size: 12pt; padding-top: 1.5em;">Optional Contact Information</th></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Contact Person</th><td width="200px">';
    print_textbox('contactName', $contactName);
    print '<br><span class="explanatoryText">Enter your name above. You may leave this blank if you wish to remain anonymous. This information will not be shared with anyone else.</span>';
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Contact Email</th><td width="200px">';
    print_textbox('contactEmail', $contactEmail);
    print '<br><span class="explanatoryText">Enter your email above. You may leave this blank if you do not wish to be contacted with survey results or updates. This information will not be shared with anyone else nor will you be put on any email lists. We may, however, contact you via email if we have questions regarding your survey.</span>';
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Feedback</th><td width="200px">';
    print_textArea('feedback', $feedback, 10, 50);
    print '<br><span class="explanatoryText">If you would like to tell us a bit about your deployment (e.g. what software you use on it, what it does well for you, what you would like to see be made better, etc) please do so in the box above.</span>';
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap></th><td width="200px">';
    print_submit('submit', 'Submit Survey');
    print '</td></tr>';

    print '</table></div>';
    print '</form>';
}

function dataPresent()
{
    global $companyName, $companyType, $contactName, $contactEmail;
    global $numberOfSystems, $numberOfOpenSystems, $numberOfUsers, $usedFor, $usedForOther;
    global $usedForSecondary, $usedForOtherSecondary;
    global $city, $province, $country, $feedback;
    global $os, $envsUsed, $envs;

    $companyName = trim($companyName);
    $companyType = trim($companyType);
    $contactName = trim($contactName);
    $contactEmail = trim($contactEmail);
    $numberOfSystems = intval($numberOfSystems);
    $numberOfOpenSystems = intval($numberOfOpenSystems);
    $numberOfUsers = intval($numberOfUsers);
    $usedFor = intval($usedFor);
    $usedForOther = trim($usedForOther);
    $usedForSecondary = intval($usedForSecondary);
    $usedForOtherSecondary = trim($usedForOtherSecondary);
    $city = trim($city);
    $province = trim($province);
    $country = trim($country);
    $feedback = trim($feedback);

    if (is_array($envsUsed))
    {
        $tmp = Array();
        foreach ($envsUsed as $key => $value)
        {
            $key = intval($key);
            if ($key < 1 || empty($envs[$key]))
            {
                continue;
            }
            $tmp[$key] = $value;
        }
        $envsUsed = $tmp;
    }

    return !empty($companyName);
}

function checkData()
{
        global $uses;
        global $companyName, $companyType, $contactName, $contactEmail;
        global $numberOfSystems, $numberOfOpenSystems, $numberOfUsers, $usedFor, $usedForOther;
        global $city, $province, $country, $feedback;
        global $usedForSecondary;
        global $os, $envsUsed;

        if ($numberOfSystems < 1)
        {
                print_msg(WARNING, 'Invalid Number of Desktopss',
                                'The number of systems must be greater than zero.',
                                'err/1');
                return false;
        }

        if ($numberOfOpenSystems < 1)
        {
                print_msg(WARNING, 'Invalid Number of Open Source Desktops',
                                'You must have at least one deployed open source desktop to participate in this survey.',
                                'err/7');
                return false;
        }

        if ($numberOfOpenSystems > $numberOfSystems)
        {
                print_msg(WARNING, 'Invalid Number of Open Source Desktops',
                                'You entered a smaller number for total # of desktops than for open source desktops.
                                Total systems should inclucde both open and proprietary desktops.
                                Please check your numbers and try again.',
                                'err/8');
                return false;
        }

        if ($numberOfUsers < 1)
        {
                print_msg(WARNING, 'Invalid Number of Users',
                                'The number of users must be greater than zero.',
                                'err/2');
                return false;
        }

        if (empty($uses[$usedFor]))
        {
                print_msg(WARNING, 'Primary Use Required',
                                'You must select something for the primary use.',
                                'err/3');
                unset($usedFor);
                return false;
        }

        if (empty($uses[$usedForSecondary]))
        {
                unset($usedForSecondary);
        }

        if (empty($country))
        {
                print_msg(WARNING, 'Country Required',
                                'You must enter the country this deployment is in.',
                                'err/5');
                return false;
        }

        if (!validateEmail($contactEmail))
        {
                print_msg(WARNING, 'Invalid Email Supplied',
                                'An email address is not required, but the one you supplied is not valid.',
                                'err/6');
                return false;
        }

        return true;
}

/*

   create sequence seq_smbSurvey;
   create table smbSurvey
   (
   id         int         primary key default nextval('seq_smbSurvey'),
   systems    int         not null default 0,
   users      int         not null default 0,
   usedFor    int         not null default 0,
 ipAddr     text,
 company    text,
 contact    text,
 email      text,
 city       text,
 province   text,
 country    text,
 feedback   text,
 usedOther  text
);
*/

function commitData()
{
    global $companyName, $companyType, $coTypes, $contactName, $contactEmail;
    global $numberOfSystems, $numberOfOpenSystems, $numberOfUsers, $usedFor, $usedForOther;
    global $usedForSecondary, $usedForOtherSecondary;
    global $city, $province, $country, $feedback;
    global $REMOTE_ADDR, $uses;
    global $os, $envsUsed, $envs;

    unset($fields);
    unset($values);
    sql_addScalarToInsert($fields, $values, 'company', $companyName);
    sql_addScalarToInsert($fields, $values, 'type', $companyType);
    sql_addScalarToInsert($fields, $values, 'contact', $contactName);
    sql_addScalarToInsert($fields, $values, 'email', $contactEmail);
    sql_addScalarToInsert($fields, $values, 'city', $city);
    sql_addScalarToInsert($fields, $values, 'province', $province);
    sql_addScalarToInsert($fields, $values, 'country', $country);
    sql_addScalarToInsert($fields, $values, 'os', $os);
    sql_addScalarToInsert($fields, $values, 'feedback', $feedback);

    if ($usedFor == 1000)
    {
        sql_addScalarToInsert($fields, $values, 'usedOther', $usedForOther);
    }
    sql_addIntToInsert($fields, $values, 'usedFor', $usedFor);

    if ($usedForSecondary == 1000)
    {
        sql_addScalarToInsert($fields, $values, 'usedOther2', $usedForOtherSecondary);
    }
    sql_addIntToInsert($fields, $values, 'usedFor2', $usedForSecondary);

    sql_addIntToInsert($fields, $values, 'users', $numberOfUsers);
    sql_addIntToInsert($fields, $values, 'systems', $numberOfSystems);
    sql_addIntToInsert($fields, $values, 'openSystems', $numberOfOpenSystems);
    sql_addScalarToInsert($fields, $values, 'ipAddr', $REMOTE_ADDR);

    $db = db_connect();
    db_insert($db, 'smbSurvey', $fields, $values);

    if (is_array($envsUsed))
    {
        $surveyID = db_seqCurrentVal($db, 'seq_smbSurvey');
        $sql = '';
        foreach ($envsUsed as $key => $value)
        {
            $sql .= "insert into surveyenvs (company, env) values ($surveyID, $key); ";
        }

        if ($sql)
        {
            db_query($db, $sql);
        }
    }
   
    print "<p><i>Thank-you</i> for participating in this survey. The following data was recorded:</p>";
    print '<div align="center"><table  cellspacing=0px cellpadding=3px class="box">';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Organization Name*</th><td width="400px">';
    print $companyName;
    print '</td></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Organization Type*</th><td width="400px">';
    print $coTypes[$companyType];
    print '</td></tr>';

    print '<tr><th colspan=2 align=left style="font-size: 12pt;">Deployment Description</th></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap># of Desktop Systems*</th><td width="200px">';
    print $numberOfSystems;
    print '</td></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap># of Open Source Desktop Systems*</th><td width="200px">';
    print $numberOfOpenSystems;
    print '</td></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap># of Users*</th><td width="200px">';
    print $numberOfUsers;
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Environments Used</th><td width="200px">';
    if (is_array($envsUsed))
    {
        $first = true;
        foreach ($envsUsed as $key => $value)
        {
            if (!$first)
            {
                print ', ';
            }
            print $envs[$key];
            $first = false;
        }
    }
    print '</td></tr>';
   
    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Primary Use*</th><td width="200px">';
    print $uses[$usedFor];

    if ($usedFor == 1000)
    {
        print ": $usedForOther";
    }
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Secondary Use</th><td width="200px">';
    print $uses[$usedForSecondary];

    if ($usedForSecondary == 1000)
    {
        print ": $usedForOtherSecondary";
    }
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Operating Systems</th><td width="200px">';
    print $os;
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Feedback</th><td width="200px">';
    print $feedback;
    print '</td></tr>';

    print '<tr><th colspan=2 align=left style="font-size: 12pt; padding-top: 1.5em;">Location</th></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>City</th><td width="200px">';
    print $city;
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Province/State</th><td width="200px">';
    print $province;
    print '</td></tr>';

    print '<tr><th valign=top class="mandatoryFormDataHeader" nowrap>Country*</th><td width="200px">';
    print $country;
    print '</td></tr>';

    print '<tr><th colspan=2 align=left style="font-size: 12pt; padding-top: 1.5em;">Optional Contact Information</th></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Contact Person</th><td width="200px">';
    print $contactName;
    print '</td></tr>';

    print '<tr><th valign=top class="formDataHeader" nowrap>Contact Email</th><td width="200px">';
    print $contactEmail;
    print '</td></tr>';

    print '</table></div>';
    print '</form>';
}

print_header("Open Source Desktops @ Work", false);

if (!dataPresent() || !checkData())
{
    printForm();
}
else
{
    commitData();
}

print_footer();

?>
