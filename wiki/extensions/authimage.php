<?php
/*
Plugin Name: AuthImage
Plugin URI: http://www.gudlyf.com/index.php?p=376
Description: Creates an authentication image to help combat spam in comments.
Version: 3.0
Author: Keith McDuffee
Author URI: http://www.gudlyf.com/

Originally modified by miEro

This version by C. Lewicki 3/25/2006
*/

session_start();

$authimage = '/wiki/extensions/authimage-inc/image.veriword.php';

function checkAICode($code)
{
    require_once("authimage-inc/class.verificator.php");
    $veri = new VeriFicator($code);
    $verified = $veri->verified();

    $return = ($verified == 1) ? 1 : 0;
    if(!isset($_SESSION['veriword'])) {
        $return = 0;
    } else {
        /*  
            session_unset();
	    session_destroy();
	    session_start();
	 */
    }
     
    return $return;
}

?>

