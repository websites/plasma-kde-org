<?php
/**
 * Solid 
 *
 * Completely ripped from developer.mozilla.org :)
 *
 * @todo document
 * @package MediaWiki
 * @subpackage Skins
 */

if( !defined( 'MEDIAWIKI' ) )
	die();

/** */
require_once('includes/SkinTemplate.php');

/**
 * Inherit main code from SkinTemplate, set the CSS and template filter.
 * @todo document
 * @package MediaWiki
 * @subpackage Skins
 */
class SkinKDE extends SkinTemplate {
	/** Using monobook. */
	function initPage( &$out ) {
		SkinTemplate::initPage( $out );
		$this->skinname  = 'kde';
		$this->stylename = 'kde';
		$this->template  = 'KDETemplate';
	}
}

/**
 * @todo document
 * @package MediaWiki
 * @subpackage Skins
 */
class KDETemplate extends QuickTemplate {
	/**
	 * Template filter callback for MonoBook skin.
	 * Takes an associative array of data set from a SkinTemplate-based
	 * class, and a wrapper for MediaWiki's localization database, and
	 * outputs a formatted page.
	 *
	 * @access private
	 */
	function execute() {
		// Suppress warnings to prevent notices about missing indexes in $this->data
		wfSuppressWarnings();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php $this->text('lang') ?>" lang="<?php $this->text('lang') ?>" dir="<?php $this->text('dir') ?>">
  <head>
    <meta http-equiv="Content-Type" content="<?php $this->text('mimetype') ?>; charset=<?php $this->text('charset') ?>" />
    <?php $this->html('headlinks') ?>
    <title><?php $this->text('pagetitle') ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/css/base.css" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/css/wiki.css" />
    <link rel="stylesheet" type="text/css" media="print" href="<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/css/wikiprint.css" />
    <!--[if lt IE 5.5000]><style type="text/css">@import "<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/IE50Fixes.css";</style><![endif]-->
    <!--[if IE 5.5000]><style type="text/css">@import "<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/IE55Fixes.css";</style><![endif]-->
    <!--[if gte IE 6]><style type="text/css">@import "<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/IE60Fixes.css";</style><![endif]-->
    <!--[if IE]><script type="<?php $this->text('jsmimetype') ?>" src="<?php $this->text('stylepath') ?>/common/IEFixes.js"></script>
    <meta http-equiv="imagetoolbar" content="no" /><![endif]-->
    <?php if($this->data['jsvarurl'  ]) { ?><script type="<?php $this->text('jsmimetype') ?>" src="<?php $this->text('jsvarurl'  ) ?>"></script><?php } ?>
    <script type="<?php $this->text('jsmimetype') ?>" src="<?php $this->text('stylepath' ) ?>/common/wikibits.js"></script>
    <?php if($this->data['usercss'   ]) { ?><style type="text/css"><?php $this->html('usercss'   ) ?></style><?php    } ?>
    <?php if($this->data['userjs'    ]) { ?><script type="<?php $this->text('jsmimetype') ?>" src="<?php $this->text('userjs'    ) ?>"></script><?php } ?>
    <?php if($this->data['userjsprev']) { ?><script type="<?php $this->text('jsmimetype') ?>"><?php      $this->html('userjsprev') ?></script><?php   } ?>
    <?php if($this->data['trackbackhtml']) print $this->data['trackbackhtml']; ?>
    <script type="text/javascript" src="<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/js/devmo.js"></script>
    <script type="text/javascript" src="<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/js/prototype.js"></script>
    <script type="text/javascript" src="<?php $this->text('stylepath') ?>/<?php $this->text('stylename') ?>/js/scriptaculous.js"></script>

  </head>
  <body <?php if($this->data['body_ondblclick']) { ?>ondblclick="<?php $this->text('body_ondblclick') ?>"<?php } ?>
        <?php if($this->data['body_onload'    ]) { ?>onload="<?php     $this->text('body_onload')     ?>"<?php } ?>
        <?php if($this->data['nsclass'        ]) { ?>class="<?php      $this->text('nsclass')         ?>"<?php } ?>>

  <div id="container">
    <p class="skipLink"><a href="#content" accesskey="2">Skip to main content</a></p>
    <div id="kde-org"><a href="http://www.kde.org/">Visit KDE.org</a></div>

  <div id="header">
    <h1><a href="/" title="Return to home page" accesskey="1">KDE</a></h1>
  </div>

    <!-- Navigation -->
    <div id="navigation">
        <div id="bar">
            <div>
                <ul id="contenttypes">
                    <li class="selected">
                        <a href="http://plasma.kde.org/wiki">Main Page</a>
                    </li>
                    <li>
                        <a href="http://solid.kde.org/wiki">Solid</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="page">
        <div id="sidebar">
        <!-- search box -->
	<form name="searchform" action="<?php $this->text('searchaction') ?>" id="searchform">
	    <input id="searchInput" name="search" type="text"
            <?php if($this->haveMsg('accesskey-search')) {
	    ?>accesskey="<?php $this->msg('accesskey-search') ?>"<?php }
            if( isset( $this->data['search'] ) ) {
            ?> value="<?php $this->text('search') ?>"<?php } ?> />
	    <input type='submit' name="go" class="searchButton" id="searchGoButton"
            value="<?php $this->msg('go') ?>"
            />&nbsp;<input type='submit' name="fulltext"
            class="searchButton"
            value="<?php $this->msg('search') ?>" />
        </form>
        <!-- end searchbox -->
	
	    <div class="pagetools">
                <div>
                    <h3>Navigation</h3>
                    <ul>
                        <?php foreach($this->data['personal_urls'] as $key => $item) {
                        ?><li id="pt-<?php echo htmlspecialchars($key) ?>"><a href="<?php
                        echo htmlspecialchars($item['href']) ?>"<?php
                        if(!empty($item['class'])) { ?> class="<?php
                        echo htmlspecialchars($item['class']) ?>"<?php } ?>><?php
                        echo htmlspecialchars($item['text']) ?></a></li><?php
                        } ?>
                    </ul>
                </div>
            </div>

            <!-- VIEWS -->
            <div class="pagetools">
                <div>
                    <h3>Views</h3>
                    <ul>
                        <?php foreach($this->data['content_actions'] as $key => $action) {
                        ?><li id="ca-<?php echo htmlspecialchars($key) ?>"
                        <?php if($action['class']) { ?>class="<?php echo htmlspecialchars($action['class']) ?>"<?php } ?>
                        ><a href="<?php echo htmlspecialchars($action['href']) ?>"><?php
                        echo htmlspecialchars($action['text']) ?></a></li><?php
                        } ?>
                    </ul>
                </div>
            </div>

            <!-- TOOLBOX -->
            <div class="pagetools">
                <div>
                    <h3>Toolbox</h3>
                    <ul>
                        <?php if($this->data['notspecialpage']) { foreach( array( 'whatlinkshere', 'recentchangeslinked' ) as $special ) { ?>
                        <li id="t-<?php echo $special?>">
                            <a href="<?php echo htmlspecialchars($this->data['nav_urls'][$special]['href'])?>"><?php echo $this->msg($special) ?></a>
                        </li>
                        <?php } } ?>

                        <?php foreach( array('upload', 'specialpages') as $special ) { ?>
                        <?php if($this->data['nav_urls'][$special]) {?>
                        <li id="t-<?php echo $special ?>">
                            <a href="<?php echo htmlspecialchars($this->data['nav_urls'][$special]['href'])?>"><?php $this->msg($special) ?></a>
                        </li>
                        <?php } ?>
                        <?php } ?>

                        <?php if(!empty($this->data['nav_urls']['print']['href'])) { ?>
                        <li id="t-print">
                            <a href="<?php echo htmlspecialchars($this->data['nav_urls']['print']['href'])?>"><?php echo $this->msg('printableversion') ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Begin Content -->

        <div id="content">
            <div class="article">
            <a name="top" id="contentTop"></a>
            <h1 class="firstHeading"><?php $this->text('title') ?></h1>
            <h3 id="siteSub"><?php $this->msg('tagline') ?></h3>
            <div id="contentSub"><?php $this->html('subtitle') ?></div>
            <div class="mainpage">
                <?php if($this->data['undelete']) { ?><div id="contentSub"><?php     $this->html('undelete') ?></div><?php } ?>
                <?php if($this->data['newtalk'] ) { ?><div class="usermessage"><?php $this->html('newtalk')  ?></div><?php } ?>
                <!-- start content -->
                <?php $this->html('bodytext') ?>
                <!-- end content -->
            </div>
        </div>

        <!-- end of the website -->
	<?php if($this->data['catlinks']) { ?>
        <div id="catlinks">
            <?php       $this->html('catlinks') ?>
        </div>
        <?php } ?>

        <div id="footer">
	     <ul>
                <?php if($this->data['lastmod'   ]) { ?><li id="f-lastmod"><?php    $this->html('lastmod')    ?></li><?php } ?>
                <?php if($this->data['viewcount' ]) { ?><li id="f-viewcount"><?php  $this->html('viewcount')  ?></li><?php } ?>
             </ul>
             <ul>
                <?php if($this->data['copyright' ]) { ?><li id="f-copyright"><?php  $this->html('copyright')  ?></li><?php } ?>
                <?php if($this->data['about'     ]) { ?><li id="f-about"><?php      $this->html('about')      ?></li><?php } ?>
            </ul>
        </div>

        <?php $this->html('reporttime') ?>

    </div>
    </div>
    </div>
    </body>
</html>
<?php
	wfRestoreWarnings();
	}
}
?>
