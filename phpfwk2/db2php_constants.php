#!/usr/local/bin/php -n
<?

// Change these vars to suit your system
$constantsPath = "/home/user/module/include";
$constantsFile = $constantsPath . "/constants.php";

// table name, and whether to quote the value
$tables = array("constant_ints"=>false,
                "constant_floats"=>false,
                "constant_strings"=>true);

/// DB environments
$dbname = "dbname";
$dbuser = "dbuser";
$dbpass = "dbpass";

// function to print lines nicely :-)
function printl($text)
{
    global $fd;
    fwrite($fd, $text);
    fwrite($fd, "\n");
}

// function to print information messages or die nicely ;-)
function info($msg, $abort=false)
{
    if ($abort)
    {
        die("db2php_constants.php:: " . $msg . "\n");
    }
    else
    {
        print("db2php_constants.php:: " . $msg . "\n");
    }
}

// function to print DB constant tables to a PHP constants file
function map_db_constants_to_php($table, $quotes)
{
    global $db;

    info("Examing table $table for values");

    $sql = "select name, value from $table;";
    $rv = pg_exec($db, $sql);

    if(! $rv)
    {
        info("Could not execute query : $sql");
        info("Script is aborting!", true);
    }

    $numrows = pg_numrows($rv);
    for($i = 0; $i < $numrows; $i++)
    {
        list($name, $value) = pg_fetch_row($rv, $i);

        if ($quotes)
        {
            printl("define(\"$name\", \"$value\");");
        }
        else
        {
            printl("define(\"$name\", $value);");
        }
    }

    info("Examination of table : $table is complete");
}


//******************************************************************************
// start of the main script

if (file_exists($constantsFile))
{
    rename($constantsFile, $constantsFile . ".bak." . time());
}

info("Creating file : " . $constantsFile);
$fd = fopen($constantsFile, "w");

printl("<?php");
printl("");
printl("if (!\$INCLUDED_CONSTANTS)");
printl("{");
printl("");
printl("\$INCLUDED_CONSTANTS = 1;");
printl("");
printl("/*");
printl(" * This file has been automatically created.");
printl(" * It was created from the phpfwk script db2php_constants.php.");
printl(" * This file is strictly a mapping of PHP style defined constants");
printl(" * for each entry in the phpfwk tables constant_ints,");
printl(" * constant_floats or constant_strings.");
printl(" */");
printl("");

info("Connecting to database : " . $dbname);
$db = pg_connect("dbname=$dbname user=$dbuser password=$dbpass");

if (! $db)
{
    info("Could not connect to database : $dbname, as user : $dbuser");
    info("Script is aborting!", true);
}

foreach($tables as $table => $quotes)
{
    map_db_constants_to_php($table, $quotes);
}

printl("");
printl("}");
printl("");
printl("?>");

// end of main script
//******************************************************************************

?>
