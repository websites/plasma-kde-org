-- DROP TABLE phpfwk_config;
CREATE TABLE phpfwk_config
(
    name            TEXT                NOT NULL PRIMARY KEY,
    value           TEXT                NOT NULL
);

INSERT INTO phpfwk_config VALUES ('BASE_SHOPPING_CURRENCY', 'CAD');
