-- returns an item price in a given currency
CREATE OR REPLACE FUNCTION item_price(INTEGER, INTEGER, TEXT) RETURNS NUMERIC(9,2) AS '
DECLARE
    in_sku ALIAS FOR $1;
    in_quant ALIAS FOR $2;
    in_currency ALIAS FOR $3;

    rv_price NUMERIC(9,2);
    currency_rate NUMERIC(9,6);

    item_rec RECORD;
BEGIN
    rv_price := 0.00;

    IF in_sku <= 0 THEN
        return 0.00;
--        RAISE EXCEPTION ''item_price(sku, quantity, currency) :: No sku was given'';
    END IF;

    IF in_quant <= 0 THEN
        return 0.00;
--        RAISE EXCEPTION ''item_price(sku, quantity, currency) :: Quantity less than 0'';
    END IF;

    SELECT INTO currency_rate rate FROM currencies WHERE currency = in_currency;
    IF NOT FOUND THEN
        return 0.00;
--        RAISE EXCEPTION ''item_price(sku, quantity, currency) :: Currency % not found'', in_currency;
    END IF;

    SELECT INTO rv_price price / currency_rate FROM skuPrices
                         WHERE sku = in_sku AND
                               minQuant <= in_quant AND
                               starts <= CURRENT_TIMESTAMP AND
                               (ends IS NULL OR ends >= CURRENT_TIMESTAMP)
                         ORDER BY minQuant DESC, starts DESC LIMIT 1;
    IF NOT FOUND THEN
--            RAISE EXCEPTION ''item_price(sku, quantity, currency) :: sku % not found'', in_sku;
    END IF;

    RETURN rv_price;
END;
' LANGUAGE 'plpgsql';

-- calculate a cart total in a given currency
CREATE OR REPLACE FUNCTION cart_total(INTEGER, TEXT) RETURNS NUMERIC(9,2) AS '
DECLARE
    in_cartID ALIAS FOR $1;
    in_currency ALIAS FOR $2;

    rv_total NUMERIC(9,2);
    currency_rate NUMERIC(9,6);

    item_rec RECORD;
BEGIN
    rv_total := 0.00;

    IF in_cartID <= 0 THEN
        RAISE EXCEPTION ''cart_total(cartID, currency) :: No cartID was given'';
    END IF;

    SELECT INTO currency_rate rate FROM currencies WHERE currency = in_currency;
    IF NOT FOUND THEN
        RAISE EXCEPTION ''cart_total(cartID, currency) :: Currency % not found'', in_currency;
    END IF;

    SELECT INTO rv_total sum((price * quantity) / currency_rate)
                         FROM shoppingCartItems WHERE cartID = in_cartID;

    RETURN rv_total;
END;
' LANGUAGE 'plpgsql';

-- re-calculates all item prices when the cart changes currency
CREATE OR REPLACE FUNCTION change_currency() RETURNS TRIGGER AS '
DECLARE
    new_rate NUMERIC(9,6);
    sku_price NUMERIC(9,2);
    item_rec RECORD;
BEGIN
    IF NEW.currency ISNULL THEN
        RAISE EXCEPTION ''The currency cannot be a NULL value'';
    END IF;

    SELECT INTO new_rate rate FROM currencies WHERE currency = NEW.currency;
    IF NOT FOUND THEN
        RAISE EXCEPTION ''change_currency() :: Currency % not found'', NEW.currency;
    END IF;

    FOR item_rec IN SELECT * FROM shoppingCartItems WHERE cartID = NEW.cartID LOOP
        UPDATE shoppingCartItems
            SET convertPrice = (item_rec.price / new_rate)
            WHERE cartID = NEW.cartID AND sku = item_rec.sku;
    END LOOP;

    RETURN NEW;
END;
' LANGUAGE 'plpgsql';

-- trigger to re-calculate prices when currency changes
DROP TRIGGER change_currency ON shoppingCarts;
CREATE TRIGGER change_currency AFTER UPDATE ON shoppingCarts
    FOR EACH ROW EXECUTE PROCEDURE change_currency();

-- Calculates the item price based on the listed sku price, and the cart currency
CREATE OR REPLACE FUNCTION calculate_price() RETURNS TRIGGER AS '
DECLARE
    cur_currency CHAR(3);
    def_currency CHAR(3);
    cur_rate NUMERIC(9,6);
    sku_price NUMERIC(9,2);
BEGIN
    SELECT INTO cur_currency currency FROM shoppingCarts WHERE cartID = NEW.cartID;
    IF NOT FOUND THEN
        RAISE EXCEPTION ''calculate_price() :: Shopping cart % not found'', NEW.cartID;
    END IF;

    SELECT INTO def_currency value FROM phpfwk_config WHERE name = ''BASE_SHOPPING_CURRENCY'';
    IF NOT FOUND THEN
        RAISE EXCEPTION ''calculate_price() :: BASE_SHOPPING_CURRENCY not found in phpfwk_config'';
    END IF;

    SELECT INTO cur_rate rate FROM currencies WHERE currency = cur_currency;
    IF NOT FOUND THEN
        RAISE EXCEPTION ''calculate_price() :: Currency % not found'', cur_currency;
    END IF;

    SELECT INTO sku_price item_price(NEW.sku, NEW.quantity, def_currency);
    IF NOT FOUND THEN
       RAISE EXCEPTION ''calculate_price() :: No price found for %'', NEW.sku;
    END IF;

    NEW.price := sku_price;
    NEW.convertPrice := sku_price / cur_rate;

    RETURN NEW;
END;
' LANGUAGE 'plpgsql';

-- trigger to calculate the price when inserting
DROP TRIGGER calculate_price ON shoppingCartItems;
CREATE TRIGGER calculate_price BEFORE INSERT OR UPDATE ON shoppingCartItems
    FOR EACH ROW EXECUTE PROCEDURE calculate_price();

-- trigger to cascade delete catalogs
