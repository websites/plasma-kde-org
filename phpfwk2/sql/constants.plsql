CREATE OR REPLACE FUNCTION constant_int_value(TEXT) RETURNS INTEGER AS '
DECLARE
    in_name ALIAS FOR $1;
    rv INTEGER;
BEGIN
    SELECT INTO rv
        value FROM constant_ints
        WHERE name = in_name;
    RETURN rv;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION constant_float_value(TEXT) RETURNS FLOAT AS '
DECLARE
    in_name ALIAS FOR $1;
    rv FLOAT;
BEGIN
    SELECT INTO rv
        value FROM constant_floats
        WHERE name = in_name;
    RETURN rv;
END;
' LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION constant_string_value(TEXT) RETURNS TEXT AS '
DECLARE
    in_name ALIAS FOR $1;
    rv TEXT;
BEGIN
    SELECT INTO rv
        value FROM constant_strings
        WHERE name = in_name;
    RETURN rv;
END;
' LANGUAGE 'plpgsql';
