-- templates, emails, etc
-- drop sequence templateIDs ;
CREATE SEQUENCE  emailTemplateIDs;

-- drop table templateCategories;
CREATE TABLE templateCategories
(
 categoryID     int             PRIMARY KEY default nextval('emailTemplateIDs'),
 title          text
);


-- drop table template ;
CREATE TABLE templates
(
 templateID     int             PRIMARY KEY default nextval('emailTemplateIDs'),
 category       int             references templateCategories(categoryID) ON delete cascade,
 name           text            not null unique,
 title          text,
 body           text
);

-- drop table templateSQL;
CREATE TABLE templateSQL
(
 templateID     int             not null references templates(templateID) ON delete cascade,
 sql            text            not null,
 provides       text,
 requires       text
);

-- drop sequence mailspoolids;
CREATE SEQUENCE mailspoolids;

-- drop table mailSpool;
CREATE TABLE mailSpool
(
 id             int             PRIMARY KEY default nextval('mailspoolids'),
 userID         int             not null references users(userID) ON delete cascade,
 categoryID     int             references templateCategories(categoryID) ON delete cascade,
 sendOn         timestamp       not null default current_timestamp,
 done           boolean         default false,
 fromAddress    text            not null,
 subject        text            not null,
 body           text            not null
);

