-- DROP SEQUENCE seq_notificationIDs;
CREATE SEQUENCE seq_notificationIDs START 1000;

-- DROP TABLE dms_notifications;
CREATE TABLE dms_notifications
(
    notificationID              INTEGER         NOT NULL PRIMARY KEY
                                                DEFAULT nextval('seq_notificationIDs'),

    id                          INTEGER,
    event                       INTEGER,
    groupID                     INTEGER         CONSTRAINT fk_groupID
                                                REFERENCES groups(groupID)
                                                ON DELETE CASCADE
);
