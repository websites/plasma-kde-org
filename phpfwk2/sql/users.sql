-- table definitions for the useristrative tables
-- used in conjunction with the auth_common.php

-- DROP SEQUENCE groupIDs;
CREATE SEQUENCE groupIDs START 1000;

-- DROP TABLE groups CASCADE;
CREATE TABLE groups
(
 groupID                INTEGER                 PRIMARY KEY DEFAULT nextval('groupIDs'),
 name                   TEXT                    NOT NULL UNIQUE,
 header                 TEXT,
 footer                 TEXT,
 mnemonic               TEXT,
 fullname               TEXT,
 homepageID             INTEGER,
 active                 BOOLEAN                 DEFAULT TRUE
);

-- default group root
INSERT INTO groups VALUES('0', 'root');
INSERT INTO groups VALUES('1', 'users');

-- TRIGGER function for deletion of users when a group is deleted
CREATE OR REPLACE FUNCTION del_groupUsers() RETURNS TRIGGER AS '
BEGIN
    DELETE FROM users WHERE userid IN (SELECT userid FROM userGroups WHERE groupID = OLD.groupID AND primaryGroup);
RETURN OLD;
END;
' LANGUAGE 'PLPGSQL';

-- DROP TRIGGER trg_groups_delGroupUsers ON groups;
CREATE TRIGGER trg_groups_delGroupUsers BEFORE DELETE ON groups
FOR EACH ROW EXECUTE PROCEDURE del_groupUsers();


-- DROP SEQUENCE userIDs;
CREATE SEQUENCE userIDs START 1000;

-- DROP TABLE users;
CREATE TABLE users
(
 userID                 INTEGER                 PRIMARY KEY DEFAULT nextval('userIDs'),
 login                  TEXT                    NOT NULL,
 password               TEXT                    NOT NULL,
 firstName              TEXT                    NOT NULL DEFAULT '',
 middleName             TEXT                    NOT NULL DEFAULT '',
 lastName               TEXT                    NOT NULL DEFAULT '',
 email                  TEXT                    DEFAULT '',
 strikes                INTEGER                 DEFAULT 0,
 lastlog                TIMESTAMP,
 active                 BOOLEAN                 DEFAULT TRUE
);

-- default user login : root, password : root
INSERT INTO users VALUES('0', 'root', '63a9f0ea7bb98050796b649e85481845', 'root');

-- DROP TABLE userGroups;
CREATE TABLE userGroups
(
 userID                 INTEGER                 CONSTRAINT fk_userid REFERENCES users(userID)
                                                ON DELETE CASCADE,
 groupID                INTEGER                 CONSTRAINT fk_groupid REFERENCES groups(groupID)
                                                ON DELETE CASCADE,
 primaryGroup           BOOLEAN                 DEFAULT FALSE
);

-- default userGroup for root
INSERT INTO userGroups VALUES('0', '0', true);

-- userTokens for logging in
-- DROP TABLE userTokens;
CREATE TABLE userTokens
(
 userID                 INTEGER                 CONSTRAINT fk_userid REFERENCES users(userID)
                                                ON DELETE CASCADE,
 token                  TEXT                    NOT NULL,
 stamp                  TIMESTAMP               NOT NULL
);

