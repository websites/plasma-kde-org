-- drop sequence seq_cmsPageIDs;
CREATE SEQUENCE seq_cmsPageIDs START 1000;

-- drop table cmsTypes;
CREATE TABLE cmsTypes
(
 typeID             int             PRIMARY KEY,
 name               varchar(25)     not null,
 object             varchar(25)
);

INSERT INTO cmsTypes (typeID, name, object) VALUES (0, 'Normal', null);
INSERT INTO cmsTypes (typeID, name, object) VALUES (1, 'FAQ', 'faq');
INSERT INTO cmsTypes (typeID, name, object) VALUES (2, 'Search', 'search');
INSERT INTO cmsTypes (typeID, name, object) VALUES (3, 'Site Map', 'sitemap');
INSERT INTO cmsTypes (typeID, name, object) VALUES (7, 'Virtual Page', 'symlink');
INSERT INTO cmsTypes (typeID, name, object) VALUES (8, 'Email Form', 'emailform');
INSERT INTO cmsTypes (typeID, name, object) VALUES (9, 'Client Login', 'clientlogin');
INSERT INTO cmsTypes (typeID, name, object) VALUES (10,'Documents', 'dms');
INSERT INTO cmsTypes (typeID, name, object) VALUES (15,'Embedded', 'embed');
-- 20 -> photo album
-- 21 -> shopping


-- drop view cmsPagesPublished; drop table cmsPages; drop table cmsFAQs; drop table cmsPagesMetadata;
CREATE TABLE cmsPagesMetadata
(
 pageID         int             PRIMARY KEY not null default nextval('seq_cmsPageIDs'),
 parentID       int             references cmsPagesMetadata(pageID) ON delete cascade,
 typeID         int             references cmsTypes(typeID) ON delete set null default 0,
 showInNav      bool            not null default true,
 addable        bool            not null default true,
 editable       bool            not null default true,
 deletable      bool            not null default true,
 adminGroup     int             references groups(groupID) ON delete set null default 0,
 editGroup      int             references groups(groupID) ON delete set null default 0,
 displayOrder   int             not null default 1,
 pubRevision    int,
 title          text
);

INSERT INTO cmsPagesMetadata VALUES (1, NULL, 0, false, true, false, false, 0, 0, 0, NULL, 'Top');
INSERT INTO cmsPagesMetadata VALUES (2, NULL, 0, false, true, false, false, 0, 0, 1, NULL, 'Left');
INSERT INTO cmsPagesMetadata VALUES (3, NULL, 0, false, true, false, false, 0, 0, 2, NULL, 'Right');
INSERT INTO cmsPagesMetadata VALUES (4, NULL, 0, false, true, false, false, 0, 0, 3, NULL, 'Bottom');

-- drop table cmsSymlinks;
CREATE TABLE cmsSymlinks
(
 pageID     int             references cmsPagesMetadata(pageID) ON delete cascade,
 realPageID int             references cmsPagesMetadata(pageID) ON delete cascade
);

-- drop table cmsEmailForms;
CREATE TABLE cmsEmailForms
(
 pageID                 int             references cmsPagesMetadata(pageID) ON delete cascade,
 toAddress              text,
 defaultSubject         text,
 buttontext             text,
 fancyEditor            boolean         default false
);

-- drop table cmsPages;
CREATE TABLE cmsPages
(
 revision       int             PRIMARY KEY default nextval('seq_cmsPageIDs'),
 pageID         int             references cmsPagesMetadata(pageID) ON delete cascade,
 editedBy       int             references users(userID) ON delete set null,
 approvedBy     int             references users(userID) ON delete set null,
 approvedOn     timestamp,
 body           text
);

CREATE INDEX i_cmsPagesIDs ON cmsPages (pageID);

-- drop view cmsPagesPublished;
create view cmsPagesPublished as
select md.pageID as pageID, md.displayOrder as displayOrder, md.editGroup as ownedBy,
md.typeID as typeID, types.name as typeName, types.object as typeObject,
md.parentID as parentID, md.showInNav as showInNav,
md.title as title, pg.body as body,
md.editGroup as editGroup
from cmsPages pg join cmsPagesMetadata md ON (pg.revision = md.pubRevision)
left join cmsTypes types ON (md.typeID = types.typeID);


-- drop table cmsSecurePageACLs
CREATE TABLE cmsSecurePageACLs
(
 pageID             int         references cmsPagesMetadata(pageID) ON delete cascade,
 groupID            int         references groups(groupID) ON delete cascade,
 userID             int         references users(userID) ON delete cascade,
 acl                int         not null default 0
);

CREATE INDEX ind_cmsACL_f ON cmsSecurePageACLs (pageID);
CREATE INDEX ind_cmsACL_fg ON cmsSecurePageACLs (pageID, groupID);
CREATE INDEX ind_cmsACL_fu ON cmsSecurePageACLs (pageID, userID);
