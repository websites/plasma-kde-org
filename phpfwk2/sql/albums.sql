-- DROP SEQUENCE albumIDs;
CREATE SEQUENCE albumIDs START 1000 INCREMENT 1;

-- DROP TABLE albums CASCADE;
CREATE TABLE albums
(
 albumID            INTEGER                     NOT NULL    PRIMARY KEY,
 name               CHARACTER VARYING(255)      NOT NULL    UNIQUE,
 numPhotosPerPage   INTEGER                     DEFAULT     9,
 numPhotosPerRow    INTEGER                     DEFAULT     3
);

-- DROP SEQUENCE photoIDs;
CREATE SEQUENCE photoIDs START 1000 INCREMENT 1;

-- DROP TABLE photos;
CREATE TABLE photos
(
 photoID            INTEGER                     NOT NULL    PRIMARY KEY,
 albumID            INTEGER,
 imagefile          CHARACTER VARYING(255)      NOT NULL,
 photo              CHARACTER VARYING(255)      NOT NULL,
 thumbnail          CHARACTER VARYING(255)      NOT NULL,
 caption            CHARACTER VARYING(1024)     DEFAULT ''
);

-- ALTER TABLE photos DROP CONSTRAINT fk_albumID;
ALTER TABLE photos
    ADD CONSTRAINT fk_albumID
    FOREIGN KEY (albumID)
    REFERENCES albums(albumID)
    ON DELETE SET NULL;

INSERT INTO cmsTypes (typeID, name, object) VALUES (20,'Photo Album', 'photoAlbum');

