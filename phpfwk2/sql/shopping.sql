-- table definitions for the useristrative tables
-- used in conjunction with the auth_common.php

-- drop sequence seq_skus;
CREATE SEQUENCE seq_skus START 1000;

-- drop table skuGroups;
CREATE TABLE skuGroups
(
 groupID        int             PRIMARY KEY DEFAULT nextval('seq_skus'),
 displayOrder   int             not null DEFAULT 1,
 active         bool            not null DEFAULT true,
 parent         int             references skuGroups(groupID) on delete cascade,
 catalog        int             references skuGroups(groupID) on delete cascade,
 name           text            not null,
 smallImg       text,
 largeImg       text
);

-- drop table skus;
CREATE TABLE skus
(
 sku            int             PRIMARY KEY DEFAULT nextval('seq_skus'),
 groupID        int             references skuGroups(groupID) on delete set null,
 displayOrder   int             not null DEFAULT 1,
 active         bool            not null DEFAULT true,
 id             char(20)        not null DEFAULT '',
 name           text            not null DEFAULT '',
 shortDesc      text,
 longDesc       text,
 smallImg       text,
 largeImg       text
);
CREATE INDEX ind_sku_groups ON skus (groupID);

-- drop table skuOptionTypes;
CREATE TABLE skuOptionTypes
(
 optionTypeID       int            PRIMARY KEY DEFAULT nextval('seq_skus'),
 name               text
);
INSERT INTO skuOptionTypes (name) VALUES ('Size');
INSERT INTO skuOptionTypes (name) VALUES ('Color');

-- drop table skuOptions;
CREATE TABLE skuOptions
(
 optionID       int            PRIMARY KEY DEFAULT nextval('seq_skus'),
 sku            int            references skus(sku) on delete cascade,
 type           int            references skuOptionTypes(optionTypeID) on delete cascade,
 name           text
);
CREATE INDEX idx_skuOptions_sku ON skuOptions (sku);

-- drop table exchangeRates;
CREATE TABLE currencies
(
 currency       char(3)        PRIMARY KEY,
 rate           numeric(9,6)   not null DEFAULT 0.0,
 longName       text
);
INSERT INTO currencies VALUES ('CAD', 1, 'Canada Dollar');
INSERT INTO currencies VALUES ('USD', 1.32589, 'US Dollar');
INSERT INTO currencies VALUES ('EUR', 1.68304, 'Euro');

-- drop table skuPrices;
CREATE TABLE skuPrices
(
 sku            int             references skus(sku) on delete cascade,
 currency       char(3)         not null DEFAULT 'CAD',
 minQuant       numeric(9,2)    not null DEFAULT 1,
 price          numeric(9,2)    not null DEFAULT 0,
 starts         timestamp,
 ends           timestamp
);
CREATE INDEX ind_sku_prices ON skuPrices (sku);


-- drop table relatedSkus;
CREATE TABLE relatedSkus
(
 sku            int             references skus(sku),
 relatedTo      int             references skus(sku)
);
CREATE INDEX ind_skusRelated ON relatedSkus (sku);


-- drop table cartStatus;
CREATE TABLE cartStatus
(
 statID        int             PRIMARY KEY,
 status        char(25)
);
INSERT INTO cartStatus (statID, status) VALUES (0, 'Incomplete');
INSERT INTO cartStatus (statID, status) VALUES (1, 'Waiting Approval');
INSERT INTO cartStatus (statID, status) VALUES (2, 'Approved');
INSERT INTO cartStatus (statID, status) VALUES (3, 'Paid');
INSERT INTO cartStatus (statID, status) VALUES (4, 'Shipped');

-- drop sequence seq_carts;
CREATE SEQUENCE seq_carts START 1000;

-- drop table shoppingCarts;
CREATE TABLE shoppingCarts
(
 cartID        int             PRIMARY KEY DEFAULT nextval('seq_carts'),
 created       timestamp       not null DEFAULT current_timestamp,
 currency      char(3)         not null DEFAULT 'CAD',
 status        int             DEFAULT 0 references cartStatus(statID),
 taxRate1      numeric(7,4),
 taxRate2      numeric(7,4),
 shipping      numeric(9,2),
 paymentBy     text,
 ccNo          text,
 ccExpiry      text,
 ccName        text,
 orderedName   text,
 orderedPhone  text,
 orderedEmail  text,
 shipName      text,
 shipStreet1   text,
 shipStreet2   text,
 shipCity      text,
 shipProvince  text,
 shipCountry   text,
 shipPostal    text,
 confirmed     bool            not null DEFAULT false
);

-- drop sequence seq_shopItems;
CREATE SEQUENCE seq_shopItems START 1000;

-- drop table shoppingCartItems;
CREATE TABLE shoppingCartItems
(
 itemID        int             PRIMARY KEY DEFAULT nextval('seq_shopItems'),
 cartID        int             not null references shoppingCarts(cartID) on delete cascade,
 sku           int             not null references skus(sku),
 quantity      int             not null DEFAULT 1,
 price         numeric(9,2)    not null DEFAULT 0,
 convertPrice  numeric(9,2)    not null DEFAULT 0
);
CREATE INDEX ind_cartItems ON shoppingCartItems (cartID);

-- drop table shoppingCartItemOptions;
CREATE TABLE shoppingCartItemOptions
(
 itemID        int             not null references shoppingCartItems(itemID) on delete cascade,
 optionID      int             references skuOptions(optionID) on delete set null
);

-- drop table greetingTypes;
CREATE TABLE greetingTypes
(
 greetingTypeID     int             PRIMARY KEY,
 greetingType       text
);
INSERT INTO greetingTypes VALUES (0, 'Shipping Confirmation Email');
INSERT INTO greetingTypes VALUES (1, 'Order Confirmation Email');

-- drop sequence seq_greetings;
CREATE SEQUENCE seq_greetings START 1000;

-- drop table emailGreetings;
CREATE TABLE emailGreetings
(
 greetingID         int             PRIMARY KEY DEFAULT nextval('seq_greetings'),
 greetingTypeID     int             not null references greetingTypes(greetingTypeID),
 fromAddress        text            not null,
 replyAddress       text            not null,
 internalCC         text,
 subject            text            not null,
 greeting           text,
 closing            text,
 active             bool            not null DEFAULT true,
 internalCC         text
);

-- drop sequence seq_clerks;
CREATE SEQUENCE seq_clerks START 1003;

-- drop table clerks;
CREATE TABLE clerks
(
 clerkID        int               PRIMARY KEY DEFAULT nextval('seq_clerks'),
 name           text,
 class          text
);
INSERT INTO clerks (clerkID, name) VALUES (1, 'No shipping');
INSERT INTO clerks (clerkID, name, class) VALUES (2, 'By Weight', 'weight');
INSERT INTO clerks (clerkID, name, class) VALUES (3, 'By Price', 'price');
INSERT INTO clerks (name, class) VALUES ('Flagworks', 'flagworks');


-- drop sequence seq_taxman;
CREATE SEQUENCE seq_taxman START 1003;

-- drop table taxman;
CREATE TABLE taxman
(
 taxmanID       int               PRIMARY KEY DEFAULT nextval('seq_taxman'),
 name           text,
 class          text
);
INSERT INTO taxman (taxmanID, name) VALUES (1, 'No tax');
INSERT INTO taxman (taxmanID, name, class) VALUES (2, 'Canadian', 'canadian');


-- drop sequence seq_stores;
CREATE SEQUENCE seq_stores START 1000;

-- drop table stores;
CREATE TABLE stores
(
 storeID        int               PRIMARY KEY DEFAULT nextval('seq_stores'),
 currency       char(3)           not null DEFAULT 'CAD' references currencies(currency) on delete set null,
 clerk          int               references clerks(clerkID) on delete set null,
 name           text              not null
);

-- drop table storeCatalogs;
CREATE TABLE storeCatalogs
(
 storeID        int               not null references stores(storeID) on delete cascade,
 skuGroup       int               not null references skuGroups(groupID) on delete cascade
);
