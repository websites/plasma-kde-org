-- DROP TABLE constant_ints;
CREATE TABLE constant_ints
(
    name        TEXT NOT NULL PRIMARY KEY,
    value       INTEGER
);

-- DROP TABLE constant_floats;
CREATE TABLE constant_floats
(
    name        TEXT NOT NULL PRIMARY KEY,
    value       FLOAT
);

-- DROP TABLE constant_strings;
CREATE TABLE constant_strings
(
    name        TEXT NOT NULL PRIMARY KEY,
    value       TEXT
);
