#!/bin/sh

function usage
{
    echo "phpfwk_dbLoader.sh v0.1"
    echo "This is a script to load the sql files into a new database"
    echo
    echo "Usage: "
    echo "  phpfwk_dbLoader.sh [OPTIONS] -d DATABASENAME -U DATABASEUSER "
    echo
    echo "Options: "
    echo "  -d <name>   name for the new database"
    echo "  -U <name>   name for the database user to own the db and its tables"
    echo "  -p <path>   prefix to phpfwk2"
    echo "  -s          install shopping sql"
    echo "  -e          install email templates sql"
    echo "  -c          install constants-in-the-database sql"
    echo "  -a          install photo album sql"
    exit
}

if [ $# -lt 2 ]
then
    usage
fi

PG_DBNAME=phpfwk
PG_USER=phpfwk
PG_PASSWORD=$3
PG_ADMINUSER=postgres
PHPFWK="/var/www/phpfwk2"
SHOPPING=0
EMAIL=0
CONSTANTS=0
PHOTOS=0

while getopts ":d:U:asecp:" Option
do
    case $Option in
        d ) PG_DBNAME=$OPTARG;;
        U ) PG_USER=$OPTARG;;
        p ) PHPFWK=$OPTARG;;
        s ) SHOPPING=1;;
        e ) EMAIL=1;;
        c ) CONSTANTS=1;;
        a ) PHOTOS=1;;
    esac
done
shift $(($OPTIND -1 ))

createuser -U $PG_ADMINUSER -A -D $PG_USER
createdb -U $PG_ADMINUSER -O $PG_USER $PG_DBNAME
createlang -U $PG_ADMINUSER plpgsql $PG_DBNAME

for i in users.sql cms.sql dms.sql cmsObjects.sql notification.sql
do
    psql -U $PG_USER -f $PHPFWK/sql/$i $PG_DBNAME
done

if [ $SHOPPING -eq 1 ]
then
    psql -U $PG_USER -f $PHPFWK/sql/shopping.sql $PG_DBNAME
    psql -U $PG_USER -f $PHPFWK/sql/shopping.plsql $PG_DBNAME
fi

if [ $EMAIL -eq 1 ]
then
    psql -U $PG_USER -f $PHPFWK/sql/email.sql $PG_DBNAME
fi

if [ $CONSTANTS -eq 1 ]
then
    psql -U $PG_USER -f $PHPFWK/sql/constants.sql $PG_DBNAME
    psql -U $PG_USER -f $PHPFWK/sql/constants.plsql $PG_DBNAME
fi

if [ $PHOTOS -eq 1 ]
then
    psql -U $PG_USER -f $PHPFWK/sql/albums.sql $PG_DBNAME
fi

