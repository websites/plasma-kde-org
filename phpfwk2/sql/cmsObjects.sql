-- drop table cmsFAQs;
CREATE TABLE cmsFAQs
(
 faqID          int            PRIMARY KEY default nextval('seq_cmsPageIDs'),
 pageID         int            not null references cmsPagesMetadata(pageID) on delete cascade,
 displayOrder   int            not null default 1,
 question       text,
 answer         text
);

-- drop table cmsDMS;
CREATE TABLE cmsDMS
(
 pageID         int            not null references cmsPagesMetadata(pageID) on delete cascade,
 folderID       int            not null references dmsFolders(folderID) on delete cascade,
 showHeader     bool           default false
);

-- DROP table cmsEmbed;
CREATE TABLE cmsEmbed
(
 pageID         int            not null references cmsPagesMetadata(pageID) on delete cascade,
 url            TEXT           not null
);
