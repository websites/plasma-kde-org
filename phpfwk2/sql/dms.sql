-- drop sequence seq_dmsIDs;
CREATE SEQUENCE seq_dmsIDs START 2;

-- drop table dmsFolders;
CREATE TABLE dmsFolders
(
 folderID           int                      PRIMARY KEY default nextval('seq_dmsIDs'),
 parentFolderID     int                      references dmsFolders(folderID) ON delete cascade,
 name               character varying(256)   not null,
 groupID            int                      references groups(groupID) ON delete set null
);

insert into dmsFolders values (1, null, 'Images');

-- drop table dmsDocuments;
CREATE TABLE dmsDocuments
(
 documentID         int         PRIMARY KEY default nextval('seq_dmsIDs'),
 folderID           int         not null references dmsFolders(folderID),
 displayOrder       int         default 0,
 created            date,
 filename           text        not null,
 title              text        not null,
 description        text
);

-- drop table dmsACLs;
CREATE TABLE dmsACLs
(
 folderID           int         references dmsFolders(folderID) ON delete cascade,
 documentID         int         references dmsFolders(folderID) ON delete cascade,
 groupID            int         references groups(groupID) ON delete cascade,
 userID             int         references users(userID) ON delete cascade,
 acl                int         not null default 0
);
CREATE INDEX ind_dmsACL_f ON dmsACLs (folderID);
CREATE INDEX ind_dmsACL_d ON dmsACLs (documentID);
CREATE INDEX ind_dmsACL_fg ON dmsACLs (folderID, groupID);
CREATE INDEX ind_dmsACL_fu ON dmsACLs (folderID, userID);
CREATE INDEX ind_dmsACL_dg ON dmsACLs (documentID, groupID);
CREATE INDEX ind_dmsACL_du ON dmsACLs (documentID, userID);
