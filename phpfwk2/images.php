<?php

function dir_exists($path, $create=false)
{
    if (file_exists($path) == false)
    {
        if ($create == true)
        {
            mkdir($path, 0755);
        }
        else
        {
            return false;
        }
    }
    return true;
}

function imagePath($table, $field, $idfield, $id)
{
    global $common_catalogImagesPath;

    if ($id)
    {
        $db = db_connect();
        $row = db_query($db, "SELECT $field FROM $table WHERE $idfield = $id;");

        if (db_numRows($row) > 0)
        {
            list($img) = db_row($row, 0);
            return "$common_catalogImagesPath/$img";
        }
    }
}

function imageForID($table, $field, $idfield, $id)
{
    $id = intval($id);

    if ($id < 1)
    {
        return false;
    }

    $db = db_connect();

    $field = addslashes($field);
    $table = addslashes($table);
    $row = db_query($db, "SELECT $field FROM $table WHERE $idfield = $id;");

    if (db_numRows($row) < 1)
    {
        return false;
    }

    list($img) = db_row($row, 0);

    if (empty($img))
    {
        return false;
    }

    return $img;
}

function imageExists($path, $table, $field, $idfield, $id)
{
    $img = imageForID($table, $field, $idfield, $id);

    if (!$img)
    {
        return false;
    }

    return file_exists("$path/$img");
}

function imageURL($path, $table, $field, $idfield, $id)
{
    $img = imageForID($table, $field, $idfield, $id);

    if (!$img)
    {
        return false;
    }

    return "$path/$img";;
}

function upload_image($imagePath, $element, &$file, $id, $thumb = true)
{
    global $HTTP_POST_FILES;

    if ($file && $id)
    {
        // create the image path based on id
        $newFileName = "$id" . "_" . $HTTP_POST_FILES[$element]['name'];

        if ($thumb)
        {
            $newFileName = "thumb_$newFileName";
        }

        $newFilePath = "$imagePath/$newFileName";

        // check fior directory
        dir_exists($imagePath, true);

        //autmatically overwrite if it is already there
        if (! move_uploaded_file($file, $newFilePath))
        {
            print_msg('ERROR', 'FILE ERROR',
                      "There was an error uploading the file : \"$newFileName\".<br>
                       Please contact the web administrator.");
            return false;
        }
        else
        {
            chmod($newFilePath, 0644);
        }

        return $newFileName;
    }

    return false;
}

/*
 * createThumbnail -> create a scaled image
 *
 * $filepath : the original filepath
 * $scaledFilePath : the new file path to save the scaled image
 * $scaledSize : the maximum size of the image length or width (in pixels)
 */
function createThumbnail($filepath, $scaledFilePath, $scaledSize = 125, $enlarge = true)
{
    if ($scaledSize < 1)
    {
        print_msg('error', 'Scaling Error!',
                  "The image $filepath was requested to be scaled to :$scaledSize.<br>
                   The Scale size must be greater than 0.");
        return false;
    }

    $im = imagick_readimage($filepath);

    if (!$im)
    {
        print_msg('error', 'Image Read Error!',
                  "handle failed!\n");
        return false;
    }
    else if ( imagick_iserror($im) )
    {
        $reason = imagick_failedreason($im);
        $description = imagick_faileddescription($im);
        print_msg('error', 'Image Read Error!',
                  "handle failed!<BR>\nReason: $reason<BR>\nDescription: $description<BR>\n");
        return false;
    }

    $scale = 1;

    $dimensions = getImageSize($filepath);
    $src_w = $dimensions[0];
    $src_h = $dimensions[1];

    if ($src_w > $scaledSize || $src_h > $scaledSize)
    {
        // major image shrinkage ... hmmmm cold water maybe
        $est_w_scale = ($src_w / $scaledSize);
        $est_h_scale = ($src_h / $scaledSize);

        if ($est_h_scale && $est_w_scale)
        {
            $scale = ($est_h_scale > $est_w_scale) ? $est_h_scale : $est_w_scale;
        }

        $dest_w = ($src_w / $scale);
        $dest_h = ($src_h / $scale);
    }
    else
    {
        if ($enlarge)
        {
            // giving the image a boner
            $est_w_scale = ($scaledSize / $src_w);
            $est_h_scale = ($scaledSize / $src_h);

            if ($est_h_scale && $est_w_scale)
            {
                $scale = ($est_h_scale < $est_w_scale) ? $est_h_scale : $est_w_scale;
            }

            $dest_w = ($src_w * $scale);
            $dest_h = ($src_h * $scale);
        }
        else
        {
            // enlarging the image was turned off
            $dest_w = $src_w;
            $dest_h = $src_h;
        }
    }

    if (!imagick_scale($im, $dest_w, $dest_h, "!"))
    {
        $reason = imagick_failedreason($im);
        $description = imagick_faileddescription($im);
        print_msg('error', 'Image Scale Error!',
                  "handle failed!<BR>\nReason: $reason<BR>\nDescription: $description<BR>\n");
        return false;
    }

    if (!imagick_writeimage($im, $scaledFilePath))
    {
        $reason = imagick_failedreason($im);
        $description = imagick_faileddescription($im);
        print_msg('error', 'Image Write Error!',
                  "handle failed!<BR>\nReason: $reason<BR>\nDescription: $description<BR>\n");
        return false;
    }

    return true;
}

?>
