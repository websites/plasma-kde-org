<?

global $shopping_customMenu;
$shopping_customMenu = false;

global $phpfwkIncludePath;
include_once("$phpfwkIncludePath/address.php");
include_once("$phpfwkIncludePath/shippingClerks.php");
include_once("$phpfwkIncludePath/shoppingTaxman.php");

global $shopping_customObjectFilePath;
static $includedShoppingExt;
if (!$includedShoppingExt && file_exists("$shopping_customObjectFilePath/shoppingExt.php"))
{
    $includedShoppingExt = true;
    include_once("$shopping_customObjectFilePath/shoppingExt.php");
}

include_once("Mail.php");

/*
 * createEmptyCart : creates an empty chopping cart
 */
function createEmptyCart()
{
    global $shopping_defaultCurrency;

    $db = db_connect();

    $id = db_seqNextVal($db, 'seq_carts');

    unset($fields, $values);

    sql_addIntToInsert($fields, $values, 'cartID', $id);
    sql_addScalarToInsert($fields, $values, 'currency', $shopping_defaultCurrency);
    db_insert($db, 'shoppingCarts', $fields, $values);

    return $id;
}

/*
 * show_image : displays an image for a group
 *
 * $grp -> the group to display
 */
function show_image($grp)
{
    global $common_baseCatalogImagesURL;

    $grp = intval($grp);
    if ($grp > 0)
    {
        $db = db_connect();

        $row = db_query($db, "SELECT name, smallImg, largeImg FROM skuGroups WHERE groupID = $grp;");

        if (db_numRows($row) > 0)
        {
            list($name, $smallImg, $largeImg) = db_row($row, 0);

            print "<h3 align=center>$name</h3>";
            if ($smallImg)
            {
                unset($href);
                if ($largeImg)
                {
                    $href = "<a href=\"$common_baseCatalogImagesURL/$largeImg\">";
                }
                print "<div align=center>$href<img border=0 src=\"$common_baseCatalogImagesURL/$smallImg\"></a></div>";
                print '<br>';
            }
            else if ($largeImg)
            {
                print "<div align=center><img border=0 src=\"$common_baseCatalogImagesURL/$largeImg\"></div>";
                print '<br>';
            }
        }
    }
}

/*
 * print_currencyList : prints a currency change list form
 *
 * $form -> the form to use for this selection
 */
function print_currencyList($form)
{
    global $cartID, $shopping_defaultCurrency;
    global $shopping_currencyFullList;
    global $common_baseImagesURL;

    $cartID = intval($cartID);

    $db = db_connect();

    $row = db_query($db, "SELECT currency FROM shoppingCarts WHERE cartID = $cartID;");

    if (db_numRows($row) > 0)
    {
        list($currency) = db_row($row, 0);
    }
    else
    {
        $currency = $shopping_defaultCurrency;
    }

    if ($shopping_currencyFullList)
    {
        print '<div style="font-size: 8pt;" align=right>';
        print_form("$form", 'currencyForm', false, 'post', 'style="margin: 0px; padding: 0px;"');
    
        $currencies = db_query($db, "SELECT currency, longName FROM currencies;");

        print 'Currency&nbsp;';
        print_selectQuery('currency', $currencies, $currency, true,
                        "onChange='document.currencyForm.submit()' style=\"font-size: 8pt;\"");
        print_hidden('changeCurrency', 1);
        print'</form></div>';
    }
    else
    {
        print '<table style="font-size: 8pt; margin: 0px;" cellpadding=0 cellspacing=0 width=100%><tr><td nowrap>';
        if ($currency == 'USD')
        {
            print "<b>Shopping in US $ </b><img border=0 src=\"$common_baseImagesURL/usa.jpg\">";
        }
        else
        {
            print "<b>Shopping in CAD $ </b><img border=0 src=\"$common_baseImagesURL/canada.jpg\">";
        }

        print '</td><td nowrap align=right>';
        $glue = '?';
        if (strstr($form, '?'))
        {
            $glue = '&';
        }

        if ($currency == 'CAD')
        {
            print "<a href=\"$form{$glue}changeCurrency=1&currency=USD\">Shop in US $ </a><img border=0 src=\"$common_baseImagesURL/non_usa.jpg\">";
        }
        else
        {
            print "<a href=\"$form{$glue}changeCurrency=1&currency=CAD\">Shop in CAD $ </a><img border=0 src=\"$common_baseImagesURL/non_canada.jpg\">";
        }
        print '</td></tr></table>';
    }
}

/*
 * print_shoppingMenu : prints the catalog tree menu (left panel)
 *
 * $form -> the form to use
 */
function print_shoppingMenu($form, $pageID, $defaultCategory = true)
{
    global $showcat, $showgrp, $shopping_customMenu;

    if ($shopping_customMenu)
    {
        return;
    }

    $db = db_connect();

    $cats = db_query($db, "SELECT g.groupID, g.name
                           FROM skuGroups g
                           JOIN storeCatalogs c on g.groupID = c.skuGroup
                           JOIN cmsShopping s ON c.storeID = s.storeID
                           WHERE g.parent IS NULL AND s.pageID = '$pageID' AND g.active
                           ORDER BY g.displayOrder, lower(g.name);");
    $numCats = db_numRows($cats);

    if ($numCats < 1)
    {
        return;
    }

    if ($numCats == 1)
    {
        // inneficient hack. fix later.
        list($groupID, $name) = db_row($cats, $i);
        $subGroups = db_query($db, "SELECT groupID, name FROM skuGroups
                                       WHERE parent = $groupID and active ORDER BY displayOrder, name;");
        if (db_numRows($subGroups) < 1)
        {
            if ($defaultCategory && !$showcat)
            {
                $showcat = $groupID;
            }
            return;
        }
    }
    print "<table width=150 cellpadding=3px cellspacing=0px height=\"100%\">";

    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }

    for ($i = 0; $i < $numCats; $i++)
    {
        list($groupID, $name) = db_row($cats, $i);
        print '<tr>';
        print "<td><a class=shop href=\"$form{$glue}showcat=$groupID\">$name</a></td>";
        print '</tr>';
        if ($defaultCategory && $i == 0 && !$showcat)
        {
            $showcat = $groupID;
        }

        if ($numCats == 1 || $showcat == $groupID)
        {
            $subGroups = db_query($db, "SELECT groupID, name FROM skuGroups
                                       WHERE parent = $groupID and active ORDER BY displayOrder, name;");
            $numSubGroups = db_numRows($subGroups);

            for ($j = 0; $j < $numSubGroups; $j++)
            {
                list($subGroupID, $subName) = db_row($subGroups, $j);
                print "<tr>";
                print "<td style='padding-left: 12px'>&bull; <a class=shop href=\"$form{$glue}showcat=$groupID&showgrp=$subGroupID\">$subName</a></td>";
                print "</tr>";
            }
        }

    }

    print "<tr><td height=\"100%\">&nbsp;</td></table>";
}

function print_customShoppingMenu($callback, $form, $pageID)
{
    global $showcat, $showgrp, $shopping_customMenu;

    if (!$callback || !function_exists($callback))
    {
        return;
    }

    $shopping_customMenu = true;
    $db = db_connect();

    $cats = db_query($db, "SELECT g.groupID, g.name
                          FROM skuGroups g
                          JOIN storeCatalogs c on g.groupID = c.skuGroup
                          JOIN cmsShopping s ON c.storeID = s.storeID
                          WHERE g.parent IS NULL AND s.pageID = '$pageID' AND active
                          ORDER BY displayOrder, name;");
    $numCats = db_numRows($cats);

    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }

    for ($i = 0; $i < $numCats; $i++)
    {
        list($groupID, $name) = db_row($cats, $i);
        $url = "$form{$glue}showcat=$groupID";
        $callback($url, $name, 0, $i == 0, $i == $numCats - 1);

        if ($numCats == 1 || $showcat == $groupID)
        {
            $subGroups = db_query($db, "SELECT groupID, name FROM skuGroups
                                       WHERE parent = $groupID and active ORDER BY displayOrder, name;");
            $numSubGroups = db_numRows($subGroups);

            for ($j = 0; $j < $numSubGroups; $j++)
            {
                list($subGroupID, $subName) = db_row($subGroups, $j);
                $url = "$form{$glue}showcat=$groupID&showgrp=$subGroupID";
                $callback($url, $subName, 1, $j == 0, $j == $numSubGroups - 1);
            }
        }
    }
}

/*
 * print_groups : prints the groups listing for a group (right panel)
 *
 * $form -> the form to use
 * $grp -> which group to print the groups listing for
 */
function print_groups($form, $grp, $topLevel = false)
{
    global $showcat, $common_baseCatalogImagesURL;

    $db = db_connect();

    $grp = intval($grp);
    if ($grp > 0)
    {
        $groups = db_query($db, "SELECT groupID, name, smallImg FROM skuGroups
                                WHERE parent = $grp AND active = true
                                ORDER BY displayOrder;");

        $numGroups = db_numRows($groups);

        if ($numGroups > 0)
        {

            print '<center>';

            $glue = '?';
            if (strstr($form, '?'))
            {
                $glue = '&';
            }

            if ($topLevel)
            {
                $cols = 4;
                print "<table border=0 cellpadding=15 cellspacing=0 align=center>";
                $numGroups = db_numRows($groups);
                for ($i = 0; $i < $numGroups; ++$i)
                {
                    list($gID, $gName, $gIcon) = db_row($groups, $i);
                    if ($i % $cols == 0)
                    {
                        print '<tr>';
                    }

                    print "<td align=center valign=bottom>";
                    print "<a class=shoplink href=\"$form{$glue}showcat=$showcat&showgrp=$gID\">";
                    if ($gIcon)
                    {
                        print "<img border=0 src=\"$common_baseCatalogImagesURL/$gIcon\"><br>";
                    }

                    print "$gName</a></td>";

                    if ($i % $cols == $cols - 1)
                    {
                        print '</tr>';
                    }
                }
                print '</table>';
            }
            else if ($numGroups <= 15)
            {
                for ($i = 0; $i < $numGroups; $i++)
                {
                    list($groupID, $name) = db_row($groups, $i);
                    print "&bull;&nbsp;<a class=shoplink href=\"$form{$glue}showcat=$showcat&showgrp=$groupID\">$name</a>&nbsp;";
                }
                print '&bull;';
            }
            else
            {
                $alphabet = array ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

                foreach ($alphabet as $letter)
                {
                    print "<a class=shoplink href=\"$form{$glue}showcat=$showcat&showgrp=$grp&showalpha=$letter\">$letter</a>&nbsp;";
                }
            }

            print '</center>';
            print '<br>';
        }
    }
}

/*
 * print_items : prints the items listing  for a group (right panel)
 *
 * $form -> the form to use
 * $grp -> which group to print the items listing for
 */
function print_items($form, $grp, $virtualItems = 0)
{
    global $cartID;
    global $common_baseCatalogImagesURL;
    global $shopping_defaultCurrency;
    global $showcat, $showgrp, $showalpha;
    global $virtual;

    $db = db_connect();

    $currency = $shopping_defaultCurrency;
    $cartID = intval($cartID);

    $cartInfo = db_query($db, "SELECT currency FROM shoppingCarts WHERE cartID = $cartID;");

    if (db_numRows($cartInfo) > 0)
    {
        list($currency) = db_row($cartInfo, 0);
    }

    $grp = intval($grp);
    if (!$grp && !$virtualItems)
    {
        return;
    }

    if (!$virtualItems)
    {
        $parents = db_query($db, "SELECT name, parent FROM skuGroups WHERE groupID = '$grp';");

        if (db_numRows($parents) < 1)
        {
            print_msg('ERROR', 'Group Not Found', "The requested group ($grp) could not be found!", "shp/101");
            return;
        }

        list($groupName, $isTopLevel) = db_row($parents, 0);

        $items = db_query($db, "SELECT sku, id, name, shortDesc, smallImg, item_price(sku, 1, '$currency')
                                FROM skus
                                WHERE groupID = $grp AND active = true
                                ORDER BY displayOrder;");
    }
    else
    {
        $items =& $virtualItems;
    }

    $numItems = db_numRows($items);

    if ($numItems < 1)
    {
        return;
    }

    print '<table class=items width=100% cellpadding=3px cellspacing=0px border=0>';

    if ($virtual)
    {
        print "<tr><th class=itemsColoredHeader colspan=100%>Items</th></tr>";
    }
    else
    {
        print "<tr><th class=itemsColoredHeader colspan=100%>Items For Sale in &quot;$groupName&quot;</th></tr>";
    }

    print "<tr><th colspan=3>Item</th><th>Price</th><th>Quantity/Options</th></tr>";
    print "<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>";

    $odd = true;
    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }

    for ($i = 0; $i < $numItems; $i++)
    {
        $class = $odd ? 'class="oddShopRow"' : 'class="evenShopRow"';
        $class2 = $odd ? 'class="oddShopRow2"' : 'class="evenShopRow2"';
        $odd = !$odd;

        list($sku, $id, $name, $shortDesc, $smallImg, $price) = db_row($items, $i);

        $addVirt = '';
        if ($virtual)
        {
            $addVirt = "virtual=$virtual&" . $virtual(true) . '&';
        }

        $itemHREF = "$form{$glue}{$addVirt}showcat=$showcat&showgrp=$showgrp&showitem=$sku";
        if ($showalpha)
        {
            $itemHREF .= "&showalpha=$showalpha";
        }
        print '<tr>';
        print "<td $class valign=top colspan=3><a href=\"$itemHREF\">$id <b>$name</b></a><br>$shortDesc</td>";

        if ($price)
        {
            print "<td $class valign=top rowspan=2><b>$price</b></td>";

            print "<td $class valign=top rowspan=2>";

            print_form("$form{$glue}{$addVirt}showcat=$showcat&showgrp=$showgrp&showalpha=$showalpha");
            print_hidden('sku', $sku);

            print '<table border=0 cellpadding=3 cellspacing=0 style="padding: 0px; margin: 0px;">';
            print "<tr><td>Qty</td><td>";
            print_textbox("quantity", '', 5, 4);
            print '</td></tr>';

            $options = db_query($db, "select t.optionTypeID, t.name, o.optionID, o.name from skuOptions o join skuOptionTypes t on (o.type = t.optionTypeID) where sku = $sku order by t.name, o.optionID;");
            $numOptions = db_numRows($options);
            $currentOptType = '';
            for ($j = 0; $j < $numOptions; $j++)
            {
                list($optionTypeID, $optType, $optID, $optName) = db_row($options, $j);
                if ($currentOptType != $optType)
                {
                    if ($j > 0)
                    {
                        print "</select></td></tr>";
                    }
                    print "<tr><td>$optType</td><td><select name=\"options[$optionTypeID]\">";
                    $currentOptType = $optType;
                }
                print "<option value=\"$optID\">$optName</option>";
            }

            if ($numOptions > 0)
            {
                print "</select></td></tr>";
            }

            print '</table>';
            print '&nbsp;';
            print_submit("addToCart", "Add To Cart");

            print '</form>';
            print '</td>';
        }
        else
        {
            print "<td $class valign=top rowspan=2>N/A</td>";
            print "<td $class valign=top rowspan=2>&nbsp;</td>";
        }
        print '</tr>';
        print '<tr>';
        print "<td $class2>&nbsp;</td>";

        if ($smallImg)
        {
            print "<td $class2 valign=top colspan=2><a href=\"$itemHREF\"><img border=0 src=\"$common_baseCatalogImagesURL/$smallImg\"></a></td>";
        }
        else
        {
            print "<td $class2 valign=top colspan=2>&nbsp;</td>";
        }
        print '</tr>';

    }
    print '</table>';
}

/*
 * print_item : prints a specific item (right panel)
 *
 * $form -> the form to use
 * $item -> which item to print
 */
function print_item($form, $item)
{
    global $cartID;
    global $common_baseCatalogImagesURL;
    global $shopping_defaultCurrency;
    global $showcat, $showgrp, $showalpha;

    $db = db_connect();

    $showgrp = intval($showgrp);
    $currency = $shopping_defaultCurrency;

    $cartInfo = db_query($db, "SELECT currency FROM shoppingCarts WHERE cartID = $cartID;");

    if (db_numRows($cartInfo) > 0)
    {
        list($currency) = db_row($cartInfo, 0);
    }

    if (!$item)
    {
        return;
    }

    $itemInfo = db_query($db, "SELECT name, shortDesc, longDesc, largeImg, item_price(sku, 1, '$currency'), groupID
                    FROM skus WHERE sku = $item;");

    if (db_numRows($itemInfo) < 1)
    {
        return;
    }

    list($name, $shortDesc, $longDesc, $largeImg, $price, $groupID) = db_row($itemInfo, 0);
    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }

    print '<table border=0 class=items width=100% cellpadding=3px cellspacing=0px>';
    print "<tr><th class=itemsColoredHeader colspan=100%>$name Details</th></tr>";

    print '<tr>';
    print "<td $class valign=top>$shortDesc</td>";
    print '</tr>';

    print '<tr>';
    print "<td $class valign=top>$longDesc</td>";
    print '</tr>';

    print '<tr>';
    if ($largeImg)
    {
            print "<td $class valign=top align=right><img src=\"$common_baseCatalogImagesURL/$largeImg\"></td>";
    }
    else
    {
            print "<td $class valign=top>&nbsp;</td>";
    }
    print '</tr>';

    print '<tr>';
    print '<td valign=bottom>';
    print_form("$form{$glue}showcat=$showcat&showgrp=$showgrp&showitem=$item");
    print_hidden('sku', $item);
    print '<table border=0>';
    print '<tr>';
    print "<td>@</td><td $class valign=top><b>$price</b></td>";
    print "<tr><td>Qty</td><td>";
    print_textbox("quantity", '', 5, 4);
    print '</td></tr>';

    $options = db_query($db, "select t.optionTypeID, t.name, o.optionID, o.name from skuOptions o join skuOptionTypes t on (o.type = t.optionTypeID) where sku = $item order by t.name, o.optionID;");
    $numOptions = db_numRows($options);
    $currentOptType = '';
    for ($j = 0; $j < $numOptions; $j++)
    {
            list($optionTypeID, $optType, $optID, $optName) = db_row($options, $j);
            if ($currentOptType != $optType)
            {
                    if ($j > 0)
                    {
                            print "</select></td></tr>";
                    }
                    print "<tr><td>$optType</td><td><select name=\"options[$optionTypeID]\">";
                    $currentOptType = $optType;
            }
            print "<option value=\"$optID\">$optName</option>";
    }

    if ($numOptions > 0)
    {
            print "</select></td></tr>";
    }



    print '<tr><td></td><td>';
    print_submit("addToCart", "Add To Cart");
    print '</td></tr></table>';

    print '</form>';
    print '</td>';
    print '</tr>';
    print '<table></td>';
    print '</tr>';

    print "<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>";

    if ($showgrp)
    {
        $grpInfo = db_query($db, "SELECT name FROM skuGroups WHERE groupID = $showgrp;");

        if (db_numRows($grpInfo) > 0)
        {
                list($groupName) = db_row($grpInfo, 0);
        }

        $backHREF = "$form{$glue}showcat=$showcat&showgrp=$showgrp";
        if ($showalpha)
        {
                $backHREF .= "&showalpha=$showalpha";
                $groupName .= " - $showalpha";
        }
        print "<tr><td align=left><a href=\"$backHREF\"><<< Back to $groupName</a></td></tr>";
    }

    print '</table>';
}

/*
 * print_shopping : prints the shopping routine
 *
 * $form -> the form to use
 *
 * This function handles the shopping routines.
 * It will determine what to display (groups, items, a specific item)
 */
function print_shopping($form)
{
    global $cartID, $common_baseImagesURL;
    global $showcat, $showgrp, $showitem, $showalpha;
    global $virtual;
    global $shopping_secureCart;

    $db = db_connect();
    $numItems = 0;

    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }

    if ($shopping_secureCart)
    {
        $secureForm = str_replace('http:', 'https:', $form);
    }
    else
    {
        $secureForm = $form;
    }

    if (!$virtual && $showgrp)
    {
        $parentGID = $showgrp;

        if ($showalpha)
        {
            $breadcrumb = array("<a href=\"$form{$glue}showcat=$showcat&showgrp=$showgrp&showalpha=$showalpha\">$showalpha</a>");
        }
        else
        {
            $breadcrumb = array();
        }

        while ($parentGID)
        {
            $row = db_query($db, "SELECT parent, name FROM skuGroups WHERE groupID = $parentGID;");
            if (db_numRows($row) > 0)
            {
                list($temp, $parentCatalogName) = db_row($row, 0);

                if (!$temp)
                {
                    $crumb = "<a href=\"$form{$glue}showcat=$showcat\">$parentCatalogName</a>";
                }
                else
                {
                    $crumb = "<a href=\"$form{$glue}showcat=$showcat&showgrp=$parentGID\">$parentCatalogName</a>";
                }

                array_unshift($breadcrumb, $crumb);
                $parentGID = $temp;
            }
            else
            {
                unset($parentGID);
            }
        }

        array_unshift($breadcrumb, "<a href=\"$form\">Shopping</a>");
        $breadcrumb = '<div class="largeAdminHeader">Shop: ' . join($breadcrumb, ' / ') . '<br></div>';
    }

    // print out the cart strip
    if ($cartID)
    {
        $items = db_query($db, "SELECT sum(quantity) FROM shoppingCartItems WHERE cartID = $cartID;");

        if (db_numRows($items) > 0)
        {
            list($numItems) = db_row($items, 0);
        }
    }

    if ($numItems > 0)
    {
        print '<table class=items width=100% cellpadding=3px cellspacing=0px>';
        print "<tr><td align=left><b>$numItems " . ($numItems > 1 ? 'items' : 'item') . " in your shopping cart</b></td>";
        print "<td width=100 align=right><a href=\"$secureForm{$glue}showcart=1&checkout=1&showcat=$showcat&showgrp=$showgrp\"><img src=\"$common_baseImagesURL/checkout.gif\" border=0 valign=middle>Checkout</a></td>";
        print "<td width=100 align=right valign=middle><a href=\"$form{$glue}showcart=1&showcat=$showcat&showgrp=$showgrp\"><img src=\"$common_baseImagesURL/cart.gif\" border=0 valign=middle>&nbsp;View Cart</a></td></tr>";
        print '</table>';
    }

    if (!$showitem && !$showalpha && !$showgrp && !$showcat && !$virtual)
    {
        return false;
    }

    // print out the currency selector and shopping breadcrumb
    print '<table width=100% cellpadding=3px cellspacing=0px style="border-bottom: 1px solid;">';
    print '<tr><td align=right>';

    $addVirt = '';
    if ($virtual)
    {
        $addVirt = "virtual=$virtual&" . $virtual(true) . '&';
    }

    $curForm = "$form{$glue}{$addVirt}showcat=$showcat&showgrp=$showgrp&showalpha=$showalpha&showitem=$showitem";
    print_currencyList($curForm);
    print '</td></tr>';
    print '</table>';

    if (!$virtual)
    {
        print '<table width=100% cellpadding=3px cellspacing=0px style="border-bottom: 1px solid;">';
        print '<tr><td align=left>';
        print $breadcrumb;
        print '</td></tr>';
        print '</table>';
    }

    if ($showitem)
    {
        print_item($form, $showitem);
    }
    else if ($virtual)
    {
        $searchItems = $virtual();
        print_items($form, 0, $searchItems);
    }
    else if ($showalpha)
    {
        show_image($showgrp);
        print_groups($form, $showgrp);

        print '<hr>';

        $alphas = db_query($db, "SELECT groupID, name FROM skuGroups
                                WHERE parent = $showgrp AND name ~* '^$showalpha'
                                ORDER BY name;");

        $numRows = db_numRows($alphas);

        for ($i = 0;$i <$numRows; $i++)
        {
            list($alphagrp, $name) = db_row($alphas, $i);
            show_image($alphagrp);
            print_items($form, $alphagrp);
        }
    }
    else if ($showgrp)
    {
        show_image($showgrp);
        print_groups($form, $showgrp);
        print_items($form, $showgrp);
    }
    else if ($showcat)
    {
        show_image($showcat);
        print_groups($form, $showcat, true);
        print_items($form, $showcat);
    }

    return true;
}

/*
 * addItemToCart : adds the selected item into the shopping cart
 *
 * The shopping cart used is based on the cookie. The cart will be created if one
 * does not exists when you try to add the first item.
 */
function addItemToCart($quiet = false)
{
    global $cartID;
    global $common_sitePath;
    global $sku, $quantity;
    global $options;

    $db = db_connect();

    $cartID = intval($cartID);
    if (!$cartID)
    {
        $cartID = createEmptyCart();
    }

    $sku = intval($sku);
    $quantity = intval($quantity);
    if ($sku > 0 && $quantity > 0)
    {
        $skuQuery = db_query($db, "SELECT name FROM skus WHERE sku = $sku;");

        if (db_numRows($skuQuery) < 1)
        {
            return false;
        }

        list($name) = db_row($skuQuery, 0);

        // check to see if we should consolidate items in the cart
        unset($optionsIn);
        if (is_array($options))
        {
            $optionCount = count($options);
            $optionsIn = join(', ', $options);
            $matchTest = "select o.itemID, count(o.optionID) = $optionCount and sum(case when o.optionID in ($optionsIn) then 1 else 0 end) = count(o.optionID) as matches from shoppingCartItemOptions o join shoppingCartItems s on (o.itemID = s.itemID) where s.cartID = $cartID group by o.itemID;";
        }
        else
        {
            $optionCount = 0;
            $matchTest = "select s.itemID, true from shoppingCartItems s where s.cartid = $cartID and s.sku = $sku;";
        }

        $consQuery = db_query($db, $matchTest);
        $numMatches = db_numRows($consQuery);

        for ($i = 0; $i < $numMatches; ++$i)
        {
            list($consItemID, $consMatches) = db_row($consQuery, $i);
            if (db_boolean($consMatches))
            {
                unset($fields);

                // woot! RACE CONDITION. but do we care? ;-)
                db_query($db, "update shoppingCartItems set quantity = quantity + $quantity where itemID = $consItemID;");

                if (!$quiet)
                {
                    print '<div align=center>';
                    print_msg('INFO', 'Item Added To Shopping Cart',
                            "$quantity <b>$name</b> " . (($quantity > 1) ? 'were' : 'was') . " added to your shopping cart");
                    print '</div>';
                }
                return true;
            }
        }

        unset($fields, $values);
        sql_addIntToInsert($fields, $values, 'cartID', $cartID);
        sql_addIntToInsert($fields, $values, 'sku', $sku);
        sql_addIntToInsert($fields, $values, 'quantity', $quantity);

        if (!db_insert($db, 'shoppingCartItems', $fields, $values))
        {
            return false;
        }

        $itemID = db_seqCurrentVal($db, 'seq_shopItems');

        if (is_array($options))
        {
            foreach ($options as $optType => $optID)
            {
                $optType = intval($optType);
                $optID = intval($optID);

                // should we check if the optType, optID and sku match up?? do we care??
                if ($optType < 1 || $optID < 1)
                {
                    continue;
                }

                unset($fields, $values);
                sql_addIntToInsert($fields, $values, 'itemID', $itemID); // here is the problem!
                sql_addIntToInsert($fields, $values, 'optionID', $optID); // here is the problem!
                db_insert($db, 'shoppingCartItemOptions', $fields, $values);
            }
        }

        if (!$quiet)
        {
            print '<div align=center>';
            print_msg('INFO', 'Item Added To Shopping Cart',
                      "$quantity <b>$name</b> " . (($quantity > 1) ? 'were' : 'was') . " added to your shopping cart");
            print '</div>';
        }
        return true;
    }

    return false;
}

/*
 * print_cartInfo : prints the entries shopping cart information
 */
function print_cartInfo($form)
{
    global $cartID;
    global $checkout;
    global $common_bizName;

    $db = db_connect();

    unset($grandtotal);

    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }
    print_form("$form{$glue}showcart=1");

    $cartID = intval($cartID);
    print '<br>';
    print "<table border=0 class=items width=100% cellpadding=3px cellspacing=0px>";
    print "<tr><th class=itemsColoredHeader colspan=100%>$common_bizName Shopping Cart #$cartID &nbsp;";
    print "</th></tr>";

    $items = db_query($db, "SELECT c.itemID, c.sku, s.ID, s.name, s.shortDesc,
                                  c.quantity, c.price, c.convertPrice,
                                  round((c.price * c.quantity), 2) as cadtotal,
                                  round((c.convertPrice * c.quantity), 2) as total
                           FROM shoppingCartItems c
                           LEFT JOIN skus s ON s.sku = c.sku
                           WHERE c.cartID = $cartID
                           ORDER BY s.name;");

    $numItems = db_numRows($items);

    if ($numItems)
    {
        print '<tr>';
        print '<th></th><th>Item</th><th class="shopRight">Quantity</th><th class="shopRight">Price</th><th class="shopRight">Total</th></tr>';
        print '<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>';
    }
    else
    {
        print "<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>";
        print "<tr><td class=oddRow valign=top align=center width=100%><b>No Items In Shopping Cart</b></td></tr>";
        print '</table>';
        return;
    }

    $odd = true;
    for ($i = 0; $i < $numItems; $i++)
    {
        $class = $odd ? 'class="oddShopRow"' : 'class="evenShopRow"';
        $odd = !$odd;

        list($itemID, $sku, $skuID, $name, $shortDesc, $quantity,
             $price, $convertPrice, $cadtotal, $total) = db_row($items, $i);

        $optionsQuery = db_query($db, "SELECT ot.name, s.name 
                                       FROM shoppingCartItemOptions c
                                       JOIN skuOptions s ON c.optionID = s.optionID
                                       JOIN skuOptionTypes ot ON s.type = ot.optionTypeID
                                       WHERE c.itemID = $itemID ORDER BY lower(ot.name);");
        $numOptions = db_numRows($optionsQuery);
        $options = array();
        for ($j = 0; $j < $numOptions; ++$j)
        {
            list($optionName, $optionValue) = db_row($optionsQuery, $j);
            $options[$j] = "$optionName: $optionValue";
        }

        $options = join(', ', $options);
        if ($options)
        {
            $options = "<br>Details : $options";
        }

        print "<tr>";

        if (! $checkout)
        {
            print "<td $class valign=top>";
            print_checkbox("del_items[$itemID]", "$skuID", $itemID);
            print '</td>';
        }
        else
        {
	    print "<td $class valign=top>$skuID</td>";
        }

        print "<td $class valign=top><b>$name</b>$options</td>";
        print "<td $class valign=top nowrap align=right>$quantity</td>";
        print "<td $class valign=top nowrap align=right>@&nbsp;$convertPrice</td>";
        print "<td $class valign=top nowrap align=right>&nbsp;$total</td>";
        print "</tr>";

        $grandcadtotal += $total;
        $grandtotal += $total;
    }

    $grandtotal += calculateShipping($cartID);
    printShipping($cartID);

    $grandtotal += calculateTaxes($cartID);
    printTaxes($cartID);

    if ($grandtotal)
    {
        $currencyInfo = db_query($db, "SELECT currency FROM shoppingCarts WHERE cartID = $cartID;");
        if (db_numRows($currencyInfo))
        {
            list($thisCurrency) = db_row($currencyInfo, 0);
        }

        print "<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>";
        print "<tr><td class=evenRow colspan=100% align=right><b>Total Cost ($thisCurrency) : $" . number_format($grandtotal, 2, '.', ',') . "</b></td></tr>";
    }

    print '</table>';

    return $grandcadtotal;
}

/*
 * cartInfoHtml : prints the entries shopping cart information in HTML (for emails)
 */
function cartInfoHtml($cartID)
{
    global $common_bizName;

    $cartID = intval($cartID);
    unset($grandtotal);
    $html = '';

    $db = db_connect();

    $html .= '<br>';
    $html .= "<table border=0 class=items width=100% cellpadding=3px cellspacing=0px>";
    $html .= "<tr><th class=itemsColoredHeader colspan=100%>$common_bizName Shopping Cart #$cartID &nbsp;";
    $html .= "</th></tr>";

    $items = db_query($db, "SELECT c.itemID, c.sku, s.ID, s.name, s.shortDesc,
                                  c.quantity, c.price, c.convertPrice,
                                  round((c.price * c.quantity), 2) as cadtotal,
                                  round((c.convertPrice * c.quantity), 2) as total
                           FROM shoppingCartItems c
                           LEFT JOIN skus s ON s.sku = c.sku
                           WHERE c.cartID = $cartID
                           ORDER BY s.name;");

    $numItems = db_numRows($items);

    if ($numItems)
    {
        $html .= '<tr>';
        $html .= '<th></th><th>Item</th><th>Quantity</th><th>Price</th><th>Total</th></tr>';
        $html .= '<tr><td colspan=100%>&nbsp;</td></tr>';
    }
    else
    {
        $html .= "<tr><td colspan=100%>&nbsp;</td></tr>";
        $html .= "<tr><td valign=top align=center width=100%><b>No Items In Shopping Cart</b></td></tr>";
        $html .= '</table>';
    }

    for ($i = 0; $i < $numItems; $i++)
    {
        list($itemID, $sku, $skuID, $name, $shortDesc, $quantity,
             $price, $convertPrice, $cadtotal, $total) = db_row($items, $i);

        $optionsQuery = db_query($db, "SELECT ot.name, s.name 
                                       FROM shoppingCartItemOptions c
                                       JOIN skuOptions s ON c.optionID = s.optionID
                                       JOIN skuOptionTypes ot ON s.type = ot.optionTypeID
                                       WHERE c.itemID = $itemID ORDER BY lower(ot.name);");
        $numOptions = db_numRows($optionsQuery);
        $options = array();
        for ($j = 0; $j < $numOptions; ++$j)
        {
            list($optionName, $optionValue) = db_row($optionsQuery, $j);
            $options[$j] = "$optionName: $optionValue";
        }

        $options = join(', ', $options);
        if ($options)
        {
            $options = "<br>Details : $options";
        }

        $html .= "<tr>";
	$html .= "<td valign=top>$skuID</td>";
        $html .= "<td valign=top><b>$name</b>$options</td>";
        $html .= "<td valign=top nowrap align=right>$quantity</td>";
        $html .= "<td valign=top nowrap align=right>@&nbsp;$convertPrice</td>";
        $html .= "<td valign=top nowrap align=right>&nbsp;$total</td>";
        $html .= "</tr>";

        $grandcadtotal += $total;
        $grandtotal += $total;
    }

    $grandtotal += calculateShipping($cartID);
    $html .= shippingHtml($cartID);

    $grandtotal += calculateTaxes($cartID);
    $html .= taxesHtml($cartID);

    if ($grandtotal)
    {
        $currencyInfo = db_query($db, "SELECT currency FROM shoppingCarts WHERE cartID = $cartID;");
        if (db_numRows($currencyInfo))
        {
            list($thisCurrency) = db_row($currencyInfo, 0);
        }

        $html .= "<tr><td colspan=100%>&nbsp;</td></tr>";
        $html .= "<tr><td colspan=100% align=right><b>Total Cost ($thisCurrency) : $" . number_format($grandtotal, 2, '.', ',') . "</b></td></tr>";
    }

    $html .= '</table>';

    return $html;
}

/*
 * show_cart : shows the shopping cart details
 *
 * $form -> the form to use
 */
function show_cart($form)
{
    global $cartID;
    global $showcart, $currency;
    global $shopping_secureCart;

    $db = db_connect();

    print '<br>';

    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }

    if ($shopping_secureCart)
    {
        $secureForm = str_replace('http:', 'https:', $form);
    }
    else
    {
        $secureForm = $form;
    }

    print_currencyList("$form{$glue}showcart=$showcart");

    print_form("$secureForm{$glue}showcart=1");

    print_cartInfo($form);

    print '<table>';
    print '<tr>';
    print '<td>';
    print_submit('deleteItems', 'Delete Checked Items');
    print '</td>';
    print '<td>';
    print_submit('checkout', 'Checkout');
    print '</td>';
    print '<td>';
    print_submit('empty', 'Empty Cart');
    print '</td>';
    print '</tr>';
    print '</table>';

    print '</form>';
}

/*
 * checkout : checkout the shopping cart (shows details and form)
 *
 * $form -> the form to use
 */
function checkout($form)
{
    global $cartID;
    global $orderedName, $orderedEmail, $orderedPhone;
    global $shipName, $shipAddress1, $shipAddress2, $shipCity, $shipPostal;
    global $shipProvince, $other_shipProvince, $shipCountry, $other_shipCountry;
    global $paymentBy, $ccName, $ccNo, $ccExpMonth, $ccExpYear;
    global $shopping_secureCart;

    if ($shopping_secureCart)
    {
        $form = str_replace('http:', 'https:', $form);
    }

    $grandtotal = print_cartInfo($form);

    if ($grandtotal <= 0)
    {
        return;
    }

    if (is_null($ccExpMonth))
    {
        $ccExpMonth = date('m');
    }

    $glue = '?';
    if (strstr($form, '?'))
    {
        $glue = '&';
    }
    print_form("$form{$glue}showcart=1&checkout=1");

    print_hidden('grandtotal', $grandtotal);
    print_hidden('showcart', 1);
    print_hidden('checkout', 1);

    print '<br>';
    print '<center>';
    print "<table class=items width=100% cellpadding=3px cellspacing=0px>";
    print "<tr><th class=itemsColoredHeader colspan=100%>Purchase / Shipping Information</th></tr>";

    print '<tr>';
    print '<td class=oddRow>Name</td>';
    print '<td class=oddRow>';
    print_textbox('orderedName', $orderedName);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=evenRow>Email</td>';
    print '<td class=evenRow>';
    print_textbox('orderedEmail', $orderedEmail);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow>Phone</td>';
    print '<td class=oddRow>';
    print_textbox('orderedPhone', $orderedPhone);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=evenRow>Ship to (name)</td>';
    print '<td class=evenRow>';
    print_textbox('shipName', $shipName);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow>Address</td>';
    print '<td class=oddRow>';
    print_textbox('shipAddress1', $shipAddress1, 40);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow></td>';
    print '<td class=oddRow>';
    print_textbox('shipAddress2', $shipAddress2, 40);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=evenRow>City</td>';
    print '<td class=evenRow>';
    print_textbox('shipCity', $shipCity);
    print '</td class=oddRow>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow>Province / State</td>';
    print '<td class=oddRow>';
    print_provSelect('shipProvince', $shipProvince);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow></td>';
    print '<td class=oddRow>Other ';
    print_textbox('other_shipProvince', $other_shipProvince);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td valign=top class=evenRow>Country</td>';
    print '<td class=evenRow>';
    print_countrySelect('shipCountry', $shipCountry);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=evenRow></td>';
    print '<td class=evenRow>Other ';
    print_textbox('other_shipCountry', $other_shipCountry);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow>Postal code </td>';
    print '<td class=oddRow>';
    print_textbox('shipPostal', $shipPostal);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=evenRow>Payment type</td>';
    print '<td class=evenRow>';
    $paymentOpts = array('Visa' => 'visa',
                         'Mastercard' => 'mastercard',
                         'American Express' => 'amex');
    print_selectArray('paymentBy', $paymentOpts, 3, $paymentBy);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow>Name on card</td>';
    print '<td class=oddRow>';
    print_textbox('ccName', $ccName);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=evenRow>Card Number</td>';
    print '<td class=evenRow>';
    print_textbox('ccNo', $ccNo);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td class=oddRow>Expiry date</td>';
    print '<td class=oddRow>';
    print_rangeSelect('ccExpMonth', $ccExpMonth, 1, 12, 1);
    print_rangeSelect('ccExpYear', $ccExpYear, date('Y'), (date('Y') + 10), 1);
    print '</td>';
    print '</tr>';

    print'<tr><td colspan=100%>&nbsp;</td></tr>';

    print '<tr>';
    print '<td colspan=100%>';
    print_submit('purchase', 'Purchase');
    print '</td>';
    print '</tr>';

    print '</table>';

    print '</form>';
}

function requiredField($field, $desc)
{
    if (! $field)
    {
        print_msg('ERROR', 'Form Input Error',
                  "The " . $desc . " field is required!");
        return false;
    }
    return true;
}

function validatePurchase()
{
    global $orderedName, $orderedEmail, $orderedPhone;
    global $shipName, $shipAddress1, $shipAddress2, $shipCity, $shipPostal;
    global $shipProvince, $other_shipProvince, $shipCountry, $other_shipCountry;
    global $paymentBy, $ccName, $ccNo, $ccExpMonth, $ccExpYear;

    if (! requiredField($orderedName, 'Order Name (your name)') ||
        ! requiredField($orderedEmail, 'Order Email') ||
        ! requiredField($orderedPhone, 'Order Conatact Phone Number') ||
        ! requiredField($shipName, 'Shipping Name') ||
        ! requiredField($shipAddress1, 'Shipping Address') ||
        ! requiredField($shipCity, 'Shipping City') ||
        ! requiredField($shipPostal, 'Shipping Postal Code') ||
        ! requiredField($ccName, 'Name on Credit Card') ||
        ! requiredField($ccNo, 'Credit Card Number')
       )
    {
        return false;
    }

    if (! validatePhone($orderedPhone, '-'))
    {
        print_msg('ERROR', 'Order Information Error',
                  'The phone number you entered is invalid.');
        return false;
    }

    if (!$shipProvince && !$other_shipProvince)
    {
        print_msg('ERROR', 'Shipping Information Error',
                  'You have not selected a provided province/state, or entered one not in the list.');
        return false;
    }

    return true;
}

function calculateShipping($cartID, $storeID = -1)
{
    global $shopping_shippingClerk;

    $db = db_connect();
    $clerkQuery = db_query($db, "SELECT class FROM clerks 
                                 WHERE name = '" . addslashes($shopping_shippingClerk) . "';");

    unset($clerk);
    if (db_numRows($clerkQuery) > 0)
    {
        list($clerkClass) = db_row($clerkQuery, 0);
        if ($clerkClass)
        {
            $clerkClass = "ShippingClerk_$clerkClass";
            $clerk = new $clerkClass($cartID);
        }
    }

    if (!isset($clerk))
    {
        $clerk = new ShippingClerk($cartID);
    }

    return $clerk->shipping();
}

function printShipping($cartID, $storeID = -1)
{
    global $shopping_shippingClerk;

    $db = db_connect();
    $clerkQuery = db_query($db, "SELECT class FROM clerks 
                                 WHERE name = '" . addslashes($shopping_shippingClerk) . "';");

    unset($clerk);
    if (db_numRows($clerkQuery) > 0)
    {
        list($clerkClass) = db_row($clerkQuery, 0);
        if ($clerkClass)
        {
            $clerkClass = "ShippingClerk_$clerkClass";
            $clerk = new $clerkClass($cartID);
        }
    }

    if (!isset($clerk))
    {
        $clerk = new ShippingClerk($cartID);
    }

    $clerk->printShipping();
}

function shippingHtml($cartID, $storeID = -1)
{
    global $shopping_shippingClerk;

    $db = db_connect();
    $clerkQuery = db_query($db, "SELECT class FROM clerks 
                                 WHERE name = '" . addslashes($shopping_shippingClerk) . "';");

    unset($clerk);
    if (db_numRows($clerkQuery) > 0)
    {
        list($clerkClass) = db_row($clerkQuery, 0);
        if ($clerkClass)
        {
            $clerkClass = "ShippingClerk_$clerkClass";
            $clerk = new $clerkClass($cartID);
        }
    }

    if (!isset($clerk))
    {
        $clerk = new ShippingClerk($cartID);
    }

    return $clerk->shippingHtml();
}

function calculateTaxes($cartID, $storeID = -1)
{
    global $shopping_taxman;

    $db = db_connect();
    $taxmanQuery = db_query($db, "SELECT class FROM taxman
                                  WHERE name = '" . addslashes($shopping_taxman) . "';");

    unset($taxman);
    if (db_numRows($taxmanQuery) > 0)
    {
        list($taxmanClass) = db_row($taxmanQuery, 0);
        if ($taxmanClass)
        {
            $taxmanClass = "ShoppingTaxman_$taxmanClass";
            $taxman = new $taxmanClass($cartID);
        }
    }

    if (!isset($taxman))
    {
        $taxman = new ShoppingTaxman($cartID);
    }

    return $taxman->taxes();
}

function printTaxes($cartID, $storeID = -1)
{
    global $shopping_taxman;

    $db = db_connect();
    $taxmanQuery = db_query($db, "SELECT class FROM taxman
                                  WHERE name = '" . addslashes($shopping_taxman) . "';");

    unset($taxman);
    if (db_numRows($taxmanQuery) > 0)
    {
        list($taxmanClass) = db_row($taxmanQuery, 0);
        if ($taxmanClass)
        {
            $taxmanClass = "ShoppingTaxman_$taxmanClass";
            $taxman = new $taxmanClass($cartID);
        }
    }

    if (!isset($taxman))
    {
        $taxman = new ShoppingTaxman($cartID);
    }

    $taxman->printTaxes();
}

function taxesHtml($cartID, $storeID = -1)
{
    global $shopping_taxman;

    $db = db_connect();
    $taxmanQuery = db_query($db, "SELECT class FROM taxman
                                  WHERE name = '" . addslashes($shopping_taxman) . "';");

    unset($taxman);
    if (db_numRows($taxmanQuery) > 0)
    {
        list($taxmanClass) = db_row($taxmanQuery, 0);
        if ($taxmanClass)
        {
            $taxmanClass = "ShoppingTaxman_$taxmanClass";
            $taxman = new $taxmanClass($cartID);
        }
    }

    if (!isset($taxman))
    {
        $taxman = new ShoppingTaxman($cartID);
    }

    return $taxman->taxesHtml();
}

function updateCart()
{
    global $cartID;
    global $orderedName, $orderedEmail, $orderedPhone;
    global $shipName, $shipAddress1, $shipAddress2, $shipCity, $shipPostal;
    global $shipProvince, $other_shipProvince, $shipCountry, $other_shipCountry;
    global $paymentBy, $ccName, $ccNo, $ccExpMonth, $ccExpYear;

    $db = db_connect();
    unset ($fields, $where);

    $province = $other_shipProvince ? $other_shipProvince : $shipProvince;
    $country = $other_shipCountry ? $other_shipCountry : $shipCountry;

    sql_addIntToUpdate($fields, 'status', 1);
    sql_addScalarToUpdate($fields, 'orderedName', $orderedName);
    sql_addScalarToUpdate($fields, 'orderedEmail', $orderedEmail);
    sql_addScalarToUpdate($fields, 'orderedPhone', $orderedPhone);
    sql_addScalarToUpdate($fields, 'shipName', $shipName);
    sql_addScalarToUpdate($fields, 'shipStreet1', $shipAddress1);
    sql_addScalarToUpdate($fields, 'shipStreet2', $shipAddress2);
    sql_addScalarToUpdate($fields, 'shipCity', $shipCity);
    sql_addScalarToUpdate($fields, 'shipProvince', $province);
    sql_addScalarToUpdate($fields, 'shipCountry', $shipCountry);
    sql_addScalarToUpdate($fields, 'shipPostal', $shipPostal);
    sql_addScalarToUpdate($fields, 'paymentBy', $paymentBy);
    sql_addScalarToUpdate($fields, 'ccName', $ccName);
    sql_addScalarToUpdate($fields, 'ccNo', $ccNo);
    sql_addScalarToUpdate($fields, 'ccExpiry', "$ccExpMonth-$ccExpYear");

    /*
    if ($country == 'CA')
    {
        sql_addScalarToUpdate($fields, 'currency', 'CAD');
        sql_addFloatToUpdate($fields, 'taxRate1', '0.07');
    }
    else
    {
        sql_addFloatToUpdate($fields, 'taxRate1', '0');
    }

    if ($country == 'US')
    {
        sql_addScalarToUpdate($fields, 'currency', 'USD');
    }
    */

    db_update($db, 'shoppingCarts', $fields, "cartID = $cartID");


    // unfortunate that we must do two updates. oh well.
    $shipping = calculateShipping($cartID);
    unset($fields);

    if ($shipping)
    {
        sql_addFloatToUpdate($fields, 'shipping', $shipping);
    }
    else
    {
        sql_addRawToUpdate($fields, 'shipping', 'null');
    }
    db_update($db, 'shoppingCarts', $fields, "cartID = $cartID");
}

function orderEmail($email)
{
    global $cartID, $phpfwk_sendmailPath, $phpfwk_sendmailArgs;

    $GREETING_TYPE = 1;

    $db = db_connect();

    $greetingInfo = db_query($db, "SELECT subject, fromAddress, replyAddress,
                                          internalCC, greeting, closing
                                   FROM emailGreetings
                                   WHERE greetingTypeID = '$GREETING_TYPE' AND
                                         active = true;");

    if (db_numRows($greetingInfo) > 0)
    {
        list($subject, $fromAddress, $replyAddress,
             $internalCC, $greeting, $closing) = db_row($greetingInfo, 0);
    }
    else
    {
        return;
    }

    $recipients = "$email";

    $headers['From']            =   $fromAddress;
    $headers['To']              =   $recipients;
    $headers['Reply-To']        =   $replyAddress;
    $headers['Subject']         =   $subject;
    $headers['Content-type']    =   "text/html; charset=iso-8859-1";

    $emailMessage =
    "
    <html>
    <body>
    <p>$greeting</p>";

    $emailMessage .= cartInfoHtml($cartID);

    $emailMessage .=
    "
    <br>
    <p>$closing</p>
    </body>
    </html>";

    $sendmail_params['sendmail_path'] = $phpfwk_sendmailPath;
    $sendmail_params['sendmail_args'] = $phpfwk_sendmailArgs;

    $mail_object =& Mail::factory('sendmail', $sendmail_params);
    $mail_object->send($recipients, $headers, $emailMessage);

    // send the internal copies out, same email, just change the recipients
    $recipients = $internalCC;
    $headers['To'] = $internalCC;
    $mail_object->send($recipients, $headers, $emailMessage);
}

function print_invoice($form = '')
{
    global $cartID;
    global $countryArray;

    $total = print_cartInfo($form);

    $db = db_connect();

    $purchaseInfo = db_query($db, "SELECT ccName, overlay(ccNo placing 'XXXXXXXXXXXX' from 3 for 12),
                                         orderedName, orderedEmail, orderedPhone,
                                         shipName, shipStreet1, shipStreet2,
                                         shipCity, shipProvince, shipCountry, shipPostal
                                  FROM shoppingCarts
                                  WHERE cartID = $cartID;");

    if (db_numRows($purchaseInfo) > 0)
    {
        list($ccName, $ccNo, $orderedName, $orderedEmail, $orderedPhone,
             $shipName, $shipStreet1, $shipStreet2, $shipCity, $shipProvince,
             $shipCountry, $shipPostal) = db_row($purchaseInfo, 0);
        if ($countryArray[$shipCountry])
        {
            $shipCountry = $countryArray[$shipCountry];
        }
    }
    else
    {
        return;
    }

    print '<br>';
    print '<center>';
    print "<table class=items width=100% cellpadding=3px cellspacing=0px>";
    print "<tr><th class=itemsColoredHeader colspan=100%>Purchase / Shipping Information</th></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print "<tr><td><b>Ordered by </b>$orderedName</td></tr>";
    print "<tr><td><b>Email </b>$orderedEmail</td></tr>";
    print "<tr><td><b>Phone </b>$orderedPhone</td></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print "<tr><td><b>Ship to </b>$shipName</td></tr>";
    print "<tr><td>$shipStreet1</td></tr>";
    print "<tr><td>$shipStreet2</td></tr>";
    print "<tr><td>$shipCity, $shipProvince</td></tr>";
    print "<tr><td>$shipCountry</td></tr>";
    print "<tr><td>$shipPostal</td></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print "<tr><td><b>Charged to </b>$ccName</td></tr>";
    print "<tr><td><b>Card number </b>$ccNo</td></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print '<tr><td><font size=-1>The full amount has been authorized to be charged to the submitted purchase information.  If Shipping and tax costs are to be determined. Someone will contact you at the above email or phone number with the details.</font></td></tr>';
    print '</table>';

    orderEmail($orderedEmail);
}

?>
