<?php

include_once("$phpfwkIncludePath/admin/include/shopping_catalogs.php");

$thisPage = $copyCatalogPage;
$gID = intval($gID);

$db = db_connect();

print_adminHeader();
print '<div style="padding: 3px">';

if ($gID < 1)
{
    print_msg('ERROR', 'Catalog ID Required',
              'An id is required to copy a catalog.', 'CopyCat/1');
    print_adminFooter();
    exit;
}

$groupInfo = db_query($db, "SELECT active, name FROM skuGroups
                            WHERE groupID = $gID;");

if (db_numRows($groupInfo) < 1)
{
    print_msg('ERROR', 'Catalog Does Not Exist',
              'The requested catalog could not be found. Perhaps someone already deleted it?', 'CopyCat/2');
    print_adminFooter();
    exit;
}

list($groupIsActive, $groupName) = db_row($groupInfo, 0);

if ($saveNow)
{
    if ($newCatName)
    {
        print "<h1>Saving new catalog &quot$newCatName&quot ...</h1>";

        $newCatID = copyCatalog($gID, $newCatName);

        if ($newCatID)
        {
            print_redirect("$catalogPage");
            //print_link("$catalogPage", $catalogPage);
        }
        else
        {
            print_copyCatalogForm($gID, $groupName);
        }
    }
    else
    {
        print_msg('ERROR', 'New Name Required', 'A name for the new catalog is required!');
        print_copyCatalogForm($gID, $groupName);
    }
}
else
{
    print_copyCatalogForm($gID, $groupName);
}

print '</div>';
print_adminFooter();

?>
