<?php

$catalogPage = "$common_baseURL/shopping_catalog.php";
$copyCatalogPage = "$common_baseURL/shopping_copyCatalog.php";

$newGroupPage = "$common_baseURL/shopping_newGroup.php";
$deleteGroupPage = "$common_baseURL/shopping_deleteGroup.php";

$newItemPage = "$common_baseURL/shopping_newItem.php";
$deleteItemPage = "$common_baseURL/shopping_deleteItem.php";

/*
 * addSkuGroupsToList : Create an array (key / value) of the groups,
 *                      From a given starting point (NULL for top level)
 *
 *                      Watch the recursion ;-)
 *
 * &$groupsArray -> the array (by reference0 to hold the list
 * $parentID -> the starting parent to go down from
 * $parentPath -> the parents path ('grandparent's name / parent's name')
 */
function addSkuGroupsToList(&$groupsArray, $parentID, $parentPath)
{
    $db = db_connect();

    unset($where);

    if (!$parentID)
    {
        sql_addToWhereClause($where, '', 'parent', 'IS', 'NULL', false, false);
    }
    else
    {
        sql_addToWhereClause($where, '', 'parent', '=', $parentID);
    }

    $groups = db_query($db, "SELECT groupID, name FROM skuGroups WHERE $where ORDER BY name;");

    $numGroups = db_numRows($groups);

    for ($i = 0; $i < $numGroups; $i++)
    {
        list($id, $name) = db_row($groups, $i);

        if ($parentPath)
        {
            $pathToAdd = "$parentPath / " . rtrim($name);
        }
        else
        {
            $pathToAdd = $name;
        }

        $groupsArray[$id] = $pathToAdd;

        addSkuGroupsToList($groupsArray, $id, $groupsArray[$id]);
     }
}

function print_groupList($parent, $gID)
{
    global $thisPage, $catalogPage, $copyCatalogPage;
    global $newGroupPage, $deleteGroupPage, $newItemPage, $deleteItemPage;
    global $shopping_defaultCurrency;

    $db = db_connect();

    unset($where);

    $title = 'Catalogs';
    $type = 'Catalog';
    $catalogName = 'System';
    $showCopy = false;

    unset($breadcrumb);
    if ($gID)
    {
        sql_addToWhereClause($where, '', 'parent', '=', $gID);

        $row = db_query($db, "SELECT parent, name FROM skuGroups
                             WHERE groupID = $gID order by displayOrder;");

        if (db_numRows($row) > 0)
        {
            list($parentGID, $catalogName) = db_row($row, 0);
            $title = "$catalogName Groups";
        }
        else
        {
            print_msg('ERROR', 'Group Not Found',
                      'Could not find the requested group!', 'NewGrp/104');
            return;
        }

        $breadcrumb = array("<a href=\"$catalogPage?parent=$parentGID&gID=$gID\">$catalogName</a>");
        $ancestorGID = $parentGID;
        while ($ancestorGID)
        {
            $row = db_query($db, "SELECT parent, name FROM skuGroups WHERE groupID = $ancestorGID;");
            if (db_numRows($row) > 0)
            {
                list($temp, $parentCatalogName) = db_row($row, 0);
                array_unshift($breadcrumb, "<a href=\"$catalogPage?parent=$temp&gID=$ancestorGID\">$parentCatalogName</a>");
                $ancestorGID = $temp;
            }
            else
            {
                unset($ancestorGID);
            }
        }
        $breadcrumb = '<div class="largeAdminHeader">Group: ' . join($breadcrumb, ' / ') . '<br></div>';
        $type = 'Group';
    }
    else
    {
        sql_addToWhereClause($where, '', 'parent', 'IS', 'NULL', false, false);
        $showCopy = true;
    }

    $catalogs = db_query($db, "SELECT groupID, name, active FROM skuGroups
                              WHERE $where ORDER BY active DESC, displayOrder, name;");
    $numCatalogs = db_numRows($catalogs);

    print $breadcrumb;
    print '<br>';

    if ($gID)
    {
        print "<a href=\"$newGroupPage?editting=1&parent=$parentGID&gID=$gID\">Edit&nbsp;$catalogName&nbsp;</a>";
        print " &bull; <a href=\"$deleteGroupPage?parent=$parentGID&gID=$gID\">Delete&nbsp;$catalogName&nbsp;</a>";
    }

    print '<table class=box cellpadding=3px cellspacing=0px width=500px>';
    print "<tr><th class=largeColoredHeader colspan=4>$title</th></tr>";

    $odd = true;
    $onActives = true;
    for ($i = 0; $i < $numCatalogs; ++$i)
    {
        list($id, $name, $active) = db_row($catalogs, $i);
        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;

        if ($onActives && !db_boolean($active))
        {
            $onActives = false;
            print "<tr><th class=largeHeader colspan=3>Inactive $title</th></tr>";
        }

        print "<tr><td $class width=100%><a href=\"$catalogPage?parent=$gID&gID=$id\">$name</a></td>";
        print "<td $class><a href=\"$newGroupPage?editting=1&parent=$gID&gID=$id\" class=\"editLink\">Edit</a></td>";
        if ($showCopy)
        {
            print "<td $class><a href=\"$copyCatalogPage?gID=$id\" class=\"editLink\">Copy</a></td>";
        }

        print "<td $class><a href=\"$deleteGroupPage?parent=$gID&gID=$id\" class=\"editLink\">Delete</a></td>";
        print "</tr>";
    }

    if ($numCatalogs < 1)
    {
        print "<tr><th colspan=4>No {$type}s In $catalogName!<p><a href=\"$newGroupPage?parent=$gID\">Click here to create one.</a></th></tr>";
        print '</table>';
    }
    else
    {
        print '</table>';
        print "<br><a href=\"$newGroupPage?parent=$gID\">Create a new $type in $catalogName</a>";
    }

    if ($gID)
    {
        // list group items
        print '<br><br>';

        $items = db_query($db, "SELECT sku, active, id, name
                               FROM skus WHERE groupID = $gID
                               ORDER BY active DESC, displayOrder;");
        $numItems = db_numRows($items);

        print '<table border=0 class=box cellpadding=3px cellspacing=0px width=500px>';
        print "<tr><th class=largeColoredHeader colspan=4>$catalogName Items</th></tr>";

        $odd = true;
        $onActives = true;
        for ($i = 0; $i < $numItems; ++$i)
        {
            list($sku, $active, $id, $name) = db_row($items, $i);

            if (!$name)
            {
                $name = '[No Name - Edit Me]';
            }

            $row = db_query($db, "SELECT item_price($sku, 1, '$shopping_defaultCurrency');");

            unset($itemPrice);
            if (db_numRows($row) > 0)
            {
                list($itemPrice) = db_row($row, 0);
            }
            $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
            $odd = !$odd;

            if ($onActives && !db_boolean($active))
            {
                $onActives = false;
                print "<tr><th class=largeHeader colspan=3>Inactive $catalogName Items</th></tr>";
            }

            print "<tr>";
            print "<td $class valign=top nowrap>$id</td>";
            print "<td $class width=100% valign=top><a href=\"$newItemPage?editting=1&gID=$gID&sku=$sku\">$name</a></td>";
            print "<td $class align=right valign=top nowrap>$itemPrice</td>";
            print "<td $class valign=top nowrap><a href=\"$deleteItemPage?gID=$gID&sku=$sku\" class=\"editLink\">Delete&nbsp;Item</td>";
            print "</tr>";
        }


        if ($numItems < 1)
        {
            print "<tr><th colspan=3>No Items In $catalogName<p><a href=\"$newItemPage?gID=$gID\">Click here to create one.</a></th></tr>";
            print '</table>';
        }
        else
        {
            print '</table>';
            print "<br><a href=\"$newItemPage?gID=$gID\">Create a new item</a>";
            print "<br><br><a href=\"$catalogPage?alpha=1&gID=$gID\">Alphabetize items</a>";
            print "<br><a href=\"$catalogPage?orderByCode=1&gID=$gID\">Order by SKU code</a>";
        }
    }
}

function print_copyCatalogForm($origID, $origName)
{
    global $thisPage, $saveNow;

    $db = db_connect();

    $origID = intval($origID);

    print_form($thisPage, 'copyCatForm');

    print_hidden('gID', $origID);
    print_hidden('saveNow', 1);

    print '<table class=box cellpadding=3px cellspacing=0px>';
    print "<tr><th class=largeColoredHeader colspan=2>Copying A Catalog</th></tr>";

    print_emptyRow();

    print "<tr><td>Copying Catalog</td><td><b>$origName</b></td></tr>";

    print '<tr><td>Enter the new catalog name</td><td>';
    print_textbox('newCatName', $newCatName, 50);
    print '</td></tr>';

    print '<tr><td></td><td>';
    print_submit('submit', $buttonText);
    print '</td></tr>';

    print '</table>';
    print '</form>';
}

function copyAllItems($fromGID, $toGID, $catID)
{
    global $common_baseCatalogImagesPath;
    $db = db_connect();

    $fromGID = intval($fromGID);
    $toGID = intval($toGID);
    $catID = intval($catID);

    // copy all skuGroups
    $subGroups = db_query($db, "SELECT groupID, name, active, displayOrder, smallImg, largeImg
                                FROM skuGroups WHERE parent = $fromGID;");
    $numSubGroups = db_numRows($subGroups);

    for ($i = 0; $i < $numSubGroups; $i++)
    {
        list($origSubGroup, $origName, $origActive, $origDisplayOrder,
             $origSmallImg, $origLargeImg) = db_row($subGroups, $i);

        unset($fields, $values);
        $newID = db_seqNextVal($db, 'seq_skus');

        sql_addIntToInsert($fields, $values, 'groupid', $newID);
        sql_addIntToInsert($fields, $values, 'displayOrder', $origDisplayOrder);
        sql_addBoolToInsert($fields, $values, 'active', db_boolean($origActive));
        sql_addIntToInsert($fields, $values, 'parent', $toGID);
        sql_addIntToInsert($fields, $values, 'catalog', $catID);
        sql_addScalarToInsert($fields, $values, 'name', $origName);

        if ($origSmallImg)
        {
            $newSmallImg = substr_replace($origSmallImg, '', 0, strlen("thumb_{$gID}_"));
            sql_addScalarToInsert($fields, $values, 'smallImg', "thumb_{$newID}_$newSmallImg");

            $src = "$common_baseCatalogImagesPath/$origSmallImg";
            $dest = "$common_baseCatalogImagesPath/thumb_{$newID}_$newSmallImg";

            if (!copy($src, $dest))
            {
                print_msg('ERROR', 'File Copy Error', "Could not copy file : $src to $dest!");
            }
        }

        if ($origLargeImg)
        {
            $newLargeImg = substr_replace($origLargeImg, '', 0, strlen("{$gID}_"));
            sql_addScalarToInsert($fields, $values, 'largeImg', "{$newID}_$newLargeImg");

            $src = "$common_baseCatalogImagesPath/$origLargeImg";
            $dest = "$common_baseCatalogImagesPath/{$newID}_$newLargeImg";

            if (!copy($src, $dest))
            {
                print_msg('ERROR', 'File Copy Error', "Could not copy file : $src to $dest!");
            }
        }

        db_insert($db, 'skuGroups', $fields, $values);

        // recursive copy of all sub groups
        copyAllItems($origSubGroup, $newID, $catID);
    }

    // copy all skus, skuprices and sku options
    $skus = db_query($db, "SELECT sku, displayOrder, active, id, name,
                                  shortDesc, longDesc, smallImg, largeImg
                           FROM skus WHERE groupid = $fromGID;");
    $numSkus = db_numRows($skus);

    for ($j = 0; $j < $numSkus; $j++)
    {
        // copy the sku information and copy image files
        list($origSku, $origDisplayOrder, $origActive, $origID,
             $origName, $origShortDesc, $origLongDesc,
             $origSmallImg, $origLargeImg) = db_row($skus, $j);

        unset($fields, $values);

        $newSku = db_seqNextVal($db, 'seq_skus');

        sql_addIntToInsert($fields, $values, 'sku', $newSku);
        sql_addIntToInsert($fields, $values, 'groupID', $toGID);
        sql_addIntToInsert($fields, $values, 'displayOrder', $origDisplayOrder);
        sql_addBoolToInsert($fields, $values, 'active', db_boolean($origActive));
        sql_addScalarToInsert($fields, $values, 'id', $origID);
        sql_addScalarToInsert($fields, $values, 'name', $origName);
        sql_addScalarToInsert($fields, $values, 'shortDesc', $origShortDesc);
        sql_addScalarToInsert($fields, $values, 'longDesc', $origLongDesc);

        if ($origSmallImg)
        {
            $newSmallImg = substr_replace($origSmallImg, '', 0, strlen("thumb_{$origSku}_"));
            sql_addScalarToInsert($fields, $values, 'smallImg', "thumb_{$newSku}_$newSmallImg");

            $src = "$common_baseCatalogImagesPath/$origSmallImg";
            $dest = "$common_baseCatalogImagesPath/thumb_{$newSku}_$newSmallImg";

            if (!copy($src, $dest))
            {
                print_msg('ERROR', 'File Copy Error', "Could not copy file : $src to $dest!");
            }
        }

        if ($origLargeImg)
        {
            $newLargeImg = substr_replace($origLargeImg, '', 0, strlen("{$origSku}_"));
            sql_addScalarToInsert($fields, $values, 'largeImg', "{$newSku}_$newLargeImg");

            $src = "$common_baseCatalogImagesPath/$origLargeImg";
            $dest = "$common_baseCatalogImagesPath/{$newSku}_$newLargeImg";

            if (!copy($src, $dest))
            {
                print_msg('ERROR', 'File Copy Error', "Could not copy file : $src to $dest!");
            }
        }

        db_insert($db, 'skus', $fields, $values);

        // copy the sku prices information
        db_query($db, "INSERT INTO skuPrices
                       SELECT '$newSku'::integer, currency, minquant,
                              price, starts, ends
                       FROM skuPrices WHERE sku = '$origSku';");

        //copy the sku options information
        db_query($db, "INSERT INTO skuOptions (sku, type, name)
                       SELECT '$newSku'::integer, type, name
                       FROM skuOptions WHERE sku = '$origSku';");
    }
}

function copyCatalog($gID, $newName)
{
    global $common_baseCatalogImagesPath;
    $db = db_connect();

    $gID = intval($gID);
    $newID = 0;

    db_startTransaction($db);
    unset($fields, $values);
    $newID = db_seqNextVal($db, 'seq_skus');

    sql_addIntToInsert($fields, $values, 'groupid', $newID);
    sql_addScalarToInsert($fields, $values, 'name', $newName);

    $oldCatInfo = db_query($db, "SELECT active, smallImg, largeImg FROM skuGroups
                                 WHERE groupID = $gID;");

    if (db_numRows($oldCatInfo) > 0)
    {
        list($oldIsActive, $oldSmallImg, $oldLargeImg) = db_row($oldCatInfo, 0);
    }

    sql_addBoolToInsert($fields, $values, 'active', db_boolean($oldIsActive));

    if ($oldSmallImg)
    {
        $newSmallImg = substr_replace($oldSmallImg, '', 0, strlen("thumb_{$gID}_"));
        sql_addScalarToInsert($fields, $values, 'smallImg', "thumb_{$newID}_$newSmallImg");

        $src = "$common_baseCatalogImagesPath/$oldSmallImg";
        $dest = "$common_baseCatalogImagesPath/thumb_{$newID}_$newSmallImg";

        if (!copy($src, $dest))
        {
            print_msg('ERROR', 'File Copy Error', "Could not copy file : $src to $dest!");
        }
    }

    if ($oldLargeImg)
    {
        $newLargeImg = substr_replace($oldLargeImg, '', 0, strlen("{$gID}_"));
        sql_addScalarToInsert($fields, $values, 'largeImg', "{$newID}_$newLargeImg");

        $src = "$common_baseCatalogImagesPath/$oldLargeImg";
        $dest = "$common_baseCatalogImagesPath/{$newID}_$newLargeImg";

        if (!copy($src, $dest))
        {
            print_msg('ERROR', 'File Copy Error', "Could not copy file : $src to $dest!");
        }
    }

    // update all display orders to be +1 are from the selected order forwards
    db_update($db, 'skuGroups', 'displayOrder = displayOrder + 1',
                   "parent IS NULL AND displayOrder >= 1");

    db_insert($db, 'skuGroups', $fields, $values);

    // copy recursively all items
    copyAllItems($gID, $newID, $gID);

    db_endTransaction($db);

    return $newID;
}

function print_groupForm($parent, $gID)
{
    global $thisPage, $saveNow, $common_baseCatalogImagesURL;
    global $groupIsActive, $groupName;

    $db = db_connect();

    $parent = intval($parent);
    $type = "Group";
    if (!$parent)
    {
        $type = "Catalog";
    }

    $title = "New $type";
    $buttonText = "Create $type";
    unset($whereGroup);
    if (! $editting)
    {
        $groupIsActive = true;
    }

    if ($gID)
    {
        $title = "Edit $type";
        $buttonText = "Save $type";
        $whereGroup = "and groupID != $gID";
    }

    print_js_checkFileUpload(array('groupForm' => 'smallImg', 'groupForm' => 'largeImg'));

    // start of form
    print_form($thisPage, 'groupForm', true, 'post', 'onSubmit="checkUpload()"');

    print_hidden('parent', $parent);
    print_hidden('gID', $gID);
    print_hidden('saveNow', 1);

    print '<table class=box cellpadding=3px cellspacing=0px>';
    print "<tr><th class=largeColoredHeader colspan=2>$title</th></tr>";

    print "<tr><td>$type name</td><td>";
    print_textbox('groupName', $groupName);
    print '</td></tr>';

    if ($parent)
    {
        if (! $gID)
        {
            // get the current maximum displayOrder set that to be the group order if none exists
            list($groupOrder) = db_row(db_query($db, "SELECT max(displayOrder) + 1 FROM skuGroups
                                                    WHERE parent = $parent;"), 0);
        }
        else
        {
            // get the record information for this groupID
            list($groupName, $curOrder, $groupIsActive) =
                db_row(db_query($db, "SELECT name, displayOrder, active
                                    FROM skuGroups WHERE groupID = $gID"), 0);

            // get the display order for the group right behind us in the order
            $row = db_query($db, "SELECT displayOrder + 1 FROM skuGroups
                                 WHERE parent = $parent AND
                                       displayOrder < $curOrder
                                 ORDER BY displayOrder DESC LIMIT 1;");

            if (db_numRows($row) > 0)
            {
                list($groupOrder) = db_row($row, 0);
            }
            else
            {
                $groupOrder = 1;
            }
        }
        $groupOrders = db_query($db, "SELECT groupID, displayOrder, name FROM skuGroups
                                      WHERE parent = $parent $whereGroup ORDER BY displayOrder, name;");
    }
    else
    {
        if (! $gID)
        {
            // get the current maximum displayOrder set that to be the group order if none exists
            list($groupOrder) = db_row(db_query($db, "SELECT max(displayOrder) + 1 FROM skuGroups
                                                    WHERE parent IS NULL;"), 0);
        }
        else
        {
            // get the record information for this groupID
            list($groupName, $curOrder, $groupIsActive) =
                db_row(db_query($db, "SELECT name, displayOrder, active
                                    FROM skuGroups WHERE groupID = $gID"), 0);

            // get the display order for the group right behind us in the order
            $row = db_query($db, "SELECT displayOrder + 1 FROM skuGroups
                                 WHERE parent IS NULL AND
                                       displayOrder < $curOrder
                                 ORDER BY displayOrder DESC LIMIT 1;");

            if (db_numRows($row) > 0)
            {
                list($groupOrder) = db_row($row, 0);
            }
            else
            {
                $groupOrder = 1;
            }
        }

        $groupOrders = db_query($db, "SELECT groupID, displayOrder, name FROM skuGroups
                                     WHERE parent is null $whereGroup ORDER BY displayOrder, name;");
    }

    // get all existing skuGroups for our parent
    $numRows = db_numRows($groupOrders);
    print '<tr><td>Show After</td><td>';
    print "<select name=\"groupOrder\">\n";

    // print an option to make this the first displayed group
    if ($numRows > 0)
    {
        print_option('** FIRST GROUP **', 1, $groupOrder);
    }

    $i = 0;
    while ($i < $numRows)
    {
        list($id, $order, $name) = db_row($groupOrders, $i);
        print_option($name, ($order + 1), $groupOrder);
        $i++;
    }
    // if we had no groups ... we must add the first group
    if ($i == 0)
    {
        print_option('** FIRST GROUP **', 1, $groupOrder);
    }

    print "<\select>\n";
    print '</td></tr>';

    print '<tr><td></td><td>';
    print_checkbox('groupIsActive', 'Is Active', db_toBoolean(true), db_boolean($groupIsActive));
    print '</td></tr>';

    print "<tr><th class=largeColoredHeader colspan=2>$type Images</th></tr>";
    $url = false;
    $url = imageURL($common_baseCatalogImagesURL, 'skuGroups', 'smallImg', 'groupID', $gID);
    if ($url)
    {
        print "<tr><td>&nbsp</td></tr>";
        print "<tr><td valign=top></td><td>";
        print "<img src=\"$url\"><br><span class=\"microHeader\">$groupImg</span><br>";
        print_checkbox('delSImg', 'Delete small image', '1', 0);
        print '</td></tr>';
    }

    print "<tr><td>Small</td><td><input size=30 name=\"groupImgSmall\" type=\"file\"></td></tr>";

    $url = false;
    $url = imageURL($common_baseCatalogImagesURL, 'skuGroups', 'largeImg', 'groupID', $gID);
    if ($url)
    {
        print "<tr><td>&nbsp</td></tr>";
        print "<tr><td valign=top></td><td>";
        print "<img src=\"$url\"><br><span class=\"microHeader\">$groupImg</span>";
        print_checkbox('delLImg', 'Delete large image', '1', 0);
        print '</td></tr>';
    }

    print "<tr><td>Large</td><td><input size=30 name=\"groupImg\" type=\"file\"></td></tr>";

    print '<tr><td></td><td>';
    print_submit('submit', $buttonText);
    print '</td></tr>';

    print '</table>';
    print '</form>';
}

function validateGroup($parent, &$gID, &$groupIsActive, &$groupName)
{
    $db = db_connect();

    if ($parent)
    {
        $type = 'Group';
    }
    else
    {
        $type = 'Catalog';
    }

    $groupName = trim($groupName);
    if (!$groupName)
    {
        print_msg('ERROR', "$type Name Required",
                  'A name is required. Please enter a name below.', 'NewGrp/4');
        return false;
    }

    unset($where);
    sql_addToWhereClause($where, '', 'name', '=', $groupName);

    if (! $parent)
    {
        sql_addToWhereClause($where, 'AND', 'parent', 'IS', 'NULL', false, false);
    }
    else
    {
        sql_addToWhereClause($where, 'AND', 'parent', '=', $parent, false, false);
    }

    if ($gID > 0)
    {
        sql_addToWhereClause($where, 'AND', 'groupID', '!=', $gID, false, false);
    }

    if (db_numRows(db_query($db, "SELECT name FROM skuGroups WHERE $where;")) > 0)
    {
        print_msg('ERROR', "$type By That Name Exists",
                  "A $type by that name already exists. Please choose a unique name for this $type.", 'NewGrp/5');
        return false;
    }

    return true;
}

function addGroup($parent)
{
    global $groupName, $groupIsActive, $groupOrder, $groupImg, $groupImgSmall;
    global $common_baseCatalogImagesPath;

    if ($parent)
    {
        $type = 'Group';
    }
    else
    {
        $type = 'Catalog';
    }

    $groupOrder = intval($groupOrder);

    print "<h3>Adding $type ...</h3>";

    $db = db_connect();

    unset($fields, $values);

    $gID = db_seqNextVal($db, 'seq_skus');

    sql_addIntToInsert($fields, $values, 'groupID', $gID);
    sql_addScalarToInsert($fields, $values, 'name', $groupName);
    sql_addBoolToInsert($fields, $values, 'active', db_boolean($groupIsActive));
    sql_addIntToInsert($fields, $values, 'displayOrder', $groupOrder, true);

    if ($parent)
    {
        sql_addIntToInsert($fields, $values, 'parent', $parent);
    }
    else
    {
        sql_addRawToInsert($fields, $values, 'parent', 'NULL');
    }

    // upload the small image
    $smallImg = upload_image($common_baseCatalogImagesPath, 'groupImgSmall', $groupImgSmall, $gID, true);
    if ($smallImg)
    {
        sql_addScalarToInsert($fields, $values, 'smallImg', $smallImg);
    }

    // upload the large image
    $largeImg = upload_image($common_baseCatalogImagesPath, 'groupImg', $groupImg, $gID, false);
    if ($largeImg)
    {
        sql_addScalarToInsert($fields, $values, 'largeImg', $largeImg);
    }

    if ($parent)
    {
        // update all display orders to be +1 are from the selected order forwards
        db_update($db, 'skuGroups', 'displayOrder = displayOrder + 1',
                      "parent = $parent AND displayOrder >= $groupOrder");
    }
    else
    {
        // update all display orders to be +1 are from the selected order forwards
        db_update($db, 'skuGroups', 'displayOrder = displayOrder + 1',
                      "parent IS NULL AND displayOrder >= $groupOrder");
    }

    // insert a new group
    db_insert($db, 'skuGroups', $fields, $values);
}

function updateGroup($parent, $gID)
{
    global $groupName, $groupIsActive, $groupOrder, $groupImg, $groupImgSmall;
    global $delSImg, $delLImg;
    global $common_baseCatalogImagesPath;

    $db = db_connect();

    if ($parent)
    {
        $type = 'Group';
    }
    else
    {
        $type = 'Catalog';
    }

    print "<h3>Saving $type ...</h3>";

    //print "groupIsActive $groupIsActive<br>";

    unset($fields, $where);

    sql_addScalarToUpdate($fields, 'name', $groupName);
    sql_addBoolToUpdate($fields, 'active', db_boolean($groupIsActive));
    sql_addIntToUpdate($fields, 'displayOrder', $groupOrder);

    // upload the small image
    $oldImg = imagePath('skuGroups', 'smallImg', 'groupID', $gID);
    $smallImg = upload_image($common_baseCatalogImagesPath, 'groupImgSmall', $groupImgSmall, $gID, true);
    if ($smallImg)
    {
        @unlink($oldImg);
        sql_addScalarToUpdate($fields, 'smallImg', $smallImg);
    }
    else if ($delSImg == 1)
    {
        @unlink($oldImg);
        sql_addNullToUpdate($fields, 'smallImg');
    }

    // upload the large image
    $oldImg = imagePath('skuGroups', 'largeImg', 'groupID', $gID);
    $largeImg = upload_image($common_baseCatalogImagesPath, 'groupImg', $groupImg, $gID, false);
    if ($largeImg)
    {
        @unlink($oldImg);
        sql_addScalarToUpdate($fields, 'largeImg', $largeImg);
    }
    else if ($delLImg == 1)
    {
        @unlink($oldImg);
        sql_addNullToUpdate($fields, 'largeImg');
    }

    if ($parent)
    {
        // update all display orders to be +1 from the selected order forwards
        db_update($db, 'skuGroups', 'displayOrder = displayOrder + 1',
                        "parent = $parent AND displayOrder >= $groupOrder;");
    }

    // update the group
    db_update($db, 'skuGroups', $fields, "groupID = $gID");
}

function print_itemForm($gID, $sku)
{
    global $thisPage, $editting, $saveNow, $id;
    global $itemIsActive, $itemName, $itemShortDesc, $itemLongDesc, $itemPrice;
    global $common_baseCatalogImagesURL;
    global $parent;
    global $shopping_defaultCurrency;

    $db = db_connect();

    if (is_null($parent))
    {
        $parent = $gID;
    }


    $title = 'New Item';
    $buttonText = 'Create Item';

    if ($sku)
    {
        $title = 'Edit Item';
        $buttonText = 'Save Item';
        $skuWhere = "and sku != $sku";
    }

    if (! $editting)
    {
        $itemIsActive = true;
    }

    print_js_checkFileUpload(array('itemForm' => 'itemThumbImg', 'itemForm' => 'itemImg'));

    // start of form
    print_form($thisPage, 'itemForm', true, 'post', 'onSubmit="checkUpload()"');

    print_hidden('gID', $gID);
    print_hidden('sku', $sku);
    print_hidden('saveNow', 1);

    print '<table class=box cellpadding=3px cellspacing=0px>';
    print "<tr><th class=largeColoredHeader colspan=2>$title</th></tr>";

    if (!$sku)
    {
        // get the current maximum displayOrder set that to be the group order if none exists
        list($itemOrder) = db_row(db_query($db, "SELECT max(displayOrder) + 1 FROM skus
                                               WHERE groupID = $gID;"), 0);
    }
    else
    {
        // get the record information for this item
        list($itemName, $curOrder, $itemIsActive,
             $itemShortDesc, $itemLongDesc, $smallImg, $largeImg, $id) =
            db_row(db_query($db, "SELECT name, displayOrder, active, shortDesc,
                                       longDesc , smallImg, largeImg, id
                                FROM skus WHERE sku = $sku"), 0);

        $row = db_query($db, "SELECT item_price($sku, 1, '$shopping_defaultCurrency');");

        if (db_numRows($row) > 0)
        {
            list($itemPrice) = db_row($row, 0);
        }

        // get the display order for the item right behind us in the order
        $row = db_query($db, "SELECT displayOrder + 1 FROM skus
                             WHERE groupID = $gID AND
                                   displayOrder < $curOrder
                             ORDER BY displayOrder DESC LIMIT 1;");

        if (db_numRows($row) > 0)
        {
            list($itemOrder) = db_row($row, 0);
        }
        else
        {
            $itemOrder = 1;
        }
    }

    $family = array();
    addSkuGroupsToList($family, NULL, '');

    print '<tr><td>Belongs to</td><td>';
    print_selectArray('parent', $family, $parent);
    print '</td></tr>';

    print '<tr><td>Name</td><td>';
    print_textbox('itemName', $itemName, 50);
    print '</td></tr>';

    print '<tr><td>ID</td><td>';
    print_textbox('id', trim($id), 20, 20);
    print '</td></tr>';

    // get all existing items for our group
    $itemOrders = db_query($db, "SELECT sku, displayOrder, name FROM skus
                                 WHERE groupID = $gID $skuWhere ORDER BY displayOrder;");
    $numRows = db_numRows($itemOrders);

    print '<tr><td>Show After</td><td>';
    print "<select name=\"itemOrder\">\n";

    // print an option to make this the first displayed group
    if ($numRows > 0)
    {
        print_option('**FIRST ITEM**', 1, $itemOrder);
    }

    $i = 0;
    while ($i < $numRows)
    {
        list($id, $order, $name) = db_row($itemOrders, $i);
        print_option($name, ($order + 1), $itemOrder);
        $i++;
    }

    // if we had no items ... we must add the first item
    if ($i == 0)
    {
        print_option('**FIRST ITEM**', 1, $itemOrder);
    }

    print "<\select>\n";
    print '</td></tr>';

    print '<tr><td></td><td>';
    print_checkbox('itemIsActive', 'Is Active', db_toBoolean(true), db_boolean($itemIsActive));
    print '</td></tr>';

    print '<tr><td>Short Description</td><td>';
    print_textbox('itemShortDesc', $itemShortDesc, 50, 50);
    print '</td></tr>';

    print '<tr><td valign=top>Full Description</td><td>';
    print_textArea('itemLongDesc', $itemLongDesc, 8, 50);
    print '</td></tr>';

    print '<tr><td>Item Price (USD)</td><td>';
    print_textbox('itemPrice', $itemPrice, 20, 10);
    print '</td></tr>';

    $url = false;
    $url = imageURL($common_baseCatalogImagesURL, 'skus', 'smallImg', 'sku', $sku);
    if ($url)
    {
        print "<tr><td>&nbsp</td></tr>";
        print "<tr><td></td><td>";
        print "<img src=\"$url\"><span class=\"microHeader\">$largeImg</span>";
        print_checkbox('delSImg', 'Delete small image', '1', 0);
        print '</td></tr>';
    }

    print '<tr><td>Item thumbnail Image</td><td><input size=30 name="itemThumbImg" type="file"></td></tr>';

    $url = false;
    $url = imageURL($common_baseCatalogImagesURL, 'skus', 'largeImg', 'sku', $sku);
    if ($url)
    {
        print "<tr><td>&nbsp</td></tr>";
        print "<tr><td valign=top></td><td>";
        print "<img src=\"$url\"><br><span class=\"microHeader\">$largeImg</span>";
        print_checkbox('delLImg', 'Delete large image', '1', 0);
        print '</td></tr>';
    }

    print '<tr><td>Item Image</td><td><input size=30 name="itemImg" type="file"></td></tr>';

    $optionTypes = db_query($db, "select optionTypeID, name from skuOptionTypes order by name;");
    $numOptions = db_numRows($optionTypes);

    for ($i = 0; $i < $numOptions; ++$i)
    {
        list($optionID, $optionName) = db_row($optionTypes, $i);
        print "<tr><th class=coloredHeader colspan=2>$optionName Options</th></tr>";
        print "<tr><td valign=top>";
        print '<tr><td>';
        print "New {$optionName}s</td><td>";
        print_textbox("newOptions[$optionID]", '');
        print "</td></tr>";
        if ($sku)
        {
            $existingOptions = db_query($db, "select optionID, name from skuOptions where sku = $sku and type = $optionID order by optionID;");
            $numExOpts = db_numRows($existingOptions, $j);
            for ($j = 0; $j < $numExOpts; ++$j)
            {
                list($optionID, $optionName) = db_row($existingOptions, $j);
                print '<tr><td></td><td>';
                print_textbox("extOptions[$optionID]", $optionName);
                print '</td></tr>';
            }
        }
    }

    print '<tr><td></td><td>';
    print_submit('submit', $buttonText);
    print '</td></tr>';

    print '</table>';
    print '</form>';
}

function validateItem($gID, $sku)
{
    global $itemName, $isItemActive, $itemShortDesc, $itemLongDesc, $itemPrice;

    $db = db_connect();

    $itemName = trim($itemName);
    if (!$itemName)
    {
        print_msg('ERROR', 'Item Name Required',
                  'A name is required. Please enter a name for this item below.', 'NewItem/4');
        return false;
    }

    /*
    unset($where);
    sql_addToWhereClause($where, '', 'name', '=', $itemName);
    sql_addToWhereClause($where, 'AND', 'groupID', '=', $gID, false, false);

    if ($sku > 0)
    {
        sql_addToWhereClause($where, 'AND', 'sku', '!=', $sku, false, false);
    }

    if (db_numRows(db_query($db, "SELECT name FROM skus WHERE $where;")) > 0)
    {
        print_msg('ERROR', 'Item By That Name Exists',
                  'An item by that name already exists in this group. Please choose a unique name for this item.', 'NewItem/5');
        return false;
    }*/

    if ($itemPrice)
    {
        $itemPrice = str_replace(",", "", $itemPrice);

        if (! is_numeric($itemPrice))
        {
            print_msg("ERROR", "Invalid Price", "The price entered is not numeric", 'NewItem/6');
            return false;
        }

        list($whole, $decimal) = split("\.", $itemPrice, 2);

        if (strlen($whole) > 9 || strlen($decimal) > 2)
        {
            print_msg("ERROR", "Invalid Price",
                    "The price entered is invalid!  The price may contain up to 7 digits, and 2 decimals.");
            return false;
        }
    }

    return true;
}

function updateItem($gID, $sku)
{
    global $itemName, $itemIsActive, $itemOrder, $itemShortDesc, $itemLongDesc;
    global $itemThumbImg, $itemImg, $itemPrice, $id;
    global $extOptions, $newOptions;
    global $common_baseCatalogImagesPath;
    global $delSImg, $delLImg;
    global $parent;

    print '<h3>Saving Item ...</h3>';

    $sku = intval($sku);
    if ($sku > 0)
    {
        $db = db_connect();

        unset($fields, $where);

        $oldThumbImg = imagePath('skus', 'smallImg', 'sku', $sku);
        $oldImg = imagePath('skus', 'largeImg', 'sku', $sku);

        $id = trim($id);
        if (strlen($id) > 20)
        {
            $id = substr($id, 0, 20);
        }

        sql_addScalarToUpdate($fields, 'name', $itemName);
        sql_addScalarToUpdate($fields, 'id', trim($id));
        sql_addBoolToUpdate($fields, 'active', db_boolean($itemIsActive));
        sql_addScalarToUpdate($fields, 'shortDesc', $itemShortDesc);
        sql_addScalarToUpdate($fields, 'longDesc', $itemLongDesc);

        // handle image elements
        $smallImg = upload_image($common_baseCatalogImagesPath, 'itemThumbImg', $itemThumbImg, $sku, true);

        if ($smallImg)
        {
            @unlink($oldThumbImg);
            sql_addScalarToUpdate($fields, 'smallImg', $smallImg);
        }
        else if ($delSImg == 1)
        {
            @unlink($oldImg);
            sql_addNullToUpdate($fields, 'smallImg');
        }

        $largeImg = upload_image($common_baseCatalogImagesPath, 'itemImg', $itemImg, $sku, false);

        if ($largeImg)
        {
            @unlink($oldImg);
            sql_addScalarToUpdate($fields, 'largeImg', $largeImg);

            if (!$smallImg)
            {
                // if still no thumbnail image ... do them a favour and make a thumbnail
                createThumbnail("$common_baseCatalogImagesPath/$largeImg",
                                "$common_baseCatalogImagesPath/thumb_$largeImg");
                sql_addScalarToUpdate($fields, 'smallImg',"thumb_$largeImg");
            }
        }
        else if ($delLImg == 1)
        {
            @unlink($oldImg);
            sql_addNullToUpdate($fields, 'largeImg');
        }

        if ($parent == $gID)
        {
            // Our parent group did not change
            // set our proper display order
            sql_addIntToUpdate($fields, 'displayOrder', $itemOrder);

            // update all display orders to be +1 from the selected order forwards
            db_update($db, 'skus', 'displayOrder = displayOrder + 1',
                    "groupID = $gID AND displayOrder >= $itemOrder;");
        }
        else
        {
            // We are being moved into another group
            sql_addIntToUpdate($fields, 'groupID', $parent);

            // change our display to be at the top.
            sql_addIntToUpdate($fields, 'displayOrder', 1);
        }

        // update the item
        db_update($db, 'skus', $fields, "sku = $sku");

        if (is_array($extOptions))
        {
            foreach ($extOptions as $optID => $optValue)
            {
                $optID = intval($optID);
                if ($optID < 1)
                {
                    continue;
                }

                if (!$optValue)
                {
                    db_delete($db, 'skuOptions', "sku = $sku and optionID = $optID");
                }
                else
                {
                    unset($fields);
                    sql_addScalarToUpdate($fields, 'name', $optValue);
                    db_update($db, 'skuOptions', $fields, "sku = $sku and optionID = $optID");
                }
            }
        }

        if (is_array($newOptions))
        {
            foreach ($newOptions as $optID => $optValues)
            {
                $optID = intval($optID);
                if ($optID < 1)
                {
                    continue;
                }

                $optValues = split(',', $optValues);
                foreach ($optValues as $optValue)
                {
                    $optValue = trim($optValue);
                    if (!$optValue)
                    {
                        continue;
                    }

                    unset($fields);
                    unset($values);
                    sql_addIntToInsert($fields, $values, 'sku', $sku);
                    sql_addIntToInsert($fields, $values, 'type', $optID);
                    sql_addScalarToInsert($fields, $values, 'name', $optValue);
                    db_insert($db, 'skuOptions', $fields, $values);
                }
            }
        }

        // insert a price
        $itemPrice = trim($itemPrice);
        if ($itemPrice)
        {
            unset($priceFields, $priceValues);

            sql_addIntToInsert($priceFields, $priceValues, 'sku', $sku);
            sql_addScalarToInsert($priceFields, $priceValues, 'currency', 'USD');
            sql_addIntToInsert($priceFields, $priceValues, 'minQuant', 1);
            sql_addFloatToInsert($priceFields, $priceValues, 'price', $itemPrice);
            sql_addRawToInsert($priceFields, $priceValues, 'starts', 'now()');

            db_insert($db, 'skuPrices', $priceFields, $priceValues);
        }
    }
}


function addItem($gID)
{
    global $itemName, $itemIsActive, $itemOrder, $itemShortDesc, $itemLongDesc;
    global $itemThumbImg, $itemImg, $itemPrice, $id;
    global $common_baseCatalogImagesPath, $newOptions;
    global $parent;
    global $shopping_defaultCurrency;

    print '<h3>Adding Item ...</h3>';

    unset($fields, $values);

    $db = db_connect();
    $sku = db_seqNextVal($db, 'seq_skus');

    $id = trim($id);
    if (strlen($id) > 20)
    {
        $id = substr($id, 0, 20);
    }

    sql_addIntToInsert($fields, $values, 'sku', $sku);
    sql_addScalarToInsert($fields, $values, 'id', $id);
    sql_addScalarToInsert($fields, $values, 'name', $itemName);
    sql_addBoolToInsert($fields, $values, 'active', db_boolean($itemIsActive));
    sql_addScalarToInsert($fields, $values, 'shortDesc', $itemShortDesc);
    sql_addScalarToInsert($fields, $values, 'longDesc', $itemLongDesc);

    // handle image elements
    $smallImg = upload_image($common_baseCatalogImagesPath, 'itemThumbImg', $itemThumbImg, $sku, true);

    if ($smallImg)
    {
        sql_addScalarToInsert($fields, $values, 'smallImg', $smallImg);
    }

    $largeImg = upload_image($common_baseCatalogImagesPath, 'itemImg', $itemImg, $sku, false);

    if ($largeImg)
    {
        // we have a large image, but no thumbnail version, so let's do them a favour and make one!
        if (!$smallImg)
        {
            createThumbnail("$common_baseCatalogImagesPath/$largeImg", "$common_baseCatalogImagesPath/thumb_$largeImg");
            sql_addScalarToInsert($fields, $values, 'smallImg',"thumb_$largeImg");
        }
        sql_addScalarToInsert($fields, $values, 'largeImg', $largeImg);
    }

    if ($parent == $gID)
    {
        // the original group selected is where we are going
        // set our groupID and display order
        sql_addIntToInsert($fields, $values, 'groupID', $gID);

        if ($itemOrder > 0)
        {
            sql_addIntToInsert($fields, $values, 'displayOrder', $itemOrder);

            // update all display orders to be +1 are from the selected order forwards
            db_update($db, 'skus', 'displayOrder = displayOrder + 1',
                            "groupID = $gID AND displayOrder >= $itemOrder;");
        }
    }
    else
    {
        // we are not going to the original group
        sql_addIntToInsert($fields, $values, 'groupID', $parent);
        sql_addIntToInsert($fields, $values, 'displayOrder', 1);
    }

    // insert a new group
    db_insert($db, 'skus', $fields, $values);

    if (is_array($newOptions))
    {
        foreach ($newOptions as $optID => $optValues)
        {
            $optID = intval($optID);
            if ($optID < 1)
            {
                continue;
            }

            $optValues = split(',', $optValues);
            foreach ($optValues as $optValue)
            {
                $optValue = trim($optValue);
                if (!$optValue)
                {
                    continue;
                }

                unset($fields);
                unset($values);
                sql_addIntToInsert($fields, $values, 'sku', $sku);
                sql_addIntToInsert($fields, $values, 'type', $optID);
                sql_addScalarToInsert($fields, $values, 'name', $optValue);
                db_insert($db, 'skuOptions', $fields, $values);
            }
        }
    }

    // insert a price
    $itemPrice = trim($itemPrice);
    if ($itemPrice)
    {
        unset($priceFields, $priceValues);

        sql_addIntToInsert($priceFields, $priceValues, 'sku', $sku);
        sql_addScalarToInsert($priceFields, $priceValues, 'currency', $shopping_defaultCurrency);
        sql_addIntToInsert($priceFields, $priceValues, 'minQuant', 1);
        sql_addFloatToInsert($priceFields, $priceValues, 'price', $itemPrice);
        sql_addRawToInsert($priceFields, $priceValues, 'starts', 'now()');

        db_insert($db, 'skuPrices', $priceFields, $priceValues);
    }
}

?>
