<?
class Administrator
{
    var $userID = -1;
    var $groupID = -1;
    var $login;
    var $password;
    var $firstName;
    var $middleName;
    var $lastName;
    var $email;
    var $strikes = -1;
    var $lastlog;
    var $isActive = false;

    var $dbUserTable = 'users';
    var $dbUserIDRow = 'userID';
    var $dbUserSequence = 'userIDs';
    var $dbUserGroupsTable = 'userGroups';
    var $dbUserGroupsIDRow = 'groupID';

    function Administrator($userID = -1, $groupID = -1, $login = '', $password = '',
                           $firstName= '', $middleName = '', $lastName = '', $email = '', $strikes = 0)
    {
        global $auth_maxPasswordFailures;

        $userID = intval($userID);

        if ($userID > -1)
        {
            $db = db_connect();
            $admin = db_query($db, "SELECT u.userID, ug.groupID, u.login, u.password, 
                                           u.firstName, u.middleName, u.lastName, u.email, u.strikes,
                                           to_char(u.lastlog, 'DD Mon, YYYY at HH24:MI')
                                    FROM {$this->dbUserTable} u
                                    LEFT JOIN {$this->dbUserGroupsTable} ug
                                         ON (ug.{$this->dbUserIDRow} = u.{$this->dbUserIDRow} AND
                                             ug.primaryGroup = true)
                                    WHERE u.{$this->dbUserIDRow} = $userID;");

            if (db_numRows($admin) > 0)
            {
                list($this->userID, $this->groupID, $this->login, $this->password,
                     $this->firstName, $this->middleName, $this->lastName, $this->email,
                     $this->strikes, $this->lastlog) = db_row($admin, 0);

                $this->userID = intval($this->userID);
                $this->groupID = intval($this->groupID);
                $this->isActive = ($this->strikes > -1) && ($this->strikes < $auth_maxPasswordFailures);
            }
        }
        else
        {
            $this->userID = -1;
            $this->groupID = (intval($groupID) == $groupID) ? $groupID : -1;
            $this->login = $login;
            $this->password = $password;
            $this->firstName = $firstName;
            $this->middleName = $middleName;
            $this->lastName = $lastName;
            $this->email = $email;
            $this->strikes = $strikes;
            $this->isActive = ($strikes > -1) && ($strikes < $auth_maxPasswordFailures);
            $this->lastlog = $lastlog;
        }
    }

    function update()
    {
    	global $auth_hashPasswords;
        if ($this->userID < 0)
        {
            return false;
        }

        $db = db_connect();

        unset($fields);

        sql_addScalarToUpdate($fields, 'login', $this->login);
        if ($this->password)
        {
            sql_addScalarToUpdate($fields, 'password',
	                          $auth_hashPasswords ? md5($this->password) : $this->password);
        }
        sql_addScalarToUpdate($fields, 'firstName', $this->firstName);
        sql_addScalarToUpdate($fields, 'middleName', $this->middleName);
        sql_addScalarToUpdate($fields, 'lastName', $this->lastName);
        sql_addScalarToUpdate($fields, 'email', $this->email);
        sql_addIntToUpdate($fields, 'strikes', $this->strikes);

        db_startTransaction($db);

        $userRV = db_update($db, $this->dbUserTable, $fields, "{$this->dbUserIDRow} = {$this->userID}");

        if ($this->groupID > -1)
        {
            db_delete($db, $this->dbUserGroupsTable,
                      "{$this->dbUserIDRow} = {$this->userID} AND
                       {$this->dbUserGroupsIDRow} = {$this->groupID}");

            unset($fields, $values);
            sql_addBoolToUpdate($fields, 'primaryGroup', false);
            db_update($db, $this->dbUserGroupsTable, $fields, "{$this->dbUserIDRow} = {$this->userID}");

            unset($fields, $values);
            sql_addIntToInsert($fields, $values, 'userID', $this->userID);
            sql_addIntToInsert($fields, $values, 'groupID', $this->groupID);
            sql_addBoolToInsert($fields, $values, 'primaryGroup', true);
            $userGroupRV = db_insert($db, $this->dbUserGroupsTable, $fields, $values);
        }

        db_endTransaction($db);

        return ($userRV && $userGroupRV);
    }

    function add()
    {
    	global $auth_hassPasswords;
        $db = db_connect();

        unset($fields, $values);

        db_startTransaction($db);

        $userExists = db_query($db, "SELECT {$this->dbUserIDRow}
                                     FROM {$this->dbUserTable}
                                     WHERE login = '" . addslashes($this->login) . "'");

        if(db_numRows($userExists) > 0)
        {
            print_msg('ERROR', 'Login Exists',
                      'Another administrator already exists with that login', 'adobj/1');
            db_abortTransaction($db);
            return false;
        }
        else
        {
            sql_addScalarToInsert($fields, $values, 'login', $this->login);
            sql_addScalarToInsert($fields, $values, 'password',
	                          $auth_hashPasswords ? md5($this->password) : $this->password);
            sql_addScalarToInsert($fields, $values, 'firstName', $this->firstName);
            sql_addScalarToInsert($fields, $values, 'middleName', $this->middleName);
            sql_addScalarToInsert($fields, $values, 'lastName', $this->lastName);
            sql_addScalarToInsert($fields, $values, 'email', $this->email);

            $userRV = db_insert($db, $this->dbUserTable, $fields, $values);
            if ($userRV)
            {
                $this->userID = db_seqCurrentVal($db, $this->dbUserSequence);
            }

            if ($this->groupID > -1 && $this->userID > -1)
            {
                unset($fields, $values);
                sql_addIntToInsert($fields, $values, 'userID', $this->userID);
                sql_addIntToInsert($fields, $values, 'groupID', $this->groupID);
                sql_addBoolToInsert($fields, $values, 'primaryGroup', true);
                db_insert($db, $this->dbUserGroupsTable, $fields, $values);
            }

            db_endTransaction($db);
            return ($userRV > 0);
        }

        return false;
    }
}
?>
