<?php

$greetingsPage = "$common_baseURL/shopping_greetings.php";
$newGreetingPage = "$common_baseURL/shopping_newGreeting.php";
$deleteGreetingPage = "$common_baseURL/shopping_deleteGreeting.php";

function print_greetingsList($greetingID)
{
    global $greetingPage, $newGreetingPage, $deleteGreetingPage;

    $db = db_connect();

    $greetings = db_query($db, "SELECT t.greetingType, g.greetingID, g.subject, g.active
                               FROM emailGreetings g
                               LEFT JOIN greetingTypes t ON t.greetingTypeID = g.greetingTypeID
                               ORDER BY g.active DESC, g.subject;");
    $numGreetings = db_numRows($greetings);

    print '<table class=box cellpadding=3px cellspacing=0px width=500px>';
    print "<tr><th class=largeColoredHeader colspan=4>Email Greetings</th></tr>";

    $odd = true;
    $onActives = true;
    for ($i = 0; $i < $numGreetings; ++$i)
    {
        list($greetingType, $greetingID, $subject, $active) = db_row($greetings, $i);
        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;

        if ($onActives && !db_boolean($active))
        {
            $onActives = false;
            print "<tr><th class=largeHeader colspan=3>Inactive $title</th></tr>";
        }

        print "<tr><td $class width=100%>$greetingType - $subject</a></td>";
        print "<td $class><a href=\"$newGreetingPage?editting=1&greetingID=$greetingID\" class=\"editLink\">Edit&nbsp;$type</a></td>";
        print "<td $class><a href=\"$deleteGreetingPage?greetingID=$greetingID\" class=\"editLink\">Delete&nbsp;$type</td
                                                                                                                  >";
        print "</tr>";
    }

    if ($numGreetings < 1)
    {
        print "<tr><th colspan=4>No email greetings!<p><a href=\"$newGreetingPage\">Click here to create one.</a></th></tr>";
        print '</table>';
    }

    print '</table>';

    print "<br><a href=\"$newGreetingPage\">Create a new email greeting</a>";
}

function print_greetingForm()
{
    global $thisPage, $saveNow;
    global $greetingID, $greetingTypeID;
    global $fromAddress, $replyAddress, $internalCC;
    global $subject, $greetingIsActive, $greeting, $closing;

    $db = db_connect();

    print_form($thisPage, 'greetingForm');

    print_hidden('greetingID', $greetingID);
    print_hidden('saveNow', 1);

    print '<table class=box cellpadding=3px cellspacing=0px>';
    print "<tr><th class=largeColoredHeader colspan=2>Add a new email greeting</th></tr>";

    $types = db_query($db, "SELECT greetingTypeID, greetingType FROM greetingTypes;");

    print "<tr><td>Greeting type : </td><td>";
    print_selectQuery('greetingTypeID', $types, $greetingTypeID);
    print '</td></tr>';

    print "<tr><td>Email subject : </td><td>";
    print_textbox('subject', $subject);
    print '</td></tr>';

    print "<tr><td>From address : </td><td>";
    print_textbox('fromAddress', $fromAddress);
    print '</td></tr>';

    print "<tr><td>Reply address : </td><td>";
    print_textbox('replyAddress', $replyAddress);
    print '</td></tr>';

    print "<tr><td>Internal CC : </td><td>";
    print_textbox('internalCC', $internalCC);
    print '</td></tr>';

    print '<tr><td></td><td>';
    print_checkbox('greetingIsActive', 'Is Active', db_toBoolean(true), db_boolean($greetingIsActive));
    print '</td></tr>';

    print "<tr><td>Email greeting: </td><td>";
    print_textarea('greeting', $greeting);
    print '</td></tr>';

    print "<tr><td>Email closing: </td><td>";
    print_textarea('closing', $closing);
    print '</td></tr>';

    print '<tr><td></td><td>';
    print_submit('submit', $buttonText);
    print '</td></tr>';

    print '</table>';
    print '</form>';
}

function validateGreeting()
{
    global $subject, $fromAddress, $replyAddress;

    $subject = trim($subject);
    if (!$subject)
    {
        print_msg('ERROR', "Email Subject Required",
                  'A subject line is required. Please enter a subject below.', 'NewGreet/4');
        return false;
    }

    $fromAddress = trim($fromAddress);
    if (!$fromAddress)
    {
        print_msg('ERROR', "Email From Address",
                  'An email address that will show in the from field is required. Please enter an address below.', 'NewGreet/4');
        return false;
    }

    $replyAddress = trim($replyAddress);
    if (!$replyAddress)
    {
        print_msg('ERROR', "Email Reply-to Required",
                  'An email address used when replied to is required. Please enter an address below.', 'NewGreet/4');
        return false;
    }

    return true;
}

function addGreeting()
{
    global $greetingTypeID;
    global $fromAddress, $replyAddress, $internalCC;
    global $subject, $greetingIsActive, $greeting, $closing;

    print "<h3>Adding $type ...</h3>";

    $db = db_connect();

    unset($fields, $values);

    $greetingID = db_seqNextVal($db, 'seq_greetings');

    if (db_boolean($greetingIsActive))
    {
        db_query($db, "UPDATE emailGreetings SET active = false
                      WHERE greetingTypeID = '$greetingTypeID';");
    }

    sql_addIntToInsert($fields, $values, 'greetingID', $greetingID);
    sql_addIntToInsert($fields, $values, 'greetingTypeID', $greetingTypeID, true);
    sql_addScalarToInsert($fields, $values, 'subject', $subject);
    sql_addScalarToInsert($fields, $values, 'fromAddress', $fromAddress);
    sql_addScalarToInsert($fields, $values, 'replyAddress', $replyAddress);
    sql_addScalarToInsert($fields, $values, 'internalCC', $internalCC);
    sql_addScalarToInsert($fields, $values, 'greeting', $greeting);
    sql_addScalarToInsert($fields, $values, 'closing', $closing);
    sql_addBoolToInsert($fields, $values, 'active', db_boolean($greetingIsActive));

    // insert a new greeting
    db_insert($db, 'emailGreetings', $fields, $values);
}

function updateGreeting()
{
    global $greetingID, $greetingTypeID;
    global $fromAddress, $replyAddress, $internalCC;
    global $subject, $greetingIsActive, $greeting, $closing;

    $db = db_connect();

    print "<h3>Saving $subject ...</h3>";

    if (db_boolean($greetingIsActive))
    {
        db_query($db, "UPDATE emailGreetings SET active = false
                      WHERE greetingTypeID = '$greetingTypeID';");
    }

    unset($fields, $where);

    sql_addIntToUpdate($fields, 'greetingTypeID', $greetingTypeID, true);
    sql_addScalarToUpdate($fields, 'subject', $subject);
    sql_addScalarToUpdate($fields, 'fromAddress', $fromAddress);
    sql_addScalarToUpdate($fields, 'replyAddress', $replyAddress);
    sql_addScalarToUpdate($fields, 'internalCC', $internalCC);
    sql_addScalarToUpdate($fields, 'greeting', $greeting);
    sql_addScalarToUpdate($fields, 'closing', $closing);
    sql_addBoolToUpdate($fields, 'active', db_boolean($greetingIsActive));

    db_update($db, 'emailGreetings', $fields, "greetingID = $greetingID");
}

?>
