<?php

include_once("$phpfwkIncludePath/images.php");
include_once("$phpfwkIncludePath/admin/include/shopping_catalogs.php");

$thisPage = $newItemPage;

$gID = intval($gID);
$sku = intval($sku);

print_adminHeader();
print "<div style='padding: 3px;'>";

if ($gID < 1)
{
    print_msg('ERROR', 'Group Required',
              'A group is required to create or edit an item .', 'NewItem/1');
    print_footer();
    exit;
}

$db = db_connect();
$groupInfo = db_query($db, "SELECT active, name FROM skuGroups
                           WHERE groupID = $gID;");

if (db_numRows($groupInfo) < 1)
{
    print_msg('ERROR', 'Group Does Not Exist',
              'The requested group could not be found. Perhaps someone deleted it?', 'NewItem/2');
    print_footer();
    exit;
}

list($isGroupActive, $groupName) = db_row($groupInfo, 0);

if ($saveNow)
{
    if (validateItem($gID, $sku))
    {
        if ($sku > 0)
        {
            updateItem($gID, $sku);
        }
        else
        {
            addItem($gID);
        }

        print_redirect("$catalogPage?gID=$gID");
        //print_link("$catalogPage?gID=$gID", "$catalogPage?gID=$gID");
    }
    else
    {
        print_itemForm($gID, $sku);
    }
}
else
{
    print_itemForm($gID, $sku);
}

print "</div>";
print_adminFooter();

?>
