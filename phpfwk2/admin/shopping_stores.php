<?php

$thisPage = "$common_baseURL/shopping_stores.php";

function print_storeList()
{
    global $thisPage;
    $db = db_connect();
    $stores = db_query($db, "select s.storeID, s.name, cu.longname, c.name from stores s left join clerks c on s.clerk = c.clerkID left join currencies cu on s.currency = cu.currency order by lower(s.name);");
    $numStores = db_numRows($stores);

    if ($numStores > 0)
    {
        print "<a href=\"$thisPage?new=1\">Create a new store</a>";
    }

    print '<table border=0 class=box cellpadding=3px cellspacing=0px width=500px>';
    print "<tr><th class=largeColoredHeader colspan=3>Stores</th></tr>";

    if ($numStores > 0)
    {
        print "<tr><td class=cmsMicroHeader>Store</td><td class=cmsMicroHeader>Default Currency</td><td class=cmsMicroHeader>Shipping</td></tr>";
    }

    $odd = true;
    for ($i = 0; $i < $numStores; ++$i)
    {
        list($storeID, $name, $currency, $clerk) = db_row($stores, $i);

        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;

        print "<tr>";
        print "<td $class valign=top><a href=\"$thisPage?storeID=$storeID\">$name</a></td>";
        print "<td $class valign=top nowrap>$currency</td>";
        print "<td $class valign=top nowrap>$clerk</td>";
        print "</tr>";
    }

    if ($numStores < 1)
    {
        print "<tr><th colspan=3>No Stores<p><a href=\"$thisPage?new=1\">Click here to create a new store.</a></th></tr>";
    }

    print '</table>';

    if ($numStores > 0)
    {
        print "<a href=\"$newItemPage?new=1\">Create a new store</a>";
    }
}

function editStore($storeID = -1)
{
    global $thisPage, $saveChanges;
    global $name, $currency, $clerk, $catalogs;

    $new = ($storeID < 0);
    $db = db_connect();

    if (!$new)
    {
        $storeQuery = db_query($db, "select s.storeID, s.name, s.currency, s.clerk from stores s  where storeID = $storeID;");
        if (db_numRows($storeQuery) < 1)
        {
            print_msg('ERROR', 'Store Does Not Exist', "Could not find the requested store (ID#$storeID). Perhaps someone deleted it?", 'store/1');
            print_storeList();
            return;
        }

        if (!$saveChanges)
        {
            list($storeID, $name, $currency, $clerk) = db_row($storeQuery, 0);
            $current = db_query($db, "select skuGroup from storeCatalogs where storeID = $storeID;");
            $numCurrent = db_numRows($current);
            $catalogs = array();
            for ($i = 0; $i < $numCurrent; ++$i)
            {
                list($skuGroup) = db_row($current, $i);
                $catalogs[$skuGroup] = $skuGroup;
            }
        }
    }

    print_form($thisPage);
    if ($new)
    {
        print_hidden('createStore', 1);
    }
    else
    {
        print_hidden('storeID', $storeID);
    }

    $clerks = db_query($db, "select clerkID, name from clerks order by lower(name);");
    $currencies = db_query($db, "select currency, longName from currencies order by lower(currency);");

    print '<table class=box cellspacing=0 cellpaddig=4 border=0>';
    print '<tr><th class="coloredHeader" colspan=2>Store Setup</th></tr>';

    print "<tr><td class=\"oddRow\" align=top nowrap>Name</td><td class=\"oddRow\">";
    print_textbox('name', $name);
    print '</td></tr>';

    print "<tr><td class=\"evenRow\" align=top nowrap>Default Currency</td><td class=\"evenRow\">";
    print_selectQuery('currency', $currencies, $currency);
    print '</td></tr>';

    print "<tr><td class=\"oddRow\" align=top nowrap>Shipping</td><td class=\"oddRow\">";
    print_selectQuery('clerk', $clerks, $clerk);
    print '</td></tr>';

    print '<tr><th class="coloredHeader" colspan=2>Catalogs</th></tr>';
    print '<tr><th class="cmsMicroheader" colspan=2>Select which catalog(s) to include in this store</th></tr>';
    $catalogQuery = db_query($db, "select groupID, name from skuGroups where parent is null order by displayOrder, lower(name);");
    $numCats = db_numRows($catalogQuery);
    $odd = true;
    for ($i = 0; $i < $numCats; ++$i)
    {
        list($groupID, $name) = db_row($catalogQuery, $i);
        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;
        print "<tr><td class=$class align=top nowrap colspan=2>";
        print_checkbox("catalogs[$groupID]", "$name", $groupID, isset($catalogs[$groupID]));
        print "</td></tr>";
    }


    print '<tr><td style="border-top: 1px solid;"></td><td style="border-top: 1px solid;">';
    print_submit('saveChanges', 'Save Store');
    print '</td></tr></table>';
    print "</form>";
}

function validateStore()
{
    global $name, $clerk, $currency, $catalogs;

    $name = trim($name);
    if (!$name)
    {
        print_msg('ERROR', 'Name Required', 'A name is required for all stores. Please enter one below and try again.', 'store/2');
        return false;
    }

    return true;
}

function saveStore($storeID)
{
    global $name, $clerk, $currency, $catalogs;

    unset($fields);
    sql_addScalarToUpdate($fields, 'name', $name);
    sql_addScalarToUpdate($fields, 'currency', $currency);
    sql_addIntToUpdate($fields, 'clerk', $clerk);

    $db = db_connect();
    db_startTransaction($db);
    db_update($db, 'stores', $fields, "storeID = $storeID");
    db_delete($db, 'storeCatalogs', "storeID = $storeID");
    if (is_array($catalogs))
    {
        foreach ($catalogs as $catalog)
        {
            unset($fields, $values);
            sql_addIntToInsert($fields, $values, 'skuGroup', $catalog);
            if (isset($fields))
            {
                sql_addIntToInsert($fields, $values, 'storeID', $storeID);
                db_insert($db, 'storeCatalogs', $fields, $values);
            }
        }
    }
    db_endTransaction($db);
}

function createStore($storeID)
{
    global $name, $clerk, $currency, $catalogs;

    unset($fields, $values);
    sql_addScalarToInsert($fields, $values, 'name', $name);
    sql_addScalarToInsert($fields, $values, 'currency', $currency);
    sql_addIntToInsert($fields, $values, 'clerk', $clerk);

    $db = db_connect();
    db_startTransaction($db);
    db_insert($db, 'stores', $fields, $values);
    $storeID = db_seqCurrentVal($db, 'seq_stores');
    if (is_array($catalogs))
    {
        foreach ($catalogs as $catalog)
        {
            unset($fields, $values);
            sql_addIntToInsert($fields, $values, 'skuGroup', $catalog);
            if (isset($fields))
            {
                sql_addIntToInsert($fields, $values, 'storeID', $storeID);
                db_insert($db, 'storeCatalogs', $fields, $values);
            }
        }
    }
    db_endTransaction($db);
    return $storeID;
}

$storeID = intval($storeID);

print_adminHeader();
print '<div style="padding: 3px">';

if ($saveChanges && $storeID > 0)
{
    if (validateStore())
    {
        saveStore($storeID);
    }
    editStore($storeID);
}
else if ($createStore)
{
    if (validateStore())
    {
        $storeID = createStore($storeID);
    }
    editStore($storeID);
}
else if ($storeID > 0)
{
    editStore($storeID);
}
else if ($new == 1)
{
    editStore();
}
else
{
    print_storeList();
}

print '</div>';
print_adminFooter();

?>
