<?
include_once("$phpfwkIncludePath/admin/include/administratorObj.php");
include_once("$phpfwkIncludePath/address.php");

// require super user access
requiresSuper();

$thisPage = 'userAdmin.php';
$tableWidth = 400;
$minLoginLength = 4;
$minPasswordLength = 5;

define(USERADMIN_EDITUSER, 1);
define(USERADMIN_NEWUSER, 2);
define(USERADMIN_UPDATE, 3);
define(USERADMIN_NEW, 4);

function printList()
{
    global $thisPage, $tableWidth, $common_bizName;
    $db = db_connect();
    $admins = db_query($db, "SELECT userID, COALESCE(lastName || ', ', '') || COALESCE(firstName, '')  || ' [' || COALESCE(login, '(none)') || ']' FROM users WHERE userID > 0 ORDER BY lower(lastName);");

    print "<h3 class=\"areaheader\">$common_bizName Administrators...</h3>";

    if (db_numRows($admins) > 0)
    {
        print_form($thisPage);
        print_hidden('editMode', USERADMIN_EDITUSER);

        print "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\"  width=\"$tableWidth\">";
        print '<tr>';

        print '<td>';
        print_selectQuery('userID', $admins);
        print '</td>';

        print '<td align="right">';
        print_submit("btnEdit", "<< Edit User");
        print '</td>';

        print '</tr>';
        print '</table>';

        print '</form>';
        print '<p>';
    }
    
    print_form($thisPage, 'frmUserAdmin');
    print_hidden('editMode', USERADMIN_NEWUSER);

    print '<table border="0" cellpadding="3" cellspacing="0"  width="">';
    print '<tr>';
    print '<td colspan="2">';
    print_submit("btnAdd", "Add A New User");
    print '</td>';
    print '</tr>';
    print '</table>';
    print '</form>';
}

function printUserForm($userID = -1)
{
    global $maxPasswordFailures, $thisPage, $tableWidth;

    $new = $userID < 0;
    $admin = new Administrator($userID);

    $title = $new ? 'Create New Administrator' : "Edit Administrator '{$admin->name}'";
    $db = db_connect();
    $groups = db_query($db, "SELECT groupID, name FROM groups ORDER BY name;");

    print_form($thisPage);

    print "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\"  class=\"box\" width=\"$tableWidth\">";
    print "<tr><td colspan=\"2\" class=\"largeColoredHeader\">$title</td></tr>";

    print '<tr><td class="align2">Login:</td>';
    print '<td>';
    print_textbox("login", $admin->login, 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">First Name:</td>';
    print '<td>';
    print_textbox("firstName", $admin->firstName, 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">Middle Name:</td>';
    print '<td>';
    print_textbox("middleName", $admin->middleName, 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">Last Name:</td>';
    print '<td>';
    print_textbox("lastName", $admin->lastName, 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">Email:</td>';
    print '<td>';
    print_textbox("email", $admin->email, 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">Group:</td>';
    print '<td>';
    print_selectQuery("groupID", $groups, $admin->groupID);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">Activation Status:</td>';
    print '<td>';
    ?>
    <input type="radio" id="strikes" name="strikes" value="<?php $admin->isActive ? 0 : $admin->strikes ?>" <? if ($admin->isActive) { print " checked";} ?>> active </input>
    <input type="radio" id="strikes" name="strikes" value="9999" <? if (!$admin->isActive) { print " checked";} ?>> not active</input>
    <?
    print '<td>';
    print '</tr>';

    if (!$new)
    {
        print '<tr><td class="align2">Consecutive Failed Logins:</td>';
        print "<td>{$admin->strikes} (max $maxPasswordFailures allowed)</td>";
        print '</tr>';

        print '<tr><td class="align2">Last Login Date:</td>';
        print "<td>{$admin->lastlog}</td>";
        print '</tr>';
    }

    print '<tr>';
    print '<td colspan="2" class="largeColoredHeader">Password</td>';
    print '</tr>';

    if (!$new)
    {
        print '<tr>';
        print '<td colspan="2">To edit the password, enter a password and confirm below. To leave the password as is, leave these fields blank.</td>';
        print '</td>';
        print '</tr>';
    }

    print '<tr><td class="align2">Password:</td>';
    print '<td>';
    print_passwordbox("password", "", "", 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr><td class="align2">Confirm Password:</td>';
    print '<td>';
    print_passwordbox("password_confirm", "", "", 25, 100);
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td></td>';
    print '<td>';

    if ($new)
    {
        print_submit("btnEdit", "Create");
        print_hidden('editMode', USERADMIN_NEW);
    }
    else
    {
        print_submit("btnEdit", "Save");
        print_hidden('editMode', USERADMIN_UPDATE);
        print_hidden('userID', $admin->userID);
    }

    print '</td>';
    print '</tr>';
    print '</table>';
    print '</form>';
}

print_adminHeader('Administrators');
print '<div style="padding: 3px;">';

$userID = (strlen($userID) > 0 && intval($userID) == $userID) ? intval($userID) : -1;;
$editMode = intval($editMode);

/*
 * $editMode states
 * (1) admin listing has been submitted, show edit form
 * (2) edit form has been submitted, validate data and update data
 * (3) add new admin has been selected, show add form
 * (4) a new addition has been submitted, validate data and insert
 * print "| $editMode |";
 */

if ($editMode == USERADMIN_NEWUSER)
{
    printUserForm();
}
else if ($editMode == USERADMIN_EDITUSER)
{
    printUserForm($userID);
}
else if ($editMode == USERADMIN_UPDATE)
{
    if (empty($login) || strlen($login) < $minLoginLength)
    {
        print_msg('ERROR', 'Login Too Short',
                  "The login that was entered is invalid ({$minLoginLength} character minimum).", 'usradm/7');
        printUserForm($userID);
    }
    else if (empty($firstName) || empty($lastName))
    {
        print_msg('ERROR', 'Invalid Name',
                  "A first name and last name is required.", 'usradm/8');
        printUserForm($userID);
    }
    else if ($email && !validateEmail($email))
    {
        print_msg('ERROR', 'Invalid Email Address',
                  "The email address that was entered is invalid.", 'usradm/9');
        printUserForm($userID);
    }
    else if ($password != $password_confirm)
    {
        print_msg('ERROR', 'Passwords Do Not Match',
                  "The password confirmation was unsuccessful.", 'usradm/10');
        printUserForm($userID);
    }
    else if ((strlen($password) > 0) && (strlen($password) < $minPasswordLength))
    {
        print_msg('ERROR', 'Password Too Short',
                  "The password field must be a minimum of {$minPasswordLength} characters.", 'usradm/11');
        printUserForm($userID);
    }
    else
    {
        $admin = new Administrator(-1, intval($groupID), trim($login), trim($password),
                                   $firstName, $middleName, $lastName, $email, $strikes);
        $admin->userID = $userID;

        if ($admin->update())
        {
            print_msg('INFORMATION', 'Status Message',
                      'The administrator account was successful updated.', 'usradm/12');
            printList($statusMsg);
        }
        else
        {
            print_msg('ERROR', 'Status Message', 
                      'The update failed, please inform the system administrator with as much troubleshooting information as possible.', 'usradm/13');
            printList($statusMsg);
        }
    }
}
else if ($editMode == USERADMIN_NEW)
{
    if (empty($login) || strlen($login) < $minLoginLength)
    {
        print_msg('ERROR', 'Invalid Login',
                  "The login that was entered is invalid ({$minLoginLength} character minimum).", 'usradm/1');
        printUserForm();
    }
    else if ($password != $password_confirm)
    {
        print_msg('ERROR', 'Bad Password Confirmation',
                  "The password confirmation was unsuccessful.", 'usradm/2');
        printUserForm();
    }
    else if ((strlen($password) > 0) && (strlen($password) < $minPasswordLength))
    {
        print_msg('ERROR', 'Bad Password Length',
                  "The password field must be a minimum of {$minPasswordLength} characters.", 'usradm/3');
        printUserForm();
    }
    else if ((empty($firstName) || empty($lastName)) ||
             strlen($firstName) < $minNameLength ||
             strlen($lastName) < $minNameLength)
    {
        print_msg('ERROR', 'Invalid Name',
                  "The first and last names entered are invalid ({$minNameLength} character minimum).", 'usradm/3');
        printUserForm();
    }
    else if (! validateEmail($email))
    {
        print_msg('ERROR', 'Invalid Email Address',
                  "The email address that was entered is invalid.", 'usradm/4');
        printUserForm();
    }
    else
    {
        $admin = new Administrator(-1, intval($groupID), trim($login), trim($password),
                                   $firstName, $middleName, $lastName, $email);

        if ($admin->add())
        {
            print_msg('INFORMATION', 'Status Message',
                      "The administrator was added successfully.", 'usradm/5');
            printList();
        }
        else
        {
            print_msg('ERROR', 'Status Message', 
                      "The attempt to add a new administrator failed. Please contact the system administrator with as much troubleshooting information as possible..", 'usradm/6');
            printList();
        }
    }
}
else
{
    printList();
}

print '</div>';
print_adminFooter();

?>
