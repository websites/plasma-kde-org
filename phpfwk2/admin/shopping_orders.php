<?php

include_once("$phpfwkIncludePath/shopping.php");
include_once("$phpfwkIncludePath/shippingClerks.php");
include_once("$phpfwkIncludePath/shoppingTaxman.php");

$thisPage = "$common_baseURL/shopping_orders.php";

function list_allCarts()
{
    global $thisPage;

    $db = db_connect();
    $orders = db_query($db, "SELECT cs.statID, max(cs.status), count(carts.cartID)
                            FROM cartStatus cs
                            LEFT JOIN shoppingCarts carts ON (cs.statID = carts.status)
                            GROUP BY cs.statID ORDER BY cs.statID;");
    $numOrders = db_numRows($orders);

    print '<table class=box cellpadding=3px cellspacing=0px>';
    print '<tr><th class=largeColoredHeader colspan=2>Orders In System</th></tr>';
    print '<tr><th align=left>Status</th><th align=right>#</th></tr>';

    $odd = true;
    for ($i = 0; $i < $numOrders; ++$i)
    {
        list($statID, $status, $count) = db_row($orders, $i);

        if ($count < 1)
        {
            $count = '-';
        }

        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;
        print "<tr><td $class><a href=$thisPage?list=1&status=$statID>$status</a></td><td $class align=right>$count</td></tr>";
    }

    print '</table>';
}

function list_statusCarts($status)
{
    $db = db_connect();

    $statusTitle = 'Unknown';
    $stats = db_query($db, "SELECT status FROM cartStatus WHERE statID = $status;");

    if (db_numRows($stats) > 0)
    {
        list($statusTitle) = db_row($stats, 0);
    }

    print '<table class=box cellpadding=3px cellspacing=0px>';
    print "<tr><th class=largeColoredHeader colspan=100%>Shopping Carts $statusTitle</th></tr>";
    print '<tr><th align=left>Cart ID</th><th align=left>Date Created</th><th align=right>Total</th></tr>';

    $carts = db_query($db, "SELECT cartID, to_char(created, 'Month DD, YYYY'),
                                  currency, cart_total(cartID, currency)
                           FROM shoppingCarts WHERE status = $status ORDER BY created;");
    $numCarts = db_numRows($carts);

    $odd = true;
    $grandTotals = array();
    for ($i = 0; $i < $numCarts; ++$i)
    {
        list($cartID, $created, $currency, $total) = db_row($carts, $i);

        $grandTotals[$currency] += $total;
        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;
        print "<tr><td $class><a href=$thisPage?cartID=$cartID&status=$status>$cartID</a></td><td $class>$created</td><td $class align=right>$total ($currency)</td></tr>";
    }

    if (count($grandTotals) > 0)
    {
        print "<tr><td colspan=2 align=right valign=top style=\"border-top: 1px black solid;\"><strong>Totals</strong></td>";
        print "<td style=\"border-top: 1px black solid;\" align=right valign=top>";
        foreach ($grandTotals as $currency => $total)
        {
            print "$total ($currency)<br>";
        }
        print '</td></tr>';
    }
    
    print '</table>';
}

function admin_cart()
{
    global $common_bizName;
    global $cartID, $status;

    $db = db_connect();


    print '<div style="font-size: 8pt;" >';
    print_form("$form", 'statusForm', false, 'post', 'style="margin: 0px; padding: 0px;"');

    $stats = db_query($db, "SELECT statID, status FROM cartStatus;");

    print 'Status: ';
    print_selectQuery('status', $stats, $status, true,
                      "onChange='document.statusForm.submit()' style=\"font-size: 8pt;\"");
    print_hidden('changeStatus', 1);
    print_hidden('cartID', $cartID);
    print'</form></div>';

    global $checkout;
    $checkout = true;
    print_cartInfo($thisPage);

    $purchaseInfo = db_query($db, "SELECT ccName, ccNo, currency,
                                         orderedName, orderedEmail, orderedPhone,
                                         shipName, shipStreet1, shipStreet2,
                                         shipCity, shipCountry, shipPostal
                                  FROM shoppingCarts
                                  WHERE cartID = $cartID;");

    if (db_numRows($purchaseInfo) > 0)
    {
        list($ccName, $ccNo, $currency, $orderedName, $orderedEmail, $orderedPhone,
             $shipName, $shipStreet1, $shipStreet2, $shipCity,
             $shipCountry, $shipPostal) = db_row($purchaseInfo, 0);
    }
    else
    {
        return;
    }

    print '<br>';
    print "<table class=box width=100% cellpadding=3px cellspacing=0px>";
    print "<tr><th class=largeColoredHeader colspan=100%>Purchase / Shipping Information</th></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print "<tr><td><b>Ordered by : </b>$orderedName</td></tr>";
    print "<tr><td><b>Email : </b>$orderedEmail</td></tr>";
    print "<tr><td><b>Phone : </b>$orderedPhone</td></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print "<tr><td><b>Ship to : </b>$shipName</td></tr>";
    print "<tr><td>$shipStreet1</td></tr>";
    print "<tr><td>$shipStreet2</td></tr>";
    print "<tr><td>$shipCity</td></tr>";
    print "<tr><td>$shipCountry</td></tr>";
    print "<tr><td>$shipPostal</td></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print "<tr><td><b>Charged to : </b>$ccName</td></tr>";
    print "<tr><td><b>Card number : </b>$ccNo</td></tr>";
    print "<tr><td colspan=100%>&nbsp;</td></tr>";

    print '</table>';
}

function shippedEmail($email)
{
    global $cartID, $phpfwk_sendmailPath, $phpfwk_sendmailArgs;;

    $GREETING_TYPE = 0;

    $db = db_connect();

    $greetingInfo = db_query($db, "SELECT subject, fromAddress, replyAddress, greeting, closing
                                  FROM emailGreetings
                                  WHERE greetingTypeID = '$GREETING_TYPE' AND active = true;");

    if (db_numRows($greetingInfo) > 0)
    {
        list($subject, $fromAddress, $replyAddress, $greeting, $closing) = db_row($greetingInfo, 0);
    }
    else
    {
        return;
    }

    $recipients = "$email";

    $headers['From']            =   $fromAddress;
    $headers['To']              =   $recipients;
    $headers['Reply-To']        =   $replyAddress;
    $headers['Subject']         =   $subject;
    $headers['Content-type']    =   "text/html; charset=iso-8859-1";

    $emailMessage =
    "
    <html>
    <body>
    <p>$greeting</p>";

    /*
    $items = db_query($db, "SELECT c.oid, c.itemID, c.sku, s.name, s.shortDesc,
                                  c.quantity, c.price, c.convertPrice,
                                  round((c.price * c.quantity), 2) as cadtotal,
                                  round((c.convertPrice * c.quantity), 2) as total
                           FROM shoppingCartItems c
                           LEFT JOIN skus s ON s.sku = c.sku
                           WHERE c.cartID = $cartID
                           ORDER BY s.name;");

    $numItems = db_numRows($items);

    if ($numItems)
    {
        $emailMessage .=
        "
        <br>
        <table width=100% cellpadding=3px cellspacing=0px>
        <tr><th colspan=100%>$common_bizName Shopping Cart #$cartID &nbsp;</th></tr>
        <tr><th align=left>Item</th><th align=left>Quantity</th>
            <th align=left>Price</th><th align=left>Total</th></tr>";

        for ($i = 0; $i < $numItems; $i++)
        {
            list($oid, $itemID, $sku, $name, $shortDesc, $quantity,
                 $price, $convertPrice, $cadtotal, $total) = db_row($items, $i);

            $emailMessage .=
            "
            <tr>
            <td valign=top><b>$name</b> $shortDesc</td>
            <td valign=top>$quantity</td>
            <td valign=top>$ $convertPrice</td>
            <td valign=top>$ $total</td>
            </tr>";

            $opts = db_query($db, "SELECT co.optionID, so.name, sot.name
                                  FROM shoppingCartItemOptions co
                                  LEFT JOIN skuOptions so ON co.optionID = so.optionID
                                  LEFT JOIN skuOptiontypes sot ON so.type = sot.optionTypeID
                                  WHERE co.itemID = $itemID;");

            $numOpts = db_numRows($opts);

            $emailMessage .=
            "
            <tr>
            <td valign=top colspan=100%>
            <b>Details : </b>";

            $q = 0;
            while ($q < $numOpts)
            {
                list($optID, $optName, $optTypeName) = db_row($opts, $j);
                $emailMessage .= "$optTypeName : $optName";
                $q++;

                if ($q != $numOpts)
                {
                    $emailMessage .= ', ';
                }
            }

            $emailMessage .=
            '
            </td>
            </tr>';

            $grandcadtotal += $total;
            $grandtotal += $total;
        }

        $taxes = db_query($db, "SELECT taxRate1, shipping FROM shoppingCarts
                               WHERE cartID = $cartID;");

        if (db_numRows($taxes) > 0)
        {
            $emailMessage .= "<tr><td colspan=100%>&nbsp;</td></tr>";

            list($tax, $shipping) = db_row($taxes, 0);

            if (! is_null($shipping))
            {
                $grandtotal += $shipping;
                $emailMessage .=
                "
                <tr><td colspan=3><b>Shipping :</b></td>
                    <td>$ " . number_format($shipping, 2, '.', ',') . "</td></tr>";
            }

            if (! is_null($tax))
            {
                $totaltax = $grandtotal * $tax;
                $grandtotal += $totaltax;
                $emailMessage .=
                "
                <tr><td colspan=3><b>Taxes :</b></td>
                    <td>$ " . number_format($totaltax, 2, '.', ',') . "</td></tr>";
            }
        }

        if ($grandtotal)
        {
           $emailMessage .=
           "
           <tr><td colspan=100%>&nbsp;</td></tr>
           <tr><td colspan=3><b>Total Cost :</b></td>
               <td><b>$ " . number_format($grandtotal, 2, '.', ',') . "</b></td></tr>";
        }

        $emailMessage .=
        "
        </table>";

    }
    */

    $emailMessage .= cartInfoHtml($cartID);

    $purchaseInfo = db_query($db, "SELECT ccName, overlay(ccNo placing 'XXXXXXXXXXXX' from 3 for 12), currency,
                                         orderedName, orderedEmail, orderedPhone,
                                         shipName, shipStreet1, shipStreet2,
                                         shipCity, shipprovince, shipCountry, shipPostal
                                  FROM shoppingCarts
                                  WHERE cartID = $cartID;");

    if (db_numRows($purchaseInfo) > 0)
    {
        list($ccName, $ccNo, $currency, $orderedName, $orderedEmail, $orderedPhone,
             $shipName, $shipStreet1, $shipStreet2, $shipCity, $shipProvince,
             $shipCountry, $shipPostal) = db_row($purchaseInfo, 0);

        $emailMessage .=
        "<br>
        <table class=box width=100% cellpadding=3px cellspacing=0px>
        <tr><th class=largeColoredHeader colspan=100%>Purchase / Shipping Information</th></tr>
        <tr><td colspan=100%>&nbsp;</td></tr>
        <tr><td><b>Ordered by : </b>$orderedName</td></tr>
        <tr><td><b>Email : </b>$orderedEmail</td></tr>
        <tr><td><b>Phone : </b>$orderedPhone</td></tr>
        <tr><td colspan=100%>&nbsp;</td></tr>
        <tr><td><b>Ship to : </b>$shipName</td></tr>
        <tr><td>$shipStreet1</td></tr>
        <tr><td>$shipStreet2</td></tr>
        <tr><td>$shipCity</td></tr>
        <tr><td>$shipProvince</td></tr>
        <tr><td>$shipCountry</td></tr>
        <tr><td>$shipPostal</td></tr>
        <tr><td colspan=100%>&nbsp;</td></tr>
        <tr><td><b>Charged to : </b>$ccName</td></tr>
        <tr><td><b>Card number : </b>$ccNo</td></tr>
        <tr><td colspan=100%>&nbsp;</td></tr>
        </table>";
    }

    $emailMessage .=
    "
    <br>
    <p>$closing</p>
    </body>
    </html>";

    $sendmail_params['sendmail_path'] = $phpfwk_sendmailPath;
    $sendmail_params['sendmail_args'] = $phpfwk_sendmailArgs;

    $mail_object =& Mail::factory('sendmail', $sendmail_params);
    $mail_object->send($recipients, $headers, $emailMessage);
}

function statusChange()
{
    global $cartID, $status;

    $db = db_connect();

    if ($status == 4)
    {
        $row = db_query($db, "SELECT confirmed, orderedEmail FROM shoppingCarts WHERE cartID = $cartID;");
        if (db_numRows($row) > 0)
        {
            list($confirmed, $orderedEmail) = db_row($row, 0);

            if (db_boolean($confirmed))
            {
                print_msg('INFO', 'Shipping Confirmation', 'A shipping confirmation email has already been sent for this order.  A new one will NOT be sent again');
            }
            else
            {
                shippedEmail($orderedEmail);
                db_query($db, "UPDATE shoppingCarts SET confirmed = true WHERE cartID = $cartID;");
                print_msg('INFO', 'Shipping Confirmation', 'A shipping confirmation email will be sent for this order.');
            }
        }
    }

    db_query($db, "UPDATE shoppingCarts SET status = $status WHERE cartID = $cartID;");
}

print_adminHeader();
print "<div style=\"margin: 3px;\">";

global $cartID, $status;

$cartID = intval($cartID);
$status = intval($status);

if ($list)
{
    list_statusCarts($status);
}
else if ($cartID)
{
    if ($changeStatus)
    {
        statusChange();
    }
    admin_cart();
}
else
{
    list_allCarts();
}

print "</div>";
print_adminFooter();

?>
