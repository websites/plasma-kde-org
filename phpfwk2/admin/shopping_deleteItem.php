<?php

include_once("$phpfwkIncludePath/images.php");
include_once("$phpfwkIncludePath/admin/include/shopping_catalogs.php");

$gID = intval($gID);
$catID = intval($catID);
$sku = intval($sku);

print_adminHeader();
print '<div style="padding: 3px">';

if ($gID < 1)
{
    print_msg('ERROR', 'Group ID Required',
              'A group id is required to delete an item .', 'DelItem/1');
    print_adminFooter();
    exit;
}

if ($sku < 1)
{
    print_msg('ERROR', 'Item ID Required',
              'An item id is required to identify the item for deletion.', 'DelItem/5');
    print_adminFooter();
    exit;
}

$db = db_connect();
$itemInfo = db_query($db, "SELECT active, name FROM skus
                          WHERE groupID = $gID AND sku = $sku;");

if (db_numRows($itemInfo) < 1)
{
    print_msg('ERROR', 'Item does not exists',
              'The item could not be found. Perhaps someone deleted it?.', 'DelItem/2');
    print_adminFooter();
    exit;
}

list($itemIsActive, $itemName) = db_row($itemInfo, 0);

unset($activeText);
if (db_boolean($groupIsActive))
{
    $activeText = '<p><strong>This item is active!</strong></p>';
}

if ($deleteNow == 1)
{
    $thumbPath = imagePath('skus', 'smallImg', 'sku', $sku);
    $imgPath = imagePath('skus', 'largeImg', 'sku', $sku);

    if (db_delete($db, 'skus', "sku = $sku"))
    {
        @unlink($thumbPath);
        @unlink($imgPath);

        print_msg('INFORMATION', 'Item Deleted',
                  "The <strong>$itemName</strong> item was deleted successfully.<br><a href=\"$catalogPage?gID=$gID\">Click here to return to the items listing.</a>");
    }
    else
    {
        print_msg('ERROR', 'Item Not Deleted',
                  "An error was encountered when trying to delete the <strong>$itemName</strong> item.<br><a href=\"$catalogPage?gID=$gID\">Click here to return to the tems listing.</a>", 'DelItem/3');
    }
}
else
{
    print_msg('WARNING', "Delete $itemName Item?",
              "Are you sure you wish to delete the <strong>$itemName</strong> item?$activeText<p><a href=\"$deleteItemPage?gID=$gID&sku=$sku&deleteNow=1\">Yes</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"$catalogPage?gID=$gID\">No</a></p>");
}

print '</div>';
print_adminFooter();

?>
