<?php

include_once("$phpfwkIncludePath/dmsCommon.php");

global $dms_mainPage;

if ($dms_mainPage)
{
    $thisPage = $dms_mainPage;
}
else
{
    $thisPage = 'dms.php';
}

$dms_currentFolder = 0;

function print_dmsHeader()
{
    global $currentLibrary, $createNewLibrary;
    static $sentinal = false;

    if ($sentinal)
    {
        return;
    }

    $sentinal = true;

    print_adminHeader('Documents');

    print '<table width=100% border=0 cellspacing=0 cellpadding=3>';
    print '<tr>';
    print '<td valign=top>';
    print_folderTree();
    print '</td>';
    print '<td valign=top width=100%>';
}

function print_dmsFooter()
{
    print '</td></tr></table>';
    print_adminFooter();
}

function print_folderTree($topFolder = 0)
{
    global $dms_currentFolder, $common_baseURL, $thisPage;
    global $folderID;

    $db = db_connect();

    // TODO: ACLs
    $canAddFolders = true;

    $topFolder = intval($topFolder);

    if ($topFolder > 0)
    {
        $where = "parentFolderID = $topFolder";
    }
    else
    {
        $where = "parentFolderID is null";
    }

    $folders = db_query($db, "SELECT folderID, name, groupID
                              FROM dmsFolders WHERE $where
                              ORDER BY lower(name);");
    $numFolders = db_numRows($folders);

    $openFolders = dmsFolderFamilyTree($folderID);

    print '<table class="folderTree" border=0 cellspacing=0 cellpadding=3>';
    print '<tr><td valign=top nowrap>';

    if ($numFolders > 0)
    {
        print '<h3><img src =' . iconURL(ICON_EXPLORE) . " border=0>&nbsp;<a href=\"$common_baseURL/$thisPage\">Document Folders</a></h3>";
    }

    for ($i = 0; $i < $numFolders; ++$i)
    {
        list($fID, $name, $groupID) = db_row($folders, $i);
        $name = stripslashes($name);
        print_folderTreeRecursive($fID, $name, $groupID, 0, $openFolders);
    }

    print '</td></tr></table>';
}

function print_folderTreeRecursive($fID, $name, $gID, $indent, &$openFolders)
{
    global $common_baseURL, $thisPage, $folderID;

    $db = db_connect();

    $folderIsOpen = in_array($fID, $openFolders);
    $folderIsAccessible = true;

    if (! is_null($gID))
    {
        $folderIsAccessible = auth_requiresGID($gID, true, false, false);
    }

    $name = trim($name);

    if (!$name)
    {
        $name = 'Unnamed';
    }

    for ($i = 0; $i < $indent; $i++)
    {
        print '&nbsp;&nbsp;';
    }

    $normalName = $name;

    if (strlen($name) > 23)
    {
        $name = substr($name, 0, 20) . '...';
    }

    $name = htmlentities($name);

    if ($folderIsOpen && $folderIsAccessible)
    {
        $name = '<img align=absmiddle src="' . iconURL(ICON_TREEOPEN) . '" border="0">&nbsp;' . $name;
    }
    else
    {
        $name = '<img align=absmiddle src="' . iconURL(ICON_TREECLOSED) . '" border="0">&nbsp;' . $name;
    }

    if ($folderID == $fID)
    {
        print "<b>$name</b><br>";
    }
    else
    {
        if ($folderIsAccessible)
        {
            print "<a href=\"$common_baseURL/$thisPage?folderID=$fID\">$name</a><br>";
        }
        else
        {
            print "$name<br>";
        }
    }

    // subfolders -> recursive print, and new folder option
    if ($folderIsOpen && $folderIsAccessible)
    {
        $subFolders = db_query($db, "SELECT folderID, name, groupID
                                    FROM dmsFolders WHERE parentFolderID = $fID
                                    ORDER BY lower(name);");
        $numSubFolders = db_numRows($subFolders);

        for ($i = 0; $i < $numSubFolders; ++$i)
        {
            list($subFolderID, $subName, $subGroupID) = db_row($subFolders, $i);
            print_folderTreeRecursive($subFolderID, $subName, $subGroupID, ($indent + 1), $openFolders);
        }
    }
}

function print_fileList($folderID)
{
    global $common_baseURL, $thisPage, $common_baseImagesURL, $admin_baseImagesURL, $offset;

    $pageLimit = 10;
    $db = db_connect();
    $folder = db_query($db, "SELECT name, groupID FROM dmsFolders WHERE folderID = $folderID;");

    if (db_numRows($folder) < 1)
    {
        print_msg('ERROR', 'Folder Not Found',
                  "The requested folder ($folderID) could not be found. Perhaps someone deleted it?", 'dms/1');
        return;
    }

    list($folder, $groupID) = db_row($folder, 0);

    if (! is_null($groupID))
    {
        auth_requiresGID(intval($groupID));
    }

    $offset = intval($offset);
    if ($offset < 1)
    {
       $offset = 0;
    }

    list($docCount) = db_row(db_query($db, "select count(documentID) from dmsDocuments where folderID = $folderID;"), 0);
    $documents = db_query($db, "SELECT documentID, title, filename,
                                       created, description, displayOrder
                                FROM dmsDocuments
                                WHERE folderID = $folderID
                                ORDER BY displayOrder
                                LIMIT $pageLimit OFFSET $offset;");
    $numDocuments = db_numRows($documents);

    print '<div class="documentLinks">';
    print "<img src=" . iconURL(ICON_NEWPAGE) . " border=0>&nbsp;<a href=\"$common_baseURL/$thisPage?newDocument=1&folderID=$folderID\">Add Document</a>";
    print '</div>';

    print "<table class=\"folderFileList\" width=\"100%\"cellspacing=0 cellpadding=5>";
    print "<tr><td colspan=3 class=\"largeColoredHeader\">$folder</td></tr>";

    $baseURL = "$common_baseURL/$thisPage?folderID=$folderID";

    $odd = true;
    for ($i = 0; $i < $numDocuments; ++$i)
    {
        list($documentID, $title, $filename,
             $created, $description, $displayOrder) = db_row($documents, $i);

        $title = stripslashes($title);

        $tdClass = $odd ? 'oddRow' : 'evenRow';
        $odd = !$odd;

        print "<tr>";
        print "<td colspan=3 class=\"$tdClass\"><a href=\"$baseURL&editDocument=$documentID\">$title</a></td>";
        print "</tr>";
    }

    if ($numDocuments < 1)
    {
        print "<tr><td>No documents!</td></tr>";
    }

    if ($numDocuments > 0)
    {
        if (needsNextPrev($offset, $numDocuments, $docCount,  $pageLimit))
	    {
	        print_nextPrev($offset, $numDocuments, $docCount, $pageLimit, "$common_baseURL/$thisPage?folderID=$folderID");
        }
    }

    print "</table>";

    print '<div class="folderLinks">';
    print "<a href=\"$common_baseURL/$thisPage?newFolder=1&parent=$folderID\"><img align=absmiddle src=\"" . iconURL(ICON_NEWFOLDER) . "\" border=\"0\">&nbsp;Add Folder</a> &bull; <a href=\"$common_baseURL/$thisPage?editFolder=1&folderID=$folderID\"><img align=absmiddle src=\"" . iconURL(ICON_EDITED) . "\" border=\"0\">&nbsp;Edit Folder</a> &bull; <a href=\"$common_baseURL/$thisPage?deleteFolder=1&folderID=$folderID\" onClick=\"return confirm('Are you sure you want to delete the folder : $folder?  This action will remove all sub-folders and documents.');\"><img align=absmiddle src=\"" . iconURL(ICON_REMOVE) . "\" border=\"0\">&nbsp;Delete Folder</a> &bull; <a href=\"$common_baseURL/$thisPage?notify=1&folderID=$folderID\"><img align=absmiddle src=\"" . iconURL(ICON_MAIL) . "\" border=\"0\">&nbsp;Notifications</a><br>";
    print '</div>';
}

function print_newFolder($parentFolderID = 0)
{
    global $common_baseURL, $newFolderName, $thisPage;
    global $parent, $groupID;

    $db = db_connect();
    $parent = intval($parent);

    print_form("$common_baseURL/$thisPage");
    print_hidden('parent', $parent);

    print '<table border="0" class="dmsForm" cellpadding="3" cellspacing="0">';
    print '<tr><td colspan="2" class="largeColoredHeader">New Folder</td></tr>';

    print '<tr>';
    print '<td nowrap class="formElementTitle">Library&nbsp;name</td>';
    print '<td width=100%>';
    print_textbox('newFolderName', $newFolderName);
    print '</td>';
    print '</tr>';

    $groups = db_query($db, "SELECT groupID, name FROM groups WHERE active ORDER BY name;"); 

    print '<tr>';
    print '<td nowrap class="formElementTitle">Restricted to</td>';
    print '<td width=100%>';
    print_selectQuery('groupID', $groups, $groupID, false, '', '-1', 'No Restrictions');
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td nowrap class="formElementTitle">&nbsp;</td>';
    print '<td>';
    print_submit("createNewFolder", "Create Folder");
    print '</td>';
    print '</tr>';
    print '</table>';

    print '</form>';
}

function print_editFolder($folderID = 0)
{
    global $common_baseURL, $newFolderName, $thisPage;
    global $groupID;

    $db = db_connect();

    $folder = db_query($db, "SELECT name, groupID FROM dmsFolders WHERE folderID = $folderID;");

    if (db_numRows($folder) < 1)
    {
        print_msg('ERROR', 'Folder Not Found',
                  "The requested folder ($folderID) could not be found. Perhaps someone deleted it?", 'dms/2');
        return;
    }

    list($newFolderName, $groupID) = db_row($folder, 0);
    $newFolderName = stripslashes($newFolderName);

    print_form("$common_baseURL/$thisPage");
    print_hidden('folderID', $folderID);

    print '<table border="0" class="dmsForm" cellpadding="3" cellspacing="0">';
    print "<tr><td colspan=\"2\" class=\"largeColoredHeader\">Edit Folder &quot;$newFolderName&quot;</td></tr>";

    print '<tr>';
    print '<td nowrap class="formElementTitle">Folder&nbsp;name</td>';
    print '<td width=100%>';
    print_textbox('newFolderName', $newFolderName);
    print '</td>';
    print '</tr>';

    $groups = db_query($db, "SELECT groupID, name FROM groups WHERE active ORDER BY name;"); 

    print '<tr>';
    print '<td nowrap class="formElementTitle">Restricted to</td>';
    print '<td width=100%>';
    print_selectQuery('groupID', $groups, $groupID, false, '', '-1', 'No Restrictions');
    print '</td>';
    print '</tr>';

    print '<tr>';
    print '<td nowrap class="formElementTitle">&nbsp;</td>';
    print '<td>';
    print_submit("saveFolder", "Save Folder");
    print '</td>';
    print '</tr>';

    print '</table>';

    print '</form>';
}

function saveFolder($folderID, $folderName, $groupID)
{
    $db = db_connect();

    unset($fields);

    sql_addScalarToUpdate($fields, 'name', $folderName);

    if ($groupID != -1)
    {
        sql_addIntToUpdate($fields, 'groupID', $groupID);
    }
    else
    {
        sql_addRawToUpdate($fields, 'groupID', 'NULL');
    }

    return db_update($db, 'dmsFolders', $fields, "folderID = $folderID");
}

function createNewFolder($parentFolderID, $folderName, $groupID)
{
    $db = db_connect();

    unset($fields, $values);

    sql_addScalarToInsert($fields, $values, 'name', $folderName);
    sql_addIntToInsert($fields, $values, 'parentFolderID', $parentFolderID);

    if ($groupID != -1)
    {
        sql_addIntToInsert($fields, $values, 'groupID', $groupID);
    }

    return db_insert($db, 'dmsFolders', $fields, $values);
}

function deleteFolder($folderID)
{
    $db = db_connect();
    db_startTransaction($db);
    dmsDeleteFolder($folderID);
    db_endTransaction($db);
}

function print_documentForm($folderID, $documentID = 0)
{
    global $common_baseURL, $thisPage, $docDisplayOrder;
    global $docFile_name, $docTitle, $docDescription;
    global $origFolderID;

    if (is_null($origFolderID))
    {
        $origFolderID = $folderID;
    }

    $db = db_connect();

    $folder = db_query($db, "SELECT name FROM dmsFolders WHERE folderID = $folderID;");

    if (db_numRows($folder) < 1)
    {
        print_msg('ERROR', 'Folder Not Found',
                  "The requested folder ($folderID) could not be found. Perhaps someone deleted it?", 'dms/2');
        return;
    }

    list($folderName) = db_row($folder, 0);
    $folderName = stripslashes($folderName);

    if ($documentID)
    {
        $docInfo = db_query($db, "SELECT folderID, filename, title, description, displayOrder,
                                         date_part('month', created),
                                         date_part('day', created),
                                         date_part('year', created)
                                  FROM dmsDocuments
                                  WHERE documentID = $documentID AND folderID = $folderID;");

        if (db_numRows($docInfo) > 0)
        {
            list($folderID, $docFilename, $docTitle, $docDescription, $docDisplayOrder,
                 $docCreatedDateMonth, $docCreatedDateDay, $docCreatedDateYear) = db_row($docInfo, 0);

            $docTitle = stripslashes($docTitle);
            $docDescription = stripslashes($docDescription);

            $docOldDisplayOrder = $docDisplayOrder;

            if (!$docFilename)
            {
                $docFilename = '???';
            }
            else
            {
                $docFilename = '<a href="' .
                                dmsFileURL($folderID, $documentID, $docFilename) .
                                "\">$docFilename</a>";
            }

            $tmp = db_query($db, "SELECT displayOrder FROM dmsDocuments
                                  WHERE folderID = $folderID AND displayOrder < $docDisplayOrder
                                  ORDER BY displayOrder DESC LIMIT 1;");

            if (db_numRows($tmp) > 0)
            {
                list($docDisplayOrder) = db_row($tmp, 0);
            }
        }
        else
        {
            $documentID = 0;
        }
    }
    else
    {
        $documentID = 0;
    }

    print_form("$common_baseURL/$thisPage", 'dms_new_doc', true);

    if ($documentID < 1)
    {
        print_hidden('newDocument', 1);
    }

    if ($documentID > 0)
    {
        $docOrderWhere = "and documentID != $documentID";
    }

    $docOrder = db_query($db, "SELECT displayOrder, title
                               FROM dmsDocuments
                               WHERE folderID = $folderID
                               $docOrderWhere
                               ORDER BY displayOrder;");

    print_hidden('documentID', $documentID);
    print_hidden('origFolderID', $origFolderID);

    print '<table class="dmsForm" cellpadding=3 cellspacing=0>';
    print "<tr><td class=\"largeColoredHeader\" colspan=2>New Document In &quot;$folderName&quot;</td></tr>";

    $family = array();
    addDmsFoldersToList($family, NULL, '');

    print '<tr><td valign="top" class="formElementTitle">Title</td><td>';
    print_textbox('docTitle', $docTitle, 40);
    print '</td></tr>';

    print '<tr><td valign="top" class="formElementTitle">Belongs to</td><td>';
    print_selectArray('folderID', $family, $folderID);
    print '</td></tr>';

    if ($documentID)
    {
        print '<tr><td valign="top" class="formElementTitle">New File</td><td>';
        print_fileUploadBox("docFile", $docFile_name);
        print "<br>Current file: $docFilename";
        print '</td></tr>';
    }
    else
    {
        print '<tr><td valign="top" class="formElementTitle">Upload File</td><td>';
        print_fileUploadBox("docFile", $docFile_name);
        print "<br>Current file: $docFilename";
        print '</td></tr>';
    }

    print '<tr><td valign="top" class="formElementTitle">Description</td><td>';
    print_textArea('docDescription', ereg_replace('<br ?/?>', '', trim($docDescription)), 10, 40);
    print '</td></tr>';

    if (db_numRows($docOrder) > 0)
    {
        print '</td></tr>';
        print '<tr><td valign=top class="formElementTitle">Show&nbsp;After</td><td>';
        print_selectQuery('docDisplayOrder', $docOrder, $docDisplayOrder, false);
        print_hidden('docOldDisplayOrder', $docOldDisplayOrder);
        print '</td></tr>';
    }

    print '<tr><td class="formElementTitle">&nbsp;</td><td>';
    print_submit('saveDocument', 'Save Document');
    print '&nbsp;';
    print_submit('deleteDocument', 'Remove Document');
    print '</td>';
    print '</tr></table></form>';
}

function createNewDocument($folderID, $docFile, $docFile_name, $title, $description, $displayOrder)
{
    global $dms_notificationObject, $dms_notificationsTable, $dms_notificationsEmail;

    $db = db_connect();

    db_startTransaction($db);

    $title = trim($title);
    if (!$title)
    {
        $title = $docFile_name;
    }

    if ($docFile && is_uploaded_file($docFile))
    {
        unset($fields, $values);

        sql_addIntToInsert($fields, $values, 'folderID', $folderID);
        sql_addIntToInsert($fields, $values, 'displayOrder', $displayOrder + 1);
        sql_addScalarToInsert($fields, $values, 'filename',
                            str_replace(array('/',' ','#','"','\''), '_', $docFile_name));
        sql_addScalarToInsert($fields, $values, 'title', $title);
        sql_addScalarToInsert($fields, $values, 'description',
                            nl2br(ereg_replace('<br ?/?>', '', trim($description))));

        $displayOrder = intval($displayOrder);

        db_update($db, 'dmsDocuments', 'displayOrder = displayOrder + 1',
                    "displayOrder > $displayOrder AND folderID = $folderID");

        db_insert($db, 'dmsDocuments', $fields, $values);

        $documentID = db_seqCurrentVal($db, 'seq_dmsIDs');

        if ($docFileName = dmsCopy($docFile, $folderID, $documentID, $docFile_name,
                                   true, 'dms/7', 'print_dmsHeader'))
        {
            chmod($docFileName, 0644);
        }
        else
        {
            db_abortTransaction($db);
            return false;
        }
    }
    else
    {
        print_msg('ERROR', 'Document Creation Failure',
                  'No document was uploaded!', 'dms/7', false, 'print_dmsHeader');
        db_abortTransaction($db);
        return false;
    }

    db_endTransaction($db);

    if ($dms_notificationObject)
    {
        $notification = new $dms_notificationObject($dms_notificationsTable,
                                                    $dms_notificationsEmail,
                                                    $documentID);

        $notification->notify($folderID, EVENT_DOC_ADDED);
    }

    return true;
}

function saveExistingDocument($origFolderID, $folderID, $documentID, $docFile, $docFile_name,
                              $title, $description, $displayOrder, $oldDisplayOrder)
{
    global $dms_notificationObject, $dms_notificationsTable, $dms_notificationsEmail;
    global $dmsFilesystemRoot;

    $db = db_connect();

    $originalInfo = dmsFileInfo($documentID);

    db_startTransaction($db);

    unset($fields);

    $displayOrder = intval($displayOrder);
    $oldDisplayOrder = intval($oldDisplayOrder);

    if (($displayOrder != $oldDisplayOrder) && ($folderID == $origFolderID))
    {
        unset($where);

        if (!$displayOrder)
        {
            sql_addIntToUpdate($fields, 'displayOrder', 0, true);
            sql_addToWhereClause($where, '', 'displayOrder', '>=', '0');
        }
        else
        {
            sql_addIntToUpdate($fields, 'displayOrder', ($displayOrder + 1), true);
            sql_addToWhereClause($where, '', 'displayOrder', '>', $displayOrder);
        }

        sql_addToWhereClause($where, 'AND', 'folderID', '=', $folderID);

        db_update($db, 'dmsDocuments', 'displayOrder = displayOrder + 1', $where);
    }
    else
    {
        sql_addIntToUpdate($fields, 'displayOrder', 0, true);
    }

    if (trim($title))
    {
        sql_addScalarToUpdate($fields, 'title', $title);
    }

    sql_addScalarToUpdate($fields, 'description', nl2br(ereg_replace('<br ?/?>', '', trim($description))));

    if ($docFile && is_uploaded_file($docFile))
    {
        // remove the original file
        if (! @unlink($originalInfo['path']))
        {
            print_msg('ERROR', 'File Error', "Could not remove file : {$originalInfo['filename']}");
/*            db_abortTransaction($db);
            return false;*/
        }

        // place the new document in the correct place
        if ($docFileName = dmsCopy($docFile, $folderID, $documentID, $docFile_name,
                                   true, 'dms/7', 'print_dmsHeader'))
        {
            sql_addScalarToUpdate($fields, 'filename',
                                  str_replace(array('/',' ','#','"','\''), '_', $docFile_name));
            chmod($docFileName, 0644);
        }
        else
        {
            db_abortTransaction($db);
            return false;
        }
    }
    else
    {
        if ($origFolderID != $folderID)
        {
            $src = $originalInfo['path'];
            $dest = dmsFileName($folderID, $documentID, $originalInfo['filename']);

            $file = "$dmsFilesystemRoot/$folderID";
            $stat = @stat($file);

            if (!$stat && !dmsMkdir($file, $printErrors, $errorPreFunc))
            {
                print_msg('ERROR', 'Directory Error', "Could not create directory $file");
                db_abortTransaction($db);
                return false;
            }

            if (! @copy($src, $dest))
            {
                print_msg('ERROR', 'File Error', "Could not copy file {$originalInfo['filename']}");
                db_abortTransaction($db);
                return false;
            }

            if (! @unlink($src))
            {
                print_msg('ERROR', 'File Error', "Could not remove file : {$originalInfo['filename']}");
                db_abortTransaction($db);
                return false;
            }
        }
    }

    sql_addIntToUpdate($fields, 'folderID', $folderID);

    if (isset($fields))
    {
        db_update($db, 'dmsDocuments', $fields, "documentID = $documentID");
    }

    db_endTransaction($db);

    if ($dms_notificationObject)
    {
        $notification = new $dms_notificationObject($dms_notificationsTable,
                                                    $dms_notificationsEmail,
                                                    $documentID);

        if ($origFolderID != $folderID)
        {
            $notification->notify($folderID, EVENT_DOC_REMOVED);
            $notification->notify($origFolderID, EVENT_DOC_MODIFIED);
        }
        else
        {
            $notification->notify($folderID, EVENT_DOC_MODIFIED);
        }
    }

    return true;
}

function deleteDocument($documentID, $folderID)
{
    global $dms_notificationObject, $dms_notificationsTable, $dms_notificationsEmail;

    if ($dms_notificationObject)
    {
        $notification = new $dms_notificationObject($dms_notificationsTable,
                                                    $dms_notificationsEmail,
                                                    $documentID);

        $notification->createNotification($folderID, EVENT_DOC_REMOVED);
    }

    if (dmsDelete($documentID))
    {
        if ($notification)
        {
            $notification->notify($folderID, EVENT_DOC_REMOVED);
        }
    }
}

function print_notificationsForm($folderID)
{
    global $common_baseURL, $thisPage;
    global $dms_notificationsTable;
    global $dmsEvents;
    global $event, $groupID;

    $folderID = intval($folderID);

    $db = db_connect();

    $folder = db_query($db, "SELECT name FROM dmsFolders WHERE folderID = $folderID;");

    if (db_numRows($folder) < 1)
    {
        print_msg('ERROR', 'Folder Not Found',
                  "The requested folder ($folderID) could not be found. Perhaps someone deleted it?", 'dmsn/1');
        return;
    }

    list($folderName) = db_row($folder, 0);

    print_form("$common_baseURL/$thisPage");
    print_hidden('folderID', $folderID);
    print_hidden('notify', 1);

    print '<table border="0" class="dmsForm" cellpadding="3" cellspacing="0">';
    print "<tr><td colspan=\"100%\" class=\"largeColoredHeader\">Notifications On $folderName</td></tr>";

    $notifications = db_query($db, "SELECT n.notificationID, n.id, n.event, n.groupID, g.name
                                    FROM $dms_notificationsTable n
                                    LEFT JOIN groups g ON (g.groupID = n.groupID)
                                    WHERE id = $folderID;");

    $numNotifications = db_numRows($notifications);

    $odd = true;
    for ($i = 0; $i < $numNotifications; $i++)
    {
        $tdClass = $odd ? 'oddRow' : 'evenRow';
        $odd = !$odd;

        list($nID, $id, $event, $gID, $gName) = db_row($notifications, $i);

        print '<tr>';
        print "<td class=$tdClass style=\"border-right: 1px solid black;\"><a href=\"$common_baseURL/$thisPage?deleteNotification=1&nID=$nID&folderID=$folderID&notify=1\">delete</a></td>";
        print "<td class=$tdClass width=100%>";
        print "NOTIFY <b>$gName</b> OF <b>" . $dmsEvents[$event] . "</b> EVENTS";
        print '</td>';
        print '</tr>';
    }

    if ($numNotifications == 0)
    {
        print '<tr><td width=100%>There are no notifications for this folder</td></tr>';
    }

    print "<tr><td colspan=\"100%\" class=\"largeColoredHeader\">Add Notification</td></tr>";

    print '<tr>';
    print '<td colspan=100%>';
    print 'Notify &nbsp;';
    $groups = db_query($db, "SELECT groupID, name FROM groups ORDER BY name;");
    print_selectQuery('groupID', $groups, $groupID);
    print '&nbsp; On &nbsp;';
    print_selectArray('event', $dmsEvents, $event);
    print '&nbsp; Event &nbsp;';
    print_submit("addNotification", "Register Notification");
    print '</td>';
    print '</tr>';

    print_emptyRow();

    print '<tr>';
    print '<td colspan=100%>';
    print "<a href=\"$common_baseURL/$thisPage?folderID=$folderID\"><<< Back to $folderName</a><br>";
    print '</td>';
    print '</tr>';

    print '</table>';
    print '</form>';

}

function addDmsNotification($folderID, $event, $groupID)
{
    global $dms_notificationsTable;

    $db = db_connect();

    unset($fields, $values, $where);

    sql_addToWhereClause($where, 'WHERE', 'id', '=', $folderID);
    sql_addToWhereClause($where, 'AND', 'event', '=', $event);
    sql_addToWhereClause($where, 'AND', 'groupID', '=', $groupID);

    $row = db_query($db, "SELECT notificationID FROM dms_notifications $where;");

    if (db_numRows($row) <= 0)
    {
        sql_addIntToInsert($fields, $values, 'id', $folderID);
        sql_addIntToInsert($fields, $values, 'event', $event);
        sql_addIntToInsert($fields, $values, 'groupID', $groupID, true);

        db_insert($db, $dms_notificationsTable, $fields, $values);
    }
}

function deleteDmsNotification($nID)
{
    global $dms_notificationsTable;

    $db = db_connect();

    db_startTransaction($db);
    db_delete($db, $dms_notificationsTable, "notificationID = $nID");
    db_endTransaction($db);
}

$parent = intval($parent);
$folderID = intval($folderID);
$editDocument = intval($editDocument);
$documentID = intval($documentID);
$nID = intval($nID);
$beenThereDoneThat = false;

if ($createNewFolder && $newFolderName)
{
    createNewFolder($parent, $newFolderName, $groupID);
}
else if ($saveFolder && $folderID > 0)
{
    $newFolderName = trim($newFolderName);
    if (!$newFolderName)
    {
        print_dmsHeader();
        print_msg('ERROR', 'Folder Name Required',
                  "A folder name is required! Please enter one and try again.", 'dms/3');
        print_editFolder($folderID);
    }
    else
    {
        saveFolder($folderID, $newFolderName, $groupID);
    }
}
else if ($deleteFolder && $folderID > 0)
{
    deleteFolder($folderID);
    unset($folderID);
}
else if ($saveDocument && $folderID)
{
    if ($documentID)
    {
        if (saveExistingDocument($origFolderID, $folderID, $documentID, $docFile, $docFile_name, $docTitle, $docDescription, $docDisplayOrder, $docOldDisplayOrder))
        {
            unset($editDocument);
        }
    }
    else if (createNewDocument($folderID, $docFile, $docFile_name, $docTitle, $docDescription, $docDisplayOrder))
    {
        unset($newDocument);
    }
}
else if ($deleteDocument)
{
    if (deleteDocument($documentID, $folderID))
    {
        unset($newDocument);
    }
}
else if ($addNotification)
{
    addDmsNotification($folderID, $event, $groupID);
}
else if ($deleteNotification)
{
    deleteDmsNotification($nID);
}

print_dmsHeader("Documents");

if ($newFolder == 1)
{
    print_newFolder($parent);
}
else if ($editFolder == 1 && $folderID > 0)
{
    print_editFolder($folderID);
}
else if ($newDocument == 1 && $folderID)
{
    print_documentForm($folderID);
}
else if ($editDocument > 0 && folderID)
{
    print_documentForm($folderID, $editDocument);
}
else if ($notify && $folderID)
{
    print_notificationsForm($folderID);
}
else if (!$beenThereDoneThat && $folderID > 0)
{
    print_fileList($folderID);
}
else
{
    print '<div class="documentLinks">';
    print "<a href=\"$common_baseURL/$thisPage?newFolder=1\"><img align=absmiddle src=\"" . iconURL(ICON_NEWFOLDER) . "\" border=\"0\">&nbsp;Create&nbsp;A&nbsp;New&nbsp;Folder</a>";
    print '</div>';

}

print_dmsFooter()

?>
