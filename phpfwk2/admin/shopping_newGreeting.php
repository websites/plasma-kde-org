<?php

include_once("$phpfwkIncludePath/images.php");
include_once("$phpfwkIncludePath/admin/include/shopping_greetings.php");

$thisPage = $newGreetingPage;
$greetingID = intval($greetingID);

print_adminHeader();
print '<div style="padding: 3px">';

if ($editting)
{
    if ($greetingID < 1)
    {
        print_msg('ERROR', 'Greeting ID Required',
                  'A greeting id is required to edit a greeting.', 'EditGreet/1');
        print_adminFooter();
        exit;
    }

    $db = db_connect();
    $greetingInfo = db_query($db, "SELECT greetingTypeID, active, subject, fromAddress,
                                         replyAddress, greeting, closing, internalCC
                                  FROM emailGreetings WHERE greetingID = $greetingID;");

    if (db_numRows($greetingInfo) < 1)
    {
        print_msg('ERROR', 'Greeting Does Not Exist',
                  'The requested greeting could not be found. Perhaps someone already deleted it?', 'EditGreet/2');
        print_adminFooter();
        exit;
    }

    list($greetingTypeID, $greetingIsActive, $subject,
         $fromAddress, $replyAddress, $greeting, $closing, $internalCC) = db_row($greetingInfo, 0);
}

if ($saveNow)
{
    if (validateGreeting($greetingIsActive, $greetingName))
    {
        if ($greetingID > 0)
        {
            updateGreeting();
        }
        else
        {
            addGreeting();
        }

        print_redirect($greetingsPage);
        //print_link($greetingPage, $greetingPage);
    }
    else
    {
        print_greetingForm($greetingID);
    }
}
else
{
    print_greetingForm();
}

print '</div>';
print_adminFooter();

?>
