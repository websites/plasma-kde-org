<?
include("$phpfwkIncludePath/photos.php");

$thisPage = 'albums.php';

define(CONTEXT_LISTALBUM, 0);
define(CONTEXT_ADDALBUM, 1);
define(CONTEXT_EDITALBUM, 2);
define(CONTEXT_DELALBUM, 3);
define(CONTEXT_LISTPHOTO, 4);
define(CONTEXT_ADDPHOTO, 5);
define(CONTEXT_EDITPHOTO, 6);
define(CONTEXT_DELPHOTO, 7);
define(CONTEXT_SHOWPHOTO, 8);


function list_albums()
{
    $db = db_connect();
    $albums = db_query($db, "SELECT albumID, name FROM albums ORDER BY name;");
    $numAlbums = db_numRows($albums);

    print '<table class=box cellpadding=3px cellspacing=0px width=500px>';
    print "<tr><th class=largeColoredHeader colspan=4>Photo Albums</th></tr>";

    $odd = true;
    for ($i = 0; $i < $numAlbums; ++$i)
    {
        list($id, $name) = db_row($albums, $i);
        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;

        $contxt = CONTEXT_LISTPHOTO;
        print "<tr><td $class width=100%><a href=\"$thisPage?context=$contxt&albumID=$id\">$name</a></td>";

        $contxt = CONTEXT_EDITALBUM;
        print "<td $class><a href=\"$thisPage?context=$contxt&albumID=$id\" class=\"editLink\">Edit&nbsp;$type</a></td>";

        $contxt = CONTEXT_DELALBUM;
        print "<td $class><a href=\"$thisPage?context=$contxt&albumID=$id\" class=\"editLink\" onClick=\"return(confirm('Are you sure you want to remove the album : $name?'));\">Delete&nbsp;$type</td>";
        print "</tr>";
    }

    if ($numAlbums < 1)
    {
        $contxt = CONTEXT_ADDALBUM;
        print "<tr><th colspan=4>No Photo Albums In System!<p><a href=\"$thisPage?context=$contxt\">Click here to create one.</a></th></tr>";
        print '</table>';
    }
    else
    {
        print '</table>';
        $contxt = CONTEXT_ADDALBUM;
        print "<br><a href=\"$thisPage?context=$contxt\">Create a new Photo Album</a>";
    }
}

function print_album_form()
{
    global $thisPage, $albumID, $numPhotosPerPage, $numPhotosPerRow;

    $new = $albumID <= 0;
    $title = $new ? 'Create New Album' : "Edit Photo Album";
    $button = $new ? 'Add Album' : "Save Album";

    print_form($thisPage);

    if (! $new)
    {
        print_hidden('albumID', $albumID);
        print_hidden('context', CONTEXT_EDITALBUM);
        list($name, $numPhotosPerPage, $numPhotosPerRow) =
            db_row(db_query(db_connect(), "SELECT name, numPhotosPerPage, numPhotosPerRow
                                        FROM albums WHERE albumID = $albumID;"), 0);
    }
    else
    {
        print_hidden('context', CONTEXT_ADDALBUM);
    }

    ?>
    <table border="0" cellpadding="3" cellspacing="0"  class="box">
    <tr>
        <td colspan="100%" class="largeColoredHeader"><?php print $title; ?></td>
    </tr>
    <tr>
        <td>Name :</td>
        <td><? print_textbox("name", $name); ?></td>
    </tr>
    <tr>
        <td>Number of photos per page :</td>
        <td><? print_rangeSelect("numPhotosPerPage", $numPhotosPerPage, 1, 25, 1); ?></td>
    </tr>
    <tr>
        <td>Number of photos per row:</td>
        <td><? print_rangeSelect("numPhotosPerRow", $numPhotosPerRow, 1, 25, 1); ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><input name=albumBtn type=submit value="<?php print $button; ?>"></td>
    </tr>
    </table>
    </form>
    <?
}

function addAlbum()
{
    global $albumID, $name, $numPhotosPerPage, $numPhotosPerRow;

    if ($albumID > 0)
    {
        return;
    }

    $db = db_connect();

    unset($fileds, $values);

    $albumID = db_seqNextVal($db, 'albumIDs');

    sql_addIntToInsert($fields, $values, 'albumID', $albumID);
    sql_addScalarToInsert($fields, $values, 'name', $name);
    sql_addIntToInsert($fields, $values, 'numPhotosPerPage', $numPhotosPerPage);
    sql_addIntToInsert($fields, $values, 'numPhotosPerRow', $numPhotosPerRow);

    db_insert($db, 'albums', $fields, $values);
}

function updateAlbum()
{
    global $albumID, $name, $numPhotosPerPage, $numPhotosPerRow;

    if (!$albumID)
    {
        return;
    }

    $db = db_connect();

    unset($fileds, $where);

    sql_addScalarToUpdate($fields, 'name', $name);
    sql_addIntToUpdate($fields, 'numPhotosPerPage', $numPhotosPerPage);
    sql_addIntToUpdate($fields, 'numPhotosPerRow', $numPhotosPerRow);
    sql_addToWhereClause($where, '', 'albumID', '=', $albumID);

    db_update($db, 'albums', $fields, $where);
}

function deletePhoto()
{
    global $photoID, $common_albumsImagesPath;

    $db = db_connect();

    $row = db_query($db, "SELECT albumID, imagefile, photo, thumbnail
                         FROM photos WHERE photoID = '$photoID';");
    if (db_numRows($row) > 0)
    {
        list($albumID, $imagefile, $thumbnail) = db_row($row, 0);
    }

    @unlink("$common_albumsImagesPath/$albumID/$thumbnail");
    @unlink("$common_albumsImagesPath/$albumID/$photo");
    @unlink("$common_albumsImagesPath/$albumID/$imagefile");

    db_query($db, "DELETE FROM photos WHERE photoID = '$photoID';");
}

function delAlbum()
{
    global $albumID, $photoID, $common_albumsImagesPath;

    $db = db_connect();

    $photos = db_query($db, "SELECT photoID FROM photos WHERE albumID = $albumID;");
    $numRows = db_numRows($photos);

    for ($i = 0; $i < $numRows; $i++)
    {
        list($photoID) = db_row($photos, $i);
        deletePhoto();
    }

    @rmdir("$common_albumsImagesPath/$albumID");

    db_delete(db_connect(), 'albums', "albumID = $albumID");
}

function list_photos()
{
    global $common_albumsImagesURL;
    global $context, $albumID, $photoOffset, $photoID;

    $photoOffset = intval($photoOffset);

    $db = db_connect();

    if (is_null($albumID))
    {
        print_msg('error', 'No Album Selected',
                  'No photo album ID was selected');
        return;
    }

    $contxt = CONTEXT_ADDPHOTO;
    print "<a href=\"$thisPage?context=$contxt&albumID=$albumID\">Add A Photo To This Album</a>";

    $albumInfo = db_query($db, "SELECT a.name, count(p.photoID) as total,
                                      (count(p.photoID) / a.numPhotosPerPage) as pages,
                                      a.numPhotosPerPage, a.numPhotosPerRow
                               FROM albums a
                               LEFT JOIN photos p ON p.albumID = a.albumID
                               WHERE a.albumID = $albumID
                               GROUP BY a.name, a.numPhotosPerPage, a.numPhotosPerRow;");

    if (db_numRows($albumInfo) >0)
    {
        list($name, $totalPhotos, $pages, $perPage, $perRow) = db_row($albumInfo, 0);

        if ($totalPhotos > ($perPage * $pages))
        {
            $pages++;
        }
    }


    print '<br><br><center>';
    print '<table class="thin" cellpadding=3px cellspacing=1px width=600px>';
    $contxt = CONTEXT_LISTPHOTO;
    print "<tr><td align=left>Photo Album : <a href=\"$thisPage?context=$contxt&albumID=$albumID\">$name</a></td>";
    print "<td align=right><b>$totalPhotos</b> photos in the album on <b>$pages</b> pages</td></tr>";
    print "<tr><td colspan=2 align=center>Pages : ";

    for ($i = 1; $i <= $pages; $i++)
    {
        $offset = ($i * $perPage) - $perPage;

        if ($offset == $photoOffset)
        {
            print "&bull;&nbsp;$i&nbsp;&bull;&nbsp;";
        }
        else
        {
            $contxt = CONTEXT_LISTPHOTO;
            print "<a href=\"$thisPage?context=$contxt&albumID=$albumID&photoOffset=$offset\">$i</a>&nbsp;";
        }
    }

    print "</td></tr>";
    print '</table>';
    print '</center>';


    if ($context == CONTEXT_LISTPHOTO)
    {
        print "<table cols=$perRow cellpadding=3px cellspacing=0px width=\"100%\">";

        $photos = db_query($db, "SELECT photoID,
                                    '$common_albumsImagesURL/'||a.albumID||'/'||p.thumbnail
                                FROM photos p
                                LEFT JOIN albums a ON a.albumID = p.albumID
                                WHERE p.albumID = $albumID
                                ORDER BY p.photoID LIMIT $perPage OFFSET $photoOffset;");

        $numRows = db_numRows($photos);

        $contxt = CONTEXT_EDITPHOTO;
        $index = 0;
        while ($index < $numRows)
        {
            print '<tr>';
            $j = 1;
            while ($j <= $perRow && $index < $numRows)
            {
                list($photoID, $thumbnail) = db_row($photos, $index);
                print "<td align=center valign=top><a href=\"$thisPage?context=$contxt&albumID=$albumID&photoOffset=$offset&photoID=$photoID\" border=0><img src=\"$thumbnail\" border=0></a></td>";
                $index++;
                $j++;
            }

            if ($j < $perRow)
            {
                $index++;
            }

            print '</tr>';
        }

        print '</table>';
    }
    else if ($context == CONTEXT_SHOWPHOTO)
    {
        print "<table cellpadding=3px cellspacing=0px width=\"100%\">";

        $photo = db_query($db, "SELECT '$common_albumsImagesURL/'||a.albumID||'/'||p.photo, caption
                                FROM photos p
                                LEFT JOIN albums a ON a.albumID = p.albumID
                                WHERE p.photoID = $photoID;");

        if (db_numRows($photo) > 0)
        {
            list($photopath, $caption) = db_row($photo, 0);
            print '<tr>';
            print "<td align=center valign=top><img src=\"$photopath\" border=0></td>";
            print '</tr>';
        }

        print '</table>';
    }
}

function print_photo_form()
{
    global $thisPage;
    global $common_albumsImagesPath, $common_albumsImagesURL;
    global $photoID, $albumID, $file, $caption, $imagefile, $thumbnail, $photo;

    $db = db_connect();

    $new = $photoID <= 0;
    $title = $new ? 'Add New Photo' : "Edit Photo";
    $button = $new ? 'Add Photo' : "Save Photo";
    ?>

    <script language="javascript">
    function checkUpload()
    {
        if (document.photoForm.file.value == "")
        {
            document.photoForm.file.disabled = true;
        }
        return true;
    }
    </script>

    <?
    print_form($thisPage, 'photoForm', true, 'post', 'onSubmit="checkUpload()"');

    if (! $new)
    {
        print_hidden('photoID', $photoID);
        print_hidden('context', CONTEXT_EDITPHOTO);

        $row = db_query($db, "SELECT albumID, imagefile, thumbnail, photo, caption
                             FROM photos WHERE photoID = '$photoID';");

        if (db_numRows($row) > 0)
        {
            list($albumID, $imagefile, $thumbnail, $photo, $caption) = db_row($row, 0);
        }

        print '<table>';
        print '<tr><td><b>Thumbnail image : </b></td>';

        if ($thumbnail)
        {
            $path = "$common_albumsImagesURL/$albumID/$thumbnail";
            print "<td><a target=_blank href=\"$path\">$path</a></td>";
        }

        print '</tr>';
        print '<tr><td><b>Photo album image : </b></td>';

        if ($photo)
        {
            $path = "$common_albumsImagesURL/$albumID/$photo";
            print "<td><a target=_blank href=\"$path\">$path</a></td>";
        }

        print '</tr>';
        print '<tr><td><b>Original image : </b></td>';

        if ($imagefile)
        {
            $path = "$common_albumsImagesURL/$albumID/$imagefile";
            print "<td><a target=_blank href=\"$path\">$path</a></td>";
        }

        print '</tr>';
        print '</table>';
    }
    else
    {
        print_hidden('context', CONTEXT_ADDPHOTO);
    }
    ?>

    <table>
    <tr>
        <td colspan="100%" class="largeColoredHeader"><?php print $title; ?></td>
    </tr>
    <tr>
        <td>Photo Album :</td>
        <td>
        <?
        $rows = db_query($db, "SELECT albumID, name FROM albums ORDER BY name;");
        print_selectQuery("albumID", $rows, $albumID);
        ?>
        </td>
    </tr>
    <tr>
        <td>Image caption :</td>
        <td><? print_textbox("caption", $caption, 60); ?></td>
    </tr>
    <tr>
        <td>Image file :</td>
        <td><input size=30 name="file" type="file"></td>
    </tr>
    </table>

    <table>
    <tr>
        <td><input name=photoBtn type=submit value="<?php print $button; ?>"></td>
        <?
        if (!$new)
        {
            print '<td><input name=photoDelBtn type=submit value="Delete"></td>';
            print '<td><input name=photoScaleBtn type=submit value="Re-Scale Image"></td>';
        }
        ?>
        <td>&nbsp;</td>
    </tr>
    </table>
    </form>

    <?
}

function photo()
{
    global $common_albumsImagesPath;
    global $HTTP_POST_FILES, $htmlPath;
    global $albumID, $photoID, $file, $caption;

    $db = db_connect();

    dir_exists($common_albumsImagesPath, true);

    $albumPath = "$common_albumsImagesPath/$albumID";
    $newFileName = $HTTP_POST_FILES['file']['name'];
    $newFilePath = "$albumPath/$newFileName";
    $thumbnailFileName = "thumb_$newFileName";
    $thumbnailFilePath = "$albumPath/$thumbnailFileName";
    $photoFileName = "photo_$newFileName";
    $photoFilePath = "$albumPath/$photoFileName";

    $update = false;
    $imagefile = "";
    $thumbnail = "";

    db_startTransaction($db);

    if (!$photoID)
    {
        unset($fields, $values);

        $photoID = db_seqNextVal($db, "photoIDs");

        sql_addIntToInsert($fields, $values, "photoID", $photoID);
        sql_addIntToInsert($fields, $values, "albumID", $albumID);
        sql_addScalarToInsert($fields, $values, "imagefile", $newFileName);
        sql_addScalarToInsert($fields, $values, "photo", $photoFileName);
        sql_addScalarToInsert($fields, $values, "thumbnail", $thumbnailFileName);
        sql_addScalarToInsert($fields, $values, "caption", $caption);

        db_insert($db, "photos", $fields, $values);
    }
    else
    {
        $update = true;

        $row = db_query($db, "SELECT imagefile, photo, thumbnail FROM photos WHERE photoID = '$photoID';");

        if (db_numRows($row) > 0)
        {
            list($imagefile, $photo, $thumbnail) = db_row($row, 0);
        }

        unset($fields, $where);

        sql_addIntToUpdate($fields, "albumID", $albumID);

        if ($file)
        {
            sql_addScalarToUpdate($fields, "imagefile", $newFileName);
            sql_addScalarToUpdate($fields, "photo", $photoFileName);
            sql_addScalarToUpdate($fields, "thumbnail", $thumbnailFileName);
        }

        sql_addScalarToUpdate($fields, "caption", $caption);

        sql_addToWhereClause($where, "", "photoID", "=", $photoID);

        db_update($db, "photos", $fields, $where);
    }

    if (! $file)
    {
        print_msg("INFO", "Image Saved",
                  "The image information has been saved.");
        db_endTransaction($db);
        return true;
    }
    else
    {
        if ($update)
        {
            // remove the old files
            @unlink("$albumPath/$thumbnail");
            @unlink("$albumPath/$photo");
            @unlink("$albumPath/$imagefile");
        }
    }

    if (! move_uploadedPhoto($file, $albumPath, $newFilePath, $newFileName))
    {
        db_abortTransaction($db);
        return false;
    }

    if (! create_scaledPhoto($newFilePath, $thumbnailFilePath, MAX_THUMBNAIL_PIXELS))
    {
        db_abortTransaction($db);
        return false;
    }

    if (! create_scaledPhoto($newFilePath, $photoFilePath, MAX_ALBUMPHOTO_PIXELS, false))
    {
        db_abortTransaction($db);
        return false;
    }

    print_msg("INFO", "Image Saved",
              "The new image has been saved and thumbnailed.");
    db_endTransaction($db);
    return true;
}

function rescale()
{
    global $common_albumsImagesPath;
    global $HTTP_POST_FILES, $htmlPath;
    global $albumID, $photoID, $file, $caption;

    $db = db_connect();

    $photoInfo = db_query($db, "SELECT imageFile, photo, thumbnail, albumID
                               FROM photos WHERE photoID = '$photoID';");

    if (db_numRows($photoInfo) > 0)
    {
        list($imagefile, $photo, $thumbnail, $albumID) = db_row($photoInfo, 0);
    }
    else
    {
        return false;
    }

    dir_exists($common_albumsImagesPath, true);

    $albumPath = "$common_albumsImagesPath/$albumID";
    $newFileName = $imagefile;
    $newFilePath = "$albumPath/$newFileName";
    $thumbnailFileName = "thumb_$newFileName";
    $thumbnailFilePath = "$albumPath/$thumbnailFileName";
    $photoFileName = "photo_$newFileName";
    $photoFilePath = "$albumPath/$photoFileName";

    db_startTransaction($db);

    unset($fields, $where);

    sql_addScalarToUpdate($fields, "photo", $photoFileName);
    sql_addScalarToUpdate($fields, "thumbnail", $thumbnailFileName);

    sql_addToWhereClause($where, "", "photoID", "=", $photoID);
    db_update($db, "photos", $fields, $where);

    // remove the old files
    @unlink("$albumPath/$thumbnail");
    @unlink("$albumPath/$photo");

    if (! create_scaledPhoto($newFilePath, $thumbnailFilePath, MAX_THUMBNAIL_PIXELS))
    {
        db_abortTransaction($db);
        return false;
    }

    if (! create_scaledPhoto($newFilePath, $photoFilePath, MAX_ALBUMPHOTO_PIXELS, false))
    {
        db_abortTransaction($db);
        return false;
    }

    print_msg("INFO", "Image Saved",
              "The image has been rescaled and thumbnailed.");
    db_endTransaction($db);
    return true;
}

print_adminHeader('Photo Albums');
print '<div style="padding: 3px;">';

$albumID = intval($albumID);

if ($context == CONTEXT_LISTALBUM)
{
    list_albums();
}
else if ($context == CONTEXT_ADDALBUM)
{
    if($albumBtn)
    {
        addAlbum();
        list_albums();
    }
    else
    {
        print_album_form();
    }
}
else if ($context == CONTEXT_EDITALBUM)
{
    if($albumBtn)
    {
        updateAlbum();
        list_albums();
    }
    else
    {
        print_album_form();
    }
}
else if ($context == CONTEXT_DELALBUM)
{
    delAlbum();
    list_albums();
}
else if ($context == CONTEXT_LISTPHOTO || $context == CONTEXT_SHOWPHOTO)
{
    list_photos();
}
else if ($context == CONTEXT_ADDPHOTO || $context == CONTEXT_EDITPHOTO)
{
    if($photoBtn)
    {
        photo();
        unset($caption);
        $context = CONTEXT_LISTPHOTO;
        list_photos();
    }
    else if ($photoDelBtn)
    {
        deletePhoto();
        $context = CONTEXT_LISTPHOTO;
        list_photos();
    }
    else if ($photoScaleBtn)
    {
        rescale();
        $context = CONTEXT_LISTPHOTO;
        list_photos();
    }
    else
    {
        print_photo_form();
    }
}

print '</div>';
print_adminFooter();
?>
