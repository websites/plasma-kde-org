<?php

include_once("$phpfwkIncludePath/admin/include/shopping_catalogs.php");

$gID = intval($gID);
$parent = intval($parent);

print_adminHeader();
print '<div style="padding: 3px">';

if ($gID < 1)
{
    print_msg('ERROR', 'Group ID Required',
              'A group id is required to delete a group.', 'DelGrp/1');
    print_footer();
    exit;
}

$db = db_connect();
$groupInfo = db_query($db, "SELECT active, name FROM skuGroups
                           WHERE groupID = $gID;");

if (db_numRows($groupInfo) < 1)
{
    print_msg('ERROR', 'Group Does Not Exist',
              'The requested group could not be found. Perhaps someone already deleted it?', 'DelGrp/2');
    print_adminFooter();
    exit;
}

list($groupIsActive, $groupName) = db_row($groupInfo, 0);
list($numItems) = db_row(db_query($db, "select count(sku) from skus where groupID = $gID;"), 0);
list($numGroups) = db_row(db_query($db, "select count(groupID) from skuGroups where parent = $gID;"), 0);


$link = "$catalogPage";
if (! $parent)
{
    $type = 'Catalog';
}
else
{
    $type = 'Group';
    $link .= "?gID=$parent";
}

if ($deleteNow == 1)
{
    if (db_delete($db, 'skuGroups', "groupID = $gID"))
    {
        print_msg('INFORMATION', "$type Deleted",
                  "The <strong>$groupName</strong> $type was deleted successfully.<br><a href=\"$link\">Click here to return to the $type listing.</a>");
    }
    else
    {
        print_msg('ERROR', "$type Not Deleted",
                  "An error was encountered when trying to delete the <strong>$groupName</strong> $type.<br><a href=\"$$link\">Click here to return to the $type listing.</a>", 'DelGrp/3');
    }
}
else
{
    unset($activeText);
    if (db_boolean($groupIsActive))
    {
        $activeText = "<p><strong>This $type is active!</strong></p>";
    }

    print_msg('WARNING', "Delete $groupName $type?",
              "Are you sure you wish to delete the <strong>$groupName</strong> $type?$activeText<p>This $type contains $numItems items and $numGroups groups</p><p><a href=\"$deleteGroupPage?parent=$parent&gID=$gID&deleteNow=1\">Yes</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"$link\">No</a></p>");
}

print '</div>';

print_adminFooter();

?>
