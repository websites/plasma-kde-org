<?php

include_once("$phpfwkIncludePath/cmsCommon.php");

$thisPage = 'cms.php';
$cms_admin = true;

function getcontentsoffile($filename)
{
    $fd = fopen("$filename", "rb");
    $content = fread($fd, filesize($filename));
    fclose($fd);
    print addslashes(str_replace("\n","",$content));
}

function print_leftSide($pageID = 0)
{
    static $sentinal = false;

    if ($sentinal)
    {
        return;
    }

    $sentinal = true;

    print '<table border=0 style="margin: 0px; padding: 0px;" cellpadding=0px cellspacing=0 height="100%">';
    print '<tr><td class="largeColoredHeader" align=center height="1em;"><img src =' . iconURL(ICON_WEBPAGE) . ' border=0>&nbsp;Web&nbsp;Pages</td>';
    print '<td valign="top" style="padding-left: 3px; padding-top: 3px;" width="100%" rowspan=2 height="100%">';
}

function print_rightSide($pageID)
{
    print '</td></tr>';
    print '<tr><td valign="top" style="border-right: solid 1px #666666;" nowrap>';
    print_CMSTree($pageID);
    print '</td></tr>';

    print '</td></tr>';
    print '</table>';
    print_adminFooter();
}

function print_CMSTree($selectedPageID = 0)
{
    global $common_baseURL, $thisPage, $currentPageID, $currentPagePath, $auth_userGID;
    global $cms_complexityLevel;

    if ($selectedPageID < 1)
    {
        $selectedPageID = $currentPageID;
    }

    $db = db_connect();
    $query = "select meta.pageID, meta.pubRevision, meta.title from cmsPagesMetadata meta
                  where meta.parentID is null order by displayOrder;";
    $pages = db_query($db, $query);
    $numPages = db_numRows($pages);

    $ancestry = array();
    $currentPagePath = cms_pathByPageID($selectedPageID, $ancestry, '', true, false);

    print '<table border=0 cellpadding=3 cellspacing=0 height="100%" width="100%">';
    for ($i = 0; $i < $numPages; ++$i)
    {
        list($pageID, $pubRevision, $title) = db_row($pages, $i);

        if (!trim($title))
        {
            $title = 'Unnamed';
        }
        print '<tr><td class="largeGreyHeader" style="border-top: 1px solid; border-bottom: none;" colspan=2>';
        $title = str_replace(' ', '&nbsp;', $title);
        $privs = new cmsPagePriveleges($pageID);
        if ($selectedPageID == $pageID)
        {
            print "<b>&gt;&gt;$title</b>\n";
        }
        else if (!$privs->canEdit)
        {
            print "<b>$title</b>\n";
        }
        else
        {
            print "<a href=\"$common_baseURL/$thisPage?currentPageID=$pageID\">$title</a>";
        }

        if ($cms_complexityLevel == CMS_BASIC || isSuper())
        {
            $query = "select meta.pageID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID order by displayOrder;";
        }
        else
        {
            $query = "select meta.pageID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID and (meta.editGroup = $auth_userGID or meta.adminGroup = $auth_userGID) order by displayOrder;";
        }
        $subPages = db_query($db, $query);
        $numSubPages = db_numRows($subPages);
        print '<td class="largeGreyHeader" style="border-top: 1px solid; border-bottom: none;">';
        if ($privs->canAdd)
        {
            print "<a href=\"$common_baseURL/$thisPage?addNewPage=$pageID\"><img src=\"" . iconURL(ICON_NEWPAGE) . "\" border=\"0\" title=\"New $title Page\" align=\"middle\"></a>\n";
        }
        print '</td></tr>';
//         print '<tr><td nowrap>';

        $odd = true;
        for ($j = 0; $j < $numSubPages; ++$j)
        {
            list($subPageID, $subPubRevision, $subTitle) = db_row($subPages, $j);
            print_CMSTreeRecursive($subPageID, $selectedPageID, $subPubRevision, $subTitle, $ancestry, $odd);
        }

//         print '</td></tr>';
    }
    print '<tr><td class="largeGreyHeader" height="100%" colspan=3>&nbsp;</td></tr>';
    print '</table>';

    return $path;
}

function print_CMSTreeRecursive($pageID, $currentPageID, $pubRevision, $title, $ancestry, &$odd, $level = 0)
{
    global $common_baseURL, $thisPage, $cms_complexityLevel, $auth_userGID;
    $pageID = intval($pageID);
    if (!$pageID)
    {
        return;
    }

    if (!trim($title))
    {
        $title = 'Unnamed';
    }

    $db = db_connect();
    if ($cms_complexityLevel == CMS_BASIC || isSuper())
    {
        $query = "select meta.pageID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID order by displayOrder;";
    }
    else
    {
        $query = "select meta.pageID, meta.pubRevision, meta.title from cmsPagesMetadata meta where meta.parentID = $pageID and (meta.editGroup = $auth_userGID or meta.adminGroup = $auth_userGID) order by displayOrder;";
    }
    $subPages = db_query($db, $query);
    $numSubPages = db_numRows($subPages);

    $normalTitle = $title;
    if (strlen($title) > 23)
    {
        $title = substr($title, 0, 20) . '...';
    }
    $title = str_replace(' ', '&nbsp;', $title);

    if ($cms_complexityLevel == CMS_COMPLETE && $numSubPages > 0 && $pageID != $ancestry[0])
    {
        $treeIcon = '<img align=absmiddle src="' . iconURL(ICON_TREECLOSED) . '" border="0">&nbsp;';
    }
    else
    {
        $treeIcon = '<img align=absmiddle src="' . iconURL(ICON_TREEOPEN) . '" border="0">&nbsp;';
    }

    if (!$pubRevision)
    {
        $title .=  '&nbsp;<img src="' . iconURL(ICON_UNPUBLISHED) . '" border="0" title="Unpublished" align="absmiddle">';
    }
    else
    {
        $revisionQuery = db_query($db, "select pg.pageID, pg.revision from cmsPages pg
                                       where pg.pageID = $pageID order by revision desc limit 1;");
        if (db_numRows($revisionQuery) > 0)
        {
            list($pageID, $revision) = db_row($revisionQuery, 0);

            if ($revision != $pubRevision)
            {
                $title .= '&nbsp;<img src="' . iconURL(ICON_EDITED) . '" border="0" title="Edited" align="absmiddle">';
            }
        }
        else
        {
            $title .=  '&nbsp;<img src="' . iconURL(ICON_UNPUBLISHED) . '" border="0" title="Unpublished" align="absmiddle">';
        }
    }

    $privs = new cmsPagePriveleges($pageID);
    if ($currentPageID == $pageID)
    {
        $title = "<u>$title</u>";
    }

    $cssClass = $odd ? 'oddRow' : 'evenRow';
    $odd = !$odd;
    //print "offset calc for $title: $offset = 3 + ($level * 5);<br>";
    print "<tr>";
    if ($level > 0)
    {
        print "<td style=\"padding-left: 8px;\">&nbsp;</td>";
        $firstColumnColspan = 1;
        if ($level > 1)
        {
            $offset = 3 + (($level + 1) * 5);
        }
    }
    else
    {
        $firstColumnColspan = 2;
    }
    print "<td style=\"padding-left: {$offset}px;\" class=\"$cssClass\" colspan=\"$firstColumnColspan\" valign=top nowrap>$treeIcon";
    print "<a title=\"$normalTitle\" href=\"$common_baseURL/$thisPage?currentPageID=$pageID\">$title</a>";
    print "<td class=\"$cssClass\" valign=top align=right>";
    if ($privs->canAdd)
    {
        print "<a title=\"Create a new subpage\" href=\"$common_baseURL/$thisPage?addNewPage=$pageID\"><img src=\"" . iconURL(ICON_NEWPAGE) . "\" border=\"0\" title=\"New $normalTitle Sub Page\" align=\"absmiddle\"></a>";
    }
    print "</td></tr>";

    if ($cms_complexityLevel < CMS_COMPLETE || $pageID == $ancestry[0])
    {
        array_shift($ancestry);
        if ($numSubPages > 0)
        {
            for ($i = 0; $i < $numSubPages; ++$i)
            {
                list($subPageID, $subPubRevision, $subTitle) = db_row($subPages, $i);
                print_CMSTreeRecursive($subPageID, $currentPageID, $subPubRevision, $subTitle, $ancestry, $odd, $level + 1);
            }
        }
    }
}

function print_page($pageID, $edit = true)
{
    global $common_baseURL, $thisPage, $editContent;
    global $revision, $type, $parentID, $displayOrder;
    global $addable, $editable, $deletable, $showInNav;
    global $adminGroup, $editGroup, $published, $title, $body;
    global $savePageChanges, $thisTableWidth;
    global $currentPagePath, $currentPageType, $cmsTypesArray;
    global $cms_complexityLevel, $cms_objectFilePath, $cms_customObjectFilePath;

    $pageID = intval($pageID);
    if ($pageID < 1)
    {
        return;
    }

    $db = db_connect();
    $privs = new cmsPagePriveleges($pageID);
    if ($privs->pageID != $pageID)
    {
        print_msg('ERROR', "No Such Page", "Could not find the requested page in the content management database", $thisTableWidth, false, 'print_leftSide');
        return;
    }

    if ($edit)
    {
        $fields = "pg.revision, cmsTypes.name, cmsTypes.object, meta.typeID, meta.parentID, meta.displayOrder, meta.showInNav, meta.pubRevision, meta.title, pg.body, meta.adminGroup, gradmin.name, meta.editGroup, gredit.name";

        $page = db_query($db,
                        "select $fields
                         from cmsPages pg join cmsPagesMetadata meta on (pg.pageID = meta.pageID)
                         left join cmsTypes on (meta.typeID = cmsTypes.typeID)
                         left join groups gradmin on (gradmin.groupID = meta.adminGroup)
                         left join groups gredit on (gredit.groupID = meta.editGroup)
                         where pg.pageID = $pageID order by revision desc limit 1;");

        if (db_numRows($page) < 1)
        {
            // somehow we ended up with metadata, but no page, published or otherwise
            // oh well, let's just make one!
            db_query($db, "insert into cmsPages (pageID) values ($pageID);");
            $page = db_query($db,
                            "select $fields
                             from cmsPages pg join cmsPagesMetadata meta on (pg.pageID = meta.pageID)
                             left join cmsTypes on (meta.typeID = cmsTypes.typeID)
                             where pg.pageID = $pageID order by revision desc limit 1;");
        }
    }
    else if ($privs->canAdd)
    {
        $page = db_query($db, "select meta.showInNav, meta.adminGroup, gradmin.name, meta.editGroup, gredit.name
                               from cmsPagesMetadata meta
                               left join groups gradmin on (gradmin.groupID = meta.adminGroup)
                               left join groups gredit on (gredit.groupID = meta.editGroup)where meta.pageID = $pageID;");
        $privs->canEdit = true;
        $privs->canPublish = true;
	$currentPageType = 0;
    }
    else
    {
        print_msg('ERROR', 'Access Violation', "New sub pages can not be added to this page with your account<br>Page: $pageID", $thisTableWidth, false, 'print_leftSide');
        return;
    }

    if (!$savePageChanges)
    {
        if ($edit)
        {
            list($revision, $type, $currentPageTypeObject, $currentPageType,
                 $parentID, $displayOrder, $showInNav,
                 $pubRevision, $title, $body, $adminGroup, $adminGroupName, $editGroup, $editGroupName) = db_row($page, 0);
            cmsRelinkText($body, true);
            $currentPageTypeEditable = db_boolean($currentPageTypeEditable);

            if ($parentID)
            {
                $temp = db_query($db, "select displayOrder from cmsPagesMetadata
                                    where parentID = $parentID and displayOrder < $displayOrder
                                    order by displayOrder desc limit 1;");

                if (db_numRows($temp) > 0)
                {
                    list($displayOrder) = db_row($temp, 0);
                }
            }
        }
        else
        {
            list($showInNav, $adminGroup, $adminGroupName, $editGroup, $editGroupName) = db_row($page, 0);
            $parentID = $pageID;
            $editable = db_toBoolean(true);
            $editContent = db_toBoolean(true);
        }
    }
    else
    {
        $parentID = $pageID;
    }

    $pageTypes = db_query($db, "select typeID, name from cmsTypes order by lower(name);");
    //print "$revision, $type, $parentID, $addable, $editable, $deletable, $pubRevision, $title, $body<br>";
    print "<form action=\"$common_baseURL/$thisPage\" method=\"post\" onSubmit='updateRTEs(); return true;'>";
    $currentPagePath = cms_pathByPageID($pageID, $ancestry, '', true, false);
    print "<table width='100%' border=0 cellpadding=3px cellspacing=0px><tr><td colspan=2 class=\"cmsMicroHeader\">$currentPagePath</td></tr></table>";
    print "<table width='100%' border=0 cellpadding=3px cellspacing=0px class=\"cmsForm\">";
    $userGroups = db_query($db, "select groupID, name from groups where groupID > 0 order by name;");

    if ($cms_complexityLevel > CMS_BASIC)
    {
        print '<tr><td colspan=2 class="largeColoredHeader">Permissions</td></tr>';
        print '<tr><td class="formElementTitle">&nbsp;</td><td class="cmsMicroHeader">Your Current Priveleges: ' .
        ($privs->canPublish ? 'Editor' : '') .
        ($privs->canPublish && $privs->canEdit ? ' and ' : '') .
        ($privs->canEdit ? 'Author' : '') .
        (!($privs->canPublish || $privs->canEdit) ? 'View Only' : '') .
        '</td></tr>';
        print '<tr><td class="formElementTitle">Admin&nbsp;Group</td><td width="100%">';
        if ($privs->canPublish)
        {
            print_selectQuery('adminGroup', $userGroups, $adminGroup);
        }
        else
        {
            print $adminGroupName;
        }
        print '</td></tr>';
        print '<tr><td class="formElementTitle">Edit&nbsp;Group</td><td>';
        if ($privs->canPublish)
        {
            print_selectQuery('editGroup', $userGroups, $editGroup);
        }
        else
        {
            print $editGroupName;
        }
        print '</td></tr>';

        if ($cms_complexityLevel > CMS_FLEXIBLE && $privs->superUser)
        {
            print '<tr><td colspan=2  class="largeColoredHeader">Access Rights</td></tr>';
            if (!cms_isTopNode($pageID))
            {
                print '<tr><td class="formElementTitle" nowrap>Can add sub pages</td><td>';
                print_boolElement('addable', db_boolean($addable));
                print '</td>
                        </tr>
                        <tr><td class="formElementTitle" nowrap>Can be edited</td><td>';
                print_boolElement('editable',  db_boolean($editable));
                print '</td></tr>
                    <tr><td class="formElementTitle" nowrap>Can be deleted</td><td>';
                print_boolElement('deletable', db_boolean($deletable));
                print '</td></tr>';
            }
        }
    }
    print '<tr><td colspan=2  class="largeColoredHeader">Settings</td></tr>';
    print '<tr><td class="formElementTitle">Title</td><td width="100%">';

    if (!$edit || $privs->canPublish)
    {
        print_textbox('title', $title);
    }
    else
    {
        print $title;
    }
    print '</td></tr>';

    if ($cms_complexityLevel > CMS_BASIC && $edit)
    {
        print '<tr>
            <td class="formElementTitle">Published</td>
            <td>';

        if ($privs->canPublish)
        {
            print_boolElement('published', $pubRevision);
        }
        else
        {
            print ($pubRevision) ? 'Yes' : 'No';
        }
    }

    print '<tr><td valign=top class="formElementTitle">In&nbsp;Navigation</td><td>';
    if ($privs->canPublish)
    {
        print_boolElement('showInNav', db_boolean($showInNav));
    }
    else
    {
        print $showInNav ? 'Yes' : 'No';
    }
    print '</td></tr>';

    if ($privs->canPublish && $cms_complexityLevel == CMS_BASIC)
    {
        print '<tr><td class="formElementTitle">Owned By</td><td width="100%">';

        if (!$edit || $privs->canPublish)
        {
            print_selectQuery('adminGroup', $userGroups, $adminGroup);
        }
        else
        {
            print $adminGroupName;
        }
        print '</td></tr>';
    }

    if ($privs->canPublish)
    {
            $pageOrder = db_query($db, "select meta.displayOrder, meta.title
                                    from cmsPagesMetadata meta
                                    where meta.parentID = $parentID and meta.pageID != $pageID
                                    order by displayOrder;");
        if (db_numRows($pageOrder) > 0)
        {
            print '</td></tr>';
            print '<tr><td valign=top class="formElementTitle">Show&nbsp;After</td><td>';
            print_selectQuery('displayOrder', $pageOrder, $displayOrder, false);
        }
    }

    print '<tr><td valign=top class="formElementTitle">Page&nbsp;Type</td><td>';
    if ($privs->canPublish)
    {
        print_selectQuery('typeID', $pageTypes, $currentPageType);
        if ($currentPageTypeObject && is_readable("$cms_customObjectFilePath/$currentPageTypeObject.php"))
        {
            include_once("$cms_customObjectFilePath/$currentPageTypeObject.php");
            $objName = "cms_$currentPageTypeObject";
            $typeEditor = new $objName($pageID);
            if ($typeEditor->isEditable)
            {
                print "<br><a href=\"$thisPage?currentPageID=$pageID&editType=1\">Edit $type details...</a>";
            }
        }
        else if ($currentPageTypeObject && is_readable("$cms_objectFilePath/$currentPageTypeObject.php"))
        {
            include_once("$cms_objectFilePath/$currentPageTypeObject.php");
            $objName = "cms_$currentPageTypeObject";
            $typeEditor = new $objName($pageID);
            //print "is editable? " . ($typeEditor->editable ? 'yes' : 'no') . '<br>';
            if ($typeEditor->isEditable)
            {
                print "<br><a href=\"$thisPage?currentPageID=$pageID&editType=1\">Edit $type details...</a>";
            }
        }
    }
    else
    {
        print $type;
    }

    if ($edit == true && $currentPageType > 0 && $cmsTypesArray[$currentPageType] && $currentPageTypeEditable)
    {
        print '<a href="' . $thisPage . '?currentPageID=' . $pageID . '&editType=1">Go to ' . strtolower(typeNameByID($currentPageType)) . ' items</a>';
    }

    print '</td></tr>';

    print '<tr><td colspan=2  class="largeColoredHeader">Content</td></tr>';
    print '<tr><td colspan=2>';

    if ($cms_complexityLevel > CMS_BASIC)
    {
        if ($editedBy)
        {
            $editedBy = intval($editedBy);
            $editorName = db_query($db, "select name from admins where adminID = $editedBy;");
            if (db_numRows($editorName) > 0)
            {
                list($editorName) = db_row($editorName, 0);
                print "Edited by $editorName on $editedOn<br>";
            }
        }

        if ($approvedBy)
        {
            $approvedBy = intval($approvedBy);
            $editorName = db_query($db, "select name from admins where adminID = $approvedBy;");
            if (db_numRows($editorName) > 0)
            {
                list($editorName) = db_row($editorName, 0);
                print "Approved by $editorName on $approveOn<br>";
            }
        }
    }

    if ($privs->canEdit)
    {
        if ($edit && !$editContent && $privs->canPublish &&
            (!$pubRevision || $revision != $pubRevision))
        {
            print_msg('information', 'This Page Has Been Edited!',
            "Below is the most recently edited version of the page. It has not yet been approved for publication.
            <br><a onClick=\"return confirm('Are you sure you wish to approve this revision?');\"
             href=\"$common_baseURL/cms.php?currentPageID=$pageID&approveRevision=$revision\">
             <img border=0 src=\"" . iconURL(ICON_OK) . "\">Click here to <b>approve</b> this revision for publication</a>
            <br><a onClick=\"return confirm('Are you sure you wish to deny this revision?');\"
             href=\"$common_baseURL/cms.php?currentPageID=$pageID&denyRevision=$revision\">
             <img border=0 src=\"" . iconURL(ICON_CANCEL) . "\">Click here to <b>deny</b> this revision</a>", $thisTableWidth, false,  'print_leftSide');
        }

        if (!$editContent)
        {
           //print "<pre>".htmlentities($body)."</pre>";
           print $body;
        }
        else
        {
            print '<br>';
            print_richTextArea('body', $body);
        }
        print '</td></tr>';
    }
    else
    {
        print "$body</td></tr>";
    }

    if ($edit)
    {
        print '<tr><td colspan=2>';

        if ($privs->canEdit)
        {
            print_submit('savePageChanges', 'Save Changes');

            if (!$editContent)
            {
                print '&nbsp;';
                print_submit('editContent', 'Edit Content');
            }
        }

        if ($privs->canDelete)
        {
            print '&nbsp;';
            print_submit('deletePage', 'Delete Page', "onClick=\"return confirm('Are you sure you wish delete \'" . str_replace("'", "\\'", $title) . "\'?')\"");
        }

        print '</td></tr></table>';
        print "<input type=hidden name=currentPageID value=\"$pageID\">";
        print "<input type=hidden name=revision value=\"$revision\">";
    }
    else
    {
        print '<tr><td colspan=2>';

        print_submit('savePageChanges', 'Create New Page');
        print '</td></tr></table>';
        print "<input type=hidden name=addNewPage value=\"$parentID\">";
    }
    print "</form>";
}

function saveExistingPage($pageID)
{
    global $addable, $editable, $displayOrder, $deletable, $adminGroup, $editGroup, $showInNav, $title, $body;
    global $published, $revision, $typeID, $auth_userID;
    global $currentPagePath, $cms_fullTextSearch, $cms_emailNotifications,$cms_complexityLevel;
    global $editContent;
    unset($fields);
    $privs = new cmsPagePriveleges($pageID);

    if ($privs->pageID != $pageID)
    {
        return false;
    }

    $db = db_connect();
    db_startTransaction($db);
    $query = "select parentID from cmsPagesMetadata where pageID = $pageID;";
    //print "$query<br>";
    list($parentID) = db_row(db_query($db, $query), 0);

    if ($cms_complexityLevel > CMS_BASIC && $privs->canPublish)
    {
        if ($cms_complexityLevel > CMS_FLEXIBLE && !cms_isTopNode($parentID))
        {
            sql_addBoolToUpdate($fields, 'addable', db_boolean($addable));
            sql_addBoolToUpdate($fields, 'editable', db_boolean($editable));
            sql_addBoolToUpdate($fields, 'deletable', db_boolean($deletable));
        }
        sql_addIntToUpdate($fields, 'editGroup', $editGroup);
    }

    if ($privs->canPublish)
    {
        sql_addIntToUpdate($fields, 'adminGroup', $adminGroup);
        sql_addBoolToUpdate($fields, 'showInNav', $showInNav);
        $displayOrder = intval($displayOrder);
        sql_addIntToUpdate($fields, 'displayOrder', $displayOrder + 1);
        $query = "select pageID from cmsPagesMetadata where parentID = $parentID and displayOrder = $displayOrder + 1 and pageID != $pageID;";

        if (db_numRows(db_query($db, $query)) > 0)
        {
            $query = "update cmsPagesMetadata set displayOrder = displayOrder + 1
                      where parentID = $parentID and displayOrder > $displayOrder;";
            db_query($db, $query);
        }

        if ($cms_complexityLevel > CMS_BASIC)
        {
            if ($published)
            {
                // we're set to be published, so check that we havea revision
                $isThereARevision = db_query($db, "select pubRevision is null from cmsPagesMetadata where pageID = $pageID;");
                if (db_numRows($isThereARevision) > 0)
                {
                    list($isThereARevision) = db_row($isThereARevision, 0);
                    if (db_boolean($isThereARevision))
                    {
                        $maxRev = db_query($db, "select max(revision) from cmsPages where pageID = $pageID;");
                        if (db_numRows($maxRev) > 0)
                        {
                            list($maxRev) = db_row($maxRev, 0);
                            db_query($db, "update cmsPagesMetadata set pubRevision = $maxRev where pageID = $pageID;");
                        }
                    }
                }
            }
            else
            {
                sql_addNullToUpdate($fields,  'pubRevision');
                if ($cms_fullTextSearch)
                {
                    putPageIntoSearch($pageID, 0);
                }
            }
        }
    }

    if ($privs->canEdit)
    {
        if ($title)
        {
            sql_addScalarToUpdate($fields, 'title', $title);
        }
        sql_addIntToUpdate($fields, 'typeID', $typeID, true);
    }

    if (isset($fields))
    {
        db_query($db, "update cmsPagesMetadata set $fields where pageID = $pageID;");
    }

    if ($privs->canEdit && $body)
    {
        unset($fields);
        unset($values);
        $delinkedBody = trim($body);
        //print htmlentities($body) . "<p>and<p>";
        cmsDelinkText($delinkedBody);
        //print htmlentities($delinkedBody) . "<br>";
        //    print "<hr>Text -4:" . str_replace("<", "&lt;", str_replace(">", "&gt;", $delinkedBody));
        sql_addIntToInsert($fields, $values, 'pageID', $pageID);

        if ($cms_complexityLevel > CMS_BASIC)
        {
            sql_addIntToInsert($fields, $values, 'editedBy', $auth_userID);
        }

        sql_addScalarToInsert($fields, $values, 'body', $delinkedBody);
        db_insert($db,'cmsPages', $fields, $values);
        $newRevision = db_seqCurrentVal($db, 'seq_cmsPageIDs');

        if ($cms_complexityLevel == CMS_BASIC)
        {
            setPublicRevision($newRevision, $pageID);
        }

        if ($admin_enableMailAlerts)
        {
            print_leftSide(); // make sure we've got our currentPagePath for the email out
            sendAdminGroupEmail($privs->adminGroup, 'cms_content_edit', SPOOL_NEXTSEND, array('revisionid' => $newRevision), array('page' => $currentPagePath));
        }
    }

    db_endTransaction($db);
    return true;
}

function saveNewPage()
{
    global $addable, $editable, $displayOrder, $deletable, $adminGroup, $editGroup, $showInNav, $title, $body;
    global $addNewPage, $currentPageID, $published, $auth_userID, $auth_adminGID;
    global $cms_complexityLevel, $cms_fullTextSearch, $cms_emailNotifications, $typeID;
    unset($fields);
    unset($values);
    $db = db_connect();
    $privs = new cmsPagePriveleges($addNewPage);
    if (cms_isTopNode($addNewPage))
    {
        $privs->canAdd = true;
    }

    if (!$privs->canAdd)
    {
        print_msg('ERROR', 'Access Violation', "New sub pages can not be added to this page with your account<br>Page: $addNewPage", 'cms/snp1', false, 'print_leftSide');
        return false;
    }

    if ($cms_complexityLevel > CMS_FLEXIBLE)
    {
        sql_addBoolToInsert($fields, $values, 'addable', db_boolean($addable));
        sql_addBoolToInsert($fields, $values, 'editable', db_boolean($editable));
        sql_addBoolToInsert($fields, $values, 'deletable', db_boolean($deletable));
    }

    sql_addBoolToInsert($fields, $values, 'showInNav', $showInNav);
    sql_addIntToInsert($fields, $values, 'parentID', $addNewPage);

    sql_addIntToInsert($fields, $values, 'adminGroup', $adminGroup);
    if ($cms_complexityLevel > CMS_BASIC)
    {
        sql_addIntToInsert($fields, $values, 'editGroup', $editGroup);
    }
    else
    {
        sql_addIntToInsert($fields, $values, 'editGroup', $privs->editGroup);
    }

    sql_addIntToInsert($fields, $values, 'typeID', $typeID, true);
    sql_addScalarToInsert($fields, $values, 'title', $title);

    db_startTransaction($db);
    if (isset($displayOrder))
    {
        $displayOrder = intval($displayOrder);
        $query = "select pageID from cmsPagesMetadata where parentID = $addNewPage and displayOrder = $displayOrder + 1;";

        if (db_numRows(db_query($db, $query)) > 0)
        {
            $query = "update cmsPagesMetadata set displayOrder = displayOrder + 1
                       where parentID = $addNewPage and displayOrder > $displayOrder;";
            db_query($db, $query);
        }
    }
    else
    {
        $displayOrder = db_query($db, "select max(displayOrder) from cmsPagesMetadata where parentID = $addNewPage;");
        if (db_numRows($displayOrder) > 0)
        {
            list($displayOrder) = db_row($displayOrder, 0);
        }
        else
        {
            $displayOrder = 0;
        }
    }

    sql_addIntToInsert($fields, $values, 'displayOrder', $displayOrder + 1);

    db_query($db, "update cmsPagesMetadata set displayOrder = displayOrder + 1
                  where parentID = $addNewPage and displayOrder > displayOrder;");
    if (db_query($db, "insert into cmsPagesMetadata ($fields) values ($values);"))
    {
        unset($fields);
        unset($values);
        $currentPageID = db_seqCurrentVal($db, 'seq_cmspageIDs');
        $delinkedBody = trim($body);
        cmsDelinkText($delinkedBody);
        sql_addIntToInsert($fields, $values, 'pageID', $currentPageID);
        sql_addScalarToInsert($fields, $values, 'body', $delinkedBody);

        if ($cms_complexityLevel > CMS_BASIC)
        {
            sql_addIntToInsert($fields, $values, 'editedBy', $auth_userID);
        }

        db_query($db, "insert into cmsPages ($fields) values ($values);");
        if (!$delinkedBody)
        {
            // set a blank intial page to being the published version for a new apge
            $revisionID = db_seqCurrentVal($db, 'seq_cmsPageIDs');
            db_query($db, "update cmsPagesMetadata set pubRevision = $revisionID where pageID = $currentPageID");
            $pageLink = cmsPageLink($currentPageID, true);
            if ($cms_fullTextSearch)
            {
                putPageIntoSearch($currentPageID, $revisionID);
            }
        }
        else if ($cms_complexityLevel == CMS_BASIC)
        {
            $newRevision = db_seqCurrentVal($db, 'seq_cmsPageIDs');
            setPublicRevision($newRevision, $currentPageID);
        }
        else
        {
            $newRevision = db_seqCurrentVal($db, 'seq_cmsPageIDs');
            print_leftSide();

            if ($cms_emailNotifications)
            {
                sendAdminGroupEmail($privs->adminGroup, 'cms_content_edit', SPOOL_NEXTSEND,
                                    array('revisionid' => $newRevision), array('page' => $currentPagePath));
            }
        }
    }

    db_endTransaction($db);
}

function setPublicRevision($revisionID, $pageID)
{
    global $cms_complexityLevel, $cms_fullTextSearch, $cms_emailNotifications, $auth_userID;
    $db = db_connect();
    db_startTransaction($db);
    $revInfo = db_query($db, "select md.title from cmsPages rev join cmsPagesMetadata md on (rev.pageID = md.pageID) where rev.revision = $revisionID and rev.pageID = $pageID;");
    if (db_numRows($revInfo) > 0)
    {
        list($pageTitle) = db_row($revInfo, 0);
        $pageLink = cmsPageLink($pageID, true);
        db_query(db_connect(), "update cmsPagesMetadata set pubRevision = $revisionID where pageID = $pageID;");

        if ($cms_fullTextSearch)
        {
            putPageIntoSearch($currentPageID, $revisionID);
        }

        if ($cms_complexityLevel > CMS_BASIC)
        {
            // mark the revision as published by ....
            db_query($db, "update cmsPages set approvedBy = $auth_userID, approvedOn = now() where revision = $revisionID;");
            print_leftSide(); // ensure we have a $currentPagePath
            if ($cms_emailNotifications)
            {
                sendAdminGroupEmail($privs->adminGroup, 'cms_content_approved', SPOOL_NEXTSEND, array('revisionid' => $revisionID), array('page' => $currentPagePath));
            }
        }
    }
    db_endTransaction($db);
}

function validateNewPage()
{
    global $title, $addNewPage;

    $title = trim(str_replace('/', ' ', $title));
    if (!trim($title))
    {
        print_msg('ERROR', 'Input Error', "A title is required for new pages. Please supply one and try again.", 'cms/np1', false, 'print_leftSide');
        return false;
    }

    $db = db_connect();
    if (db_numRows(db_query($db, "select pageID from cmsPagesPublished where
                                pageID in (select pageID from cmsPagesMetadata where parentID = $addNewPage)
                                and title = '" . addslashes($title) . "';")) > 0)
    {
        print_msg('ERROR', 'Input Error', "A page with that same title already exists. Please modify the title and try again.",  'cms/np2', false, 'print_leftSide');
        return false;
    }

    return true;
}

$currentPageID = intval($currentPageID);
$addNewPage = intval($addNewPage);
$approveRevision = intval($approveRevision);

print_adminHeader();

// save the pages
if ($savePageChanges)
{
    if ($currentPageID)
    {
        saveExistingPage($currentPageID, $privs);
    }
    else if ($addNewPage && validateNewPage())
    {
        saveNewPage();
        unset($addNewPage);
    }

    unset($savePageChanges);
}

if ($currentPageID)
{
    $privs = new cmsPagePriveleges($currentPageID);
    if ($deletePage)
    {
        if ($privs->canDelete)
        {
            $parentPage = db_query(db_connect(), "select parentID from cmsPagesMetadata where pageID = $currentPageID;");
            if (db_numRows($parentPage) > 0)
            {
                list($parentPage) = db_row($parentPage, 0);
            }
            else
            {
                $parentPage = 0;
            }
            db_query(db_connect(), "delete from cmsPagesMetadata where pageID = $currentPageID;");
            $currentPageID= $parentPage;
        }
        else
        {
            print_msg('ERROR', 'Access Violation',
                                'You are not allowed to delete this page!',
                                'cms/del1', false, 'print_leftSide');
        }
    }
    else if ($approveRevision)
    {
        if ($privs->canPublish)
        {
            setPublicRevision($approveRevision, $currentPageID);
        }
        else
        {
            print_msg('ERROR', 'Access Violation', 'You are not allowed to published this page.', 'cms/pb1', false,  'print_leftSide');
        }
    }
    else if ($denyRevision)
    {
        $db = db_connect();
        if ($privs->canPublish)
        {
            $revInfo = db_query($db, "select md.title from cmsPages rev join cmsPagesMetadata md on (rev.pageID = md.pageID) where rev.revision = $denyRevision and rev.pageID = $currentPageID;");
            if (db_numRows($revInfo) > 0)
            {
                list($editedBy) = db_row($revInfo, 0);
                print_leftSide(); // ensure we have a $currentPagePath
                if ($admin_enableMailAlerts)
                {
                    sendAdminGroupEmail($privs->adminGroup, 'cms_content_denied', SPOOL_NEXTSEND, array('revisionid' => $denyRevision, 'deniedby' => $auth_userID), array('page' => $currentPagePath));
                    sendAdminEmail($editedBy, 'cms_content_denied', SPOOL_NEXTSEND, array('revisionid' => $approveRevision, 'deniedby' => $auth_userID), array('page' => $currentPagePath));
                }
                db_query($db, "delete from cmsPages where revision = $denyRevision and pageID = $currentPageID;");
            }
        }
        else
        {
            print_msg('ERROR', 'Access Violation', 'You are not allowed to published this page.', 'cms/dr1', false,  'print_leftSide');
        }
    }
}

print_leftSide($addNewPage);

if ($currentPageID)
{
    $edittedType = false;

    if (intval($editType) > 0)
    {
        // get and execute the edit callback for this type
        $db = db_connect();
        $type = db_query($db, "select md.title, cmsTypes.name, cmsTypes.object from cmsTypes join cmsPagesMetadata md on (cmsTypes.typeID = md.typeID) where md.pageID = $currentPageID;");

        if (db_numRows($type) > 0)
        {
            list($pageTitle, $typeName, $typeObject) = db_row($type, 0);
            if ($type && is_readable("$cms_customObjectFilePath/$typeObject.php"))
            {
                include_once("$cms_customObjectFilePath/$typeObject.php");
                $objName = "cms_$typeObject";
                $typeEditor = new $objName($currentPageID);
                if ($typeEditor->isEditable)
                {
                    $edittedType = !$typeEditor->edit("$thisPage?currentPageID=$currentPageID", $typeName, $pageTitle);
                }
            }
            else if ($type && is_readable("$cms_objectFilePath/$typeObject.php"))
            {
                include_once("$cms_objectFilePath/$typeObject.php");
                $objName = "cms_$typeObject";
                $typeEditor = new $objName($currentPageID);
                if ($typeEditor->isEditable)
                {
                    $edittedType = !$typeEditor->edit("$thisPage?currentPageID=$currentPageID", $typeName, $pageTitle);
                }
            }
        }
    }

    if (!$edittedType)
    {
        print_page($currentPageID);
    }
}
else if ($addNewPage)
{
    print_page($addNewPage, false);
}
else
{
    print_msg("INFORMATION", "CMS Status Report", "DB loaded properly...<br>Page types registered...<br>Base nodes installed...<br>Ready for editting and/or new pages");
}

print_rightSide($addNewPage);

?>
