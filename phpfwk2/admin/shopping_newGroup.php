<?php

include_once("$phpfwkIncludePath/images.php");
include_once("$phpfwkIncludePath/admin/include/shopping_catalogs.php");

$thisPage = $newGroupPage;
$parent = intval($parent);
$gID = intval($gID);

print_adminHeader();
print '<div style="padding: 3px">';

if ($editting)
{
    if ($gID < 1)
    {
        print_msg('ERROR', 'Group ID Required',
                  'A group id is required to edit a group.', 'EditGrp/1');
        print_adminFooter();
        exit;
    }

    $db = db_connect();
    $groupInfo = db_query($db, "SELECT active, name FROM skuGroups
                               WHERE groupID = $gID;");

    if (db_numRows($groupInfo) < 1)
    {
        print_msg('ERROR', 'Group Does Not Exist',
                  'The requested group could not be found. Perhaps someone already deleted it?', 'EditGrp/2');
        print_adminFooter();
        exit;
    }

    list($groupIsActive, $groupName) = db_row($groupInfo, 0);
}

if ($saveNow)
{
    if (validateGroup($parent, $gID, $groupIsActive, $groupName))
    {
        if ($gID > 0)
        {
            updateGroup($parent, $gID);
        }
        else
        {
            addGroup($parent);
        }

        print_redirect("$catalogPage?gID=$parent");
        //print_link("$catalogPage?gID=$parent", "$catalogPage?gID=$parent");
    }
    else
    {
        print_groupForm($parent, $gID);
    }
}
else
{
    print_groupForm($parent, $gID);
}

print '</div>';
print_adminFooter();

?>
