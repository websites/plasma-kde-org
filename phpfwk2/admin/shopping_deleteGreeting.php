<?php

include_once("$phpfwkIncludePath/admin/include/shopping_greetings.php");

$greetingID = intval($greetingID);

print_adminHeader();
print '<div style="padding: 3px">';

if ($greetingID < 1)
{
    print_msg('ERROR', 'Greeting ID Required',
              'A greeting id is required to delete a greeting.', 'DelGreet/1');
    print_adminFooter();
    exit;
}

$db = db_connect();
$greetingInfo = db_query($db, "SELECT active, subject FROM emailGreetings
                              WHERE greetingID = $greetingID;");

if (db_numRows($greetingInfo) < 1)
{
    print_msg('ERROR', 'Greeting Does Not Exist',
              'The requested greeting could not be found. Perhaps someone already deleted it?', 'DelGreet/2');
    print_adminFooter();
    exit;
}

list($greetingIsActive, $subject) = db_row($greetingInfo, 0);
unset($activeText);

if (db_boolean($greetingIsActive))
{
    $activeText = "<p><strong>This $type is active!</strong></p>";
}

if ($deleteNow == 1)
{
    if (db_delete($db, 'emailGreetings', "greetingID = $greetingID"))
    {
        print_msg('INFORMATION', "Email Greeting Deleted",
                  "The <strong>$subject</strong> Email Greeting was deleted successfully.<br><a href=\"$greetingsPage\">Click here to return to the greetings listing.</a>");
    }
    else
    {
        print_msg('ERROR', "Email Greeting Not Deleted",
                  "An error was encountered when trying to delete the <strong>$subject</strong> Email Greeting.<br><a href=\"$$greetingsPage\">Click here to return to the greetings listing.</a>", 'DelGreet/3');
    }
}
else
{
    print_msg('WARNING', "Delete Email Greeting : $subject?",
              "Are you sure you wish to delete the <strong>$subject</strong> greeting?$activeText<p><a href=\"$deleteGreetingPage?greetingID=$greetingID&deleteNow=1\">Yes</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"$greetingsPage\">No</a></p>");
}

print '</div>';

print_adminFooter();

?>
