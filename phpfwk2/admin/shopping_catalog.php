<?php

include_once("$phpfwkIncludePath/admin/include/shopping_catalogs.php");

print_adminHeader();
print '<div style="padding: 3px">';

$parent = intval($parent);
$gID = intval($gID);

if ($gID > 0)
{
    if ($alpha == 1)
    {
        $db = db_connect();
        $items = db_query($db, "SELECT sku FROM skus WHERE groupID = $gID ORDER BY name;");
        $numItems = db_numRows($items);

        unset($sql);
        for ($i = 0; $i < $numItems; ++$i)
        {
            list($sku) = db_row($items, $i);
            $sql .= "UPDATE skus SET displayOrder = " . ($i + 1) . " WHERE sku = $sku; ";
        }

        if ($sql)
        {
            db_query($db, $sql);
        }
    }
    else if ($orderByCode == 1)
    {
        db_query(db_connect(), "SELECT orderGroupByCodes($gID);");
    }
}

print_groupList($parent, $gID);

print '</div>';
print_adminFooter();

?>
