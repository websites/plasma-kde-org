<?
include_once("$phpfwkIncludePath/admin/include/administratorObj.php");
include_once("$phpfwkIncludePath/address.php");
include_once("$phpfwkIncludePath/cmsCommon.php");

// require super user access
requiresSuper();

// page vars
$thisPage = 'groupAdmin.php';
$minLoginLength = 4;
$minPasswordLength = 5;

define(GROUPADMIN_EDITGROUP, 1);
define(GROUPADMIN_NEWGROUP, 2);
define(GROUPADMIN_UPDATE, 3);
define(GROUPADMIN_NEW, 4);

$orderCols = array('Short Name' => 0, 'Fullname' => 1, 'Active' => 2);
$orderColIDs = array(0 => 'lower(name)', 1 => 'lower(fullname)', 2 => 'active');

/*
 * UI FUNCTIONS
 */
function printList()
{
    global $thisPage, $common_bizName, $auth_groupTable;
    global $orderCols, $orderColIDs, $oBy, $oHow, $alpha;
    $db = db_connect();

    if (!$alpha)
    {
        $alpha = 'All';
    }

    print "<h3 class=\"areaheader\">$common_bizName Administration Groups...</h3>";

    unset($where);
    sql_addToWhereClause($where, 'where', 'groupID', '>', 0);
    sql_addAlphaToWhereClause($where, 'name');
    sql_addSortColToOrderBy($orderBy, $orderColIDs);

    $groups = db_query($db, "SELECT groupID, name, fullname, active FROM $auth_groupTable $where $orderBy;");
    $numGroups = db_numRows($groups);

    print_form($thisPage);
    print_hidden("editMode", GROUPADMIN_NEWGROUP);
    print_submit("btnAdd", "Add A New Group");
    print '</form>';

    print '<table border="0" class=box cellpadding="5" cellspacing="0" width="100%">';
    print '<tr><td colspan=3 align=center>';
    print_alphaSelector($thisPage);
    print '</tr></td></tr>';
    print_sortableHeaders($orderCols, $orderColIDs, "$thisPage?alpha=$alpha");
    $odd = true;
    for ($i = 0; $i < $numGroups; ++$i)
    {
        list($groupID, $name, $fullname, $active) = db_row($groups, $i);
        if (!$name)
        {
            $name = '???';
        }
        $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
        $odd = !$odd;

        print "<tr>";
        print "<td $class valign=top><a href=\"$thisPage?editMode=1&groupID=$groupID\">$name</a></td>";
        print "<td $class valign=top>$fullname</td>";
        print "<td $class valign=top>" . (db_boolean($active) ? 'Yes' : 'No') . "</td>";
        print "</tr>";
    }
    print '</table></form><p>';

    print_form($thisPage);
    print_hidden("editMode", GROUPADMIN_NEWGROUP);
    print_submit("btnAdd", "Add A New Group");
    print '</form>';
}

function print_groupForm($groupID = -1)
{
    global $maxPasswordFailures, $thisPage, $common_baseURL, $cms_TopPageID;
    global $auth_userTable, $auth_userGroupsTable, $auth_groupTable;

    $new = $groupID < 0;

    $db = db_connect();
    if (!$new)
    {
        $groupName = db_query($db, "SELECT name, header, footer, homepageID, mnemonic, fullname, active
                                   FROM $auth_groupTable WHERE groupID = $groupID;");
        if (db_numRows($groupName) > 0)
        {
            list($name, $header, $footer, $homepageID, $mnemonic, $fullname, $active) = db_row($groupName, 0);
            $active = db_boolean($active);
        }
        else
        {
            print_msg('ERROR', 'No Such Group',
                      'The requested administrator group could not be found for edditing. Perhaps someone deleted it?', 'grpadm/1');
            return;
        }
    }

    $title = $new ? 'Create New Administrator Group' : "Edit Administrator Group '$name'";
    $groups = db_query($db, "select groupID, name from $auth_groupTable order by name;");
    $homepages = db_query($db, "SELECT pageID, title FROM cmsPagesPublished WHERE parentID in (select pageID from cmsPagesMetadata where parentID is null);");
    ?>

    <form action="<?php $thisPage ?>" method="post">
    <table border="0" cellpadding="5" cellspacing="0"  class="box">
    <tr>
        <td colspan="2"  class="largeColoredHeader"><?php print $title; ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">Group Name</td>
        <td><?php print_textbox("name", $name, 25, 100); ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">Is Active?</td>
        <td><?php print_boolElement("active", $active); ?></td>
    </tr>
    <tr>
        <td colspan="2"  class="largeColoredHeader">CMS Integration</td>
    <tr>
        <td class="formElementTitle">Header File</td>
        <td><?php print_textbox("header", $header, 25, 100); ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">Footer File</td>
        <td><?php print_textbox("footer", $footer, 25, 100); ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">Mnemonic</td>
        <td><?php print_textbox("mnemonic", $mnemonic, 25, 100); ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">Full Name</td>
        <td><?php print_textbox("fullname", $fullname, 25); ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">Home Page</td>
        <td><?php print_selectQuery("homepageID", $homepages, $homepageID); ?></td>
    </tr>
    <tr>
        <td class="formElementTitle">&nbsp;</td>
        <td>
        <?
            if ($new)
            {
                print_submit("btnEdit", "Create");
                print_hidden('editMode', GROUPADMIN_NEW);
            }
            else
            {
                print_submit("btnEdit", "Save");
                print_hidden('editMode', GROUPADMIN_UPDATE);
                print_hidden('groupID', $groupID);
            }
    print '</td></tr>';

    if (!$new)
    {
        $users = db_query($db, "SELECT u.userID, g.primaryGroup,
                                      COALESCE(u.lastName, '') || COALESCE(', ' || u.firstName, '') || COALESCE(' [' || u.login || ']', '')
                               FROM $auth_userTable u join $auth_userGroupsTable g on (u.userID = g.userID and g.groupID = $groupID) ORDER BY lower(lastName);");
        $numUsers = db_numRows($users);
        print '<tr><td valign=top style="border-top: 1px solid;">&nbsp;</td><td style="border-top: 1px solid;">';

        if ($numUsers < 1)
        {
            print 'No users belong to this group';
        }

        $hasPrimaryMembers = false;
        for ($i = 0; $i < $numUsers; ++$i)
        {
            list($userID, $primary, $user) = db_row($users, $i);
            $primary = db_boolean($primary);

            if ($i == 0 && $primary)
            {
                print '<strong>Users: Primary Group</strong><br>';
                $hasPrimaryMembers = true;
            }
            else if ($i == 0 || ($hasPrimaryMembers && !$primary))
            {
                print '<strong>Users: Extra Group</strong><br>';
                $hasPrimaryMembers = false;
            }
            print "<a href=\"$common_baseURL/userAdmin.php?adminID=$userID&editMode=1\">$user</a><br>";
        }
        print '</td></tr>';
    }

    print '</table></form>';
}

// start the page - html head, css, body
print_adminHeader('Administrators');
print '<div style="padding: 3px">';

$groupID = (strlen($groupID) > 0 && intval($groupID) == $groupID) ? intval($groupID) : -1;
$editMode = intval($editMode);

/*
 * $editMode states
 * (1) admin listing has been submitted, show edit form
 * (2) edit form has been submitted, validate data and update data
 * (3) add new admin has been selected, show add form
 * (4) a new addition has been submitted, validate data and insert
 * print "| $editMode |";
 */

if ($editMode == GROUPADMIN_EDITGROUP)
{
    print_groupForm($groupID); // print the edit form
}
else if ($editMode == GROUPADMIN_UPDATE && $groupID > -1)
{
    // validate the data
    if (empty($name))
    {
        print_msg('ERROR', 'Invalid Name', "A name is required.", 'grpadm/11');
        print_groupForm($groupID);
    }
    else if (db_numRows(db_query(db_connect(), "SELECT groupID FROM $auth_groupTable
                                                WHERE name = '" . addslashes($name) . "' AND
                                                      groupID != $groupID;")) > 0)
    {
        print_msg('ERROR', 'Group Exists By That Name',
                  'Another groups already exists with that name', 'grpadm/2');
        print_groupForm($groupID);
    }
    else
    {
        unset($fields);
        sql_addScalarToUpdate($fields, 'name', $name);
        sql_addScalarToUpdate($fields, 'header', trim($header));
        sql_addScalarToUpdate($fields, 'footer', trim($footer));
        sql_addScalarToUpdate($fields, 'mnemonic', trim($mnemonic));
        sql_addScalarToUpdate($fields, 'fullname', trim($fullname));
        sql_addIntToUpdate($fields, 'homepageID', $homepageID);
        sql_addBoolToUpdate($fields, 'active', $active);

        if (db_update(db_connect(), $auth_groupTable, $fields, "groupID = $groupID"))
        {
            print_msg('INFORMATION', 'Status Message',
                      'The group was successfuly updated.', 'grpadm/4');
            print_groupForm($groupID);
        }
        else
        {
            $statusMsg = 'The update failed, please inform the system administrator with as much troubleshooting information as possible.';
            print_msg('ERROR', 'Status Message', $statusMsg, 'grpadm/3');
            print_groupForm($groupID);
        }
    }
}
else if ($editMode == GROUPADMIN_NEWGROUP)
{
    print_groupForm(); // print the new administrator form
}
else if ($editMode == GROUPADMIN_NEW)
{
    // validate the data
    // validate the data
    if (empty($name))
    {
        print_msg('ERROR', 'Invalid Name', "A name is required.", 'grpadm/13');
        print_groupForm($groupID);
    }
    else if (db_numRows(db_query(db_connect(), "SELECT groupID FROM $auth_groupTable
                                                WHERE name = '" . addslashes($name) . "' AND
                                                groupID != $groupID;")) > 0)
    {
        print_msg('ERROR', 'Group Exists By That Name',
                  'Another groups already exists with that name', 'grpadm/14');
        print_groupForm($groupID);
    }
    else
    {
        unset($fields);
        unset($values);
        sql_addScalarToInsert($fields, $values, 'name', $name);
        sql_addScalarToInsert($fields, $values, 'header', trim($header));
        sql_addScalarToInsert($fields, $values, 'footer', trim($footer));
        sql_addScalarToInsert($fields, $values, 'mnemonic', $mnemonic);
        sql_addScalarToInsert($fields, $values, 'fullname', $fullname);
        sql_addBoolToInsert($fields, $values, 'active', $active);

        if (db_insert(db_connect(), $auth_groupTable, $fields, $values))
        {
            print_msg('INFORMATION', 'Status Message', 'The group was successfuly updated.', 'grpadm/4');
            $groupID = db_seqCurrentVal(db_connect(), "groupIDs");
            print_groupForm($groupID);
        }
        else
        {
            $statusMsg = 'The update failed, please inform the system administrator with as much troubleshooting information as possible.';
            print_msg('ERROR', 'Status Message', $statusMsg, 'grpadm/3');
            print_groupForm($groupID);
        }
    }

}
else
{
    printList(); // print the list of admins
}

// end the body and html
print '</div>';
print_adminFooter();

?>
