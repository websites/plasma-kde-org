<?

define(MAX_THUMBNAIL_PIXELS, 125);
define(MAX_ALBUMPHOTO_PIXELS, 500);

function dir_exists($path, $create = false)
{
    if (! file_exists($path))
    {
        if ($create)
        {
            if(! @mkdir($path, 0755))
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    return true;
}

function move_uploadedPhoto(&$upload, $dir, $filepath, $filename)
{
    if (! dir_exists($dir, true))
    {
        print_msg("ERROR", "DIRECTORY ERROR",
                  "The directory : \"$dir\" could not be created.<br>
                   Please contact the web administrator.");
    }

    if (file_exists($filepath))
    {
        print_msg("ERROR", "FILE ERROR",
                  "The file : \"$filename\" already exists on the server.<br>
                   Please choose another name for this file.");
        return false;
    }
    else
    {
        if (! move_uploaded_file($upload, $filepath))
        {
            print_msg("ERROR", "FILE ERROR",
                      "There was an error uploading the file : \"$filename\".<br>
                       Please contact the web administrator.");
            return false;
        }
        else
        {
            chmod($filepath, 0644);
        }
    }

    return true;
}

/*
 * create_scaledPhoto -> create a scaled photo
 *
 * $filepath : the original filepath
 * $scaledFilePath : the new file path to save the scaled image
 * $scaledSize : the maximum size of the image length or width (in pixels)
 */
function create_scaledPhoto($filepath, $scaledFilePath, $scaledSize, $enlarge = true)
{
    if ($scaledSize < 1)
    {
        print_msg('error', 'Scaling Error!',
                  "The image $filepath was requested to be scaled to :$scaledSize.<br>
                   The Scale size must be greater than 0.");
        return false;
    }

    $im = imagick_readimage($filepath);
    if ( imagick_iserror($im) )
    {
        $reason = imagick_failedreason($im);
        $description = imagick_faileddescription($im);
        print_msg('error', 'Image Read Error!',
                  "handle failed!<BR>\nReason: $reason<BR>\nDescription: $description<BR>\n");
        return false;
    }

    $scale = 1;

    $dimensions = getImageSize($filepath);
    $src_w = $dimensions[0];
    $src_h = $dimensions[1];

    if ($src_w > $scaledSize || $src_h > $scaledSize)
    {
        // major image shrinkage ... hmmmm cold water maybe
        $est_w_scale = ($src_w / $scaledSize);
        $est_h_scale = ($src_h / $scaledSize);

        if ($est_h_scale && $est_w_scale)
        {
            $scale = ($est_h_scale > $est_w_scale) ? $est_h_scale : $est_w_scale;
        }

        $dest_w = ($src_w / $scale);
        $dest_h = ($src_h / $scale);
    }
    else
    {
        if ($enlarge)
        {
            // giving the image a boner
            $est_w_scale = ($scaledSize / $src_w);
            $est_h_scale = ($scaledSize / $src_h);

            if ($est_h_scale && $est_w_scale)
            {
                $scale = ($est_h_scale < $est_w_scale) ? $est_h_scale : $est_w_scale;
            }

            $dest_w = ($src_w * $scale);
            $dest_h = ($src_h * $scale);
        }
        else
        {
            // eblargibng the image was turned off
            $dest_w = $src_w;
            $dest_h = $src_h;
        }
    }

    if (!imagick_scale($im, $dest_w, $dest_h, "!"))
    {
        $reason = imagick_failedreason($im);
        $description = imagick_faileddescription($im);
        print_msg('error', 'Image Scale Error!',
                  "handle failed!<BR>\nReason: $reason<BR>\nDescription: $description<BR>\n");
        return false;
    }

    if (!imagick_writeimage($im, $scaledFilePath))
    {
        $reason = imagick_failedreason($im);
        $description = imagick_faileddescription($im);
        print_msg('error', 'Image Write Error!',
                  "handle failed!<BR>\nReason: $reason<BR>\nDescription: $description<BR>\n");
        return false;
    }

    return true;
}
?>
