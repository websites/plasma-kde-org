<?
/*
 *  Template Editor
 */
include_once('include/common.php');
include_once('include/auth_common.php');

requiresSuper();

$thisPage = 'emailTemplates.php';

//db_query($db,"select categoryid, title from templatecategories order by title;");
function print_templates()
{
    global $baseAdminURL;
    $db = db_connect();
    $categories = db_query($db,"select categoryID, title from templatecategories order by title;");
    $numCategories = db_numRows($categories);

    print "<ul>";
    for ($i=0 ; $i < $numCategories; ++$i)
    {
        list($categoryID, $categoryTitle) = db_row($categories, $i);
        print "<li>$categoryTitle ";
        print "<a href=\"$baseAdminURL/emailTemplates.php?action=editCateg&categoryID=$categoryID\">Edit</a>";

        $templateInfo = db_query($db, "select templateID, name from templates where categoryID = $categoryID order by name;");
        $numTemplates = db_numRows($templateInfo);
        // templates in this category
        print "<ul>";
        if ($numTemplates > 0)
        {
            for ($j=0 ; $j < $numTemplates; $j++)
            {
                list($templateID, $templateName) = db_row($templateInfo, $j);
                print "<li>$templateName ";
                print "<a href=\"$baseAdminURL/emailTemplates.php?action=edittemplate&templateID=$templateID\">Edit</a>";
                print " | <a href=\"$baseAdminURL/emailTemplates.php?action=sql&templateID=$templateID\">Database</a>";
                print "</li>";
            }
        }
        print "<li><a href=\"$baseAdminURL/emailTemplates.php?action=addtemplate&categoryID=$categoryID\">Add new template</a></li>
               </ul><br></li>";
    }
    print "<br><li><a href=\"$baseAdminURL/emailTemplates.php?action=addcateg\">Add new category</a></li>";
    print "</ul>";
}

/*
 *  Get the data for the new category to be added
 */
function print_addTemplateCategory()
{
    global $baseAdminURL, $thisPage;
?>
    <h3 class="areaheader">Add Template Category</h3>
    <form action="<?php print "$baseAdminURL/$thisPage"; ?>" method="post">
    Category Name: <input type="text" name="categoryName"> <input type="submit" name="submit" value="Add Category">
    <input type="hidden" name="action" value="addcategdb">
    </form>
<?
}

function print_editTemplateCategory($categoryID)
{
    global $baseAdminURL, $thisPage;

    if ($categoryID < 1)
    {
        return;
    }

    $db = db_connect();
    unset($categoryName);
    $result = db_query($db, "select title from templatecategories where categoryID = $categoryID;");
    if (db_numRows($result) == 1)
    {
        list($categoryName) = db_row($result,0);
    }

?>
    <h3 class="areaheader">Edit Template Category</h3>
    <form action="<?php print "$baseAdminURL/$thisPage"; ?>" method="post">
    Category Name: <input type="text" value="<?php print $categoryName?>" name="categoryName">
            <input type="hidden" name="action" value="editcategdb">
            <input type="hidden" name="categoryDI" value="<?php print $categoryID;?>">
            <input type="submit" name="submit" value="Save Changes"></td>
    </form>
<?
}

/*
 *  Insert new category into the categories table.
 */
function createTemplateCategory($value)
{
    $db = db_connect();
    unset($fields);
    unset($values);
    sql_addScalarToInsert($fields, $values, 'title', $value);
    $results = db_query($db, "insert into templatecategories ($fields) values ($values);");
}

function saveTemplateCategory($categoryID, $newvalue)
{
    $db = db_connect();
    unset($fields);
    sql_addScalarToUpdate($fields, 'title', $value);
    $result = db_query($db, "update templatecategories set $fields where categoryiD = $categoryID;");
}

/*
 *  Delete a template category


function delTemplateCateg($categoryID)
{
    $db = db_connect();
    $result = db_query($db,"delete from templatecategories where categoryid = $categoryID");
}
 */

/*
 *  Add new template
 */

function print_editTemplate($templateID = 0, $templateCategory = 0)
{
    global $printTemplates, $action, $baseAdminURL, $thisPage;

    $db = db_connect();

    if ($templateID)
    {
        if ($action == 'edittemplatedb')
        {
            global $templateCategory, $templateName, $templateTitle, $templateBody;
        }
        else
        {
            $template = db_query($db,"select categoryID, name, title, body from templates where templateid = $templateID;");
            if (db_numRows($template) < 1)
            {
                $templateID = 0;
            }
            else
            {
                list($templateCategory, $templateName, $templateTitle, $templateBody) = db_row($template, 0);
            }
        }

        $provides = array();
        $providesQuery = db_query($db, "select provides from templateSQL where templateID = $templateID;");
        $numProvides = db_numRows($providesQuery);
        for ($i = 0; $i < $numProvides; ++$i)
        {
            list($temp) = db_row($providesQuery, $i);
            $temp = explode("\n", $temp);
            foreach ($temp as $provide)
            {
                array_push($provides, trim($provide));
            }
        }
        $provides = '[[' . implode($provides, ']], [[') . ']]';
    }

    $categories = db_query($db, "select categoryID, title from templateCategories order by title;");
    $numCategories = db_numRows($categories);

    if ($numCategories < 1)
    {
        print_inputError('No categories have been defined. Please defined one first.');
        $printTemplates = true;
        return;
    }

    ?>
    <h3 class="areaheader">Edit Template</h3>

    <form action="<?php print "$baseAdminURL/$thisPage"; ?>" method="post">
    <table border="0">
        <tr>
            <td valign='top'>Category:</td>
            <td><?php print_selectQuery('templateCategory', $categories, $templateCategory, true); ?>
            </td>
        </tr>
        <tr>
            <td valign='top'>Name:</td>
            <td><?php ($templateID > 0) ? print $templateName : print_textbox('templateName', $templateName); ?></td>
        </tr>
        <?php
        if ($templateID > 0)
        {
        ?>
        <tr>
            <td valign='top'>Provides:</td>
            <td><?php print $provides; ?></td>
        </tr>
        <?php
        }
        ?>
        <tr>
            <td valign="top">Title:</td>
            <td><?php print_textarea('templateTitle', $templateTitle, 5, 60); ?></td>
            </td>
        </tr>
        <tr>
            <td valign="top">Body:</td>
            <td><?php print_textarea('templateBody', $templateBody, 10, 60); ?></td>
        </tr>
        <tr>
            <td colspan="2">
            <input type="hidden" name="templateID" value="<?php print $templateID; ?>">
            <input type="hidden" name="action" value="<?php print (($templateID > 0) ? 'edittemplatedb' : 'addtemplatedb'); ?>">
            <input type="submit" name="newtemplate" value="<?php print (($templateID > 0) ? 'Save Changes' : 'Add Template'); ?>">
            </td>
        </tr>
    </table>
    </form>
<?
}

/*
 *  Add new template to the templates system
 */
function createTemplate($templateCategory,$templateName,$templateTitle,$templateBody)
{
    $db = db_connect();
    unset($fields);
    unset($values);
    sql_addIntToInsert($fields, $values, 'categoryID', $templateCategory);
    sql_addScalarToInsert($fields, $values, 'name', $templateName);
    sql_addScalarToInsert($fields, $values, 'title', $templateTitle);
    sql_addScalarToInsert($fields, $values, 'body', $templateBody);
    $result = db_query($db,"insert into templates ($fields) values ($values);");
}

/*
 *  Modify the contents of a template record...
 */
function saveTemplate($templateID, $templateCategory, $templateName, $templateTitle, $templateBody)
{
    if (!$templateID)
    {
        return;
    }

    unset($fields);
    sql_addScalarToUpdate($fields, 'categoryID', $templateCategory);
    sql_addScalarToUpdate($fields, 'title', $templateTitle);
    sql_addScalarToUpdate($fields, 'body', $templateBody);

    if ($fields)
    {
        db_query(db_connect(), "update templates set $fields where templateid =  $templateID;");
    }
}

/*
 *  Delete template.... Maybe should not be allowed, but their you go
 *
function delTemplate($templateID)
{
    $db = db_connect();
    $result = db_query($db,"delete from templates where templateid= $templateID;");
}*/

function print_editTemplateSql($templateID)
{
    global $printTemplates, $baseAdminURL, $thisPage;
    if ($templateID < 1)
    {
        $printTemplates = true;
        return;
    }

    $db = db_connect();
    $templateName = db_query($db, "select name, title, body from templates where templateID = $templateID;");

    if (db_numRows($templateName) < 1)
    {
        $printTemplates = true;
        return;
    }

    list($templateName, $templateTitle, $templateBody) = db_row($templateName, 0);

    $statements = db_query($db, "select sql, provides, requires from templatesql where templateID = $templateID;");
    $numStatements = db_numRows($statements);

?>
    <h3 class="areaheader">SQL for Template '<?php print $templateName; ?>'</h3>
    <pre><?php print "Subject: $templateTitle<br><br>"; print str_replace('\n', '<br>', $templateBody); ?></pre>
    <form action="<?php print "$baseAdminURL/$thisPage"; ?>" method="post">
    <table border="0">
        <tr>
            <th align=left valign=top>SQL</th>
            <th align=left valign=top>Provides</th>
            <th align=left valign=top>Requires</th>
        </tr>
        <?
            for ($i = 0; $i < $numStatements; ++$i)
            {
                list($sql, $provides, $requires) = db_row($statements, $i);
                print '<tr><td>';
                print_textarea("sqlsql[$i]", $sql, 5, 70);
                print '</td><td>';
                print_textarea("sqlprov[$i]", $provides, 5, 20);
                print '</td><td>';
                print_textarea("sqlreq[$i]", $requires, 5, 20);
                print "</td><tr colspan=3><td><hr></td></tr>";
            }
        ?>
        <tr>
            <td valign=top><textarea wrap=virtual name="sqlsql[<?php print $i; ?>]" rows="5" cols="70"></textarea></td>
            <td valign=top><textarea  name="sqlprov[<?php print $i; ?>]" rows="5" cols="20"></textarea></td>
            <td valign=top><textarea  name="sqlreq[<?php print $i; ?>]" rows="5"  cols="20"></textarea></td>
        </tr>
        <tr>
            <td valign=top><textarea wrap=virtual name="sqlsql[<?php print ($i + 1); ?>]" rows="5" cols="70"></textarea></td>
            <td valign=top><textarea  name="sqlprov[<?php print ($i + 1); ?>]" rows="5" cols="20"></textarea></td>
            <td valign=top><textarea  name="sqlreq[<?php print ($i + 1); ?>]" rows="5"  cols="20"></textarea></td>
        </tr>

        <tr>
            <td colspan=3><hr></td>
        </tr>

        </tr>
        <tr>
            <td colspan="2">
                <input type="hidden" name="action" value="sqlDB">
                <input type="hidden" name="templateID" value="<?php print $templateID; ?>">
                <input type="submit" value="Save Changes">
            </td>
        </tr>
        <tr>
            <td colspan="2">
            Example: Select name as username from employees where employeeID = [[userID]] and employee = ((employee));<br>
            [[]] will be substituted by the variable of the included name and is for numeric or boolean values<br>
            (()) will be substituted and quoted so is more suitable for string and other such values
            </td>
        </tr>
     </table>
     </form>
<?
}


function saveTemplateSql($templateID, $sqlStatements, $sqlProvides, $sqlRequires)
{
    if (!$templateID || !is_array($sqlStatements))
    {
        return;
    }

    if (!is_array($sqlProvides))
    {
        $sqlProvides = array();
    }

    if (!is_array($sqlRequires))
    {
        $sqlRequires = array();
    }

    $db = db_connect();
    db_startTransaction($db);
    $result = db_query($db,"delete from templateSQL where templateID = $templateID;");
    foreach ($sqlStatements as $key => $value)
    {
        if ($sqlStatements[$key] != "")
        {
            unset($fields);
            unset($values);
            sql_addIntToInsert($fields, $values, 'templateID', $templateID);
            sql_addScalarToInsert($fields, $values, 'sql', $sqlStatements[$key]);
            sql_addScalarToInsert($fields, $values, 'provides', $sqlProvides[$key]);
            sql_addScalarToInsert($fields, $values, 'requires', $sqlRequires[$key]);
            db_query($db,"insert into templateSQL ($fields) values ($values);");
        }

    }
    db_endTransaction($db);
}


print_header('Email Template Manager');

/*
 *  Main controll code
 */

$printTemplates = false;
$templateID = intval($templateID);
$categoryID = intval($categoryID);

switch ($action)
{
    case 'addcateg':
        print_addTemplateCategory();
        break;
    case 'addcategdb':
        createTemplateCategory($categoryName);
        $printTemplates = true;
        break;
    case 'editCateg':
        print_editTemplateCategory($categoryID);
        break;
    case 'editcategdb':
        saveTemplateCategory($categoryID, $categoryName);
        $printTemplates = true;
        break;
    /*case 'delCateg':
        delTemplateCategory($templateID);
        break; */
    case 'addtemplate':
        print_editTemplate(0, $categoryID);
        break;
    case 'addtemplatedb':
        createTemplate($templateCategory, $templateName, $templateTitle, $templateBody);
        $printTemplates = true;
        break;
    case 'edittemplate':
        print_editTemplate($templateID);
        break;
    case 'edittemplatedb':
        saveTemplate($templateID, $templateCategory, $templateName, $templateTitle, $templateBody);
        $printTemplates = true;
        break;
/*    case "deletetemplate":
        delTemplate($templateID);
        break;*/
    case 'sql':
        // List all the sql in one window associated with the particular template
        print_editTemplateSql($templateID);
        break;
    case 'sqlDB':
        saveTemplateSql($templateID, $sqlsql, $sqlprov, $sqlreq);
        $printTemplates = true;
        break;
    default:
        $printTemplates = true;
        break;
}

if ($printTemplates)
{
    print '<h3 class="areaheader">EMail Templates...</h3>';
    print_templates();
}

print_footer();
?>
