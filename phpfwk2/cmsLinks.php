<?php

include_once("$phpfwkIncludePath/dmsCommon.php");

// generating links for CMS pages

// each CMS Type may have a link callback function of the form:
//     callback(mixed $identifier, bool $url)
// the callback should then return a url or a path to object that
// $identifier refers to

// when grabbing cms data FROM the db, run the result through cmsRelinkText
// when putting it INTO the db, run it first through cmsDelinkText

function cmsPageLink($identifier, $url)
{
    global $baseURL;
    return $baseURL . "/?currentPage=$identifier";
}

function cmsLinkBuilderRecursive($pageID, $title, $baseURL)
{
    global $thisPage;
    static $depth = 0;

    print "<a href=\"$baseURL&linkPageID=$pageID\">$title</a>";
    $childPages = db_query(db_connect(), "SELECT pageID, title FROM cmsPagesPublished
                                        WHERE parentID = $pageID;");
    $numChildPages = db_numRows($childPages);

    if ($numChildPages > 0)
    {
        print '<ul>';
        for ($i = 0; $i < $numChildPages; ++$i)
        {
            list($pageID, $title) = db_row($childPages, $i);
            print '<li>';
            cmsLinkBuilderRecursive($pageID, $title, $baseURL);
        }
        print '</ul>';
    }

    print "<br>";
}

function cmsLinkBuilder()
{
    global $cms_TopPageID, $cms_LeftPageID, $linkPageID;
    global $thisPage;
    $db = db_connect();
    $linkPageID = intval($linkPageID);
    print '<td valign=top class="listingColumn">';
    print '<span class="listingColumnHeader">Internal Web Pages</span><p>';
    if (!$linkPageID)
    {
        $pages = db_query($db, "SELECT pageID, title, displayOrder FROM cmsPagesPublished
                               WHERE parentID = $cms_TopPageID AND showInNav ORDER BY displayOrder;");

        $numRows = db_numRows($pages);
        print "<b>Top</b><br>";
        for($i = 0; $i < $numRows; ++$i)
        {
            list($pageID, $title) = db_row($pages, $i);
            print "<a href=\"$thisPage&linkPageID=$pageID\">$title</a><br>";
        }

        $pages = db_query($db, "SELECT pageID, title, displayOrder FROM cmsPagesPublished
                               WHERE parentID = $cms_LeftPageID AND showInNav ORDER BY displayOrder;");

        $numRows = db_numRows($pages);
        print "<hr><b>Left</b><br>";
        for($i = 0; $i < $numRows; ++$i)
        {
            list($pageID, $title) = db_row($pages, $i);
            cmsLinkBuilderRecursive($pageID, $title, $thisPage);
        }
    }
    else
    {
        cmsInternalLink(cmsPageLink($linkPageID, true));
    }
}

function cmsInternalLink($url)
{
?>
    <script language="javascript" type="text/javascript">
    <!--
    var highlightedText = window.dialogArguments;
    if (highlightedText == '')
        linkText = '-- add link text here --';
    else
        linkText = highlightedText;

    function returnSelected()
    {
        var htmlStr = "<a href='<?= $url ?>'>" + linkText + "</a>";
        window.returnValue = htmlStr;
        window.close();
    }

    returnSelected();
    //-->
    </script>
<?
}

function cmsImgFromCode($id, $encode = false)
{
    $info = dmsFileInfo($id);

    if (!$info)
    {
        return '';
    }

    if (file_exists($info['path']))
    {
        list($width, $height) = getImageSize($info['path']);
    }

    if ($encode)
    {
        $encoding = "cmslink=\"$id\"";
    }

    $title = $info['title'];
    if ($info['description'])
    {
        $title .= ": " . $info['description'];
    }

    return "$encoding title=\"$title\" alt=\"{$info['title']}\" src=\"{$info['URL']}\"" .
	   ($width > 0 ? " width=\"$width\"" : '') . 
           ($height > 0 ? " height=\"$height\"" : '');
}

function cmsHrefAttrs($typeObject, $id, $encode = false)
{
    global $cms_objectFilePath;
    //print "($typeObject, $id, $encode = false)<br>";

    if ($typeObject == 'dms')
    {
        $url = dmsFileNameByID($id);
    }
    else if ($typeObject && is_readable("$cms_objectFilePath/$typeObject.php"))
    {
        include_once("$cms_objectFilePath/$typeObject.php");
        $objName = "cms_$typeObject";
        $renderer = new $objName($id);
        $url = $renderer->link($id);
    }

    if (!$url)
    {
        $url = cms_renderer::link($id);
    }

    if ($encode)
    {
        $encoding = "cmslink=\"$typeObject::$id\"";
    }

    return "$encoding href=\"$url\"";
}

function cmsHrefFromCode($encoding, $keepEncoding = false)
{
    list($cmsType, $id) = explode('::', $encoding);
    if (isset($cmsType) && $id)
    {
        return cmsHrefAttrs($cmsType, $id, $keepEncoding);
    }
}

function cmsDelinkText(&$text)
{
    global $dms_baseURL, $dms_imageFolderID;
    $text = stripslashes($text);
    $text = preg_replace('/<img[^>]*cmslink="([0-9]+)"[^>]+usemap="([^"]+)"[^>]*>/msUi', '<img cmsmaplinkhack="\1" usemap="\2">', $text);
    $text = preg_replace('/<a[^>]*cmslink="([a-zA-Z0-9]+::.*)"[^>]*>/msUi', '<a cmslink="\1">', $text);
    $text = preg_replace('/<img[^>]*cmslink="0-9]+"[^>]*>/msUi', '<img cmslink="\1">', $text);
    $url = str_replace('/', '\/', "$dms_baseURL/$dms_imageFolderID/");
    $text = preg_replace("/<img [^>]*src=\"$url([0-9]+)_[^\"]*\"[^>]*usemap=\"(.+)\"[^>]*>/msUi", '<img cmsmaplinkhack="\1" usemap="\2">', $text);
    $text = preg_replace("/<img [^>]*src=\"$url([0-9]+)_[^\"]*\"[^>]*>/msUi", '<img cmslink="\1">', $text);
    $text = preg_replace("/<a.*href=\"([a-zA-Z0-9]+::.*)\"[^>]*>/msUi", '<a cmslink="\1">', $text);
    $text = preg_replace('/cmsmaplinkhack/', 'cmslink', $text);
    //print htmlentities($text) . "<br>";
}

function cmsRelinkText(&$text, $keepEncoding = false)
{
    global $dms_baseURL;
    preg_match_all('/<a cmslink="([a-zA-Z0-9]+::.*)".*>/Ui', $text, $matches, PREG_SET_ORDER);
    foreach ($matches as $match)
    {
        $code = $match[1];
        $href = cmsHrefFromCode($code, $keepEncoding);
        //print "        $href = cmsHrefFromCode($code, $keepEncoding);<br>";
        $text = str_replace($match[0], "<a $href>", $text);
    }

    preg_match_all('/<img cmslink="(.*)"(.*)>/Ui', $text, $matches, PREG_SET_ORDER);
    foreach ($matches as $match)
    {
       $code = $match[1];
       $extra = 'border=0 ';
       if (preg_match('/(usemap="[^"]*")/', $text, $map))
       {
           $extra .= $map[1];
       }

       $href = cmsImgFromCode($code, $keepEncoding);
       $text = str_replace($match[0], "<img $href $extra>", $text);
    }
}

?>
