<?

class ShoppingTaxman
{
    var $cartID;
    var $storeID;

    function ShoppingTaxman($cartID, $storeID = -1)
    {
        $this->cartID = intval($cartID);
        $this->storeID = intval($storeID);
    }

    function taxes()
    {
        return 0;
    }

    function printTaxes()
    {
        return '';
    }

    function taxesHtml()
    {
        return '';
    }
}

class ShoppingTaxman_canadian extends ShoppingTaxman
{
    function ShoppingTaxman_ewf($cartID, $storeID = -1)
    {
        ShoppingTaxman::ShoppingTaxman($cartID, $storeID);
    }

    function taxes()
    {
        $db = db_connect();

        $tax = 0.00;

        $cartInfo = db_query($db, "SELECT cart_total({$this->cartID}, 'CAD'), shipCountry
                                   FROM shoppingCarts WHERE cartID = {$this->cartID};");

        if (db_numRows($cartInfo) > 0)
        {
            list($total, $country) = db_row($cartInfo, 0);

            if ($country == 'CA')
            {
                $tax = round(($total * 0.07), 2);
            }
        }

        return $tax;
    }

    function printTaxes()
    {
        $db = db_connect();

        $tax = 0.00;
        $country = '';

        $cartInfo = db_query($db, "SELECT cart_total({$this->cartID}, 'CAD'), shipCountry
                                   FROM shoppingCarts WHERE cartID = {$this->cartID};");

        if (db_numRows($cartInfo) > 0)
        {
            list($total, $country) = db_row($cartInfo, 0);

            if ($country == 'CA')
            {
                $tax = round(($total * 0.07), 2);
            }
        }

        $taxes = number_format($tax, 2, '.', ',');

        if ($country == 'CA')
        {
            print "<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>";
            print "<tr><td class=oddRow colspan=4><b>Taxes:</b> (GST 7%)</td><td align=right>$&nbsp;$taxes</td></tr>";
        }
        else
        {
            print "<tr><td class=oddRow colspan=100%>&nbsp;</td></tr>";
            print "<tr><td class=oddRow colspan=4><b>Taxes:</b></td><td align=right>$taxes</td></tr>";
            print "<tr><td class=oddRow valign=top align=left colspan=2 width=50%><i>Taxes will be based on the shipping information provided. Any taxes will be added to your invoice amount shown here.  You will be contacted with the total price being charged to your credit card.</i></td></tr>";
        }
    }

    function taxesHtml()
    {
        $html = '';

        $db = db_connect();

        $tax = 0.00;
        $country = '';

        $cartInfo = db_query($db, "SELECT cart_total({$this->cartID}, 'CAD'), shipCountry
                                   FROM shoppingCarts WHERE cartID = {$this->cartID};");

        if (db_numRows($cartInfo) > 0)
        {
            list($total, $country) = db_row($cartInfo, 0);

            if ($country == 'CA')
            {
                $tax = round(($total * 0.07), 2);
            }
        }

        $taxes = number_format($tax, 2, '.', ',');

        if ($country == 'CA')
        {
            $html .= "<tr><td colspan=100%>&nbsp;</td></tr>";
            $html .= "<tr><td colspan=4><b>Taxes:</b> (GST 7%)</td><td align=right>$&nbsp;$taxes</td></tr>";
        }
        else
        {
            $html .= "<tr><td colspan=100%>&nbsp;</td></tr>";
            $html .= "<tr><td colspan=4><b>Taxes:</b></td><td align=right>$taxes</td></tr>";
            $html .= "<tr><td valign=top align=left colspan=2 width=50%><i>Taxes will be based on the shipping information provided. Any taxes will be added to your invoice amount shown here.  You will be contacted with the total price being charged to your credit card.</i></td></tr>";
        }

        return $html;
    }
}


global $shopping_customObjectFilePath;

if (file_exists("$shopping_customObjectFilePath/shoppingTaxman.php"))
{
    include_once("$shopping_customObjectFilePath/shoppingTaxman.php");
}

?>
