#! /usr/bin/php -q
<?
/*
 *  cron_mailer.php
 *  This one is designed to run as a cron job, and send email out from mailspool table
 *  I will also echo nothing to reduce clutter around cron..
 *
 *  Bounce count 3 will prohibit mailsending to that address :-)
 *  TODO: cron_bouncecontroll.php should manage bounce controll ...
 *
 *  SO: put userid in instead of email address, in case user changes email address :-) after spool is being generated
 *      select doables, group by hrid, then jsid, to get a list of candidates that need to be sent email.
 *      get email adresses using hrid then jsid from the respective tables.
 *      have a second run to get all the emails of one user in the previous resultset, cycle through the emails and
 *      combine them into ONE_BIG_EMAIL... Yeah.
 *      Send the sucker using directMail().
 *
 */
include ("common.php");

$standardFooter = '';
$digestTopic = '';

function sendMail($userID, $toAddress, $fromAddress, $subject, $toc, $body)
{
    global $debug, $baseURL, $standardFooter;

    if ($toc)
    {
        $message .= wordwrap("<h3>Table of Contents</h3>\n$toc<hr>\n\n");
    }

    $message .= $body;
    $message .= $standardFooter;

    $headers =
"From: $fromAddress
Content-type: text/html; charset=iso-8859-1";

//"Content-type: text/plain; charset=iso-8859-1\r\n"

    if ($debug)
    {
        print "----- Message for $toAddress -----\n";
        print "Subject: $subject\n";
        print "$headers\n$message";
        print "--------------------------------------------------------------------\n";
    }
    else
    {
        mail($toAddress, $subject, $message, $headers);
    }
}

$debug = false;
$db = db_connect();
$query = "select spool.id, spool.userID, spool.fromAddress, spool.subject, spool.body, spool.categoryID,
          mem.email, cat.title
          from mailspool spool join members mem on (spool.userID = mem.userID)
          left join templateCategories cat on (spool.categoryID = cat.categoryID)
          where not done and sendOn <= now()
          order by userID, categoryID, sendOn;";
if ($debug)
{
    print $query . "\n\n";
}
$messages = db_query($db, $query);
$numMessages = db_numRows($messages);

if ($debug)
{
    print "number of messages in queue: $numMessages\n";
}

if ($numMessages > 0)
{
    $messageIDs = array();
    $currentUserID = 0;
    $currentCategoryID = 0;
    $numItems = 0;
    for ($i = 0 ; $i < $numMessages; ++$i)
    {
        list($messageID, $userID, $fromAddress, $subject, $body,
             $categoryID, $toAddress, $categoryTitle) = db_row($messages, $i);

        if ($currentUserID != $userID)
        {
            if ($i > 0)
            {
                if ($numItems > 1)
                {
                    $currentTOC .= '</ol></li></ul>';
                    sendMail($currentUserID, $currentToAddress, $currentFromAddress, $digestTopic, $currentTOC, $currentEmail);
                }
                else
                {
                    sendMail($currentUserID, $currentToAddress, $currentFromAddress, $currentSubject, '', nl2br($currentBody));
                }
            }
            unset($currentTOC);
            unset($currentEmail);
            unset($currentCategoryID);
            $currentToAddress = $toAddress;
            $currentFromAddress = $fromAddress;
            $currentUserID = $userID;
            $currentBody = $body;
            $currentSubject = $subject;
            $numItems = 0;
        }

        ++$numItems;
        if ($currentCategoryID != $categoryID)
        {
            $currentCategoryID = $categoryID;

            if (!$categoryTitle)
            {
                $categoryTitle = 'Miscelaneous';
            }

            $currentTOC .= "</ol><br></li>\n<li>$categoryTitle\n\t<ol>";
        }

/*
        if ($debug)
        {
            print "----- Message $i -----\n$messageID, $userID, $fromAddress, $subject, $body, $toEmail\n\n";
        }
*/

        $currentTOC .= "\n\t\t<li value=\"$numItems\"><a href=\"#$numItems\">$subject</a></li>";
        $currentEmail .= "<p><strong><a name=$numItems>$numItems. $subject</a></strong><br>\n" . nl2br($body) . "</p>\n\n";
        array_push($messageIDs, $messageID);

        // set the spooled message to done 100 at a time
        if ($i > 0 && $i % 100 == 0)
        {
            $query = "update mailspool set done = true where id in (" . implode(',', $messageIDs) . ");";
            if ($debug)
            {
                print "$query\n\n";;
            }
            else
            {
                db_query($db, $query);
            }
            $messageIDs = array();
        }
    }

    // send the last email out
    if ($numItems > 1)
    {
        $currentTOC .= '</ol></li></ul>';
        sendMail($currentUserID, $currentToAddress, $currentFromAddress, $digestTopic, $currentTOC, $currentEmail);
    }
    else
    {
        sendMail($currentUserID, $currentToAddress, $currentFromAddress, $currentSubject, '', nl2br($currentBody));
    }

    // set the spooled messages to done
    if ($i % 100 != 0)
    {
        $query = "update mailspool set done = true where id in (" . implode(',', $messageIDs) . ");";
        // update the last messages
        if ($debug)
        {
            print "$query\n\n";
        }
        else
        {
            db_query($db, $query);
        }
    }
}

?>
