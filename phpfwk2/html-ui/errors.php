<?php

/*
 * print a status message in a nicely formatted manner
 *
 * $messageType: one of 'ERROR', 'WARNING', 'INFORMATION' (case insensitive)
 * $title: the title of the message
 * $message: the actual message
 * $msgID : an Id for easy eye catching
 * $sendEmail: if $techSupportEmail is set and sendEmail is true, this message is sent to tech support
 * $preFunc: a function to run prior to printing anything out. useful for header type stuff
 */
function print_msg($messageType, $title, $message,
                   $msgID = '', $sendMail = false, $preFunc = '', $spacing = false)
{
    global $siteHost, $techSupportEmail;

    if ($preFunc && function_exists($preFunc))
    {
        $preFunc();
    }

    if ($sendMail && isset($techSupportEmail))
    {
        mail($techSupportEmail,
            "Status Message: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" ,
            "$messageType: $title\n\n$message");
    }

    unset($statusClass);
    unset($iconPath);

    switch (strtoupper($messageType))
    {
        case 'ERROR':
            $statusClass = "errorMsg";
            $iconPath = $siteHost . "/images/error_icon.gif";
            break;

        case 'WARNING':
            $statusClass = "warningMsg";
            $iconPath = $siteHost . "/images/warning_icon.gif";
            break;

        case 'INFORMATION':
        default:
            $statusClass = "infoMsg";
            $iconPath = $siteHost . "/images/information_icon.gif";
            break;
    }

    print "<table cellpadding=0 cellspacing=0 class=\"$statusClass\" width=\"$width\">";

    if ($title)
    {
        print "<tr><td class=\"{$statusClass}Header\" valign=top colspan=2>$title</td></tr>";
    }

    print "<tr><td width=5% class=\"{$statusClass}Icon\" valign=top><img src=\"$iconPath\" align=absmiddle></td>";
    print "<td class=\"{$statusClass}Body\" valign=top>$message";

    if ($msgID)
    {
        print "<div style=\"font-size: 8pt; margin-top: 1em; color: #E72300\" align=right>$msgID</div>";
    }

    print "</td></tr></table>";

    if ($spacing)
    {
        print '<br>';
    }
}

/*
 * handle php errors using the above status message
 * see PHP's trigger_error for information on usage
 * the important items are the definition of the $*Errors arrays
 * define a $techSupportEmail somewhere and fill up the $emailErrors array to get nice emails when errors happen
 */
function phpErrorHandler($errno, $errmsg, $filename, $linenum, $vars)
{
    global $techSupportEmail;

    if (error_reporting() == 0)
    {
        return;
    }

    // define an assoc array of error string
    // in reality the only entries we should
    // consider are 2,8,256,512 and 1024
    $handleErrors = array (
                1   =>  "Error",
                2   =>  "Warning",
                4   =>  "Parsing Error",
                /*8   =>  "Notice",*/
                16  =>  "Core Error",
                32  =>  "Core Warning",
                64  =>  "Compile Error",
                128 =>  "Compile Warning",
                256 =>  "User Error",
                512 =>  "User Warning",
                1024=>  "User Notice"
                );
    $backtraceErrors = array();
    $emailErrors = array();
    $logErrors = array();
    $dieErrors = array(4, 16, 64);

    if (!isset($handleErrors[$errno]))
    {
        return;
    }

    // set of errors for which a var trace will be saved

    $dt = date("Y-m-d H:i:s (T)");
    $err = "When: $dt<br>
           Script: $filename:$linenum<br>
           Message: $errmsg";

    if ($techSupportEmail && in_array($errno, $emailErrors))
    {
        $email = $err . '\n\n';
        foreach ($vars as $key => $value)
        {
            $email .= "Parameter: $key\nvalue: $value\n\n";
        }
        mail($techSupportEmail,
            "Error: http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}" ,
            "Error #$errno: {$handleErrors[$errno]}\n\n$email");
    }

    if (in_array($errno, $backtraceErrors))
    {
        $err .= "<p><table border=0>";
        foreach ($vars as $key => $value)
        {
            $err .= "<tr><td style=\"border-top: 1px black solid;\">parameter: $key<br>value: $value</td></tr>";
        }
        $err .= '</table>';
    }

    print_msg('error', "Error #$errno: {$handleErrors[$errno]}", $err);

    // save to the error log, and e-mail me if there is a critical user error
    if (ini_get('error_log') && in_array($errno, $logErrors))
    {
        error_log($err, 3, ini_get('error_log'));
    }

    if (in_array($errno, $dieErrors))
    {
        die();
    }
}

// automatically set the error handler to OURS!
set_error_handler("phpErrorHandler");
?>
