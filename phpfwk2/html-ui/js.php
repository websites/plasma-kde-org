<?php

/*
 * redirect the browser
 *
 * $where: the url of the redirection
 * $forceReload: force the page to reload
 */
function print_redirect($where, $forceReload = true)
{
    print '<script language="javascript">';
    print "window.location.href = \"$where\";";
    if ($forceReload)
    {
        print "window.location.replace(\"$where\");";
    }
    print '</script>';
}

/*
 * replace the current page
 */
function print_replace($where)
{
    print '<script language="javascript">';
    print "window.location.replace(\"$where\");";
    print '</script>';
}

/*
 * print_anchorRedirect - redirect to an anchor tag on the page
 *
 * $anchor: the anchor name (# is stripped out)
 */
function print_anchorRedirect($anchor)
{
    ereg_replace('#', '', $anchor);
    print '<script language="javascript">';
    print "window.location.hash = '$anchor';";
    print 'window.location.reload();';
    print '</script>';
}

function print_js_checkFileUpload($files)
{
    print '<script language="javascript">';
    print 'function checkUpload()';
    print '{';

    foreach ($files as $form => $file)
    {
        print "if (document.$form.$file.value == \"\")";
        print '{';
        print "document.$form.$file.disabled = true;";
        print '}';
    }

    print 'return true;';
    print '}';
    print '</script>';
}

function print_js_popUp($funcName, $width = 800, $height = 800, $resize = 1, $tool = 0, $scroll = 1, $location = 0, $menu = 0, $status = 0)
{
    $tool = intval($tool);
    $scroll = intval($scroll);
    $location = intval($location);
    $menu = intval($menu);
    $resize = intval($resize);
    $width = intval($width);
    $height = intval($height);

    $attrs = "toolbar=$tool,scrollbars=$scroll,location=$location,statusbar=$status,menubar=$menu,resizable=$resize,width=$width,height=$height";

    print "<script language=\"javascript\">";
    print "function {$funcName}_popUp(URL)";
    print "{";
    print "window.open(URL, null, '$attrs');";
    print "}";
    print "</script>";
}

/*
 * print_js_resetElements - resets form fields
 *
 * $prefix -> the prfeix for the function name
 * $elements -> the elements and the value
 */
function print_js_resetElements($funcprefix, $elmprefix, $elements)
{
    print '<script language="javascript">';
    print "function {$funcprefix}_resetElements()";
    print '{';

    if (is_array($elmprefix))
    {
        foreach ($elmprefix as $part)
        {
            $prefix .= "$part.";
        }
    }

    if (is_array($elements))
    {
        foreach ($elements as $element => $value)
        {
            print "$prefix$element.value = $value;";
        }
    }

    print '}';
    print '</script>';
}

/*
 *
 * print_js_confirmSubmit - confirmation message
 *
 * $msg -> the confirmation message
 */
function print_js_confirmSubmit()
{
    print '<script language="javascript">';
    print "function confirmSubmit(msg)";
    print '{';

    print "var agree=confirm(msg);";
    print 'if (agree)';
    print ' return true ;';
    print 'else';
    print ' return false ;';

    print '}';
    print '</script>';
}

function print_dhtmlShowAndHide($funcprefix, $ontext, $offtext)
{
    ?>
    <script language="javascript">
    <? print "function {$funcprefix}_showAndHideElement(objID,textID)"; ?>
    {
        objNode = document.getElementById(objID);
        textNode = document.getElementById(textID);

        <? print "if (textNode.innerHTML == \"$ontext\")"; ?>
        {
            <? print "textNode.innerHTML = \"$offtext\";"; ?>
            objNode.style.display = 'none';
        }
        else
        {
            <? print "textNode.innerHTML = \"$ontext\";"; ?>
            objNode.style.display = '';
        }
    }
    </script>
    <?
}
?>
