<?php

/*
 * print_openDiv -> prints our a <DIV> element
 *
 * $class: the css class for this div
 */
function print_openDiv($class = '')
{
    print "<div class=\"$class\">\n";
}

function print_closeDiv()
{
    print "</div>\n";
}

/*
 * print_radio -> prints out a <INPUT TYPE="radio"> element
 *
 * $inputName -> the input name
 * $displayName -> the name shown for the option
 * $value -> the value="" portion of the option
 * $prev-> the  previously selected radio
 */
function print_radio($inputName, $displayName, $value, $prev, $attrs = '')
{
    print "<input type=radio class=\"radioCheck\" id=\"$inputName\" name=\"$inputName\" value=\"$value\" $attrs ";

    if ($prev == $value)
    {
        print "checked";
    }

    print ">" . $displayName . "</option>\n";
}

/*
 * print_textbox -> prints out a <INPUT TYPE="text"> element
 *
 * $inputName -> the input name
 * $attrs -> addition attributes to put into the element, such as onMouseOver
 * $value -> the value="" portion of the option
 * $size -> the textbox size (default 25)
 */
function print_textbox($inputName, $value, $size=25, $maxlen=0, $attrs = '')
{
    if ($maxlen)
    {
        print "<input type=\"text\" size=\"$size\" maxlength=\"$maxlen\" class=\"text\" id=\"$inputName\" name=\"$inputName\" value=\"" . stripslashes($value) . "\" $attrs></input>\n";
    }
    else
    {
        print "<input type=\"text\" size=\"$size\" class=\"text\" id=\"$inputName\" name=\"$inputName\" value=\"" . stripslashes($value) . "\" $attrs></input>\n";
    }
}

/*
 * print_passwordbox -> prints out a <INPUT TYPE="password"> element
 *
 * $inputName -> the input name
 * $attrs -> addition attributes to put into the element, such as onMouseOver
 * $value -> the value="" portion of the option
 * $size -> the textbox size (default 25)
 */
function print_passwordbox($inputName, $value, $size=25, $maxlen=0, $attrs = '')
{
    if ($maxlen)
    {
        print "<input type=\"password\" size=\"$size\" maxlength=\"$maxlen\" class=\"text\" id=\"$inputName\" name=\"$inputName\" value=\"" . stripslashes($value) . "\" $attrs></input>\n";
    }
    else
    {
        print "<input type=\"password\" size=\"$size\" class=\"text\" id=\"$inputName\" name=\"$inputName\" value=\"" . stripslashes($value) . "\" $attrs></input>\n";
    }
}


/*
 * print_fileUploadBox -> prints out a <INPUT TYPE="file"> element
 *
 * $inputName -> the input name
 * $attrs -> addition attributes to put into the element, such as onMouseOver
 * $value -> the value="" portion of the option
 * $size -> the textbox size (default 25)
 */
function print_fileUploadBox($inputName, $value, $size=25, $maxlen=0, $attrs = '')
{
    if ($maxlen)
    {
        print "<input type=\"file\" size=\"$size\" maxlength=\"$maxlen\" class=\"text\" id=\"$inputName\" name=\"$inputName\" value=\"" . stripslashes($value) . "\" $attrs></input>\n";
    }
    else
    {
        print "<input type=\"file\" size=\"$size\" class=\"text\" id=\"$inputName\" name=\"$inputName\" value=\"" . stripslashes($value) . "\" $attrs></input>\n";
    }
}

/*
 * print_submit -> prints out a <INPUT TYPE="submit"> element
 *
 * $inputName -> the input name
 * $value -> the value="" portion of the option
 * $cssClass -> the CSS classname to reference [CSS must be included in the calling page]
 */
function print_submit($inputName, $value, $attrs = '')
{
    print "<input type=\"submit\" id=\"$inputName\" name=\"$inputName\" value=\"$value\" class=\"button\" $attrs>";
}

/*
 * print_button -> prints out a <INPUT TYPE="button"> element
 *
 * $inputName -> the input name
 * $value -> the value="" portion of the option
 * $cssClass -> the CSS classname to reference [CSS must be included in the calling page]
 */
function print_button($inputName, $value, $attrs = '')
{
    print "<input type=\"button\" id=\"$inputName\" name=\"$inputName\" value=\"$value\" class=\"button\" $attrs>";
}

/*
 * print_textarea -> prints out a <textarea> element
 *
 * $inputName -> the input name
 * $value -> the text of the option
 * $rows -> # of rows (default 5)
 * $cols -> # of columns (default 40)
 * $wrap -> what sort of line wrapping to use (e.g. virtual or hard) (default 'virtual')
 */
function print_textarea($inputName, $value, $rows = 5, $cols = 40, $wrap = 'virtual', $attrs = '')
{
    print "<textarea wrap=\"$wrap\" name=\"$inputName\" cols=\"$cols\" rows=\"$rows\" class=\"textarea\" $attrs>$value</textarea>";
}

/*
 * print_checkbox -> prints out a <INPUT TYPE="checkbox"> element
 *
 * $inputName -> the input name
 * $displayName -> the name shown for the option
 * $value -> the value="" portion of the option
 * $prev-> the  previously selected radio
 */
function print_checkbox($inputName, $displayName, $value, $checked = false, $attrs = '')
{
    print "<input type=\"checkbox\" class=\"radioCheck\" id=\"$inputName\" name=\"$inputName\" value=\"$value\" ";

    if ($checked)
    {
        print "checked";
    }

    print " $attrs>" . $displayName . "</input>\n";
}

/*
 * print_option -> prints out a <SELECT> <OPTION> element
 *
 * $displayName -> the name shown in the listbox
 * $value -> the value="" portion of the option
 * $prev -> the previous selected option
 */
function print_option($displayName, $value, $prev, $zero = false)
{
    print "<option class=\"option\" ";

    if ($prev == $value)
    {
        print "selected ";
    }

    if ($value)
    {
        print "value=\"$value\"";
    }
    else if ($zero)
    {
        print 'value="0"';
    }
    else
    {
        print 'value=""';
    }

    print ">$displayName</option>\n";
}

/*
 * print_options
 *
 * prints <SELECT> options based on the array passed in
 */
function print_options($optionArray)
{
    while (list($key, $value) = each($optionArray))
    {
        print "<option class=\"option\" value=\"$key\">$value</option>\n";
    }
}

/*
 * print_emailIcon
 */
function print_emailIcon($email, $img, $hrefAttrs = '', $imgAttrs = '')
{
    print "<a href=\"mailto:$email\" $hrefAttrs><img title=\"email : $email\" src=\"$img\" $imgAttrs></a>";
}

/*
 * print_emailLink
 */
function print_emailLink($email)
{
    print "<a href=\"mailto:$email\">$email</a>";
}

/*
 * print_imageLink
 */
function print_imageLink($action, $img, $title = '', $hrefAttrs = '', $imgAttrs = '')
{
    print "<a href=\"$action\" $hrefAttrs><img title=\"$title\" src=\"$img\" $imgAttrs></a>";
}

/*
 * print_break -> prints out the <BR> tag(s)
 */
function print_break($lines = 1)
{
    for ($i = 0; $i < $lines; $i++)
    {
        print "<br>\n";
    }
}

function print_rule($class = '')
{
    print "<hr class=\"$class\">";
}

function print_link($target, $value, $attrs = '')
{
    print "<a href=\"$target\" $attrs>$value</a>";
}

function print_hidden($name, $value)
{
    print "<input type=\"hidden\" id=\"$name\" name=\"$name\" value=\"$value\">\n";
}

function print_emptyRow($class = '')
{
    if ($class)
    {
        print "<tr><td colspan=100% class=\"$class\">&nbsp;</td></tr>";
    }
    else
    {
        print '<tr><td colspan=100%>&nbsp;</td></tr>';
    }
}

?>
