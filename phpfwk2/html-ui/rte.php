<?php

function RTESafe($strText)
{
    //returns safe code for preloading in the RTE
    $tmpString = trim($strText);

    //convert all types of single quotes
    $tmpString = str_replace(chr(145), chr(39), $tmpString);
    $tmpString = str_replace(chr(146), chr(39), $tmpString);
    $tmpString = str_replace("'", "&#39;", $tmpString);

    //convert all types of double quotes
    $tmpString = str_replace(chr(147), chr(34), $tmpString);
    $tmpString = str_replace(chr(148), chr(34), $tmpString);
    //$tmpString = str_replace("\"", "\"", $tmpString);

    //replace carriage returns & line feeds
    $tmpString = str_replace(chr(13) . chr(10), "\\n", $tmpString);
    $tmpString = str_replace(chr(10) . chr(13), "\\n", $tmpString);
    $tmpString = str_replace(chr(10), "\\n", $tmpString);
    $tmpString = str_replace(chr(13), "\\n", $tmpString);

    return $tmpString;
}

function print_rteForm($thisPage, $name = '', $upload = false, $method = 'post', $attrs = '')
{
    print_form($thisPage, $name, $upload, $method, $attrs . " onSubmit='updateRTEs(); return true;'");
}

function print_richTextArea($inputName, $value, $height = 350, $width = 605)
{
    global $common_baseURL, $common_htmlPath;

    static $initialized = false;

    if (!$initialized)
    {
        print "<script language=\"JavaScript\" type=\"text/javascript\" src=\"$common_baseURL/rte/richtext.js\"></script>";
    }

    print '<script language="JavaScript">';

    if (!$initialized)
    {
        print "\ninitRTE(\"$common_baseURL/rte/images/\", \"$common_baseURL/rte/\", \"$common_htmlPath/style.css\");\n";
        $initialized = true;
    }

    $content = RTESafe($value);
    print "\nwriteRichText('$inputName', '$content', $width, $height, true, false);\n";
    print '</script>';
}

function print_htmlArea($inputName, $value, $height = 350, $width = 605, $tables = true, $url = '')
{
    global $common_baseURL;

    if (!$url)
    {
        $url = $common_baseURL;
    }

    print '<script type="text/javascript">';
    print "_editor_url = \"$url/htmlarea/\";";
    print '_editor_lang = "en";';
    print '</script>';

    print "<script type=\"text/javascript\" src=\"$url/htmlarea/htmlarea.js\"></script>";

    print '<script type="text/javascript">';

    if ($tables)
    {
        print 'HTMLArea.loadPlugin("TableOperations");';
    }

    print 'var editor = null;';
    print 'function initEditor(id)';
    print '{';
        print 'editor = new HTMLArea(id);';

        if ($tables)
        {
            print 'editor.registerPlugin(TableOperations);';
        }

        print 'editor.generate();';
        print 'return false;';
    print '}';

    print '</script>';

    print "<textarea name=$inputName id=$inputName style=\"width: {$width}px; height: {$height}px;\">$value</textarea>";
    print "<script type=\"text/javascript\">initEditor('$inputName');</script>";
}

?>
