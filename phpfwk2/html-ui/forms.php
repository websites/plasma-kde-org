<?php

/*
 * print_form
 *
 * $action -> the form action to be taken
 * $name -> the form name
 * $upload -> whether this form needs to upload files
 * $method -> post or get, default is post
 * $attrs -> things like css attributes, javascript, etc.
 */
function print_form($action, $name = '', $upload = false, $method = 'post', $attrs = '')
{
    if ($upload)
    {
        $enctype = 'enctype="multipart/form-data"';
    }

    print "<form $enctype method=\"$method\" name=\"$name\" id=\"$name\" action=\"$action\" $attrs>";
}

/*
 * prints an array of values in vertical columns
 */
function print_tableVert(&$options, $tableWidth = 3, $attrs = '')
{
    if (!is_array($options))
    {
        $options = array();
    }

    reset($options);

    $optionsMatches = 0;
    $max = count($options);
    $rows = ceil($max / $tableWidth);

    for ($currentRow = 0; $currentRow < $rows; ++$currentRow)
    {
        print "<tr>\n";
        for ($i = 0; $i < $tableWidth; ++$i)
        {
            $value = $options[$currentRow + ($rows * $i)];

            if (!$value)
            {
              continue;
            }

            print "<td nowrap>$value</td>\n";
        }
        print "</tr>\n";
    }

    return $optionsMatches;
}


/*
 * checkboxTable - turn an array into a table rows of checkboxes. does not create
 *                 the <table> tags
 *
 * $optionName: the checkboxes will be arranged in an array by this name
 * $options: an array options to turn into a table of checkboxes (uses key/value)
 * $previousChoices: choices to turn on.
 *                   the keys in this array should match keys in $options
 * $tableWidth: the number of table cells (<td>) in a row
 * $previousByKey: if the previous selected items are the keys of $previousChoices
 */

function print_checkboxTable($optionName, &$options, &$previousChoices,
                             $tableWidth = 3, $previousByKey = false, $attrs = '')
{
    if (!is_array($options))
    {
        $options = array();
    }

    reset($options);

    if (!is_array($previousChoices))
    {
        $previousChoices = array();
    }

    $numOptions = count($options);
    $optionMatches = 0;
    $value = reset($options);
    $key = key($options);

    for ($i = 0; ($i + $tableWidth - 1) < $numOptions;)
    {

        print "<tr>\n";
        for ($j = 0; $j < $tableWidth; ++$i, ++$j)
        {
            if ($previousByKey ? $previousChoices[$key]
                                 : in_array($key, $previousChoices))
            {
                $checked = 'checked';
                ++$optionMatches;
            }
            else
            {
                $checked = '';
            }

            print "<td nowrap><input type=\"checkbox\" id=\"{$optionName}_{$key}\" name=\"{$optionName}[$key]\" $checked $attrs>$value</td>\n";
            $value = next($options);
            $key = key($options);
        }
        print "</tr>\n";
        print "<tr><td colspan=\"$tableWidth\" height=\"$spacerHeight\"></td></tr>\n";
    }

    if ($i < $numOptions)
    {
        print "<tr>\n";
        for (; $i < $numOptions; ++$i)
        {
            if ($previousByKey ? $previousChoices[$key]
                                 : in_array($key, $previousChoices))
            {
                $checked = 'checked';
                ++$optionMatches;
            }
            else
            {
                $checked = '';
            }

            print "<td nowrap><input type=\"checkbox\"  id=\"{$optionName}_{$key}\" name=\"{$optionName}[$key]\" $checked>$value</td>\n";
            $value = next($options);
            $key = key($options);
        }
        print "</tr>\n";
        print "<tr><td colspan=\"$tableWidth\" height=\"10\"></td></tr>\n";
    }

    return $optionMatches;
}

/*
 * like checkboxTable above, but in vertical columns
 */
function print_checkboxTableVert($optionName, &$options, &$previousChoices, $tableWidth = 3,
                                 $previousByKey = false, $attrs = '')
{
    if (!is_array($options))
    {
        $options = array();
    }

    $keys = array_keys($options);
    reset($options);

    if (!is_array($previousChoices))
    {
        $previousChoices = array();
    }

    $optionsMatches = 0;
    $max = count($options);
    $rows = ceil($max / $tableWidth);

    for ($currentRow = 0; $currentRow < $rows; ++$currentRow)
    {
        print "<tr>\n";
        for ($i = 0; $i < $tableWidth; ++$i)
        {
            $key = $keys[$currentRow + ($rows * $i)];
            $value = $options[$key];

            if (!$value)
            {
              continue;
            }

            if ($previousByKey ? $previousChoices[$key]
                                 : in_array($key, $previousChoices))
            {
                $checked = 'checked';
                ++$optionsMatches;
            }
            else
            {
                $checked = '';
            }

            print "<td nowrap><input type=\"checkbox\"  id=\"{$optionName}_{$key}\" name=\"{$optionName}[$key]\" $attrs $checked>$value</td>\n";
        }
        print "</tr>\n";
    }

    return $optionsMatches;
}


/*
 * checkboxQueryTable - turn a db query result into a table of checkboxes. does not create
 *                 the <table> tags
 *
 * $optionName: the checkboxes will be arranged in an array by this name
 * $query: an array options to turn into a table of checkboxes (uses key/value)
 * $previousChoices: choices to turn on.
 *                   the keys in this array should match keys in $options
 * $tableWidth: the number of table cells (<td>) in a row
 */

function print_checkboxQueryTable($optionName, &$query, &$previousChoices,
                                  $tableWidth = 3, $attrs = '')
{
    if (!is_array($previousChoices))
    {
        $previousChoices = array();
    }

    $optionMatches = 0;
    $numOptions = db_numRows($query);

    for ($i = 0; ($i + $tableWidth - 1) < $numOptions;)
    {
        print "<tr>\n";
        for ($j = 0; $j < $tableWidth; ++$i, ++$j)
        {
            list($key, $value) = db_row($query, $i);

            if (in_array($key, $previousChoices))
            {
                $checked = 'checked';
                ++$optionMatches;
            }
            else
            {
                $checked = '';
            }

            print "<td nowrap><input type=\"checkbox\" id=\"{$optionName}_{$key}\" name=\"{$optionName}[$key]\" value=\"$key\" $checked $attrs>$value</td>\n";
        }
        print "</tr>\n";
    }

    if ($i < $numOptions)
    {
        print "<tr>\n";
        for (; $i < $numOptions; ++$i)
        {
            list($key, $value) = db_row($query, $i);
            if (in_array($key, $previousChoices))
            {
                $checked = 'checked';
                ++$optionMatches;
            }
            else
            {
                $checked = '';
            }

            print "<td nowrap><input type=\"checkbox\" id=\"{$optionName}_{$key}\" name=\"{$optionName}[$key]\" value=\"$key\" $checked $attrs>$value</td>\n";
        }
        print "</tr>\n";
    }

    return $optionMatches;
}

/*
 * like checkboxQueryTable above, but in vertical columns
 */
function print_checkboxQueryTableVert($optionName, $query, &$previousChoices,
                                      $tableWidth = 3, $attrs = '', $unescape = false)
{
    if (!is_array($previousChoices))
    {
        $previousChoices = array();
    }

    $optionsMatches = 0;
    $max = db_numRows($query);
    $rows = ceil($max / $tableWidth);

    for ($currentRow = 0; $currentRow < $rows; ++$currentRow)
    {
        print "<tr>\n";
        for ($i = 0; $i < $tableWidth && $currentRow + ($rows * $i) < $max; ++$i)
        {
            list($key, $value) = db_row($query, $currentRow + ($rows * $i));

            if (!$value)
            {
              continue;
            }

            if ($unescape)
            {
                $value = stripslashes($value);
            }


            if (in_array($key, $previousChoices))
            {
                $checked = 'checked';
                ++$optionsMatches;
            }
            else
            {
                $checked = '';
            }

            print "<td nowrap><input type=\"checkbox\" class=\"radioCheck\" id=\"{$optionName}_{$key}\" name=\"{$optionName}[$key]\" value=\"$key\" $checked $attrs>$value</td>\n";
        }
        print "</tr>\n";

        if ($doubleSpace)
        {
            print "<tr><td colspan=\"" . $tableWidth . "\" height=\"10\"></td>\n";
        }
    }

    return $optionsMatches;
}

/*
 * selectQuery - turn a DB query result into a pull down menu (<select>)
 *
 * $optionName -> the name of the <select>
 * $options -> the array of options, uses key/value
 * $previousChoice -> the option (if any) to select by default.
 *                    should match a key in $options
 * $selRequired -> if false, will preppend a "blank"
 */

function print_selectQuery($optionName, &$query, $previousChoice = '',
                           $selRequired = true, $attrs = '', $selKey = '', $selVal = '')
{
    print "<select id=\"$optionName\" name=\"$optionName\" $attrs>\n";

    if (!$selRequired)
    {
        print "<option value=\"$selKey\">$selVal</option>\n";
    }

    $numRows = db_numRows($query);

    for ($i = 0; $i < $numRows; ++$i)
    {
        list($key, $value) =  db_row($query, $i);
        if ($previousChoice == $key)
        {
            print "<option value=\"$key\" selected>$value</option>\n";
        }
        else
        {
            print "<option value=\"$key\">$value</option>\n";
        }
    }

    print "</select>\n";
}

/*
 * selectArray - turn an array of options in a pull down menu (<select>)
 *
 * $optionName: the name of the <select>
 * $options: the array of options, uses key/value
 * $numOptions: number of items in $options to use
 * $previousChoice: the option (if any) to select by default.
 *                  should match a key in $options
 * $selRequired -> if false, will preppend a "blank"
 */

function print_selectArray($optionName, $options, $previousChoice = '', $selRequired = true, $attrs = '')
{
    if (!is_array($options))
    {
        $options = array();
    }

    print "<select id=\"$optionName\" name=\"$optionName\" $attrs>\n";

    if (!$selRequired)
    {
        print "<option value=\"\">\n";
    }

    foreach ($options as $key => $value)
    {
        if ($previousChoice == $key)
        {
            print "<option value=\"$key\" selected>$value</option>\n";
        }
        else
        {
            print "<option value=\"$key\">$value</option>\n";
        }
    }

    print "</select>\n";
}

/*
 * print_boolElement
 *
 * prints out a boolean selection element (radio buttons)
 *
 * $name -> name of the select
 * $prev -> the previous selected value
 * $trueDisplay -> the display value of the "true" element
 * $falseDisplay -> the display value of the "false" element
 * $trueValue -> the value of the "true" element
 * $falseValue -> the value of the "false" element
 */
function print_boolElement($name, $prev,
                           $trueDisplay = 'Yes', $falseDisplay = 'No',
                           $trueValue = true, $falseValue = false, $attrs = '')
{
    print_radio($name, $trueDisplay, $trueValue, $prev, $attrs);
    print_radio($name, $falseDisplay, $falseValue, $prev, $attrs);
}

/*
 * print_rangeSelect
 *
 * prints out a <SELECT> object with a given range of numeric values
 *
 * $name -> name of the select
 * $min -> the minimum value
 * $max -> the maximum value
 * $step -> the range step
 * $prev -> the previous selected value
 */
function print_rangeSelect($name, $prev = "", $min = 0, $max = 10, $step = 1,
                           $selRequired = true, $attrs = '',  $selKey = '', $selVal = '')
{
    print "<select id=\"$name\" name=\"$name\" $attrs>\n";

    if (!$selRequired)
    {
        print "<option value=\"$selKey\">$selVal</option> \n";
    }

    $i = $min;

    while ($i <= $max)
    {
        if ($i == $prev)
        {
            print "<option value=\"$i\" selected>$i</option>\n";
        }
        else
        {
            print "<option value=\"$i\">$i</option>\n";
        }

        $i += $step;
    }

    print "</select>\n";
}


/*
 * print_rangeSelect
 *
 * prints out a <SELECT> object with a given range of numeric values
 *
 * $name -> name of the select
 * $prev -> the previous selected value
 * $ranges -> assumes an array of values following an order of (min, max, step)
 */

function print_multiRangeSelect($name, $prev, $ranges, $selRequired = true, $attrs = '')
{
    if (!is_array($ranges))
    {
        return;
    }

    $numValues = count($ranges);

    if ($numvalues % 3)
    {
        return;
    }

    print "<select id=\"$name\" name=\"$name\" $attrs>\n";

    if (!$selRequired)
    {
        print "<option value=\"\"> \n";
    }

    $idx = 0;
    while ($idx < $numValues)
    {
        $i = $ranges[$idx];
        $max = $ranges[($idx + 1)];
        $step = $ranges[($idx + 2)];

        while ($i <= $max)
        {
            if ($i == $prev)
            {
                print "<option value=\"$i\" selected>$i</option>\n";
            }
            else
            {
                print "<option value=\"$i\">$i</option>\n";
            }

            $i += $step;
        }

        $idx += 3;
    }

    print "</select>\n";
}

function print_reverseRangeSelect($name, $prev = '', $min = 0, $max = 10, $step = 1,
                                  $selRequired = true, $attrs = '')
{
    print "<select id=\"$name\" name=\"$name\" $attrs>\n";

    if (!$selRequired)
    {
        print "<option value=\"\"> \n";
    }

    $i = $max;

    while ($i >= $min)
    {
        if ($i == $prev)
        {
            print "<option value=\"$i\" selected>$i</option>\n";
        }
        else
        {
            print "<option value=\"$i\">$i</option>\n";
        }

        $i -= $step;
    }

    print "</select>\n";
}

/*
 * print_provSelect
 *
 * prints out a <SELECT> object with provinces and states
 *
 * $name -> name of the select
 * $current_prov -> the currently selected province
 */
function print_provSelect($name, $current_prov = '', $states = true, $provs = true,
                          $mandatory = true, $attrs = '')
{
    print "<select id=\"$name\" name=\"$name\" $attrs>";
    if ($current_prov)
    {
        print "<option>$current_prov</option>";
    }

    if (!$mandatory)
    {
        print '<option></option>';
    }

    if ($provs)
    {
        //<option value="" style="enabled: false">-- Provinces --</option>
        ?>
        <option>Alberta</option>
        <option>British Columbia</option>
        <option>Manitoba</option>
        <option>New Brunswick</option>
        <option>Newfoundland</option>
        <option>North West Terr.</option>
        <option>Nova Scotia</option>
        <option>Ontario</option>
        <option>Saskatchewan</option>
        <option>P.E.I.</option>
        <option>Quebec</option>
        <option>Yukon</option>
        <?php
    }

    if ($states)
    {
        //<option value="">-- States --</option>
        ?>
        <option>AL</option>
        <option>AK</option>
        <option>AZ</option>
        <option>AR</option>
        <option>CA</option>
        <option>CO</option>
        <option>CT</option>
        <option>DC</option>
        <option>DE</option>
        <option>FL</option>
        <option>GA</option>
        <option>HI</option>
        <option>ID</option>
        <option>IL</option>
        <option>IN</option>
        <option>IA</option>
        <option>KS</option>
        <option>KY</option>
        <option>LA</option>
        <option>ME</option>
        <option>MD</option>
        <option>MA</option>
        <option>MI</option>
        <option>MN</option>
        <option>MS</option>
        <option>MO</option>
        <option>MT</option>
        <option>NE</option>
        <option>NV</option>
        <option>NH</option>
        <option>NJ</option>
        <option>NM</option>
        <option>NY</option>
        <option>NC</option>
        <option>ND</option>
        <option>OH</option>
        <option>OK</option>
        <option>OR</option>
        <option>PA</option>
        <option>RI</option>
        <option>SC</option>
        <option>SD</option>
        <option>TN</option>
        <option>TX</option>
        <option>UT</option>
        <option>VT</option>
        <option>VA</option>
        <option>WA</option>
        <option>WV</option>
        <option>WI</option>
        <option>WY</option>
        <?
    }
    print '</select>';
}

/*
 * print_countrySelect
 *
 * prints out a <SELECT> object with countries listed
 * currently only does Canada and US
 *
 * $name -> name of the select
 * $current_country -> the currently selected country
 */
function print_countrySelect($name, $current_country = '', $mandatory = true, $attrs = '')
{
    global $countryArray;

    print "<select id=\"$name\" NAME=\"$name\" $attrs>\n";

    if (!$mandatory)
    {
        print '    <option value=""></option>';
    }

    foreach ($countryArray as $code => $name)
    {
        if ($current_country == $code)
        {
            print "   <option value=\"$code\" selected>$name</option>\n";
        }
        else
        {
            print "   <option value=\"$code\">$name</option>\n";
        }
    }

    print "</select>\n";
}

function hideInputs(&$inputArray)
{
    if (!is_array($inputArray))
    {
        return;
    }

    reset($inputArray);

    while (list($key, $value) = each($inputArray))
    {
        $key = str_replace('"', "'", $key);
        $value = str_replace('"', "'", $value);
        print "<input type=\"hidden\" id=\"$key\" name=\"$key\" value=\"$value\">\n";
    }
}

function print_linkAsButton($target, $desc, $inputs = 0, $attrs = '')
{
    print "<form action=\"$target\" $attrs>\n";

    if ($inputs && is_array($inputs))
    {
        foreach($inputs as $element => $value)
        {
            print "<input type=\"hidden\" name=\"$element\" value=\"$value\">\n";
        }
    }

    print "<input type=submit value=\"$desc\">\n";
    print "</form>\n";
}


function print_formAsImage($target, $img, $alt, $inputs = 0, $attrs = '')
{
    print "<form action=\"$target\" $attrs>\n";

    if ($inputs && is_array($inputs))
    {
        foreach($inputs as $element => $value)
        {
            print "<input type=\"hidden\" name=\"$element\" value=\"$value\">\n";
        }
    }

    print "<input type=image src=\"$img\" border=0 alt=\"$alt\">\n";
    print "</form>\n";
}

?>
