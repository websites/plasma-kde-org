<?php

global $regularTimes, $militaryTimes;

$regularTimes = array('Midnight' => 'Midnight',
                      '12:30 AM' => '12:30 AM',
                      '1:00 AM' => '1:00 AM',
                      '1:30 AM' => '1:30 AM',
                      '2:00 AM' => '2:00 AM',
                      '2:30 AM' => '2:30 AM',
                      '3:00 AM' => '3:00 AM',
                      '3:30 AM' => '3:30 AM',
                      '4:00 AM' => '4:00 AM',
                      '4:30 AM' => '4:30 AM',
                      '5:00 AM' => '5:00 AM',
                      '5:30 AM' => '5:30 AM',
                      '6:00 AM' => '6:00 AM',
                      '6:30 AM' => '6:30 AM',
                      '7:00 AM' => '7:00 AM',
                      '7:30 AM' => '7:30 AM',
                      '8:00 AM' => '8:00 AM',
                      '8:30 AM' => '8:30 AM',
                      '9:00 AM' => '9:00 AM',
                      '9:30 AM' => '9:30 AM',
                      '10:00 AM' => '10:00 AM',
                      '10:30 AM' => '10:30 AM',
                      '11:00 AM' => '11:00 AM',
                      '11:30 AM' => '11:30 AM',
                      'Noon' => 'Noon',
                      '12:30 PM' => '12:30 PM',
                      '1:00 PM' => '1:00 PM',
                      '1:30 PM' => '1:30 PM',
                      '2:00 PM' => '2:00 PM',
                      '2:30 PM' => '2:30 PM',
                      '3:00 PM' => '3:00 PM',
                      '3:30 PM' => '3:30 PM',
                      '4:00 PM' => '4:00 PM',
                      '4:30 PM' => '4:30 PM',
                      '5:00 PM' => '5:00 PM',
                      '5:30 PM' => '5:30 PM',
                      '6:00 PM' => '6:00 PM',
                      '6:30 PM' => '6:30 PM',
                      '7:00 PM' => '7:00 PM',
                      '7:30 PM' => '7:30 PM',
                      '8:00 PM' => '8:00 PM',
                      '8:30 PM' => '8:30 PM',
                      '9:00 PM' => '9:00 PM',
                      '9:30 PM' => '9:30 PM',
                      '10:00 PM' => '10:00 PM',
                      '10:30 PM' => '10:30 PM',
                      '11:00 PM' => '11:00 PM',
                      '11:30 PM' => '11:30 PM');

$militaryTimes = array('00:00' => '00:00',
                       '00:30' => '00:30',
                       '01:00' => '01:00',
                       '01:30' => '01:30',
                       '02:00' => '02:00',
                       '02:30' => '02:30',
                       '03:00' => '03:00',
                       '03:30' => '03:30',
                       '04:00' => '04:00',
                       '04:30' => '04:30',
                       '05:00' => '05:00',
                       '05:30' => '05:30',
                       '06:00' => '06:00',
                       '06:30' => '06:30',
                       '07:00' => '07:00',
                       '07:30' => '07:30',
                       '08:00' => '08:00',
                       '08:30' => '08:30',
                       '09:00' => '09:00',
                       '09:30' => '09:30',
                       '10:00' => '10:00',
                       '10:30' => '10:30',
                       '11:00' => '11:00',
                       '11:30' => '11:30',
                       '12:00' => '12:00',
                       '12:30' => '12:30',
                       '13:00' => '13:00',
                       '13:30' => '13:30',
                       '14:00' => '14:00',
                       '14:30' => '14:30',
                       '15:00' => '15:00',
                       '15:30' => '15:30',
                       '16:00' => '16:00',
                       '16:30' => '16:30',
                       '17:00' => '17:00',
                       '17:30' => '17:30',
                       '18:00' => '18:00',
                       '18:30' => '18:30',
                       '19:00' => '19:00',
                       '19:30' => '19:30',
                       '20:00' => '20:00',
                       '20:30' => '20:30',
                       '21:00' => '21:00',
                       '21:30' => '21:30',
                       '22:00' => '22:00',
                       '22:30' => '22:30',
                       '23:00' => '23:00',
                       '23:30' => '23:30');

/*
 *
 */
function print_calendarSelector($name, $prev, $dateFormat='MM/DD/YYYY')
{
    static $printedJavascript = 0;
    global $phpfwkIncludePath;

    if (!$printedJavascript)
    {
        print '<script type="text/javascript">';
        include_once("$phpfwkIncludePath/html-ui/js/calendar.js");
        print '</script>';
        $printedJavascript = 1;
    }

    if ($prev)
    {
        print "<script>DateInput('$name', true, '$dateFormat', '$prev')</script>";
    }
    else
    {
        print "<script>DateInput('$name', true, '$dateFormat')</script>";
    }
}

/*
 * print_dateSelectorToday -> print a set of three widgets that allows the user to select the date
 *
 * The date used will default to today
 * 
 * $name: the name of the element
 */
function print_dateSelectorToday($name)
{
    print_dateSelector($name, date('m'), date('d'), date('Y'));
}

/*
 * print_dateSelector -> print a set of three widgets that allows the user to select the date
 */
function print_dateSelector($inName, $month, $day, $year,
                            $pastYears = 5, $futureYears = 5,
                            $reverseYears = false, $attrs = '')
{
        // create array so we can name months
        $monthName = array(1=> "January", "February", "March",
            "April", "May", "June", "July", "August",
            "September", "October", "November", "December");

        // if date invalid or not supplied, use current time

        // make month selector
        print "<select class=\"select\" id=\"{$inName}Month\" name=\"{$inName}Month\" $attrs>\n";
        for($currentMonth = 1; $currentMonth <= 12; $currentMonth++)
        {
            print "<OPTION value=\"";
            print intval($currentMonth);
            print "\"";
            if($month == $currentMonth)
            {
                print " selected";
            }
            print ">" . $monthName[$currentMonth] . "\n";
        }
        print "</select>";

        // make day selector
        print "<select class=\"select\" id=\"{$inName}Day\" name=\"{$inName}Day\" $attrs>\n";
        for($currentDay=1; $currentDay <= 31; $currentDay++)
        {
            print "<OPTION value=\"$currentDay\"";
            if($day==$currentDay)
            {
                print " selected";
            }
            print ">$currentDay\n";
        }
        print "</select>";

        // make year selector
        print "<select class=\"select\" id=\"{$inName}Year\" name=\"{$inName}Year\" $attrs>\n";
        $startYear = date('Y', Time());

        if ($reverseYears == true)
        {
            if ($year > $startYear + $futureYears)
            {
                print "<option value=\"$year\" selected>$year</option>";
            }

            for($currentYear = $startYear + $futureYears;
                $currentYear >= $startYear - $pastYears;
                $currentYear--)
            {
                print "<option value=\"$currentYear\"";
                if($year == $currentYear)
                {
                    print " selected";
                }
                print ">$currentYear</option>\n";
            }

            if ($year < $startYear - $pastYears)
            {
                print "<option value=\"$year\" selected>$year</option>";
            }
        }
        else
        {
            if ($year < $startYear - $pastYears)
            {
                print "<option value=\"$year\" selected>$year</option>";
            }

            for($currentYear = $startYear - $pastYears;
                $currentYear <= $startYear + $futureYears;
                $currentYear++)
            {
                print "<option value=\"$currentYear\"";
                if($year == $currentYear)
                {
                    print " selected";
                }
                print ">$currentYear</option>\n";
            }

            if ($year > $startYear + $futureYears)
            {
                print "<option value=\"$year\" selected>$year</option>";
            }
        }
        print "</select>";
}

function print_monthSelector($inName, $month, $year)
{
        // create array so we can name months
        $monthName = array(1=> "January", "February", "March",
                               "April", "May", "June", "July", "August",
                               "September", "October", "November", "December");

        // if date invalid or not supplied, use current time
        $useDate = Time();

        // make month selector
        if ($inName)
        {
            print "<select class=\"select\" id=\"" . $inName . "Month\" NAME=" . $inName . "Month>\n";
        }
        else
        {
            print "<select class=\"select\" id=\"month\" NAME=month>\n";
        }

        if (! $month)
        {
            $month = date( "m", $useDate);
        }
        for($currentMonth = 1; $currentMonth <= 12; $currentMonth++)
        {
            print "<option  class=\"option\" value=\"";
            print intval($currentMonth);
            print "\"";
            if($month == $currentMonth)
            {
                print " selected";
            }
            print ">" . $monthName[$currentMonth] . "\n";
        }
        print "</select>&nbsp;&nbsp;";

        // make year selector
        if ($inName)
        {
            print "<select class=\"select\" id=\"" . $inName . "Year\" NAME=" . $inName . "Year>\n";
        }
        else
        {
            print "<select class=\"select\" id=\"year\" NAME=year>\n";
        }

        if (! $year)
        {
            $startYear = date( "Y", $useDate);
            $year = $startYear;
        }
        else
        {
            $startYear = $year;
        }

        for($currentYear = $startYear - 5; $currentYear <= $startYear+5; ++$currentYear)
        {
            print "<option class=\"option\" value=\"$currentYear\"";
            if($year == $currentYear)
            {
                print " selected";
            }
            print ">$currentYear\n";
        }
        print "</select>";
}

?>
