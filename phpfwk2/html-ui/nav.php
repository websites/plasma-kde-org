<?php

/*
 * needsNextPrev
 * Takes an offset into a search, how many are showing on this page,
 * the total number of matches in the search and how many are shown at a time
 * and returns whether or not a next/previous interface is necessary
 */
function needsNextPrev($offset, $numShowing, $total,  $atatime)
{
    return $offset > 0 || ($numShowing >= $atatime && $offset < $total);
}


/*
 * print_nextPrev
 * Prints out a next/previous interface. Must be wrapped in a <table></table>
 *
 * $offset => The offset into the search that we are currently at
 * $numShowing => How many items are showing on this results page
 * $total => The total number of matches in this results set
 * $atatime => How many items are shown per page
 * $actionURL => The URL to be activiated when clicking on the interface
 * $colSpan => How many columns are in the table for it to use. Minimum 3.
 * $bottom => True if this is appearing at the bottom of the list, false if above
 */
function print_nextPrev($offset, $numShowing, $total, $atatime, $actionURL, $colSpan = 3, $bottom = true)
{
    global $common_baseImagesURL;
    $colSpan -= 2;

    $endLetter = $actionURL[strlen($actionURL)];
    if ($endLetter != '?' && $endLetter != '&')
    {
        if (strchr($actionURL, '?'))
        {
            $actionURL .= '&';
        }
        else
        {
            $actionURL .= '?';
        }
    }

    if ($bottom)
    {
        // print a top border if on the bottom?
        $cellStyle = '';
    }
    else
    {
        // print a bottom border if on the top?
        $cellStyle = '';
    }

    print "<tr><td width=1% style=\"$cellStyle\" valign=top nowrap>";

    if ($offset >= $atatime)
    {
        $leftImgLg = "<img valign=\"middle\" src=\"$common_baseImagesURL/left.gif\" border=0>";
        print "<a href=\"{$actionURL}offset=" . ($offset - $no_results - $atatime) . "\" style=\"font-weight: normal\">{$leftImgLg}Prev</a>";
    }
    else
    {
        $leftImg = "<img valign=\"middle\" src=\"$common_baseImagesURL/leftDisabled.gif\" border=0>";
        print "<span style=\"color: lightgray;\">{$leftImg}Prev</span>";
    }

    // the speed nav row of numbers
    print "</td><td width=98% colspan=\"$colSpan\" valign=top style=\"font-size: 6pt; $cellStyle\" nowrap><center>";
    $startSpeedNav = $offset - ($atatime * 3);
    $endSpeedNav = $offset + ($atatime * 5);

    if ($startSpeedNav < 0)
    {
        $endSpeedNav -= $startSpeedNav;
        $startSpeedNav = 0;
    }

    if ($endSpeedNav > $total)
    {
        if ($startSpeedNav != 0)
        {
            $startSpeedNav -= $endSpeedNav - $total - $atatime;
            $startSpeedNav -= $startSpeedNav % $atatime;
        }

        if ($startSpeedNav < 0)
        {
            $startSpeedNav = 0;
        }

        $endSpeedNav = $total;
    }

    $i = floor($startSpeedNav / $atatime) + 1;
    $currentSpeedNav = floor(($offset - $startSpeedNav) / $atatime) + $i;

    for ($j = $startSpeedNav; $j < $endSpeedNav; ++$i, $j += $atatime)
    {
        if ($i > 0)
        {
            print '&nbsp;';
        }

        if ($currentSpeedNav == $i)
        {
            print $i;
        }
        else
        {
            print "<a href=\"{$actionURL}offset=$j\" style=\"font-size: 6pt; font-weight: $weight;\">$i</a>";
        }
    }

    if ($endSpeedNav < $total)
    {
        print '..';
    }
    print "</center></td><td width=1% align=right valign=top style=\"$cellStyle\" nowrap>";

    if ($numShowing >= $atatime && $offset < $total)
    {
        $offset += $atatime;
        $queryparts["offset"] = $offset;
        $nextNum = ($total - $offset);
        $rightImgLg = "<img valign=\"middle\" src=\"$common_baseImagesURL/right.gif\" border=0>";
        print "<a href=\"{$actionURL}offset=$offset\" style=\"font-weight: normal\">Next{$rightImgLg}</a>";
    }
    else
    {
        $rightImg = "<img valign=\"middle\" src=\"$common_baseImagesURL/rightDisabled.gif\" width=\"16\" height=\"16\" border=0>";
        print "<span style=\"color: lightgray;\">Next{$rightImg}</span>";
    }

    print '</td></tr>';
}

/*
 * print_alphaSelector - prints out a 0-Z (+ALL) list for limiting lists
 *
 * $baseURL => the base url to use when building the links
 */
function print_alphaSelector($baseURL, $hyperbolic = true)
{
    global $alpha;
    $alphabet = array ('0-9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'All');

    $glue = '?';
    if (strstr($baseURL, '?'))
    {
        $glue = '&';
    }

    for ($i = 0; $i < 28; ++$i)
    {
        if ($alpha == $alphabet[$i])
        {
            print "<span style=\"font-size: 12pt; font-weight: bold;\">$alphabet[$i]</span>&nbsp;";
        }
        else if ($hyperbolic &&
                 ($i > 0 && $alpha ==  $alphabet[$i - 1]) ||
                 ($i < 27 && $alpha ==  $alphabet[$i + 1]))
        {
            print "<span style=\"font-size: 11pt; font-weight: bold;\"><a href=\"{$baseURL}{$glue}alpha=$alphabet[$i]\">$alphabet[$i]</a></span>&nbsp;";
        }
        else
        {
            print "<a href=\"{$baseURL}{$glue}alpha=$alphabet[$i]\">$alphabet[$i]</a>&nbsp;";
        }
    }
}

/*
 * print_sortableHeaders - prints out table headers with up/down buttons
 *
 * $orderCols => an array keyed by the visible column name that index integer ids
 * $orderColIDs => an array keyed by the int ids in $orderCols;
 *                 if an entry appears in $orderCols, but not here it doesn't get sort arrows
 * $baseURL => the base url to use when building the sort arrow links
 * $headerClass => optional CSS class used for the headers
 */
function print_sortableHeaders(&$orderCols, &$orderColIDs, $baseURL, $headerClass = 'largeColoredHeader')
{
    global $common_baseImagesURL;
    if (!is_array($orderCols))
    {
        return;
    }

    $glue = '?';
    if (strstr($baseURL, '?'))
    {
        $glue = '&';
    }

    $sortUp = "<img src=\"$common_baseImagesURL/up_black.gif\" border=0>";
    $sortDown = "<img src=\"$common_baseImagesURL/down_black.gif\" border=0>";
    print '<tr>';
    foreach ($orderCols as $name => $id)
    {
        print "<td class=\"$headerClass\">$name";

        if ($orderColIDs[$id])
        {
            print "&nbsp;<a href=\"{$baseURL}{$glue}oBy={$id}&oHow=u\">$sortUp</a>";
            print "<a href=\"{$baseURL}{$glue}oBy={$id}&oHow=d\">$sortDown</a>&nbsp;";
        }
    }
    print '</tr>';
}

/*
 * print_tabs - prints out tabs that the user can click on
 *
 * $tabs => an array keyed by the visible column name that re indexed by their special value
 * $tabVarName => the variable name to use to store the tab value
 * $headerClass => optional CSS class used for the headers
 */
function print_tabs(&$tabs, $baseURL, $tabVarName, $currentValue)
{
    if (!is_array($tabs))
    {
        return;
    }

    $width = 'width="' . number_format(100 / (count($tabs) + 2), 0) . '%"';

    $glue = '?';
    if (strstr($baseURL, '?'))
    {
        $glue = '&';
    }

    print '<table width=100% cellpadding=3 cellspacing=0 border=0 align=center>';
    print '<tr>';
    print "<td class=\"tabSpacer\" $width>&nbsp;</td>";
    foreach ($tabs as $name => $id)
    {
        if ($id == $currentValue)
        {
            print "<td $width class=\"tabOn\" align=center>$name</td>";
        }
        else
        {
            print "<td $width class=\"tabOff\" align=center><a href=\"{$baseURL}{$glue}{$tabVarName}=$id\">$name</a></td>";
        }
        print "<td class=\"tabSpacer\" width=\"5px;\">&nbsp;</td>";
    }
    print "<td class=\"tabSpacer\" $width>&nbsp;</td></tr>";
    print '</table>';
}

?>
