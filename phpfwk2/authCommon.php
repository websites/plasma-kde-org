<?php

global $logOut;
global $auth_userID, $auth_userGID, $auth_username, $auth_email;

$auth_userID = -1;
$auth_userGID = -1;
unset($auth_username, $auth_email);

function isLoggedIn()
{
    global $auth_userID;
    return $auth_userID != -1;
}

function isSuper()
{
    global $auth_userID, $auth_userGID;
    return $auth_userID == 0 || $auth_userGID == 0;
}

function requiresSuper()
{
    if (!isSuper())
    {
        // we assume that the header printer is gated to only be executed ONCE
        print_authHeader();
        print_msg('ERROR', 'Access Denied',
                  'Your account does not have the required priveleges to view this page.');
        print_authFooter();
        exit();
    }
}

function auth_currentUID()
{
    global $auth_userID;
    return $auth_userID;
}

function auth_currentGID()
{
    global $auth_userGID;
    return $auth_userGID;
}

function auth_requiresGID($GID, $superOkToo = true, $halt = true, $msg = true, $quiet = false)
{
    global $auth_userID, $auth_userGID, $auth_quietOnGroupError;

    $db = db_connect();

    $origGID = $GID;

    if (is_string($GID))
    {
        $row = db_query($db, "SELECT groupID FROM groups WHERE name = '". addslashes($GID) . "';");

        if (db_numRows($row) > 0)
        {
            list($GID) = db_row($row, 0);
        }
        else
        {
            $GID = intval($GID);
        }
    }

    $thisUsersGroups = array();
    $groups = db_query($db, "SELECT groupID FROM userGroups WHERE userID = $auth_userID ORDER BY groupID;");
    $numGroups = db_numRows($groups);

    for ($i = 0; $i < $numGroups; $i++)
    {
        list($id) = db_row($groups, $i);
        array_push($thisUsersGroups, $id);
    }

    if (in_array($GID, $thisUsersGroups) || ($superOkToo && ($auth_userID == 0 || $auth_userGID == 0)))
    {
        return true;
    }

    if ($msg)
    {
        $errMsg = "Your account does not have the required priveleges to view this page.";

        if (!$auth_quietOnGroupError)
        {
            if (!$quiet)
            {
                $errMsg .= " You need to be a member of group : <b>$origGID</b> in order to access this resource.";
            }
        }

        print_authHeader();
        print_msg('ERROR', 'Access Denied', $errMsg);
        print_authFooter();
    }

    if ($halt)
    {
        exit();
    }
    else
    {
        return false;
    }
}

function auth_printLogin($actionURL = '')
{
    global $auth_strictURL, $REQUEST_URI;
    global $bizName, $auth_baseURL;

    if (trim($actionURL) || !$auth_strictURL)
    {
        $actionURL = ereg_replace('logOut=1', '', $REQUEST_URI);
        print_form("$actionURL");
    }
    else // if we don't have an actionURL AND we are doing strictURLs.
    {
        print_form("$auth_baseURL/index.php");
    }

    auth_printLoginForm();
    print '</form>';
}

function auth_failure()
{
    global $auth_userID, $auth_username, $auth_email, $$auth_cookieName;
    global $auth_path, $auth_tokenTable, $auth_cookieName, $auth_cookieExpire;

    $db = db_connect();

    if ($_COOKIE[$auth_cookieName])
    {
        unset($where);
        sql_addToWhereClause($where, '', 'token', '=', $_COOKIE[$auth_cookieName]);
        db_delete($db, $auth_tokenTable, $where);
    }
    else if ($$auth_cookieName)
    {
        unset($where);
        sql_addToWhereClause($where, '', 'token', '=', $$auth_cookieName);
        db_delete($db, $auth_tokenTable, $where);
    }

    $auth_userID = -1;
    $auth_userGID = -1;

    if ($auth_cookieExpire)
    {
        setcookie($auth_cookieName, '', (time() - $auth_cookieExpire), $auth_path);
    }
    else
    {
        setcookie($auth_cookieName, '', 0, $auth_path);
    }

    unset($$auth_cookieName, $_COOKIE[$auth_cookieName]);
    unset($auth_username, $auth_email);
}

function auth_failureNotice($mandatory, $onFailureCallback, $onFailureShowLogin = true, $error = '')
{
    $doCallback = $onFailureCallback && function_exists($onFailureCallback);
    $printHTML = $doCallback || $error || $onFailureShowLogin;

    if ($printHTML)
    {
        print_authHeader('Login');
    }

    if ($error)
    {
        print "<div align=center>";
        print_msg(ERROR, 'Authentication Error', $error);
        print "</div>";
    }

    if ($onFailureShowLogin)
    {
        auth_printLogin();
    }

    if ($doCallback)
    {
        $onFailureCallback();
    }

    if ($printHTML)
    {
       print_authFooter();
    }

    if ($mandatory)
    {
        exit();
    }
}

function userAuth($mandatory = true, $errors = false, $onFailureCallback = '')
{
    global $baseURL, $auth_path, $bizName, $auth_userID, $auth_userGID, $auth_username, $auth_email;
    global $authUser, $authGroup, $authPassword, $auth_cookieName, $$auth_cookieName, $auth_cookieExpire;
    global $auth_maxPasswordFailures, $auth_tokenTimeOut;
    global $auth_userTable, $auth_groupTable, $auth_userGroupsTable, $auth_tokenTable, $auth_hashPasswords;
    global $auth_groupLogin, $auth_caseSensitive;
    
    if (!$auth_caseSensitive)
    {
        $authUser = strtoupper($authUser);
        $authGroup = strtoupper($authGroup);
        $authPassword = strtoupper($authPassword);
    }

    $db = db_connect();
    if ($_COOKIE[$auth_cookieName])
    {
        // we have a cookie set, use it to check the user login, against the tokenTable
        unset($where);

        $adminSQL = "SELECT tok.userID, ug.groupID,
                            (abs(EXTRACT(epoch FROM CAST(CURRENT_TIMESTAMP as TIMESTAMP)) -
                              EXTRACT(epoch FROM CAST(tok.stamp as TIMESTAMP))) > $auth_tokenTimeOut),
                            COALESCE(u.firstName, '')||' '||COALESCE(u.lastName, ''), u.email
                     FROM $auth_tokenTable tok
                     JOIN $auth_userTable u on (tok.userID = u.userID)
                     LEFT JOIN $auth_userGroupsTable ug ON
                          (tok.userID = ug.userID AND ug.primaryGroup)";

        sql_addToWhereClause($where, '', 'tok.token', '=', addslashes($_COOKIE[$auth_cookieName]));
        $adminSQL .= " WHERE $where";

        $adminQuery = db_query($db, $adminSQL);

        if (db_numRows($adminQuery) < 1)
        {
            auth_failure();
            $error = "Verification of your account useage failed.  Please try again.";
            auth_failureNotice($mandatory, $onFailureCallback, true, $quietly ? '' : $error);
            return;
        }
        else
        {
            list($auth_userID, $auth_userGID, $staled, $auth_username, $auth_email) = db_row($adminQuery, 0);

            if (db_boolean($staled))
            {
                auth_failure();
                $error = "To protect your privacy, this session has timed out after " . ($auth_tokenTimeOut / 60 ) . " minutes of inactivity. Please log in again.";
                auth_failureNotice($mandatory, $onFailureCallback, true, $quietly ? '' : $error);
                return;
            }

            // the account is verified and logged in, update the stamp in the tokens
            unset($fields, $where);
            sql_addRawToUpdate($fields, 'stamp', 'CURRENT_TIMESTAMP');
            sql_addToWhereClause($where, '', 'userID', '=', $auth_userID);
            db_update($db, $auth_tokenTable, $fields, $where);

            // set the cookie
            if ($auth_cookieExpire)
            {
                setcookie($auth_cookieName, $$auth_cookieName, (time() + $auth_cookieExpire), $auth_path);
            }
            else
            {
                setcookie($auth_cookieName, $$auth_cookieName, 0, $auth_path);
            }

            // post login success
            auth_postLoginSuccess($mandatory, $errors, $onFailureCallback);
        }
    }
    else if ($authPassword && $authUser)
    {
        // do we need a group?
        if ($auth_groupLogin)
        {
            if (!$authGroup)
            {
                auth_failure();
                $error = "A group name is required for login.";
                auth_failureNotice($mandatory, $onFailureCallback, false, $quietly ? '' : $error);
                return;
            }
            else
            {
                // check for the existence of the group
                $groupRow = db_query($db, "SELECT groupID FROM groups
                                           WHERE name = '" . addslashes($authGroup) . "' AND
                                                 active = true;");
                if (db_numRows($groupRow) > 0)
                {
                    list($loginGroup) = db_row($groupRow, 0);
                }
                else
                {
                    auth_failure();
                    $error = "The group <b>$authGroup</b> could not be found. Please use an active group.";
                    auth_failureNotice($mandatory, $onFailureCallback, false, $quietly ? '' : $error);
                    return;
                }
            }
        }

        // hash the password
	$crypted = $auth_hashPasswords ? md5($authPassword) : $authPassword;

        // get the adminid
        unset($where);

        $adminSQL = "SELECT u.userID, ug.groupID,
                            COALESCE(u.firstName, '')||' '||COALESCE(u.lastName, ''),
                            u.strikes, u.email, u.active
                     FROM $auth_userTable u";

        if ($auth_groupLogin && $authGroup)
        {
            $adminSQL .= " JOIN $auth_userGroupsTable ug ON (u.userID = ug.userID AND
                                                             ug.primaryGroup AND
                                                             ug.groupID = $loginGroup)";
        }
        else
        {
            $adminSQL .= " LEFT JOIN $auth_userGroupsTable ug ON (u.userID = ug.userID AND
                                                                  ug.primaryGroup)";
        }

        sql_addToWhereClause($where, '', 'u.login', '=', addslashes($authUser));
        sql_addToWhereClause($where, 'AND', 'u.password', '=', addslashes($crypted));

        $adminSQL .= " WHERE $where;";

        $adminQuery = db_query($db, $adminSQL);

        if (db_numRows($adminQuery) > 0)
        {
            // create token
            list($auth_userID, $auth_userGID, $auth_username, $strikes, $auth_email, $user_active) = db_row($adminQuery, 0);

            if (!db_boolean($user_active))
            {
                auth_failure();
                $error = "This account has been disabled.  Please contact the administrator.";
                auth_failureNotice($mandatory, $onFailureCallback, true, $quietly ? '' : $error);
                return;
            }

            if ($strikes > $auth_maxPasswordFailures)
            {
                auth_failure();
                $error = "Failed attempts exceeded maximum. Account disabled.";
                auth_failureNotice($mandatory, $onFailureCallback, true, $quietly ? '' : $error);
                return;
            }

            // made it past ... so we are logged in
            $$auth_cookieName = generateToken();

            unset($where);
            sql_addToWhereClause($where, '', 'userID', '=', $auth_userID);
            db_delete($db, $auth_tokenTable, $where);

            unset($fields, $values);
            sql_addIntToInsert($fields, $values, 'userID', $auth_userID, true);
            sql_addScalarToInsert($fields, $values, 'token', $$auth_cookieName);
            sql_addRawToInsert($fields, $values, 'stamp', 'CURRENT_TIMESTAMP');
            db_insert($db, $auth_tokenTable, $fields, $values);

            unset($fields, $where);
            sql_addRawToUpdate($fields, 'lastlog', 'CURRENT_TIMESTAMP');
            sql_addIntToUpdate($fields, 'strikes', '0');
            sql_addToWhereClause($where, '', 'userID', '=', $auth_userID);
            db_update($db, $auth_userTable, $fields, $where);

            // set the cookie
            if ($auth_cookieExpire)
            {
                setcookie($auth_cookieName, $$auth_cookieName, (time() + $auth_cookieExpire), $auth_path);
            }
            else
            {
                setcookie($auth_cookieName, $$auth_cookieName, 0, $auth_path);
            }

            // post login success
            auth_postLoginSuccess($mandatory, $errors, $onFailureCallback);
        }
        else
        {
            unset($fields, $where);
            sql_addRawToUpdate($fields, 'lastlog', 'CURRENT_TIMESTAMP');
            sql_addRawToUpdate($fields, 'strikes', 'strikes + 1');
            sql_addToWhereClause($where, '', 'login', '=', $authUser);
            db_update($db, $auth_userTable, $fields, $where);

            auth_failure();
            $error = "The username or password was entered incorrectly. Please try again";
            auth_failureNotice($mandatory, $onFailureCallback, true, $quietly ? '' : $error);
            return;
        }
    }
    else
    {
        auth_failure();
        auth_failureNotice($mandatory, $onFailureCallback);
        return;
    }
}

global $logOut;
if ($logOut)
{
    auth_failure();
}

global $auth_mandatory;
if ($auth_mandatory)
{
    userAuth();
}

?>
