<?

define(CONTEXT_LISTPHOTO, 4);
define(CONTEXT_SHOWPHOTO, 8);

function print_albumMenu($url)
{
    $glue = '?';
    if (strstr($url, '?'))
    {
        $glue = '&';
    }

    $db = db_connect();
    $albums = db_query($db, "SELECT albumID, name FROM albums ORDER BY name;");
    $numAlbums = db_numRows($albums);

    print '<table>';
    print "<tr><td class=\"navStripeRight\" width=100%>&nbsp;</td></tr>";
    for ($i = 0; $i < $numAlbums; ++$i)
    {
        list($id, $name) = db_row($albums, $i);
        print "<tr><td class=\"navStripeRight\" width=100%><a class='nav' href=\"{$url}{$glue}&albumID=$id\">&bull; $name</a></td></tr>";
    }
    print '</table>';
}

function print_albumChooser($url)
{
    global $common_albumsImagesURL;
    $db = db_connect();

    $glue = '?';
    if (strstr($url, '?'))
    {
        $glue = '&';
    }

    // should be ordered by a displayOrder, and selected by a cms page
    $albums = db_query($db, "SELECT albumID, name FROM albums ORDER BY name;");
    $numAlbums = db_numRows($albums);

    $numPerRow = 2; // should be configurable!
    print '<table align=center cellspacing=0 cellpadding=10>';
    for ($i = 0; $i < $numAlbums; ++$i)
    {
        if ($i % $numPerRow == 0)
        {
            print "<tr>";
        }
        list($albumID, $name) = db_row($albums, $i);
        unset($thumbnail);
        $photo = db_query($db, "SELECT p.thumbnail
                                FROM photos p WHERE p.albumid = $albumID AND p.thumbnail IS NOT NULL
                                ORDER BY p.photoid LIMIT 1;");
        if (db_numRows($photo) > 0)
        {
            list($thumbnail) = db_row($photo, 0);
            $thumbnail = "<img src=\"$common_albumsImagesURL/$albumID/$thumbnail\" border=0><br>";
        }
        print "<td align=center valign=bottom><a href=\"{$url}{$glue}albumID=$albumID\">{$thumbnail}{$name}</a></td>";
        if ($i % $numPerRow == $numPerRow)
        {
            print "</tr>";
        }
    }
    print '</table>';
}

function print_album($url, $albumID)
{
    global $common_albumsImagesURL;
    global $context, $albumID, $photoOffset, $photoID;

    $glue = '?';
    if (strstr($url, '?'))
    {
        $glue = '&';
    }

    if (is_null($context))
    {
        $context = CONTEXT_LISTPHOTO;
    }

    $photoOffset = intval($photoOffset);

    $db = db_connect();

    if ($albumID < 1)
    {
        print_albumChooser($url);
        return;
    }

    $albumInfo = db_query($db, "SELECT a.name, count(p.photoID) as total,
                                      (count(p.photoID) / a.numPhotosPerPage) as pages,
                                      a.numPhotosPerPage, a.numPhotosPerRow
                               FROM albums a
                               LEFT JOIN photos p ON p.albumID = a.albumID
                               WHERE a.albumID = $albumID
                               GROUP BY a.name, a.numPhotosPerPage, a.numPhotosPerRow;");

    if (db_numRows($albumInfo) < 1)
    {
        // FIXME: print an error msg that we couldn't find the album?
        print_albumChooser($url);
        return;
    }


    list($name, $totalPhotos, $pages, $perPage, $perRow) = db_row($albumInfo, 0);

    if ($totalPhotos > ($perPage * $pages))
    {
        $pages++;
    }

    print '<div align=center>';
    print "<h3>$name</h3>";
    print "<a href=\"$url";
    if ($context == CONTEXT_SHOWPHOTO)
    {
        $contxt = CONTEXT_LISTPHOTO;
        print "{$glue}context=$contxt&albumID=$albumID&photoOffset=$offset";
    }
    print "\">&lt;&lt;&nbsp;Back to the album listing</a>";
    print '<table class="thin" cellpadding=3px cellspacing=1px width=600px>';

    // FIXME: $contxt and $context? a little ugly for maintenance, readability
    $contxt = CONTEXT_LISTPHOTO;
//     print "<tr><td align=left>Photo Album : <a href=\"$url{$glue}context=$contxt&albumID=$albumID\">$name</a></td>";
//     print "<td align=right><b>$totalPhotos</b> photos in the album on <b>$pages</b> pages</td></tr>";

    if ($pages > 1)
    {
        print "<tr><td colspan=2 align=center>Pages : ";
        for ($i = 1; $i <= $pages; $i++)
        {
            $offset = ($i * $perPage) - $perPage;

            if ($offset == $photoOffset)
            {
                print "&bull;&nbsp;$i&nbsp;&bull;&nbsp;";
            }
            else
            {
                $contxt = CONTEXT_LISTPHOTO;
                print "<a href=\"$url{$glue}context=$contxt&albumID=$albumID&photoOffset=$offset\">$i</a>&nbsp;";
            }
        }
        print "</td></tr>";
    }

    print '</table>';

    if ($context == CONTEXT_LISTPHOTO)
    {
        print "<table cols=$perRow  align=center cellspacing=0 cellpadding=10>";

        $photos = db_query($db, "SELECT photoID,
                                   '$common_albumsImagesURL/'||a.albumID||'/'||p.thumbnail,
                                   caption
                                FROM photos p
                                LEFT JOIN albums a ON a.albumID = p.albumID
                                WHERE p.albumID = $albumID
                                ORDER BY p.photoID LIMIT $perPage OFFSET $photoOffset;");

        $numRows = db_numRows($photos);

        $contxt = CONTEXT_SHOWPHOTO;
        $index = 0;
        while ($index < $numRows)
        {
            print '<tr>';
            $j = 1;
            while ($j <= $perRow && $index < $numRows)
            {
                list($photoID, $thumbnail, $caption) = db_row($photos, $index);
                // FIXME: 20 chars appropos?
                if (strlen($caption) > 20)
                {
                    $caption = substr($caption, 0, 17) . '...';
                }
                print "<td align=center valign=top><a href=\"$url{$glue}context=$contxt&albumID=$albumID&photoOffset=$offset&photoID=$photoID\" border=0><img src=\"$thumbnail\" border=0><br>$caption</a></td>";
                $index++;
                $j++;
            }

            if ($j < $perRow)
            {
                $index++;
            }

            print '</tr>';
        }

        print '</table>';
    }
    else
    {
        print "<table cellpadding=3px cellspacing=0px width=\"100%\">";

        $photo = db_query($db, "SELECT '$common_albumsImagesURL/'||a.albumID||'/'||p.photo,
                                      '$common_albumsImagesURL/'||a.albumID||'/'||p.imagefile,
                                      caption
                               FROM photos p
                               LEFT JOIN albums a ON a.albumID = p.albumID
                               WHERE p.photoID = $photoID;");

        if (db_numRows($photo) > 0)
        {
            list($photopath, $file, $caption) = db_row($photo, 0);

            print "<tr><td width=100% align=center><i>$caption</i></td></tr>";
            print '<tr>';
            print "<td align=center valign=top><a href=\"$file\"><img src=\"$photopath\" border=0></a></td>";
            print '</tr>';
        }

        print '</table>';
    }
    print '</div>';
}

?>
