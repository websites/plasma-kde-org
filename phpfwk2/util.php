<?

/*
 *  util.php
 *
 * Basically it's just shit we can't figure out a better place for.
 */

/*
 * init_var - assigns a value to a variable if it is null
 *
 * &$var: the variable in question
 * $value: the value to assign to it
 */
function init_var(&$var, $value, $iffalse = true, $emptyString = true)
{
    if (is_null($var) || ($iffalse && !$var))
    {
        $var = $value;
    }
    else if (is_string($var) && (strlen(rtrim($var)) == 0))
    {
        $var = $value;
    }
}

/*
 * random number functions
 */
function seedRand()
{
    static $rand_seeded = false;

    if ($rand_seeded)
    {
        return;
    }

    list($usec,$sec) = explode(" ", microtime());
    mt_srand(((float)$sec+ (float)$usec * 100000));

    $rand_seeded = true;
}

function generateToken($length = 20)
{
    seedRand();
    $atoms = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

    $num_atoms = count($atoms);

    for($i = 0; $i < $length; $i++)
    {
        $password .= $atoms[mt_rand(0, $num_atoms - 1)];
    }

    return substr($password, 0, $length);
}

/*
 * initialize_var - assigns a value to a variable if it is null
 *
 * &$var: the variable in question
 * $value: the value to assign to it
 */
function initialize_var(&$var, $value)
{
    if (is_null($var))
    {
        $var = $value;
    }
}

/*
 * take an array and put it into vertical columns like a phonebook
 * $theList => an array
 * $callback => name of a function to process each entry
 * $column => # of columns to split things into (default = 3)
 */
function vertColumns($theList, $callback, $columns = 3)
{
    if (!is_array($theList))
    {
        return;
    }

    $max = sizeof($theList);
    $rows  = ceil($max / $columns);
    $keys = array_keys($theList);
    for ($currentRow = 0; $currentRow < $rows; ++$currentRow)
    {
        for ($i = 0; $i < $columns; ++$i)
        {
            $key = $keys[$currentRow + ($rows * $i)];
            $callback($key, $theList[$key], $i + 1, $columns);
        }
    }
}

/*
 * take an array and put it into vertical columns like a phonebook
 * $theList => a database query of the form (key, value)
 * $callback => name of a function to process each entry
 * $column => # of columns to split things into (default = 3)
 */
function vertColumnsFromQuery($theQuery, $callback, $columns = 3)
{
    $max = db_numRows($theQuery);
    $rows  = ceil($max / $columns);

    for ($currentRow = 0; $currentRow < $rows; ++$currentRow)
    {
        for ($i = 0; $i < $columns; ++$i)
        {
            unset($key, $value);
            if (($currentRow + ($rows * $i)) < $max)
            {
                list($key, $value) = db_row($theQuery, ($currentRow + ($rows * $i)));
            }
            $callback($key, $value, $i + 1, $columns);
        }
    }
}

/*
 * given any english word, return the root.
 * useful for searching
 */

function stem($word)
{
    $step2list = array('ational'=>'ate',
                       'tional'=>'tion',
                       'enci'=>'ence',
                       'anci'=>'ance',
                       'izer'=>'ize',
                       'iser'=>'ise',
                       'bli'=>'ble',
                       'alli'=>'al',
                       'entli'=>'ent',
                       'eli'=>'e',
                       'ousli'=>'ous',
                       'ization'=>'ize',
                       'isation'=>'ise',
                       'ation'=>'ate',
                       'ator'=>'ate',
                       'alism'=>'al',
                       'iveness'=>'ive',
                       'fulness'=>'ful',
                       'ousness'=>'ous',
                       'aliti'=>'al',
                       'iviti'=>'ive',
                       'biliti'=>'ble',
                       'logi'=>'log');

    $step3list = array('icate'=>'ic',
                       'ative'=>'',
                       'alize'=>'al',
                       'alise'=>'al',
                       'iciti'=>'ic',
                       'ical'=>'ic',
                       'ful'=>'',
                       'ness'=>'');

   $c =    "[^aeiou]";           # consonant
   $v =    "[aeiouy]";           # vowel
   $C =    "${c}[^aeiouy]*";     # consonant sequence
   $V =    "${v}[aeiou]*";       # vowel sequence

   $mgr0 = "^(${C})?${V}${C}";                # [C]VC... is m>0
   $meq1 = "^(${C})?${V}${C}(${V})?" . '$';   # [C]VC[V] is m=1
   $mgr1 = "^(${C})?${V}${C}${V}${C}";        # [C]VCVC... is m>1
   $_v   = "^(${C})?${v}";                    # vowel in stem

    if (strlen($word)<3) return $word;

    $word=preg_replace("/^y/", "Y", $word);

    // Step 1a
    $word=preg_replace("/(ss|i)es$/", "\\1", $word);    // sses-> ss, ies->es
    $word=preg_replace("/([^s])s$/", "\\1", $word);     //    ss->ss but s->null

    // Step 1b
    if (preg_match("/eed$/", $word))
    {
        $stem=preg_replace("/eed$/", "", $word);
        if (ereg("$mgr0", $stem))
        {
            $word=preg_replace("/.$/", "", $word);
        }
    }
    elseif (preg_match("/(ed|ing)$/", $word))
    {
        $stem=preg_replace("/(ed|ing)$/", "", $word);
        if (preg_match("/$_v/", $stem))
        {
            $word=$stem;

            if (preg_match("/(at|bl|iz|is)$/", $word))
            {
                $word=preg_replace("/(at|bl|iz|is)$/", "\\1e", $word);
            }
            elseif (preg_match("/([^aeiouylsz])\\1$/", $word))
            {
                $word=preg_replace("/.$/", "", $word);
            }
            elseif (preg_match("/^${C}${v}[^aeiouwxy]$/", $word))
            {
                $word.="e";
            }
        }
    }

    // Step 1c (weird rule)
    if (preg_match("/y$/", $word))
    {
        $stem=preg_replace("/y$/", "", $word);
        if (preg_match("/$_v/", $stem))
        {
            $word=$stem."i";
        }
    }

    // set up our stupidly long re's here
    $step2RE = "/(ational|tional|enci|anci|izer|iser|bli|alli|entli|eli|ousli|ization|isation|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/";
    $step3RE = "/(icate|ative|alize|alise|iciti|ical|ful|ness)$/";
    $step4RE = "/(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize|ise)$/";

    // Step 2: get ready for some long motherfucking regexps
    if (preg_match($step2RE, $word, $matches))
    {
        $stem = preg_replace($step2RE, "", $word);
        $suffix = $matches[1];
        if (preg_match("/$mgr0/", $stem))
        {
            $word = $stem.$step2list[$suffix];
        }
    }

    // Step 3
    if (preg_match($step3RE, $word, $matches))
    {
        $stem=preg_replace($step3RE, "", $word);
        $suffix=$matches[1];
        if (preg_match("/$mgr0/", $stem))
        {
            $word=$stem.$step3list[$suffix];
        }
    }

    // Step 4
    if (preg_match($step4RE, $word, $matches))
    {
        $stem=preg_replace($step4RE, "", $word);
        $suffix=$matches[1];
        if (preg_match("/$mgr1/", $stem))
        {
            $word=$stem;
        }
    }
    elseif (preg_match("/(s|t)ion$/", $word))
    {
        $stem=preg_replace("/(s|t)ion$/", "\\1", $word);
        if (preg_match("/$mgr1/", $stem)) $word=$stem;
    }

    // Step 5
    if (preg_match("/e$/", $word, $matches))
    {
        $stem=preg_replace("/e$/", "", $word);
        if (preg_match("/$mgr1/", $stem) ||
            (preg_match("/$meq1/", $stem) &&
            ~preg_match("/^${C}${v}[^aeiouwxy]$/", $stem)))
        {
            $word = $stem;
        }
    }

    if (preg_match("/ll$/", $word) & preg_match("/$mgr1/", $word))
    {
        $word = preg_replace("/.$/", "", $word);
    }

    // and turn initial Y back to y
    preg_replace("/^Y/", "y", $word);

    return $word;
}


/*
 * break a string up along quotes and allow for escaping with backslashes
 *
 * $string => the string to parse
 * $stemWords => use the stem() function on the tokens, defaults to false
 *
 * returns an array of tokens
 */

function tokenize($string, $stemWords = false)
{
    $string = trim(ereg_replace('[[:space:]]+', ' ', $string));
    $numChars = strlen($string);
    $marker = '';
    $startChar = 0;
    $endChar = 0;
    $current = 0;
    $arrayPos = 0;

    for ($i = 0; $i < $numChars; ++$i, ++$current)
    {
        if ($string[$i] == '\\' &&
            $i > 0 &&
            $string[$i - 1] != '\\')
        {
            ++$i;
        }
        else if ($string[$i] == ' ')
        {
            if ($marker == '')
            {
                $endChar = $current - 1;
            }
        }
        else if ($string[$i] == '\'')
        {
            if ($marker == '\'')
            {
                if ($current - 1 == $endChar)
                {
                    ++$startChar;
                }

                $endChar = $current - 1;
            }
            else if ($marker == '')
            {
                if ($current > $startChar)
                {
                    $terms[$arrayPos] = substr($string, $startChar, $current - $startChar);
                    ++$arrayPos;
                    $startChar = $i + 1;
                }
                $marker = '\'';
                $startChar = $current + 1;
            }
        }

        $string[$current] = $string[$i];

        if ($endChar - $startChar > 1)
        {
            $temp = trim(substr($string, $startChar, $endChar - $startChar + 1));

            if ($stemWords)
            {
                $temp = stem($temp);
            }

            $terms[$arrayPos] = $temp;

            ++$arrayPos;
            $startChar = $current + 1;
            if ($marker != '') { ++$current; ++$startChar; ++$i; }
            $marker = '';
        }
    }

    if ($current - $startChar > 1)
    {
        $temp = trim(substr($string, $startChar, $current - $startChar));

        if ($stemWords)
        {
            $temp = stem($temp);
        }

        $terms[$arrayPos] = $temp;
    }

    return $terms;
}

function parseCSV($data, $callback, $headerLines = 0)
{
    //print "parseCSV($data, $callback, $header = false)<br>";
    if (!$data || !$callback || !function_exists($callback))
    {
        return;
    }

    if (!is_array($data))
    {
        $lines = preg_split("/[\\r\\n]+/", $data);
    }
    else
    {
        $lines = $data;
    }

    $numLines = count($lines);
    //    print "$numLines <== lots to process?<br>";
    unset($marker);
    $terms = array();
    for ($j = $headerLines; $j < $numLines; ++$j)
    {
        $string = trim($lines[$j]);
        //        print "{{$string}}<br>";
        if (isset($marker))
        {
            // we had reached a premature ending of the line
            $string = array_pop($terms) . $string;
        }
        else
        {
            if (count($terms) > 0)
            {
                $callback($terms);
            }
            unset($marker);
            $terms = array();
        }

        $numChars = strlen($string);
//        print "$numChars<br>";
        $startChar = 0;
        $endChar = -1;
        $current = 0;
        $arrayPos = 0;
        $i = 0;

        for (; $i < $numChars; ++$i, ++$current)
        {
            $char = $string[$i];
            if ($char == '\\' &&
                    $i > 0 &&
                    $string[$i - 1] != '\\')
            {
                ++$i;
            }
            else if ($char == ',')
            {
                if (!isset($marker))
                {
                    $endChar = $current;
                }
            }
            else if ($char == '"')// || $char == '\'')
            {
                if ($marker == $char)
                {
                    unset($marker);
                    $string[$i] = ' ';
                }
                else if (!isset($marker))
                {
                    if ($string[$i + 1] != '"')
                    {
                        $marker = $char;
                    }
                    ++$i;
                }
            }

            if ($current != $i)
            {
                $string[$current] = $string[$i];
            }

//print "            if ($i > 0 && $endChar - $startChar > -1)<br>";
            if ($i > 0 && $endChar - $startChar > -1)
            {
                if ($endChar - $startChar > 1)
                {
                    array_push($terms, trim(substr($string, $startChar, $current - $startChar)));
                }
                else
                {
                    array_push($terms, '');
                }

                $startChar = $current + 1;
                unset($marker);
            }
        }

//print "substr($string, $startChar, $current - $startChar) is: " . substr($string, $startChar, $current - $startChar) . "<br>";
        if ($current - $startChar > -1)
        {
            if ($current - $startChar > 1)
            {
                array_push($terms, substr($string, $startChar, $current - $startChar));
            }
            else
            {
                array_push($terms, '');
            }
        }
    }

    if (count($terms) > 0)
    {
        $callback($terms);
    }
}

?>
