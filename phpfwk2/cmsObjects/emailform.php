<?

global $phpfwkIncludePath;
include_once("$phpfwkIncludePath/html-ui.php");
include_once("$phpfwkIncludePath/address.php");

class cms_emailform extends cms_renderer
{
    function cms_emailform($pageID)
    {
        $this->pageID = intval($pageID);
        $this->isEditable = true;
    }

    function render($staticBodyText)
    {
        global $sendEmail, $common_contactEmail;
        global $subject, $message, $name, $email, $toAddress;

        $normalRenderer = new cms_normal($this->pageID, true);
        $normalRenderer->render($staticBodyText);

        $db = db_connect();
        $formDetails = db_query($db, "SELECT toAddress, defaultSubject, buttonText, fancyEditor
                                      FROM cmsEmailForms WHERE pageID = {$this->pageID};");

        $toAddress = $common_contactEmail;
        unset($defaultSubject);
        unset($buttonText);

        if (db_numRows($formDetails) > 0)
        {
            list($toAddress, $defaultSubject, $buttonText, $fancyEditor) = db_row($formDetails, 0);
        }

        if ($sendEmail)
        {
	    $subject = str_replace("\r", "", $subject);
	    $subject = str_replace("\n", "", $subject);
	    $name = str_replace("\r", "", $name);
	    $name = str_replace("\n", "", $name);

            if (!$email)
            {
                print_msg('ERROR', 'Return Address Required',
                          'Your email address is required so that we can get back to you. Please enter one and try again.');
                $this->printForm($defaultSubject, $buttonText, db_boolean($fancyEditor));
            }
            else if (!validateEmail($email))
            {
                print_msg('ERROR', 'Invalid Email Address Supplied',
                          "The email address you supplied does not appear to be valid. Please enter a valid email address and try again.");
                $this->printForm($defaultSubject, $buttonText, db_boolean($fancyEditor));
            }
            else
            {
                $this->sendMail($toAddress, $email, $name, $subject, $message, db_boolean($fancyEditor));
            }
        }
        else
        {
            $this->printForm($defaultSubject, $buttonText, db_boolean($fancyEditor));
        }

        $normalRenderer->renderRest($staticBodyText);
    }

    function printForm($defaultSubject, $buttonText, $htmlArea = false)
    {
        global $common_baseURL;
        global $name, $email, $subject, $message;

        print_form(cms_currentBaseURL(true));

        print '<table border="0" width="100%">';

        print '<tr>';
        print '<td>Your&nbsp;Name</td>';
        print '<td align="left">';
        print_textbox('name', $name, 30);
        print '</td>';
        print '</tr>';

        print '<tr>';
        print '<td>Your&nbsp;Email</td>';
        print '<td align="left">';
        print_textbox('email', $email, 30);
        print '</td>';
        print '</tr>';

        print '<tr>';
        print '<td>Subject</td>';
        print '<td align="left">';
        print_textbox('subject', ($subject ? $subject : $defaultSubject), 30);
        print '</td>';
        print '</tr>';

        print '<tr>';
        print '<td valign="top">Message</td>';
        print '<td width="95%">';

        if ($htmlArea)
        {
            print_htmlArea("message", $message, 250, 605, false);
        }
        else
        {
            print_textarea("message", $message, 10, 60);
        }

        print '</td>';
        print '</tr>';

        print '<tr>';
        print '<td></td>';
        print '<td>';
        print_submit('sendEmail', $buttonText);
        print '</td>';
        print '</tr>';

        print '</table>';
        print '</form>';
    }

    function sendMail($to, $email, $name, $subject, $message, $html = false)
    {
        global $common_bizName, $phpfwk_sendmailPath, $phpfwk_sendmailArgs;

        $subject = trim($subject);
        $name = trim($name);
        $email = trim($email);

        if ($html)
        {
            require_once('Mail.php');

            $headers['From']            =   addslashes($name) . " <" . addslashes($email) . ">" ;
            $headers['To']              =   $to;
            $headers['Reply-To']        =   $email;
            $headers['Subject']         =   $subject ? $subject : "No Subject";
            $headers['Content-type']    =   "text/html; charset=iso-8859-1";

            $sendmail_params['sendmail_path'] = $phpfwk_sendmailPath;
            $sendmail_params['sendmail_args'] = $phpfwk_sendmailArgs;

            $mail_object =& Mail::factory('sendmail', $sendmail_params);
            $mail_object->send($to, $headers, $message);
            //print_msg("INFO", "New Email Sent to : $to ; Reply-To : $email", $message);
        }
        else
        {
            mail($to, $subject? $subject : "No Subject", $message, "From: " . addslashes($name) . " <" . addslashes($email) . ">");
        }

        //print "mail($to, $subject? $subject : \"No Subject\", $message, \"From: \" . addslashes($name) . \" <\" . escape($email) . \">\");";
        print_msg("INFORMATION", 'Message Sent!',
                  "<i>Thank-you</i> for your message. It has been sent to $common_bizName staff who will reply promptly.");
    }

    function edit($formAction, $typeName, $pageTitle)
    {
        global $saveChanges;
        global $toAddress, $defaultSubject, $buttonText, $fancyEditor;

        $db = db_connect();

        if ($this->pageID < 1)
        {
            return true;
        }

        if ($saveChanges)
        {
            if ($toAddress && !validateEmail($toAddress))
            {
                print_msg('WARNING', 'Invalid Email Address',
                          "The email address entered ($toAddress) is not valid. Setting to defaults instead.", 'cms/efe1');
                unset($toAddress);
            }

            db_startTransaction($db);

            db_delete($db, 'cmsEmailForms', "pageID = $this->pageID");

            unset($fields);
            unset($values);

            sql_addIntToInsert($fields, $values, 'pageID', $this->pageID);
            sql_addScalarToInsert($fields, $values, 'toAddress', $toAddress);
            sql_addScalarToInsert($fields, $values, 'defaultSubject', $defaultSubject);
            sql_addScalarToInsert($fields, $values, 'buttontext', $buttonText);
            sql_addBoolToInsert($fields, $values, 'fancyEditor', $fancyEditor);

            if (db_insert($db, 'cmsEmailForms', $fields, $values))
            {
                print_msg('INFORMATION', 'Saved',
                          "Your changes have been saved:<p>Email address: $toAddress<br>
                           Default subject: $defaultSubject<br>Button Text: $buttonText");
            }

            db_endTransaction($db);

            return true;
        }
        else
        {
            $current = db_query($db, "SELECT toAddress, defaultSubject, buttonText, fancyEditor
                                      FROM cmsEmailForms WHERE pageID = {$this->pageID};");

            if (db_numRows($current) > 0)
            {
                list($toAddress, $defaultSubject, $buttonText, $fancyEditor) = db_row($current, 0);
            }

            $this->print_editHeader($typeName, $pageTitle);

            $pages = db_query($db, "SELECT pageID, title FROM cmsPagesMetadata
                                    WHERE parentID IS NOT NULL AND
                                    pageID != {$this->pageID} ORDER BY title;");

            print_form($formAction);

            $this->print_requiredEditHiddens();

            print "<p>You can customize the email address this page should be delivered to, the default subject line and the text that appears on the submit button below:</p>";

            print '<table border=0 cellspacing=0 cellpadding=3>';

            print '<tr><td align=top nowrap>Email Address</td><td align=top nowrap>';
            print_textbox('toAddress', $toAddress);
            print '</td></tr>';

            print '<tr><td align=top nowrap>Default Subject</td><td align=top nowrap>';
            print_textbox('defaultSubject', $defaultSubject);
            print '</td></tr>';

            print '<tr><td align=top nowrap>Button Text</td><td align=top nowrap>';
            print_textbox('buttonText', $buttonText);
            print '</td></tr>';

            print '<tr><td align=top nowrap>&nbsp;</td><td align=top nowrap>';
            print_checkbox('fancyEditor', 'Use the fancy editor for the email message (bold, italics, font)', 1, db_boolean($fancyEditor));
            print '</td></tr>';

            print_emptyRow();

            print '<tr><td></td><td>';
            print_submit('saveChanges', 'Save');
            print '</td></tr></table>';

            print "</form>";

            $this->print_editFooter($typeName, $pageTitle);
            return false;
        }

        return true;
    }
}

?>
