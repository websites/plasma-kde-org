<?

class cms_faq extends cms_renderer
{
    function cms_faq($pageID)
    {
        $this->pageID = intval($pageID);
        $this->isEditable = true;
    }

    function render($staticBodyText)
    {
        global $sendEmail, $common_contactEmail;
        global $editType, $offset;
        $normalRenderer = new cms_normal($this->pageID, true);
        $normalRenderer->render($staticBodyText);

        if (! $offset)
            $offset = 0;

        $limit = 16; // one greater than the required

        $db = db_connect();
        $questions = $this->faq_getQuestions($this->pageID);
        $rowCount = count($questions);
        print '<h3>Frequently Asked Questions</h3>';
        if ($rowCount > 0)
        {
            // first the questions
            //print '<table border="0" cellpadding="3" cellspacing="0" width="100%">';

            print '<a name="qlist"><ol>';
            $qNo = 0;
            foreach ($questions as $question)
            {
                ++$qNo;
                /*print '<tr>';
                print "<td valign=top>$qNo.</td><td><a href=\"#$qNo\">$question</a></td>";
                print '</td></tr>';*/
                print "<li><a href=\"#$qNo\">{$question['q']}</a>";
            }

            print '</ol><hr>';
            //print '</table>';

            // and now the answers
            print '<table border="0" cellpadding="3" cellspacing="0" width="100%">';

            $qNo = 0;
            foreach ($questions as $question)
            {
                ++$qNo;
                print "<tr><td valign=top align=right nowrap><a name=\"$qNo\">Q$qNo.</td><td width=\"100%\">{$question['q']}</td></tr>";
                print "<tr><td align=right style=\"border-bottom: 1px solid #666167;\" valign=top><a name=\"$qNo\">A.</td><td style=\"border-bottom: 1px solid #666167;\">{$question['a']}<br><br><a style=\"font-size: 8pt;\" href=\"#qlist\">Return to top</a></td></tr>";
            }

            print '</table>';
        }
        else
        {
            print_msg('INFORMATION', 'No FAQs', 'There are no frequently asked questions for this area.', 'faq/5');
        }

        $normalRenderer->renderRest($staticBodyText);
    }

    function edit($formAction, $typeName, $pageTitle)
    {
        global $cmsFAQ_add, $cmsFAQ_insert, $cmsFAQ_edit, $cmsFAQ_update, $cmsFAQ_delete, $questionID;
        $list = true;
        $this->print_editHeader($typeName, $pageTitle);
        if ($cmsFAQ_add)
        {
            $this->faq_addFAQ($formAction, $pageTitle);
            $list = false;
        }
        else if ($cmsFAQ_insert)
        {
            if (!$btnBack)
            {
                $this->faq_insertFAQ();
            }
        }
        else if ($cmsFAQ_edit)
        {
            $this->faq_editFAQ($formAction, $questionID);
            $list = false;
        }
        else if ($cmsFAQ_update)
        {
            if (!$btnBack)
            {
                $this->faq_updateFAQ($questionID);
            }
        }
        else if ($cmsFAQ_delete)
        {
            $this->faq_deleteFAQ($questionID);
        }

        if ($list)
        {
            $this->faq_listFAQs($formAction, $pageTitle);
        }
        $this->print_editFooter($typeName, $pageTitle);
    }

    function faq_listFAQs($formAction, $pageTitle)
    {
        global $editType, $offset;

        if (! $offset)
            $offset = 0;

        $glue = '?';
        if (strstr($formAction, '?'))
        {
            $glue = '&';
        }

        $limit = 16; // one greater than the required

        $db = db_connect();
        $query = "SELECT faqID, question, answer FROM cmsFAQs WHERE pageID = {$this->pageID} ORDER BY displayorder;";
        $questions = db_query($db, $query);
        $rowCount = db_numRows($questions);

        if ($rowCount > 0)
        {
            // add "new" question
            print "<a href=\"$formAction{$glue}currentPageID={$this->pageID}&editType=1&cmsFAQ_add=1\">Add a new FAQ</a>";
            print '<table class=box border="0" cellpadding="3" cellspacing="0" width="100%">';
            print '<tr><td class="largeColoredHeader">Questions</td><td class="largeColoredHeader">Actions</td></tr>';

            for ($i = 0; $i < $rowCount; $i++)
            {
                list($faqID, $question, $answer) = db_row($questions, $i);

                $qNo = $i + 1;
                print '<tr class="' . ($odd ? 'oddRow' : 'evenRow') . '">';
                print "<td width=\"80%\" valign=top>Q$qNo. $question</td>";
                print "<td nowrap valign=top><a href=\"$formAction{$glue}currentPageID={$this->pageID}&editType=1&cmsFAQ_edit=1&questionID=$faqID\">Edit</a> &nbsp;&nbsp;";
                print "<a href=\"$formAction{$glue}currentPageID={$this->pageID}&editType=1&cmsFAQ_delete=1&questionID=$faqID\" onClick=\"return(confirm('Are you sure that you want to delete this question?'))\">Delete</a>";
                print '</td></tr>';

                // toggle the row colour
                $odd = !$odd;
            }

            print '</table>';
        }
        else
        {
            print_msg('INFORMATION', "No FAQs for '$pageTitle' Page", "There are no frequently asked questions currently in the database for the '$pageTitle' page.", 'faq/5');
        }

        // add "new" question
        print "<a href=\"$formAction{$glue}currentPageID={$this->pageID}&editType=1&cmsFAQ_add=1\">Add a new FAQ</a>";
    }

    function faq_getQuestions()
    {
        global $editType, $offset;
        $questionArray = array();

        if (! $offset)
            $offset = 0;

        $limit = 16; // one greater than the required

        $db = db_connect();
        $query = "SELECT faqID, question, answer FROM cmsFAQs WHERE pageID = {$this->pageID} ORDER BY displayorder;";
        $questions = db_query($db, $query);
        $rowCount = db_numRows($questions);

        for ($i = 0; $i < $rowCount; $i++)
        {
            list($faqID, $question, $answer) = db_row($questions, $i);
            $questionArray[$i] = array('id' => $faqID, 'q' => $question, 'a' => $answer);
        }

        return $questionArray;
    }

    function faq_printQuestion($questionID)
    {
        $questionID = intval($questionID);

        if ($questionID < 1)
        {
            print_msg('ERROR', 'FAQ Not Found', 'An FAQ ID is required for editting.', 'faqprint/1');
            return;
        }

        $db = db_connect();
        $questions = db_query($db,  "SELECT faqID, pageID, displayOrder, question, answer FROM cmsFAQs WHERE faqID = $questionID and pageID = {$this->pageID};");

        if (db_numRows($questions) < 1)
        {
            print_msg('ERROR', 'FAQ Not Found', 'The requested FAQ could not be found. Perhaps it was deleted?', 'faqprint/2');
            return;
        }

        list($faqID, $displayOrder, $question, $answer) = db_row($questions, 0);
    ?>
        <table border="0" cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <th valign="top">Question</td><td width=100%><?php print $question; ?></td>
        </tr>
        <tr>
            <th valign="top">Answer</td><td><?php print $answer; ?></td>
        </tr>
        </table>
    <?
    }

    function faq_editFAQ($formAction, $questionID)
    {
        global $editType;

        $questionID = intval($questionID);

        if ($questionID < 1)
        {
            print_msg('ERROR', 'FAQ Not Found', 'An FAQ ID is required for editting.', 'faq/1');
            return;
        }

        $db = db_connect();
        $questions = db_query($db,  "SELECT faqID, displayOrder, question, answer FROM cmsFAQs WHERE faqID = $questionID and pageID = {$this->pageID};");

        if (db_numRows($questions) < 1)
        {
            print_msg('ERROR', 'FAQ Not Found', 'The requested FAQ could not be found for editting. Perhaps it was deleted?', 'faq/2');
            return;
        }

        list($faqID, $displayOrder, $question, $answer) = db_row($questions, 0);
        $displayOrder = intval($displayOrder);
        $query = "SELECT displayOrder, question FROM cmsFAQs WHERE pageID = {$this->pageID} and faqID != $questionID ORDER BY displayOrder;";
        $questions = db_query($db, $query);
        $prevDisplayOrder = db_query($db, "select displayOrder from cmsFAQs where pageID = {$this->pageID} and displayOrder < $displayOrder + 1 and faqID != $questionID order by displayOrder desc limit 1;");

        if (db_numRows($prevDisplayOrder) < 1)
        {
            $prevDisplayOrder = 0;
        }
        else
        {
            list($prevDisplayOrder) = db_row($prevDisplayOrder, 0);
        }

        print_form($formAction);
        print_hidden('prevDisplayOrder', $displayOrder);
        print_hidden('cmsFAQ_update', 1);
        print_hidden('editType', 1);
        print_hidden('questionID', $questionID);
    ?>
        <table border="0" cellpadding="3" cellspacing="3" width="100%">
        <tr>
            <td class="largeColoredHeader" colspan="2">Edit a Frequently Asked Question</td>
        </tr>
        <tr>
            <td valign="top">Question</td><td><? print_richTextArea('question', $question, 50) ?></td>
        </tr>
        <tr>
            <td valign="top">Answer</td><td><? print_richTextArea('answer', $answer) ?></td>
        </tr>

        <?php if (db_numRows($questions) > 0) { ?>
        <tr>
            <td>Show After</td>
            <td>
                <?php
                    print_selectQuery('displayOrder', $questions, $prevDisplayOrder, false);
                ?>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <td align="right" colspan="2">
                <? print_submit('addFAQs', 'Save FAQ'); ?>
                &nbsp;&nbsp;
                <? print_submit('btnBack', 'Cancel');  ?></td>
    </td>
        </tr>
        </table>
        </form>
    <?
    }

    function faq_updateFAQ($questionID)
    {
        global $question, $answer, $displayOrder;

        if (intval($questionID) > 0)
        {
            // build the update statement
            unset($fields);

            $db = db_connect();
            db_startTransaction($db);
            sql_addScalarToUpdate($fields, 'question', $question);
            sql_addScalarToUpdate($fields, 'answer', $answer);
            sql_addIntToUpdate($fields, 'displayOrder', $displayOrder + 1);
            db_update($db, 'cmsFAQs', 'displayOrder = displayOrder + 1', "pageID = {$this->pageID} and displayOrder > $displayOrder - 1;");
            db_update($db, 'cmsFAQs', $fields, "faqID = $questionID");
            db_endTransaction($db);

            print_msg('INFORMATION', 'Update Successful', 'The update to your question has been made successfully.', 'faq/6');
        }
    }

    function faq_getQuestionsList($exclude = 0)
    {
        $db = db_connect();
        unset($where);
        sql_addToWhereClause($where, 'where', 'pageID', '=', $this->pageID);

        if ($exclude > 0)
        {
            sql_addToWhereClause($where, 'where', 'pageID', '=', $this->pageID);
        }
        $query = "SELECT faqID, question FROM cmsFAQs WHERE pageID = {$this->pageID} ORDER BY displayOrder;";
        $questions = db_query($db, $query);
        $numRows = db_numRows($questions);

        print '<option value="">';

        // loop over the recordset -- build <option>'s
        for($i = 0; $i < $numRows; $i++)
        {
            list($faqID, $question) = db_rowArray($questions, $i);
            if (strlen($question) > 40)
            {
                $question = substr($question, 0, 40) . '...';
            }
            print "<option value=\"" . ($i + 1) . "\">" . $question . "\n";
        }
    }

    function faq_addFAQ($formAction, $pageTitle)
    {
        ?>
        <form id="frmAddFAQs" method="post" name="frmAddFAQs" action="<?= $formAction ?>">
        <input type="hidden" id="currentPageID" name="currentPageID" value="<?= $this->pageID ?>">
        <input type="hidden" id="editType" name="editType" value="1">
        <input type="hidden" id="cmsFAQ_insert" name="cmsFAQ_insert" value="1">
        <table class=box border="0" cellpadding="3" cellspacing="0" width="100%">
        <tr>
            <th class="largeColoredHeader" colspan="2">Add a FAQ to the '<?php print $pageTitle; ?>' Page</th>
        </tr>
        <tr>
            <td valign="top">Question: </td><td><? print_richTextArea('question', $question, 50) ?></td>
        </tr>
        <tr>
            <td valign="top">Answer: </td><td><? print_richTextArea('answer', $answer) ?></td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <? print_submit('addFAQs', 'Save FAQ'); ?>
                &nbsp;&nbsp;
                <? print_submit('btnBack', 'Cancel');  ?></td>
        </tr>
        </table>
        </form>
    <?
    }

    function faq_insertFAQ()
    {
        global $question, $answer;
        global $editType;

        unset($fields);
        unset($values);

        $db = db_connect();

        list($displayOrder) = db_row(db_query($db, "select max(displayOrder) + 1 from cmsFAQs where pageID = {$this->pageID};"), 0);

        sql_addIntToInsert($fields, $values, 'pageID', $this->pageID);
        sql_addIntToInsert($fields, $values, 'displayOrder', $displayOrder);
        sql_addScalarToInsert($fields, $values, 'question', $question);
        sql_addScalarToInsert($fields, $values, 'answer', $answer);
    /*    sql_addScalarToInsert($fields, $values, 'question', nl2br(ereg_replace('<br ?/?>', '', trim($question))));
        sql_addScalarToInsert($fields, $values, 'answer', nl2br(ereg_replace('<br ?/?>', '', trim($answer))));*/


        if (db_insert($db, 'cmsFAQs', $fields, $values))
        {
            print_msg('INFORMATION', 'Successful Addition', "The new FAQ was added successfully.", 'faq/3');
        }

        return;
    }

    function faq_deleteFAQ($questionID)
    {
        $db = db_connect();
        $query = "DELETE FROM cmsFAQs WHERE faqID = $questionID;";

        if (intval($questionID) > 0)
        {
            $db = db_connect();

            db_startTransaction($db);
            db_query($db, $query);
            db_endTransaction($db);

            // print status message for the user
            print_msg('INFORMATION', 'Successful Deletion', 'The deletion of the question was successful.', 'faq/4');
        }
    }
}

?>
