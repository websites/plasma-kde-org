<?

class cms_embed extends cms_renderer
{
    function cms_embed($pageID)
    {
        $this->pageID = intval($pageID);
        $this->isEditable = true;
    }

    function render($staticBodyText)
    {
        $db = db_connect();
        $embedRow = db_query($db, "SELECT url FROM cmsEmbed WHERE pageID = {$this->pageID};");
        if (db_numRows($embedRow) > 0)
        {
            list($url) = db_row($embedRow, 0);

            print "<iframe src=\"$url\" width=100% height=100%>THIS PAGE CONTAINS AN EMBEDDED PAGE.  YOUR BROWSER DOES NOT SUPPORT THE USE OF IFRAMES.</iframe>";
            return;
        }
    }

    function edit($formAction, $typeName, $pageTitle)
    {
        global $saveChanges, $url;

        if ($this->pageID < 1)
        {
            return true;
        }

        $db = db_connect();
        if ($saveChanges)
        {
            db_startTransaction($db);
            db_delete($db, 'cmsEmbed', "pageID = $this->pageID");

            unset($fields);
            unset($values);
            sql_addIntToInsert($fields, $values, 'pageID', $this->pageID);
            sql_addScalarToInsert($fields, $values, 'url', $url);
            db_insert($db, 'cmsEmbed', $fields, $values);
            db_endTransaction($db);
            return true;
        }
        else
        {
            $this->print_editHeader($typeName, $pageTitle);

            $embedRow = db_query($db, "SELECT url FROM cmsEmbed WHERE pageID = {$this->pageID};");

            if (db_numRows($embedRow) > 0)
            {
                list($url) = db_row($embedRow, 0);
            }
            else
            {
                unset($url);
            }

            list($ownedBy) = db_row(db_query($db, "select adminGroup from cmsPagesMetadata where pageID = {$this->pageID};"), 0);

            print_form($formAction);
            $this->print_requiredEditHiddens();
            print "<p>Enter the URL to be embedded when viewing this page:</p>";
            print "URL : ";
            print_textbox('url', $url, 45);
            print '&nbsp;&nbsp;';
            print_submit('saveChanges', 'Save');
            print "</form>";
            $this->print_editFooter($typeName, $pageTitle);
            return false;
        }

        return true;
    }
}

?>
