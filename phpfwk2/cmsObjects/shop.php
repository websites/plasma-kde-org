<?

global $phpfwkIncludePath;
include_once("$phpfwkIncludePath/shopping.php");
include_once("$phpfwkIncludePath/address.php");

global $countryArray;

class cms_shop extends cms_renderer
{
    function cms_shop($pageID)
    {
        $this->pageID = $pageID;
        $this->isEditable = true;
    }

    function render($staticBodyText)
    {
        global $common_contactEmail;
        global $changeCurrency, $currency, $addToCart, $del_items, $empty;
        global $showcat, $showgrp;
        global $showcart, $checkout, $purchase;
        global $cartID, $shopping_customMenu;
        global $shopping_defaultCurrency, $shopping_shippingClerk;

        $cartID = intval($cartID);
        $showcat = intval($showcat);
        $showgrp = intval($showgrp);
        $showitem = intval($showitem);

        $db = db_connect();

        $shop = db_query($db, "SELECT s.name, s.currency, cl.name
                               FROM stores s
                               JOIN cmsShopping c ON (s.storeID = c.storeID)
                               LEFT JOIN clerks cl ON (cl.clerkID = s.clerk)
                               WHERE c.pageID = {$this->pageID};");

        if (db_numRows($shop) > 0)
        {
            list($shopName, $shopping_defaultCurrency, $shopping_shippingClerk) = db_row($shop, 0);
        }

        $url = cms_currentBaseURL(true);

        print '<table border=0 cellpadding=0 cellspacing=0 width="100%" height="100%">';

        if (!$shopping_customMenu)
        {
            print '<tr>';
            print '<td valign=top nowrap class="shopMenu" style="border-bottom: none;" height="100%">';
            print_shoppingMenu($url, $this->pageID, strlen($staticBodyText) < 1);
            print '</td>';
        }

        print '<td valign=top width=100% height="100%">';

        if ($changeCurrency)
        {
            if (! $cartID)
            {
                $cartID = createEmptyCart();
            }

            db_query($db, "UPDATE shoppingCarts SET currency = '" . addslashes($currency) . "'
                           WHERE cartID = $cartID;");
        }

        if ($addToCart)
        {
            // add an item to the cart
            addItemToCart();
        }

        if ($empty)
        {
            // empty the cart (delete it)
            db_query($db, "DELETE FROM shoppingCarts WHERE cartID = $cartID;");
            $cartID = 0;
        }

        // if we have a purchase ... validate
        $validatedPurchase = false;
        if ($purchase && validatePurchase())
        {
            $validatedPurchase = true;
        }

        // set or delete the cookie as needed, prior to headers
        if ($cartID)
        {
            setcookie('cartID', $cartID, time() + '3600', "$common_sitePath");
        }
        else
        {
            setcookie('cartID', 0);
        }

        // set out listing names appropriately
        if ($showcart)
        {
            // delete a shopping cart item
            if (count($del_items) > 0)
            {
                foreach ($del_items as $id)
                {
                    db_query($db, "DELETE FROM shoppingCartItems WHERE itemID= $id;");
                }
            }

            // checkout the cart and purchase, or just show the cart
            if ($checkout)
            {
                if ($purchase)
                {
                    if ($validatedPurchase)
                    {
                        updateCart();
                        print_invoice($url);
                        setcookie('cartID', 0);
                    }
                    else
                    {
                        checkout($url);
                    }
                }
                else
                {
                    checkout($url);
                }
            }
            else
            {
                show_cart($url);
            }
        }
        else if (!print_shopping($url))
        {
            print $staticBodyText;
        }
        else
        {
            print '<br>';
        }

        print '</td></tr></table>';
    }

    function edit($formAction, $typeName, $pageTitle)
    {
        global $saveChanges, $store;

        if ($this->pageID < 1)
        {
            return true;
        }

        $db = db_connect();

        if ($saveChanges)
        {
            db_startTransaction($db);

            db_delete($db, 'cmsShopping', "pageID = $this->pageID");

            $store = intval($store);

            if ($store > 0)
            {
                unset($fields, $values);

                sql_addIntToInsert($fields, $values, 'storeID', $store);
                sql_addIntToInsert($fields, $values, 'pageID', $this->pageID);

                db_insert($db, 'cmsShopping', $fields, $values);
            }

            db_endTransaction($db);

            return true;
        }
        else
        {
            $current = db_query($db, "SELECT storeID FROM cmsShopping
                                      WHERE pageID = {$this->pageID};");
            $store = -1;

            if (db_numRows($current) > 0)
            {
                list($store) = db_row($current, 0);
            }

            $this->print_editHeader($typeName, $pageTitle);

            print_form($formAction);

            $this->print_requiredEditHiddens();
            print "<p>Select which store to display on this page:</p>";
            print '<table border=0 cellpadding=4 cellspacing=0 class=box>';
            print '<tr><th class="coloredHeader">Stores</th></tr>';

            $stores = db_query($db, "SELECT storeID, name FROM stores ORDER BY lower(name);");
            $numStores = db_numRows($stores);
            $odd = false;

            print '<tr><td class="oddRow" align=top nowrap>';
            print_radio("store", 'No store', -1, $store < 1);
            print "</td></tr>";

            for ($i = 0; $i < $numStores; ++$i)
            {
                list($storeID, $name) = db_row($stores, $i);

                $class = $odd ? 'class="oddRow"' : 'class="evenRow"';
                $odd = !$odd;

                print "<tr><td $class align=top nowrap>";
                print_radio("store", $name, $storeID, $store == $storeID);
                print "</td></tr>";
            }

            print '<tr><td align=center>';
            print_submit('saveChanges', 'Save');
            print '</td></tr></table>';
            print "</form>";

            $this->print_editFooter($typeName, $pageTitle);
            return false;
        }

        return true;
    }
}
?>
