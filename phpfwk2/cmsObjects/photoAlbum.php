<?
global $phpfwkIncludePath;
include_once("$phpfwkIncludePath/albums.php");

class cms_photoAlbum extends cms_renderer
{
    function cms_photoAlbum($pageID)
    {
        $this->pageID = $pageID;
        $this->isEditable = false;
    }

    function render($staticBodyText)
    {
        global $albumID, $photoOffset;

        $url = cms_currentBaseURL(true);
        print '<table border=0 cellpadding=3 cellspacing=0 width="100%" height="100%">';
        print '<tr><td valign=top nowrap class="navStripeRight" width=150 height="100%">';
        print_albumMenu($url);
        print '</td><td valign=top width=100% height="100%">';

        $albumID = intval($albumID);
        print_album($url, $albumID);

        print '</td></tr></table>';
    }
}
?>
