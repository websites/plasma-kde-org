<?

class cms_symlink extends cms_renderer
{
    function cms_symlink($pageID)
    {
        $this->pageID = intval($pageID);
        $this->isEditable = true;
    }

    function render($staticBodyText)
    {
        $db = db_connect();
        $linkedTo = db_query($db, "select realPageID from cmsSymlinks where pageID = {$this->pageID};");
        if (db_numRows($linkedTo) > 0)
        {
            list($realPageID) = db_row($linkedTo, 0);
            cms_printPage($realPageID, $title);
            return;
        }
    }

    function edit($formAction, $typeName, $pageTitle)
    {
        global $saveChanges, $realPageID;

        if ($this->pageID < 1)
        {
            return true;
        }

        $db = db_connect();
        if ($saveChanges)
        {
            db_startTransaction($db);
            db_delete($db, 'cmsSymlinks', "pageID = $this->pageID");

            unset($fields);
            unset($values);
            sql_addIntToInsert($fields, $values, 'pageID', $this->pageID);
            sql_addIntToInsert($fields, $values, 'realPageID', $realPageID);
            db_insert($db, 'cmsSymlinks', $fields, $values);
            db_endTransaction($db);
            return true;
        }
        else
        {
            $this->print_editHeader($typeName, $pageTitle);
            $currentSymlink = db_query($db, "select realPageID from cmsSymlinks where pageID = {$this->pageID};");
            if (db_numRows($currentSymlink) > 0)
            {
                list($currentSymlink) = db_row($currentSymlink, 0);
            }
            else
            {
                $currentSymlink = 0;
            }

            list($ownedBy) = db_row(db_query($db, "select adminGroup from cmsPagesMetadata where pageID = {$this->pageID};"), 0);
            $pages = db_query($db, "select pageID, title from cmsPagesMetadata where parentID is not null and pageID != {$this->pageID} and adminGroup = $ownedBy order by title;");
            print_form($formAction);
            $this->print_requiredEditHiddens();
            print "<p>Select which page's content should be shown when viewing this page:</p>";
            print "Pages: ";
            print_selectQuery('realPageID', $pages, $currentSymlink);
            print '&nbsp;&nbsp;';
            print_submit('saveChanges', 'Save');
            print "</form>";
            $this->print_editFooter($typeName, $pageTitle);
            return false;
        }

        return true;
    }
}

?>
