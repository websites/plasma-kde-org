<?
global $phpfwkIncludePath;
include_once("$phpfwkIncludePath/dmsCommon.php");

class cms_dms extends cms_renderer
{
    function cms_dms($pageID)
    {
        $this->pageID = $pageID;
        $this->isEditable = true;
    }

    function render($staticBodyText)
    {
        global $sendEmail, $common_contactEmail, $offset;
        $pageLimit = 100;

        $offset = intval($offset);
        if ($offset < 1)
        {
            $offset = 0;
        }

        $normalRenderer = new cms_normal($this->pageID, true);
        $normalRenderer->render($staticBodyText);

        $db = db_connect();
        $folderID = db_query($db, "select d.folderID, d.name, c.showHeader from cmsDMS c join dmsFolders d on (c.folderID = d.folderID) where pageID = {$this->pageID};");

        if (db_numRows($folderID) < 1)
        {
            return;
        }

        list($folderID, $folderName) = db_row($folderID, 0);
        list($docCount) = db_row(db_query($db, "select count(documentID) from dmsDocuments where folderID = $folderID;"), 0);
        $documents = db_query($db, "select documentID, created, filename, title, description from dmsDocuments where folderID = $folderID order by displayOrder limit " . ($pageLimit + 1) . " offset $offset;");
        $numShowing = $numDocuments = db_numRows($documents);
        $actionURL = cms_currentBaseURL(true);

        if ($numDocuments > 0)
        {
            print "<table border=0 cellpadding=3 cellspacing=0>";

            if ($showHeader)
            {
                print "<tr><th class=header colspan=3>$folderName</th></tr>";
            }
        }

        if ($numDocuments > $pageLimit)
        {
            $numDocuments = $pageLimit;
        }

        for ($i = 0; $i < $numDocuments; ++$i)
        {
            list($documentID, $created, $filename, $title, $description) = db_row($documents, $i);
            $url = dmsFileURL($folderID, $documentID, $filename);
            print "<tr><td colspan=3><a href=\"$url\">$title</a>";
            if (trim($description))
            {
                print "<br><div class=\"dmsDescription\">$description</span>";
            }
            print "</td></tr>";
        }

        if ($numDocuments > 0)
        {
            if (needsNextPrev($offset, $numShowing, $docCount,  $pageLimit))
            {
                 print_nextPrev($offset, $numShowing, $docCount, $pageLimit, $actionURL, 1);
            }
            print "</table>";
        }

        $normalRenderer->renderRest($staticBodyText);
    }

    function edit($formAction, $typeName, $pageTitle)
    {
        global $saveChanges, $folderID, $showHeader;

        if ($this->pageID < 1)
        {
            return true;
        }

        $db = db_connect();
        if ($saveChanges)
        {
            $folderID = intval($folderID);

            if ($folderID < 1)
            {
                print_msg('WARNING', 'Invalid Folder', "A valid document folder must be selected!", 'cms/edms1');
                unset($folderID);
                return false;
            }


            db_startTransaction($db);
            db_delete($db, 'cmsDMS', "pageID = $this->pageID");

            unset($fields);
            unset($values);
            sql_addIntToInsert($fields, $values, 'pageID', $this->pageID);
            sql_addIntToInsert($fields, $values, 'folderID', $folderID);
            sql_addBoolToInsert($fields, $values, 'showHeader', db_boolean($showHeader));
            if (db_insert($db, 'cmsDMS', $fields, $values))
            {
                print_msg('INFORMATION', 'Saved', "Your changes to this document listing page have been saved.");
            }
            db_endTransaction($db);
            return true;
        }
        else
        {
            $current = db_query($db, "select folderID, showHeader from cmsDMS where pageID = {$this->pageID};");
            if (db_numRows($current) > 0)
            {
                list($currentFolderID, $showHeader) = db_row($current, 0);
            }

            $this->print_editHeader($typeName, $pageTitle);

            $folders = db_query($db, "select folderID, name from dmsFolders order by lower(name);");
            $size = db_numRows($folders);
            if ($size > 100)
            {
                $size = 100;
            }
            else if ($size < 1)
            {
                print_msg('WARNING', 'No Document Folders', "No document folders could be found!", 'cms/edms3');
                $this->print_editFooter($typeName, $pageTitle);
                return false;
            }

            print_form($formAction);
            $this->print_requiredEditHiddens();
            print "<p>Select which document folder to display from the list below:</p>";
            print '<table border=0>';
            print '<tr><td valign=top nowrap>Folders</td><td valign=top nowrap>';
            print_selectQuery('folderID', $folders, $currentFolderID, true, "size=$size");
            print '</td></tr>';
            print '<tr><td valign=top nowrap></td><td valign=top nowrap>';
            print_checkbox('showHeader', 'Print folder title as listing header', db_toBoolean(true), db_boolean($showHeader));
            print '</td></tr>';
            print '<tr><td></td><td>';
            print_submit('saveChanges', 'Save');
            print '</td></tr></table>';
            print "</form>";

            $this->print_editFooter($typeName, $pageTitle);
            return false;
        }

        return true;
    }
}

?>
