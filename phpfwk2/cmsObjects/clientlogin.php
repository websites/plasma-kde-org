<?

global $common_includesPath, $cms_admin;

if (!$cms_admin)
{
    include_once("$common_includesPath/auth_client.php");
    include_once("$common_includesPath/address.php");
}

class cms_clientlogin extends cms_renderer
{
    function cms_clientlogin($pageID)
    {
        $this->pageID = $pageID;
        $this->isEditable = false;
    }

    function render($staticBodyText)
    {
        global $authUser, $authPassword, $auth_cookieName, $auth_userName, $auth_companyName, $editUserSettings;
        $normalRenderer = new cms_normal($this->pageID, true);

        $username = trim($username);
        $password = trim($password);
        if (($authUser && ($localBoy || $authPassword)) || $_COOKIE[$auth_cookieName])
        {
            userAuth(false);

            if (isLoggedIn())
            {
                if ($editUserSettings == 1 &&!$this->editSettings())
                {
                    return;
                }

                // wheee! now show them the main menu, nes pas?
                $baseURL = cms_currentBaseURL(true);
                print '<table width="100%" border=0 cellpadding=3 cellspacing=0>';
                print '<tr><td valign=top rowspan=3 width="100%">';
                print $staticBodyText;
                print '</td><th class="smallheader" valign=top nowrap>';
                print "Logged In";
                print "</td></tr>";
                print "<tr><td valign=top align=center nowrap class=box style=\"border: 1px solid gray; border-top: none;\" width=\"100px\"><em>$auth_userName</em><br>";
                print "$auth_companyName<br><br>";
                print "<div align=center><a href=\"$baseURL&editUserSettings=1\">My&nbsp;Settings</a><br><br>";
                print "<a href=\"$baseURL&logOut=1\">Log out</a></div>";

                print '</td></tr>';
                print '<tr><td height="100%"></td></tr></table>';
            }
        }
        else
        {
            auth_printLogin(cms_currentBaseURL());
        }

    }

    function editSettings()
    {
        global $auth_userName, $authUser, $auth_email, $auth_userID;
        global $saveChanges, $email, $password, $passwordConfirm;
        global $first_name, $last_name;
        global $auth_userName, $auth_userTable;

        if (!isLoggedIn())
        {
            return;
        }

        if ($saveChanges == 1)
        {
            $email = trim($email);
            $password = trim($password);
            $passwordConfirm = trim($passwordConfirm);
            $first_name = trim($first_name);
            $last_name = trim($last_name);

            if (!$first_name)
            {
                print_msg('ERROR', 'First Name Requierd', 'A first name is required! Please check it and try again', 'cle/4');
            }
            else if (!$last_name)
            {
                print_msg('ERROR', 'Invalid Email', 'A last name is required! Please check it and try again', 'cle/1');
            }
            else if ($email && !validateEmail($email))
            {
                print_msg('ERROR', 'Invalid Email', 'The email address as entered is not valid. Please check it and try again', 'cle/1');
            }
            else if ($password && $password != $passwordConfirm)
            {
                print_msg('ERROR', 'Invalid Password', 'The new password and password confirmation as entered did not match. Please try again.', 'cle/2');
            }
            else
            {
                unset($fields);
                $auth_userName = "$first_name $last_name";
                sql_addScalarToUpdate($fields, 'first_name', $first_name);
                sql_addScalarToUpdate($fields, 'last_name', $last_name);
                sql_addScalarToUpdate($fields, 'email', $email);
                if ($password)
                {
                    sql_addScalarToUpdate($fields, 'password', $password);
                }

                if (isset($fields))
                {
                    db_update(db_connect(), $auth_userTable, $fields, "userID = $auth_userID");
                }

                return true;
            }
        }

        $extras = db_query(db_connect(), "select first_name, last_name from $auth_userTable where userID = $auth_userID;");
        if (db_numRows($extras) > 0)
        {
            // pathalogically safe =)
            list($first_name, $last_name) = db_row($extras, 0);
        }
        print "<h3>Alter Account Settings for $auth_userName ($authUser)</h3>";
        print "<p>Use may use this page to modify your password or email address. To alter other information related to your account, click on the Contact Us link above and send us an email.</p>";
        print_form(cms_currentBaseURL(true));
        print_hidden('editUserSettings', 1);
        print_hidden('saveChanges', 1);
        print "<table class=box cellpadding=3 cellspacing=0>";
        print "<tr><th class=\"header\" colspan=2>Personal Information</td></tr>";
        print "<tr><td>First Name</td><td>";
        print_textBox('first_name', $first_name);
        print "</td></tr>";
        print "<tr><td>Last Name</td><td>";
        print_textBox('last_name', $last_name);
        print "</td></tr>";
        print "<tr><td>Email Address</td><td>";
        print_textBox('email', $auth_email);
        print "</td></tr>";
        print "<tr><td></td><td>";
        print_submit('submit', 'Update My Settings');
        print "</td></tr>";
        print "<tr><td>&nbsp;</td></tr>";
        print "<tr><th class=\"header\" colspan=2>Password</td></tr>";
        print "<tr><td class=\"dataheader\" colspan=2 align=center>If you do not wish to change your password,<br>do not enter anything below.</td></tr>";
        print "<tr><td>Change Password</td><td>";
        print_passwordBox('password', '');
        print "</td></tr>";
        print "<tr><td>Confirm Password</td><td>";
        print_passwordBox('passwordConfirm', '');
        print "</td></tr>";
        print "<tr><td></td><td>";
        print_submit('submit', 'Update My Settings');
        print "</td></tr>";
        print "</table>";
        print "</form>";
        return false;
    }

/*    function edit($formAction, $typeName, $pageTitle)
    {
    }*/
}

?>
