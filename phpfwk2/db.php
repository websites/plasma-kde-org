<?php

include_once("$phpfwkIncludePath/sql.php");

if ($dbLocalOnly)
{
    $db_connectString = "dbname=$dbName user=$dbUsername password=$dbPassword";
}
else
{
    $db_connectString = "host=$dbHost port=$dbPort dbname=$dbName user=$dbUsername password=$dbPassword";
}

function db_connect($persistent = false, $new = false)
{
    global $db_connectString, $dbSingleDatabaseOnly, $db_connectionSingleton;
    static $db_connectionSingleton = 0;

    if (!$db_connectionSingleton || (!$dbSingleDatabaseOnly && $new))
    {
        if ($persistent)
        {
            $db_connectionSingleton = pg_pconnect($db_connectString);
        }
        else
        {
            $db_connectionSingleton = pg_connect($db_connectString);
        }
    }

    return $db_connectionSingleton;
}

function db_close($db)
{
    pg_close($db);
}

function db_query($db_connection, $sql, $debug = 0, $profile = 0)
{
    global $phpfwk_dbErrorMsg, $phpfwk_dbDebug, $phpfwk_dbProfile;

    if ($debug || $phpfwk_dbDebug)
    {
        static $i = 0;
        ++$i;
        print "$i: " . htmlentities($sql) . "<br>";
        if ($profile || $phpfwk_dbProfile)
        {
            if ($i == 1)
            {
                pg_exec($db_connection, "VACUUM ANALYZE;");
            }

            $howBad = pg_exec($db_connection, "EXPLAIN ANALYZE $sql");
            $numBad = db_numRows($howBad);
            print "<pre>";
            for ($j = 0; $j < $numBad; ++$j)
            {
                list($line) = db_row($howBad, $j);
                print "$line\n";
            }
            print "</pre>";
        }
    }

    $rv = pg_exec($db_connection, $sql);

    if (!$rv && $phpfwk_dbErrorMsg)
    {
        print_msg('ERROR', "Database Execution Error!",
                  pg_errormessage($db_connection) . "<br><br><b>Statement passed to database:</b><br><br>$sql");
    }

    return $rv;
}

function db_numRows($db_query)
{
    return pg_numrows($db_query);
}

function db_row($db_query, $row)
{
    return pg_fetch_row($db_query, $row);
}

function db_rowArray($db_query, $row)
{
    return pg_fetch_array($db_query, $row);
}

function db_array($db_query)
{
    $rv = array();

    $numRows = pg_numrows($db_query);

    for ($i = 0; $i < $numRows; $i++)
    {
        list($val) = pg_fetch_row($db_query, $i);
        array_push($rv, $val);
    }

    if (count($rv) <=0)
    {
        return NULL;
    }
    return $rv;
}

function db_startTransaction($db_connection)
{
    return pg_exec($db_connection, "BEGIN;");
}

function db_endTransaction($db_connection)
{
    return pg_exec($db_connection, "COMMIT;");
}

function db_abortTransaction($db_connection)
{
    return pg_exec($db_connection, "ABORT;");
}

function db_seqNextVal($db_connection, $sequence)
{
    $rc = -1;
    $seqVal = db_query($db_connection, "SELECT nextval('" . addslashes($sequence) . "');");
    if (db_numRows($seqVal) > 0)
    {
        list($rc) = db_row($seqVal, 0);
    }
    return $rc;
}

function db_seqCurrentVal($db_connection, $sequence)
{
    $rc = -1;
    $seqVal = db_query($db_connection, "SELECT currval('" . addslashes($sequence) . "');");
    if (db_numRows($seqVal) > 0)
    {
        list($rc) = db_row($seqVal, 0);
    }
    return $rc;
}

/*
 * conversions from db data formats to PHP data formats
 */

function db_boolean($bool)
{
    if ($bool == 't' || $bool == 'true')
    {
        return true;
    }

    return false;
}

function db_toBoolean($bool)
{
    if ($bool)
    {
        return 'true';
    }

    return 'false';
}

/*
 * INSERT, UPDATE, and DELETE
 */

function db_insert($db, $table, $fields, $values, $debug = 0, $profile = 0)
{
    return db_query($db, "INSERT INTO $table ($fields) VALUES ($values);", $debug, $profile);
}

function db_update($db, $table, $fields, $where = '', $debug = 0, $profile = 0)
{
    if ($where)
    {
        return db_query($db, "UPDATE $table SET $fields WHERE $where;", $debug, $profile);
    }

    return db_query($db, "UPDATE $table SET $fields;", $debug, $profile);
}

function db_delete($db, $table, $where = '', $debug = 0, $profile = 0)
{
    if ($where)
    {
        return db_query($db, "DELETE FROM $table WHERE $where;", $debug, $profile);
    }

    return db_query($db, "DELETE FROM $table;", $debug, $profile);
}

?>
