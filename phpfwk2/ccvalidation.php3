<?php  

/**********************/
/* validates CC#s
/* takes the number
/* and an optional type [visa|mastercard|amex|discover] for further checking
/**********************/

function  validateCC($ccnum,  $type  =  'unknown')
{
    //Clean  up  input
    $type  =  strtolower($type);
    $ccnum  =  ereg_replace( '[-[:space:]]',  '',$ccnum);

    //Do  type  specific  checks
    if  ($type  ==  'mastercard')
    {
        if  (strlen($ccnum)  !=  16  ||  !ereg( '^5[1-5]',  $ccnum))
        {
            return false;
        }
    }
    elseif  ($type  ==  'visa')
    {
        if  ((strlen($ccnum)  !=  13  &&
              strlen($ccnum)  !=  16)  ||
              substr($ccnum,  0,  1)  !=  '4')
        {
            return false;
        }
    }
    else if  ($type  ==  'amex')
    {
        if  (strlen($ccnum)  !=  15  ||  !ereg( '^3[47]',  $ccnum))
        {
            return false;
        }
    }
    elseif  ($type  ==  'discover')
    {
        if  (strlen($ccnum)  !=  16  ||
             substr($ccnum,  0,  4)  != '6011')
        {
            return false;
        }
    }
    else if ($type != 'unknown')
    {
        //invalid  type  entered
        return false;
    }


    //  Start  MOD  10  checks  
    $dig = (string)$ccnum;
    $numdig = sizeof($dig);
    $j = 0;
    for ($i = ($numdig - 2); $i >= 0; $i -= 2)
    {
        $dbl[$j] = $dig[$i] * 2;
        $j++;
    }

    $dblsz = sizeof($dbl);
    $validate = 0;
    for ($i = 0; $i < $dblsz; ++$i)
    {
        $add = (string)$dbl[$i];
        for ($j = 0; $j < sizeof($add); ++$j)
        {
            $validate  +=  $add[$j];
        }
        $add = '';
    }

    for ($i = ($numdig - 1); $i >= 0; $i -= 2)
    {
        $validate += $dig[$i];
    }

    return (substr($validate,  -1,  1)  ==  '0');
}

?>
