<?

$VALID = 1;

/*
 * validate_requiredFormField -> validation for a required form field
 *
 * $field -> the php var for the required form field
 * $desc -> a description of the filed
 */
function validate_requiredFormField($field, $desc)
{
    global $VALID;

    if (! $field)
    {
        print_msg('ERROR', 'Input Error', "The <b>$desc</b> field is required!");
        $VALID = 0;
        return 0;
    }

    return 1;
}

/*
 * validate_textlength-> validation for a text form field
 *
 * $field -> the php var for the required form field
 * $desc -> a description of the filed
 * $length -> the length of the string
 * $exact -> an exact match on the length (default true)
 */
function validate_textLength($field, $desc, $length, $exact=true)
{
    global $VALID;

    if ($field)
    {
        $len = strlen($field);

        if ($exact && ($len != $length))
        {
            print_msg('ERROR', 'Input Error',
                      "The <b>$desc</b>  field is invalid!  There must be $length characters, and there are currently $len characters.");

            $VALID = 0;
            return 0;
        }
        else if ($len > $length)
        {
            print_msg('ERROR', 'Input Error',
                      "The <b>$desc</b> field is invalid!  There must be no more than $length characters, and there are currently $len characters.");

            $VALID = 0;
            return 0;
        }
    }

    return 1;
}

/*
 * validate_numericFormat -> validation for a numeric form field
 *
 * $field -> the php var for the required form field
 * $desc -> a description of the field
 * $maxLength -> the max length of the digits
 * $precision -> the number of decimals (0 if no decimals)
 * $exact -> an exact match on the length (default true)
 */
function validate_numericFormat(&$field, $desc, $maxLength, $precision=0, $exact=true)
{
    global $VALID;

    if ($field)
    {
        $field = str_replace(",", "", $field);

        if (is_numeric($field) == false)
        {
            print_msg('ERROR', 'Input Error',
                      "The <b>$desc</b> field is not numeric!");
            $VALID = 0;
            return 0;
        }

        list($whole, $decimal) = split("\.", $field, 2);

        if ($exact)
        {
            if (strlen($whole) != ($maxLength - $precision) ||
                strlen($decimal) != $precision)
            {
                print_msg('ERROR', 'Input Error',
                          "The <b>$desc</b> field is invalid!  It must be numeric format of exactly " . ($maxLength - $precision) . " digits, and $precision decimals.");

                $VALID = 0;
                return 0;
            }
        }
        else
        {
            if (strlen($whole) > ($maxLength - $precision) ||
                strlen($decimal) > $precision)
            {
                print_msg('ERROR', 'Input Error',
                          "The <b>$desc</b> field is invalid!  It must be numeric format of up to " . ($maxLength - $precision) . " digits, and up to $precision decimals.");

                $VALID = 0;
                return 0;
            }
        }
    }
    else
    {
        $field = "0";
    }

    return 1;
}

/*
 * validate_numericFormat -> validation for a numeric form field
 *
 * $field -> the php var for the required form field
 * $desc -> a description of the field
 * $min -> the minimum value
 * $max -> the maximum value
 */
function validate_numericRange($field, $desc, $min, $max)
{
    global $VALID;

    if ($field)
    {
        if ($field < $min || $field > $max)
        {
            print_msg('ERROR', 'Input Error',
                      "The <b>$desc</b> field is outside the range limit.  The value must be within the range of $min to $max !");

            $VALID = 0;
            return 0;
        }
    }

    return 1;
}

?>
