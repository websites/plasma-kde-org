<?

class ShippingClerk
{
    var $cartID;
    var $storeID;

    function ShippingClerk($cartID, $storeID = -1)
    {
        $this->cartID = intval($cartID);
        $this->storeID = intval($storeID);
    }

    function shipping()
    {
        return 0;
    }

    function printShipping()
    {
        return 0;
    }

    function shippingHtml()
    {
        return 0;
    }
}

class ShippingClerk_weight extends ShippingClerk
{
    function ShippingClerk_weight($cartID, $storeID = -1)
    {
        ShippingClerk::ShippingClerk($cartID, $storeID);
    }
}

class ShippingClerk_price extends ShippingClerk
{
    function ShippingClerk_weight($cartID, $storeID = -1)
    {
        ShippingClerk::ShippingClerk($cartID, $storeID);
    }
}

global $shopping_customObjectFilePath;

if (file_exists("$shopping_customObjectFilePath/shippingClerks.php"))
{
    include_once("$shopping_customObjectFilePath/shippingClerks.php");
}

?>
