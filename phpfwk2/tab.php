<?
class Tab
{
    var $name;
    var $tabID;
    var $function;
    var $link;
    var $prefix;
    var $suffix;
    var $class;

    function Tab($name, $tabID, $function = '', $link, $class= 'tab')
    {
        $this->name = $name;
        $this->tabID = $tabID;
        $this->function = $function;
        $this->link = $link;
        $this->prefix = '';
        $this->suffix = '';
        $this->class = $class;
    }

    function printTab($activeID, $numTabs)
    {
        if ($numTabs > 0)
        {
            $width = 'width="' . number_format(100 / $numTabs, 0) . '%"';
        }

        print "<td class=\"{$this->class}Spacer\" width=\"5px\">&nbsp;</td>";

        $mode = $this->tabID;
        $link = $this->link;

        $glue = '?';
        if (strstr($link, '?'))
        {
            $glue = '&';
        }

        if ($activeID == $this->tabID)
        {
            print "<th nowrap valign=top $width class=\"{$this->class}On\"><div class=\"{$this->class}Title\">{$this->prefix}</div><a href=\"$link{$glue}mode=$mode\" class=\"{$this->class}LinkOn\">{$this->name}</a><div class=\"{$this->class}Title\">{$this->suffix}</div></th>";
        }
        else
        {
            print "<th nowrap valign=top $width class=\"{$this->class}Off\"><div class=\"{$this->class}Title\">{$this->prefix}</div><a href=\"$link{$glue}mode=$mode\" class=\"{$this->class}LinkOff\">{$this->name}</a><div class=\"{$this->class}Title\">{$this->suffix}</div></th>";
        }
    }

    function printBody($activeID)
    {
        if ($activeID == $this->tabID)
        {
            $function = $this->function;
            print ($function ? $function() : '');
            return true;
        }

        return false;
    }

    function setTitlePrefix($pre)
    {
        $this->prefix = $pre;
    }

    function setTitleSuffix($suf)
    {
        $this->suffix = $suf;
    }
}

/*
 *
 * printAllTabs -> prints out the tab structure
 *
 * $tabs -> assoc array of tab objects and their link
 * $mode -> what mode are we in
 */
function printAllTabs(&$tabs, $mode)
{
    if (!$mode)
    {
        $mode = 0;
    }

    $numTabs = count($tabs);

    print '<table width="100%" border=0 cellpadding=0 cellspacing=0 align="center">';
    print '<tr>';
    
    $activeClass = '';

    foreach ($tabs as $tab)
    {
        if (!is_null($tab))
        {
            $tab->printTab($mode, $numTabs);
        }

        if ($mode == $tab->tabID)
        {
            $activeClass = $tab->class;
        }
    }

    print "<td class=\"{$activeClass}Spacer\" width=\"5px\">&nbsp;</td>";
    print '</tr>';

    print '<tr><td colspan=100%>';
        print '<table width="100%" cellspacing="0" align="center">';
            print "<tr><td class=\"{$activeClass}Widget\">";
                foreach ($tabs as $tab)
                {
                    if ($tab->printBody($mode) == true)
                    {
                        break;
                    }
                }
            print '</td></tr>';
        print '</table>';
    print '</td></tr>';
    print '</table>';
}

?>
