<?
// hardcoded IDs of the two root level nodes
$cms_TopPageID = 1;
$cms_LeftPageID = 2;
$cms_RightPageID = 3;
$cms_BottomPageID = 4;
unset($cms_pageRenderer);

include_once("$phpfwkIncludePath/cmsLinks.php");

class cmsPagePriveleges
{
    var $canPublish = false;
    var $canAdd     = false;
    var $canDelete  = false;
    var $canEdit    = false;
    var $superUser  = false;
    var $adminGroup = 0;
    var $editGroup  = 0;
    var $pageID     = 0;

    function cmsPagePriveleges($pageID)
    {
        global $auth_userGID;
        global $cms_complexityLevel;

        $this->pageID = intval($pageID);
        if ($this->pageID > 0)
        {
            $topNode = cms_isTopNode($this->pageID);
            $db = db_connect();
            $page = db_query($db, "SELECT addable, editable, deletable, displayOrder, adminGroup, editGroup
                                  FROM cmsPagesMetadata WHERE pageID = $pageID;");
            if (db_numRows($page) < 1)
            {
                $this->pageID = 0;
                return;
            }

            list($addable, $editable, $deletable, $displayOrder,
                 $this->adminGroup, $this->editGroup) = db_row($page, 0);
            $this->adminGroup = intval($this->adminGroup);
            $this->editGroup = intval($this->editGroup);

            if ($cms_complexityLevel > CMS_BASIC)
            {
                $this->superUser = isSuper();
                $admin = $this->superUser || ($auth_userGID == $this->adminGroup);
                $editor = $this->superUser || ($auth_userGID == $this->editGroup);

                $this->canPublish = db_boolean($editable) && $admin;
                $this->canDelete = db_boolean($deletable) && $admin;
                $this->canAdd = $topNode || (db_boolean($addable) && ($admin || $editor));
                $this->canEdit = !$topNode && db_boolean($editable) && ($admin || $editor);
            }
            else
            {
                $this->superUser = true;

                $topNode = cms_isTopNode($this->pageID);
                $this->canPublish = !$topNode;
                $this->canDelete = !$topNode;
                $this->canAdd = true;
                $this->canEdit = !$topNode;
            }
        }
    }
}

class cms_renderer
{
    var $pageID = 0;
    var $isEditable = false;
    var $ownedBy = 0;

    function cms_renderer($pageID)
    {
        $this->pageID = $pageID;
    }

    function render($staticBodyText)
    {
        print $staticBodyText;
    }

    // convenience function to be called when printing forms in edit()
    function print_requiredEditHiddens()
    {
        print_hidden('currentPageID', $this->pageID);
        print_hidden('editType', 1);
    }

    function print_editHeader($typeName, $pageTitle)
    {
        print '<table cellpadding=3 cellspacing=0 border=0 width=100% class="cmsForm">';
        print "<tr><td class=\"largeColoredHeader\" nowrap>Edit $typeName Specifics for &quot;$pageTitle&quot;</td></tr>";
        print '<tr><td>';
    }

    function print_editFooter($typeName, $pageTitle)
    {
        print '</td></tr>';
        print '</table>';
    }

    // returns bool, true == done, false == not finished
    // remember to call print_requiredEditHiddens
    function edit($formAction, $typeName, $pageTitle)
    {
        print_msg('INFORMATION', 'Editting Not Supported',
                  'Editting of the basic cms page renderer is not supported');
        return true;
    }

    // return bool, true == printed a nav, false == doesn't provide this feaure
    // reimplement this method if you want a custom second-level navigation
    function print_sidebarNav()
    {
        return false;
    }

    function link($id)
    {
        global $cms_searchEngineFrendlyLinks;

        if ($cms_searchEngineFrendlyLinks)
        {
            return "/cms/$id";
        }

        return "/?pID=$id";
    }

    function imagePath($id)
    {
        global $dms_baseURL;
        $pathParts = explode('::', $id);
        $path = "/dms";

        foreach ($pathParts as $pathPart)
        {
            $tmp = $pathPart;
            $path .= "/$pathPart";
        }

        $tmp = intval($tmp);
        if ($tmp > 0)
        {
            $filename = db_query(db_connect(), "SELECT filename FROM dmsDocuments WHERE documentID = $tmp;");
            if (db_numRows($filename) > 0)
            {
                list($filename) = db_row($filename, 0);
                $path .= "";
            }
        }

        return $path;
    }
}

if (is_readable("$cms_customObjectFilePath/normal.php"))
{
    include_once("$cms_customObjectFilePath/normal.php");
}
else
{
    include_once("$cms_objectFilePath/normal.php");
}

function cms_getRenderer($pageID, $typeObject = '')
{
    global $cms_pageRenderer, $cms_customObjectFilePath, $cms_objectFilePath;

    static $renderer = 0;

    if ($renderer && $renderer->pageID = $pageID)
    {
        return $renderer;
    }

    if ($typeObject == '')
    {
        $result = db_query(db_connect(), "SELECT pg.body, pg.typeID, pg.typeObject, pg.ownedBy
                                          FROM cmsPagesPublished pg WHERE pg.pageID = $pageID;");

        if (db_numRows($result) < 1)
        {
            // if no results are returned, then we couldn't find the page? WIERD!
            cms_setCurrentPageID(0);
            $renderer = new cms_normal($pageID);
            return $renderer;
        }
    }

    // if there is a 'view' callback for this document type, execute it
    if ($typeObject && is_readable("$cms_customObjectFilePath/$typeObject.php"))
    {
        include_once("$cms_customObjectFilePath/$typeObject.php");

        $objName = "cms_$typeObject";
        $renderer = new $objName($pageID);
        $renderer->ownedBy = $editGroup;
    }
    else if ($typeObject && is_readable("$cms_objectFilePath/$typeObject.php"))
    {
        include_once("$cms_objectFilePath/$typeObject.php");

        $objName = "cms_$typeObject";
        $renderer = new $objName($pageID);
        $renderer->ownedBy = $editGroup;
    }
    else
    {
        $renderer = new cms_normal($pageID);
    }

    return $renderer;
}

function cms_init()
{
    global $cmsObject, $cmsPageID;
    global $common_pageTitle, $common_pageHeaderFile, $common_pageFooterFile;
    global $cms_homepageID, $common_mnemonic, $common_bizName, $common_siteName;
    $cmsPageID = cms_currentPageID();

    $results = db_query(db_connect(), "select pg.title, pg.typeObject, gr.header, gr.footer, gr.homepageID, gr.mnemonic, gr.fullname from cmsPagesPublished pg left join groups gr on (pg.editGroup = gr.groupID) where pg.pageID = $cmsPageID;");
    if (db_numRows($results) > 0)
    {
        list($title, $typeObject, $head, $foot, $home, $mnemonic, $bizName) = db_row($results, 0);

        if (!$common_pageTitle)
        {
            $common_pageTitle = trim($title);
        }

        if (trim($head))
        {
            $common_pageHeaderFile = $head;
        }

        if (trim($foot))
        {
            $common_pageFooterFile = $foot;
        }

        if ($home > 0)
        {
            $cms_homepageID = $home;
        }

        if (trim($mnemonic))
        {
            $common_mnemonic = $mnemonic;
        }

        if (trim($bizName))
        {
            $common_bizName = $bizName;
            $common_siteName = $bizName;
        }

        //inititalize this puppy while we have a typeObject around
        cms_getRenderer($cmsPageID, $typeObject);
        return true;
    }

    return false;
}

function cms_isTopNode($pageID)
{
    global $cms_TopPageID, $cms_LeftPageID, $cms_RightPageID, $cms_BottomPageID;
    return $pageID == $cms_TopPageID ||
           $pageID == $cms_LeftPageID ||
           $pageID == $cms_RightPageID ||
           $pageID == $cms_BottomPageID;
}

function cms_currentBaseURL($complete = false)
{
    global $cms_baseURL;

    if (!$complete)
    {
        return $cms_baseURL;
    }

    return $cms_baseURL . cms_renderer::link(cms_currentPageID());
}

function cms_currentPageID()
{
    global $pID;
    $pID = intval($pID);

    if ($pID < 1)
    {
        return cms_defaultHomepageID();
    }

    return intval($pID);
}

function cms_setCurrentPageID($pageID = 0)
{
    $pID = $pageID;
}

function cms_defaultHomepageID()
{
    global $cms_homepageID;
    return $cms_homepageID;
}

// Get the body copy from the database
function cms_printPage($pageID)
{
    global $newsID, $cmsSupressBody, $cms_objectFilePath, $cms_customObjectFilePath;
    global $cms_pageRenderer;

    $pageID = intval($pageID);
    $db = db_connect();
    $result = db_query($db, "SELECT pg.body, pg.typeID, pg.typeObject, pg.ownedBy
                             FROM cmsPagesPublished pg WHERE pg.pageID = $pageID;");

    if (db_numRows($result) < 1)
    {
        // if no results are returned, error
        print_msg('INFORMATION', "Empty Page",
                  "There was no content found for page $pageID", 'cms/1');
        cms_setCurrentPageID(0);
        return false;
    }

    list($body, $typeID, $typeObject, $editGroup) = db_row($result, 0);

    // print the body
    if (isset($cmsSupressBody))
    {
        unset($body);
    }
    else
    {
        cmsRelinkText($body);
    }

    $renderer = cms_getRenderer($pageID, $typeObject);

    if ($renderer)
    {
        $renderer->render($body);
        return true;
    }

    return false;
}

// Return a pageID from the passed in path
function cms_pageIDByPath($path)
{
    // make a connection to the pgsql server
    $db = db_connect();

    // parse out the individual node
    $node = split ('[/]', $path);

    if(count($node) < 1)
        return 0;

    // grab the root node if there is one
    $title = addslashes(trim($node[0]));
    $query = "SELECT pageID FROM cmsPagesPublished WHERE title = '$title' AND parentID IS NULL;";
    $parent = db_query($db, $query);

    if (db_numRows($parent) > 0)
    {
        list($pageID) = db_row($parent, 0);
    }
    else
    {
        return 0;
    }

    for($i = 1; $i < count($node); $i++)
    {
        $title = addslashes(trim($node[$i]));
        $query = "SELECT pageID, title FROM cmsPagesPublished
                  WHERE parentID = $pageID AND title = '$title';";
        $child = db_query($db, $query);
        if (db_numRows($parent) > 0)
        {
            list($pageID) = db_row($child, 0);
        }
        else
        {
            return 0;
        }
    }

    // return the leaf node's pageID
    return $pageID;
}

function breadCrumbLinker($pageID, $title)
{
    return cms_currentBaseURL() . cms_renderer::link($pageID);
}

// Return a path from the passed in pageID
function cms_pathByPageID($pageID, &$idAncestry, $linkCallback = 'breadCrumbLinker',
                          $noRootAncestor = false, $published = true)
{
    // make a connection to the pgsql server
    $db = db_connect();

    $table = ($published ? 'cmsPagesPublished' : 'cmsPagesMetadata');

    if (is_array($idAncestry))
    {
        array_unshift($idAncestry, $pageID);
    }

    while($pageID)
    {
        $query = "SELECT parentID, title FROM $table WHERE pageID = $pageID;";
        $parent = db_query($db, $query);
        if (db_numRows($parent) > 0)
        {
            list($parentPageID, $title) = db_row($parent, 0);
            if ($parentPageID > 0)
            {
                if (is_array($idAncestry))
                {
                    // prepend element to the beginning of array
                    array_unshift($idAncestry, $parentPageID ? $parentPageID : 'Root');
                }

                if ($linkCallback && function_exists($linkCallback))
                {
                    $link = $linkCallback($pageID, $title);
                    if ($link)
                    {
                        $title = "<a href=\"$link\">$title</a>";
                    }
                }

                if (isset($path))
                {
                    $path = "/ $title $path";
                }
                else
                {
                    $path = "/ $title";
                }
            }
            $pageID = $parentPageID;
        }
        else
        {
            $pageID = 0;
        }
    }

    /* pop off the top/left node */
    if ($noRootAncestor && is_array($idAncestry))
    {
        // shift an element off the beginning of array
        array_shift($idAncestry);
    }

    // return the path that's been built
    return($path);
}

function cms_basicNavTDPrinter($pageID, $title, $last = false, $hasChildren = false, $depth = 0)
{
    global $cms_currentPage;
    $url = cms_currentBaseURL();

    $title = str_replace(' ', '&nbsp;', $title);
    print "<tr><td><a class='nav' style=\"text-decoration: none;\" href=\"$url" .
          cms_renderer::link($pageID) . 
          "\">$title</a></td></tr>";
}

function cms_basicNavPrinter($pageID, $title, $last = false, $hasChildren = false, $depth = 0)
{
    global $cms_currentPage;
    $url = cms_currentBaseURL();

    $title = str_replace(' ', '&nbsp;', $title);
    print "<a class='nav' style=\"text-decoration: none; \" href=\"$url" .
          cms_renderer::link($pageID) . 
          "\">$title</a><br>";
}

function cms_printNav($linkCallback, $startPage = 0, $idAncestry = '', $depth = 0)
{
    $startPage = intval($startPage);

    if (!function_exists($linkCallback))
    {
        $linkCallback = 'cms_basicNavPrinter';
    }

    //print "cms_printNav(\$linkCallback = $linkCallback, \$startPage = $startPage, \$idAncestry = $idAncestry, \$depth = $depth)<br>";
    if ($startPage < 1)
    {
        return;
    }

    $db = db_connect();// make a connection to the pgsql server
    $query = "SELECT pageID, title, displayOrder FROM cmsPagesPublished
              WHERE parentID = $startPage AND showInNav ORDER BY displayOrder;";
    $leftNodes = db_query($db, $query);

    $numRows = db_numRows($leftNodes);
    for($i = 0; $i < $numRows; ++$i)
    {
        list($pageID, $title, $displayOrder) = db_row($leftNodes, $i);
        list($hasSubPages) = db_row(db_query($db, "SELECT count(pageID) > 0 FROM cmsPagesPublished
                                                 WHERE parentID = $pageID;"), 0);

        if (!$title)
        {
            $title = 'Unnamed';
        }

        if (is_array($idAncestry) && $pageID == $idAncestry[0])
        {
            $linkCallback($pageID, $title, true, db_boolean($subPages), $depth);
            $childPage = array_shift($idAncestry);

            cms_printNav($linkCallback, $childPage, $idAncestry, $depth + 1);
        }
        else
        {
            $linkCallback($pageID, $title, $i == $numRows - 1, db_boolean($hasSubPages), $depth);
        }
    }
}

function cms_printSidebarNav($sidebarNavCallback)
{
    $renderer = cms_getRenderer(cms_currentPageID());
    if ((!$renderer ||
         !$renderer->print_sidebarNav($sidebarNavCallback)) &&
         function_exists( $sidebarNavCallback))
    {
        $sidebarNavCallback();
    }
}

function cms_currentPageAncestry()
{
    static $ancestry = 0;

    if ($ancestry == 0)
    {
        $ancestry = array();
        cms_pathByPageID(cms_currentPageID(), $ancestry);
    }

    return $ancestry;
}

?>
