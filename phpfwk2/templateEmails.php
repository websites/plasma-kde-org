<?php

define ('SPOOL_WEEKEND', 0);
define ('SPOOL_NEXTSEND', 1);
define ('SPOOL_TODAY', 2);
define ('SPOOL_IMMEDIATELY', 4);

function parseTemplate($templateID, $values = array(), $literals = array())
{
    $templateID = intval($templateID);
    if ($templateID < 1)
    {
        return false;
    }

    $db = db_connect();
    $templateInfo = db_query($db,"select title, body from templates where templateID = $templateID;");
    if (db_numRows($templateInfo) < 1)
    {
        return false;
    }

    if (!is_array($values))
    {
        $values = array();
    }

    if (!is_array($literals))
    {
        $literals = array();
    }

    list($templateTitle, $templateBody) = db_row($templateInfo, 0);
    $templateTitle  = processTemplateText($templateTitle);
    $templateBody   = processTemplateText($templateBody);

    $templateSQL =  db_query($db, "select sql from templateSQL where templateid = $templateID;");;
    $numSQLStatements = db_numRows($templateSQL);
    for ($i = 0; $i < $numSQLStatements; ++$i)
    {
        list($sql) = db_row($templateSQL, $i);
        eval("\$queries[$i] = \"" . processTemplateText($sql, true) . "\";");
    }

    // reset the values array as we no long are going to be using the
    // values supplied to us, but rather the values that come out of the db
    // we assign $literals to it in case there are any hardcoded values
    $values = $literals;
    for ($i = 0; $i < $numSQLStatements; ++$i)
    {
        $result = db_query($db, $queries[$i]);
        if (db_numRows($result) == 1)
        {
            $values = array_merge($values, db_rowArray($result, 0));
        }
    }


/*    print "<hr>family values: <br>";
    foreach ($values as $key => $value)
    {
       print "<br>\$values[$key] => $value";
    }
    print "<hr>";
*/

    eval('$title = "' . $templateTitle . '";');
    eval('$body = "' . $templateBody . '";');

    return array('title' => $title, 'body' => $body);
}

function processTemplateText($data, $processParens = false)
{
    // Step 1
    // escape \$ and \{\} for array replacement and string replacement
    $data = str_replace('{',"\{",$data);
    $data = str_replace('}',"\}",$data);
    $data = str_replace('$',"\$",$data);

    // Step 2
    // [[ with "{$values['"
    // ]] with "']}"
    $data = str_replace('[[',"{\$values['",$data);
    $data = str_replace(']]',"']}",$data);

    if ($processParens)
    {
        // Step 3
        // (( with '
        // )) with '
        $data = str_replace('((', "'{\$values['", $data);
        $data = str_replace('))', "']}'", $data);
    }

    return $data ;
}

function templateByName($template)
{
    if (!$template)
    {
        return 0;
    }

    $templateID = db_query(db_connect(), 'select templateID, categoryID from templates where name = \'' . addslashes($template) . '\';');

    if (db_numRows($templateID) > 0)
    {
        return db_row($templateID, 0);
    }

    return 0;
}

function getSendTime($sendOn)
{
    if (strlen($sendOn) > 1)
    {
        return $sendOn;
    }

    if ($sendOn == SPOOL_NEXTSEND)
    {
        return date('r', strtotime("now"));
    }
    else if ($sendOn == SPOOL_TODAY)
    {
        return date('r', strtotime("3:00pm"));
    }
    else if ($sendOn == SPOOL_WEEKEND)
    {
        return date('r', strtotime("3:00pm next friday"));
    }

    return date('r', strtotime("now"));
}

function spoolEmail($userID, $subject, $body, $sendOn = 0, $categoryID = 0)
{
    global $contactEmail, $bizName, $memberTable;

    $userID = intval($userID);
    if ($userID < 1)
    {
        return false;
    }

    $db = db_connect();
    if ($sendOn == SPOOL_IMMEDIATELY)
    {
        $memberInfo = db_query($db, "select email from users where userID = $userID;");
        if (db_numRows($memberInfo) < 1)
        {
            return false;
        }

        list($memberEmail) = db_row($memberInfo, 0);
        if (!$memberEmail)
        {
            return false;
        }

        return mail($memberEmail, "[$bizName]: " . $title, $body, "From: $bizName <$contactEmail>");
    }

    unset($fields);
    unset($values);
    sql_addIntToInsert($fields, $values, 'userID', $userID);
    sql_addIntToInsert($fields, $values, 'categoryID', $categoryID);
    sql_addScalarToInsert($fields, $values, 'sendOn', getSendTime($sendOn));
    sql_addScalarToInsert($fields, $values, 'subject', $subject);
    sql_addScalarToInsert($fields, $values, 'body', $body);
    sql_addScalarToInsert($fields, $values, 'fromAddress', "$bizName <$contactEmail>");
    $result = db_query($db, "insert into mailSpool ($fields)  values ($values);");

    return true;
}

function sendEmail($userID, $template, $sendOn = 0, $values = array(), $literals = array())
{
    list($templateID, $categoryID) = templateByName($template);

    if (!$templateID)
    {
        return false;
    }

    $message = parseTemplate($templateID, $values, $literals);

    if (!$message)
    {
        return false;
    }

    return spoolEmail($userID, $message['title'], $message['body'], $sendOn, $categoryID);
}

?>
