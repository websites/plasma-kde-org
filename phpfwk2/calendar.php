<?php
// prints out a calendar with events
// construct with a month and a year then hand in your events one by one
// to the addEvent method, supplying a description and a start and end timestamp
// the cssStyle is optional, and if neglected defaults to calendarEventDay
// you can either define your own CSS or simply call print_defaultStyle.

// CSS styles used:
//        calendarEventDay => default style for days with events
//        calendarDay => style used for days w/out events
//        calendarUnDay => style used for the blank squares before 1st and after the end of the month
//        calendarHeader => used for the headers
//        calendarDayHeader => used for the day headers
//        calendarTable => used for the table itself

// sample usage:

/*
$calendar = new phpfwk_calendar($month, $year);
$calendar->print_defaultStyle();

$year = $calendar->year;
$firstOfMonth = "{$calendar->month}-1-$year";
$endOfMonth = "{$calendar->month}-{$calendar->endDay}-$year";

$db = db_connect();
$query = "select description, extract(epoch from startDay::timestamp),
          extract(epoch from endDay::timestamp)
          from specials where locationID = $locationID and
          startDay >= '$firstOfMonth'::date and
          startDay <=  '$endOfMonth'::date
          order by startDay;";
$eventQuery = db_query($db, $query);
$numEvents = db_numRows($eventQuery);
$events = array();
for ($i = 0; $i < $numEvents; ++$i)
{
    list($startDay, $endDay, $title) = db_row($eventQuery, $i);
    $calendar->addEvent($title, $startDay, $endDay);
//    $events[$day][1] = ($i % 2) ? 'calendarEventDay' : 'calendarEventDay2';
}

$calendar->print_calendar($events);
*/

class phpfwk_calendarEvent
{
    var $description;
    var $location;

    function phpfwk_calendarEvent($description, $location = '')
    {
        $this->description = $description;
        $this->location = $location;
    }
}

class phpfwk_calendar
{
    var $events = array();
    var $month = 0;
    var $year = 0;
    var $startDay;
    var $endDay;
    var $prevMonth;
    var $prevMonthEnd;
    var $prevYear;
    var $nextMonth;
    var $nextYear;
    var $textMonth;
    var $title = '';
    var $dayAction;

    function phpfwk_calendar($title = '', $month = 0, $year = 0, $dayAction = '')
    {
        $this->month = intval($month);
        $this->year = intval($year);

        if ($this->year == 0)
        {
            $this->year = date('Y');
        }

        if ($this->month < 1 || $this->month > 12)
        {
            $this->month = date('m');
        }

        $date = mktime (0, 0, 0, $this->month, 1, $this->year);

        $this->textMonth = date('F', $date);
        $this->startDay = date('w', $date);
        $this->endDay = date('t', $date);

        $date = mktime (0, 0, 0, $this->month - 1, 1, $this->year);
        $this->prevMonth = date('m', $date);
        $this->prevMonthEnd = date('t', $date);
        $this->prevYear = date('Y', $date);

        $date = mktime (0, 0, 0, $this->month + 1, 1, $this->year);
        $this->nextMonth = date('m', $date);
        $this->nextYear = date('Y', $date);

        if ($dayAction)
        {
            $this->dayAction = $dayAction;
        }
        else
        {
            $dayAction = NULL;
        }

        $this->title = $title;
    }

    function setTitle($title)
    {
        if ($title)
        {
            $this->title = $title . ': ';
        }
    }

    function print_defaultStyle()
    {
    ?>
        <style type="text/css">
        <!--
        .calendarEventDay { background-color: #99FFCC; }
        .calendarToday { background-color: #FFCCCC; }
        .calendarDay { background-color: white; }
        .calendarUnDay { background-color: #CCCCCC; }
        .calendarHeader { background-color: #9999CC; }
        .calendarDayHeader { background-color: #9999CC; }
        .calendarTable { background-color: darkgrey; }
        -->
        </style>
    <?php
    }

    function print_defaultListStyle()
    {
    ?>
        <style type="text/css">
        <!--
        /*
        Calendar stylesheet
        */

        .calendarEventDay { background-color: lightgrey; }
        .calendarDay { background-color: white; }
        .calendarUnDay { background-color: white; }
        .calendarHeader { background-color: white; }
        .calendarDayHeader { background-color: white; }
        .calendarTable { background-color: white; }

        .calendarmonth { font-weight: bold; background-color: #000000; color: #FFFFFF; }
        .calendarothermonth { font-weight: bold; background-color: #6699CC; color: #FFFFFF; }
        .calendardate { background-color: #666666; color: #FFFFFF; }
        .calendarday { background-color: #336699; color: #FFFFFF; }
        .calendaritem1a { background-color: #DDDDDD; }
        .calendaritem1b { background-color: #EEEEEE; }
        .calendaritem2a { background-color: #CCCCCC; }
        .calendaritem2b { background-color: #E6E6E6; }

        #calendartable, #calendartable TD, #calendartable P { font-size: 11px; }
        #calendarheadrow, #calendarheadrow TD A:link, #calendarheadrow TD A:active, #calendarheadrow TD A:visited { font-weight: bold; color: #FFFFFF;  text-decoration: none; }
        #calendarheadrow, #calendarheadrow TD A:link, #calendarheadrow TD A:active, #calendarheadrow TD A:visited { font-weight: bold; color: #FFFFFF;  text-decoration: none; }
        #calendarheadrow TD A:hover, #calendarheadrow TD A:hover { font-weight: bold; color: #FFFFFF; text-decoration: underline; }

        -->
        </style>
    <?php
    }

    function print_calendar($uri)
    {
        global $common_baseImagesURL;
        $spacerImg = "<img src=\"$common_baseImagesURL/trans.gif\" height=50>";

        // build the full URI
        if (strstr($uri, '?'))
        {
            $uri .= '&';
        }
        else
        {
            $uri .= '?';
        }

        if ($this->dayAction)
        {
            if (strstr($this->dayAction, '?'))
            {
                $$this->dayAction .= '&';
            }
            else
            {
                $this->dayAction .= '?';
            }
        }

        print_js_popUp('cal');
        ?>

        <table border="1" cellspacing="0" cellpadding="2" bordercolor="lightgrey" width="100%">

        <tr>
            <th class="calendarHeader"><a href="<?php print "{$uri}month={$this->prevMonth}&year={$this->prevYear}"; ?>">&lt;&lt;</a></th>
            <th colspan="5" class="calendarHeader"><?php print "{$this->title} - {$this->textMonth} {$this->year}"; ?></th>
            <th class="calendarHeader"><a href="<?php print "{$uri}month={$this->nextMonth}&year={$this->nextYear}"; ?>">&gt;&gt;</a></th>
        </tr>

        <tr>
            <th width="14%" class="calendarDayHeader">Sun</th>
            <th width="14%" class="calendarDayHeader">Mon</th>
            <th width="14%" class="calendarDayHeader">Tue</th>
            <th width="14%" class="calendarDayHeader">Wed</th>
            <th width="14%" class="calendarDayHeader">Thu</th>
            <th width="14%" class="calendarDayHeader">Fri</th>
            <th width="14%" class="calendarDayHeader">Sat</th>
        </tr>

        <tr>

        <?
        for ($i = 0; $i < $this->startDay; ++$i)
        {
            print '<td class="calendarUnDay" width="14%">&nbsp;</td>';
        }

        for ($j = 1; $j <= $this->endDay; ++$i, ++$j)
        {
            if ($i % 7 == 0)
            {
                print "</tr><tr>\n";
            }

            $today = $this->events[$j];
            $numEvents = is_array($today) ? count($today) : 0;

            if ($this->dayAction)
            {
                $onclick = "onClick=cal_popUp(\"{$this->dayAction}month={$this->month}&year={$this->year}&day=$j\")";
            }
            else
            {
                $onclick = '';
            }

            if ($numEvents > 0)
            {
                $class = ($j == date('d') && $this->month == date('m')) ? 'calendarToday' : 'calendarEventDay';
                print "<td $onclick class=\"$class\" valign=\"top\" width=\"14%\" height=\"50px\">$j<br>";

                foreach ($today as $event)
                {
                    print $event->description . "<br>";
                }
            }
            else
            {
                $class = ($j == date('d') && $this->month == date('m')) ? 'calendarToday' : 'calendarDay';
                print "<td $onclick class=\"$class\" valign=\"top\" width=\"14%\" height=\"50px\">$j<br>$spacerImg";
            }

            print "</td>\n";
        }

        for (; $i % 7 != 0; ++$i)
        {
            print "<td class=\"calendarUnDay\" width=\"14%\" height=\"50px\">&nbsp;</td>\n";
        }

        print '</tr></table>';
    }

    function print_list($uri)
    {
        global $baseImagesURL;
        // build the full URI
        $spacerImg = "<img src=\"$baseImagesURL/trans.gif\" height=50>";
        if (strstr($uri, '?'))
        {
            $uri .= '&';
        }
        else
        {
            $uri .= '?';
        }

        // print the month heading row
        ?>
        <table width="554" border="0" cellspacing="2" cellpadding="2" id="calendartable">
        <tr id="calendarheadrow">
            <td colspan="2" valign="middle" class="calendarothermonth"><a href="<?php print "{$uri}month={$this->prevMonth}&year={$this->prevYear}"; ?>">&lt;&lt;</a></td>
            <td align="center" valign="middle" class="calendarmonth"><?php print "{$this->title}{$this->textMonth} {$this->year}"; ?></td>
            <td align="right" valign="middle" class="calendarothermonth"><a href="<?php print "{$uri}month={$this->nextMonth}&year={$this->nextYear}"; ?>">&gt;&gt;</a></td>
        </tr>

        <?php
        for ($j = 1; $j <= $this->endDay; ++$i, ++$j)
        {
            $today = $this->events[$j];
            $numEvents = is_array($today) ? count($today) : 0;

            if ($numEvents > 0)
            {
                // their are events on this day so print the day heading row
                $weekday = date ('l', strtotime ($j." ".$this->textMonth." ".$this->year));
                $eventStyle = 0;
?>
                <tr>
                    <td width="20" class="calendardate"><?php print $j ?></td>
                    <td width="130" class="calendarday"><?php print $weekday ?></td>
                    <td width="254" style="background-color:#7F98B2">&nbsp;</td>
                    <td width="150" style="background-color:#BFCCD9">&nbsp;</td>
                </tr>
<?php
                $odd = false;
                foreach ($today as $event)
                {
                    // print each event
                    $descStyle = $odd ? 'calendaritem1a' : 'calendaritem1b';
                    $eventStyle = $odd ? 'calendaritem2a' : 'calendaritem2b';
?>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2" class="<?php print $descStyle ?>" valign="top">
                        <?php print $event->description ?></td>
                        <td class="<?php $eventStyle ?>" valign="top"><?php print $event->location ?></td>
                    </tr>

<?php
                    $odd = !$odd;
               }
            }
        }
    }

    function addEvent($description, $startTS, $endTS)
    {
        if ($startTS < 0)
        {
            $startTS = 0;
        }
        $startDay = date('j', $startTS);
        $startMonth = date('n', $startTS);

        if ($endTS < $startTS)
        {
            $endTS = $startTS;
            $endDay = $startDay;
            $endMonth = $startMonth;
        }
        else
        {
            $endDay = date('j', $endTS);
            $endMonth = date('n', $endTS);
        }

        if ($startMonth != $this->month)
        {
            $startDay = 1;
        }

        if ($endMonth != $this->month)
        {
            $endDay = $this->endDay;
        }

        ++$endDay;
        for ($j = $startDay; $j < $endDay; ++$j)
        {
            if (!is_array($this->events[$j]))
            {
                $this->events[$j] = array();
            }
            $event =& new phpfwk_calendarEvent($description);
            array_push($this->events[$j], $event);
        }
    }
}
?>
