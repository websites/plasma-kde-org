<?php

function validatePostalCode(&$postal)
{
    $postal = strtoupper($postal);
    if ($postal)
    {
        if (eregi('([A-Z][0-9][A-Z]) {0,1}([0-9][A-Z][0-9])',$postal,$matches))
        {
            $postal = $matches[1] . ' ' . $matches[2];
        }
        else
        {
            print_msg("ERROR", "Input Error",
                      "The postal code is not valid as entered. <br> Please enter it correctly in the appropriate field below using the format A9A 9A9.");
            return false;
        }
    }
    return true;
}

/****************************************/
/* validates address information        */
/****************************************/
function validateAddress(&$addr_street, &$addr_street2, &$addr_city, &$addr_prov, &$addr_country, &$addr_postal)
{
    global $countryArray;

    $postal = strtoupper($postal);
    if ($country == "CA" || $country == 'Canada')
    {
        if (ereg('([A-Z][0-9][A-Z]) {0,1}([0-9][A-Z][0-9])',$postal,$matches))
        {
            $postal = $matches[1] . ' ' . $matches[2];
        }
        else
        {
            if ($printErrors)
            {
                print_msg('ERROR', 'Postal Code Format Error',
                          "The postal code is not valid as entered. <BR> Please enter it correctly in the appropriate field below.");
            }
            return false;
        }

        if (! $prov || strlen($prov) < 3)
        {
            if ($printErrors)
            {
                print_msg('ERROR', 'Address Is Incomplete',
                          "Please select a <B>province</B> from the appropriate pull down menu below.");
            }
            return false;
        }
    }
    elseif ($country == 'US' || $country == 'United States')
    {
        $postal = ereg_replace('[^0-9]','',$postal);
        if (strlen($postal) != 5 && strlen($postal) != 9)
        {
            if ($printErrors)
            {
                print_msg('ERROR', 'ZIP Code Format Error', 
                          "The ZIP code is not valid as entered. <BR> Please enter it correctly (5 digits only, optionally with the 4 digit local code, please) in the appropriate field below.");
            }
            return false;
        }
        if (! $prov || strlen($prov) != 2)
        {
            if ($printErrors)
            {
                print_msg('ERROR', 'Address Is Incomplete',
                          "Please select a <B>state</B> from the appropriate pull down menu below.");
            }
            return false;
        }
    }
    else if (!$countryArray[$country])
    {
        if ($printErrors)
        {
            print_msg('ERROR', 'Address IS Incomplete',
                      "A valid country must be specified in your address. <BR> Please enter either 'Canada' or 'United States' in the appropriate field below.");
        }
        return false;
    }

    if (! $addr_street)
    {
        if ($addr_street2)
        {
            $addr_street = $addr_street2;
            $addr_street2 = '';
        }
        else
        {
            if ($printErrors)
            {
                print_msg('ERROR', 'Address Is Incomplete',
                          "A street address is required. <BR> Please enter it in the appropriate field below.");
            }
            return false;
        }
    }

    if (! $city)
    {
        if ($printErrors)
        {
            print_msg('ERROR', 'Address Is Incomplete',
                      "The city is missing from your address. <BR> Please enter it in the appropriate field below.");
        }
        return false;
    }
    else
    {
        $words = explode(' ',$city);
        for ($i = 0; $i < sizeof($words); $i++)
        {
            ereg('^([a-zA-Z]{1})(.*)',$words[$i], $matches);
            $temp_city .= strtoupper($matches[1]) . strtolower($matches[2]) . ' ';
        }
        $addr_city = chop($temp_city);
    }

    return true;
}

/****************************************/
/* validates email information          */
/****************************************/
function validateEmail($email, $checkDNS = false)
{
    $email = trim($email);
    if (preg_match('/^[\w_\-][\w_\-\.]*@(?:[\w_\-]{2,}\.)+[a-zA-Z]{2,3}$/', $email, $pieces) &&
        (!$checkDNS || !checkdnsrr($pieces[1])))
    {
        return true;
    }

    return false;
}


/*
 *
 * validate_phone
 * takes a phone number, removes garbage from it (leaving only numbers)
 * and returns a properly formatted phone number
 * if $area_code is > 0, it will expect, test for and return an area code,
 * otherwise, if 0 it won't
 * $seperator is what will be used to break up the 3/4 part of the #
 * $area_delim is what will be used to enclose the area code
 *
 * $number -> phone number
 * $phoneSep -> the seperator character to use (e.g. '.' or '-')
 * $reqAreaCode -> boolean require area code
 * $areaDelimLeft -> character to use on the left of the area code
 * $areaDelimRight -> character to use on the right of the area code
 * $areaSep -> the character(s) to use to seperate the area code from the rest of the number
 * $extensions -> boolean param controlling whether or not to allow extensions in the form: ext\s*[0-9]+
 */

function validatePhone(&$number, $reqAreaCode = true, $extensions = true, $phoneSep = ' ',
                        $areaDelimLeft = '(', $areaDelimRight = ')', $areaSep = ' ')
{
    $num = trim($number);
    $rc = false;

    if ($extensions && eregi('(ext[[:space:]]*([0-9]*))$', $num, $ext))
    {
        $extension = $ext[2];
        $num = str_replace($ext[1], '', $num);
    }

    $num = ereg_replace('[^0-9]', '', $num);

    if (ereg('^[1]?([0-9]*)$', $num, $matches)) 
    {
        $len = strlen($matches[1]);
    }

    if ($reqAreaCode)
    {
        if ($len == 10)
        {
            ereg('([0-9]{3})([0-9]{3})([0-9]{4})',$num,$matches);
            $num = $areaDelimLeft . $matches[1] . $areaDelimRight . $areaSep . $matches[2] . $phoneSep . $matches[3];
            $rc = true;
        }
    }
    else if ($len == 10)
    {
        ereg('([0-9]{3})([0-9]{3})([0-9]{4})',$num,$matches);
        $num = $areaDelimLeft . $matches[1] . $areaDelimRight . $areaSep . $matches[2] . $phoneSep . $matches[3];
        $rc = true;
    }
    else if ($len == 7)
    {
        ereg('([0-9]{3})([0-9]{4})$',$num,$matches);
        $num = $matches[1] . $phoneSep . $matches[2];
        $rc = true;
    }

    if ($rc)
    {
        if ($extension)
        {
            $num .= " ext $extension";
        }
        
        $number = $num;    
     }
 
    return $rc;
}

global $countryArray;
$countryArray = array(
                'AF' => 'Afghanistan',
                'AL' => 'Albania',
                'DZ' => 'Algeria',
                'AS' => 'American Samoa',
                'AD' => 'Andorra',
                'AO' => 'Angola',
                'AI' => 'Anguilla',
                'AQ' => 'Antarctica',
                'AG' => 'Antigua And Barbuda',
                'AR' => 'Argentina',
                'AM' => 'Armenia',
                'AW' => 'Aruba',
                'AU' => 'Australia',
                'AT' => 'Austria',
                'AZ' => 'Azerbaijan',
                'BS' => 'Bahamas',
                'BH' => 'Bahrain',
                'BD' => 'Bangladesh',
                'BB' => 'Barbados',
                'BY' => 'Byelorussian SSR',
                'BE' => 'Belgium',
                'BZ' => 'Belize',
                'BJ' => 'Benin',
                'BM' => 'Bermuda',
                'BT' => 'Bhutan',
                'BO' => 'Bolivia',
                'BA' => 'Bosnia Hercegovina',
                'BW' => 'Botswana',
                'BV' => 'Bouvet Island',
                'BR' => 'Brazil',
                'IO' => 'British Indian Ocean Territory',
                'BN' => 'Brunei Darussalam',
                'BG' => 'Bulgaria',
                'BF' => 'Burkina Faso',
                'BI' => 'Burundi',
                'KH' => 'Cambodia',
                'CM' => 'Cameroon', 
                'CA' => 'Canada', 
                'CV' => 'Cape Verde',
                'KY' => 'Cayman Islands',
                'CF' => 'Central African Republic',
                'TD' => 'Chad',
                'CL' => 'Chile',
                'CN' => 'China',
                'CX' => 'Christmas Island',
                'CC' => 'Cocos (Keeling) Islands',
                'CO' => 'Colombia',
                'KM' => 'Comoros',
                'CG' => 'Congo',
                'CD' => 'Congo, The Democratic Republic Of',
                'CK' => 'Cook Islands',
                'CR' => 'Costa Rica',
                'CI' => 'Cote D\'Ivoire',
                'HR' => 'Croatia',
                'CU' => 'Cuba',
                'CY' => 'Cyprus',
                'CZ' => 'Czech Republic',
                'CS' => 'Czechoslovakia',
                'DK' => 'Denmark',
                'DJ' => 'Djibouti',
                'DM' => 'Dominica',
                'DO' => 'Dominican Republic',
                'TP' => 'East Timor',
                'EC' => 'Ecuador',
                'EG' => 'Egypt',
                'SV' => 'El Salvador',
                'GB' => 'Great Britain',
                'GQ' => 'Equatorial Guinea',
                'ER' => 'Eritrea',
                'EE' => 'Estonia',
                'ET' => 'Ethiopia',
                'FK' => 'Falkland Islands (Malvinas)',
                'FO' => 'Faroe Islands',
                'FJ' => 'Fiji',
                'FI' => 'Finland',
                'FR' => 'France',
                'FX' => 'France, Metropolitan',
                'GF' => 'French Guiana',
                'PF' => 'French Polynesia',
                'TF' => 'French Southern Territories',
                'GA' => 'Gabon',
                'GM' => 'Gambia',
                'GE' => 'Georgia',
                'DE' => 'Germany',
                'GH' => 'Ghana',
                'GI' => 'Gibraltar',
                'GR' => 'Greece',
                'GL' => 'Greenland',
                'GD' => 'Grenada',
                'GP' => 'Guadeloupe',
                'GU' => 'Guam',
                'GT' => 'Guatemela',
                'GG' => 'Guernsey',
                'GN' => 'Guinea',
                'GW' => 'Guinea-Bissau',
                'GY' => 'Guyana',
                'HT' => 'Haiti',
                'HM' => 'Heard and McDonald Islands',
                'HN' => 'Honduras',
                'HK' => 'Hong Kong',
                'HU' => 'Hungary',
                'IS' => 'Iceland',
                'IN' => 'India',
                'ID' => 'Indonesia',
                'IR' => 'Iran (Islamic Republic Of)',
                'IQ' => 'Iraq',
                'IE' => 'Ireland',
                'IM' => 'Isle Of Man',
                'IL' => 'Israel',
                'IT' => 'Italy',
                'JM' => 'Jamaica',
                'JP' => 'Japan',
                'JE' => 'Jersey',
                'JO' => 'Jordan',
                'KZ' => 'Kazakhstan',
                'KE' => 'Kenya',
                'KI' => 'Kiribati',
                'KP' => 'Korea, Democratic People\'s Republic Of',
                'KR' => 'Korea, Republic Of',
                'KW' => 'Kuwait',
                'KG' => 'Kyrgyzstan',
                'LA' => 'Lao People\'s Democratic Republic',
                'LV' => 'Latvia',
                'LB' => 'Lebanon',
                'LS' => 'Lesotho',
                'LR' => 'Liberia',
                'LY' => 'Libyan Arab Jamahiriya',
                'LI' => 'Liechtenstein',
                'LT' => 'Lithuania',
                'LU' => 'Luxembourg',
                'MO' => 'Macau',
                'MK' => 'Macedonia',
                'MG' => 'Madagascar',
                'MW' => 'Malawi',
                'MY' => 'Malaysia',
                'MV' => 'Maldives',
                'ML' => 'Mali',
                'MT' => 'Malta',
                'MH' => 'Marshall Islands',
                'MQ' => 'Martinique',
                'MR' => 'Mauritania',
                'MU' => 'Mauritius',
                'YT' => 'Mayotte',
                'MX' => 'Mexico',
                'FM' => 'Micronesia, Federated States Of',
                'MD' => 'Moldova, Republic Of',
                'MC' => 'Monaco',
                'MN' => 'Mongolia',
                'MS' => 'Montserrat',
                'MA' => 'Morocco',
                'MZ' => 'Mozambique',
                'MM' => 'Myanmar',
                'NA' => 'Namibia',
                'NR' => 'Nauru',
                'NP' => 'Nepal',
                'NL' => 'Netherlands',
                'AN' => 'Netherlands Antilles',
                'NT' => 'Neutral Zone',
                'NC' => 'New Caledonia',
                'NZ' => 'New Zealand',
                'NI' => 'Nicaragua',
                'NE' => 'Niger',
                'NG' => 'Nigeria',
                'NU' => 'Niue',
                'NF' => 'Norfolk Island',
                'MP' => 'Northern Mariana Islands',
                'NO' => 'Norway',
                'OM' => 'Oman',
                'PK' => 'Pakistan',
                'PW' => 'Palau',
                'PS' => 'Palestine',
                'PA' => 'Panama',
                'PG' => 'Papua New Guinea',
                'PY' => 'Paraguay',
                'PE' => 'Peru',
                'PH' => 'Philippines',
                'PN' => 'Pitcairn',
                'PL' => 'Poland',
                'PT' => 'Portugal',
                'PR' => 'Puerto Rico',
                'QA' => 'Qatar',
                'RE' => 'Reunion',
                'RO' => 'Romania',
                'RU' => 'Russian Federation',
                'RW' => 'Rwanda',
                'SH' => 'Saint Helena',
                'KN' => 'Saint Kitts And Nevis',
                'LC' => 'Saint Lucia',
                'PM' => 'Saint Pierre and Miquelon',
                'VC' => 'Saint Vincent and The Grenadines',
                'WS' => 'Samoa',
                'SM' => 'San Marino',
                'ST' => 'Sao Tome and Principe',
                'SA' => 'Saudi Arabia',
                'SN' => 'Senegal',
                'SC' => 'Seychelles',
                'SL' => 'Sierra Leone',
                'SG' => 'Singapore',
                'SK' => 'Slovakia',
                'SI' => 'Slovenia',
                'SB' => 'Solomon Islands',
                'SO' => 'Somalia',
                'ZA' => 'South Africa',
                'GS' => 'South Georgia and The Sandwich Islands',
                'ES' => 'Spain',
                'LK' => 'Sri Lanka',
                'SD' => 'Sudan',
                'SR' => 'Suriname',
                'SJ' => 'Svalbard and Jan Mayen Islands',
                'SZ' => 'Swaziland',
                'SE' => 'Sweden',
                'CH' => 'Switzerland',
                'SY' => 'Syrian Arab Republic',
                'TW' => 'Taiwan',
                'TJ' => 'Tajikista',
                'TZ' => 'Tanzania, United Republic Of',
                'TH' => 'Thailand',
                'TG' => 'Togo',
                'TK' => 'Tokelau',
                'TO' => 'Tonga',
                'TT' => 'Trinidad and Tobago',
                'TN' => 'Tunisia',
                'TR' => 'Turkey',
                'TM' => 'Turkmenistan',
                'TC' => 'Turks and Caicos Islands',
                'TV' => 'Tuvalu',
                'UG' => 'Uganda',
                'UA' => 'Ukraine',
                'AE' => 'United Arab Emirates',
                'UK' => 'United Kingdom',
                'US' => 'United States',
                'UM' => 'United States Minor Outlying Islands',
                'UY' => 'Uruguay',
                'SU' => 'USSR',
                'UZ' => 'Uzbekistan',
                'VU' => 'Vanuatu',
                'VA' => 'Vatican City State',
                'VE' => 'Venezuela',
                'VN' => 'Vietnam',
                'VG' => 'Virgin Islands (British)',
                'VI' => 'Virgin Islands (U.S.)',
                'WF' => 'Wallis and Futuna Islands',
                'WG' => 'West Bank and Gaza',
                'EH' => 'Western Sahara',
                'YE' => 'Yemen, Republic of',
                'YU' => 'Yugoslavia',
                'ZR' => 'Zaire',
                'ZM' => 'Zambia',
                'ZW' => 'Zimbabwe');
?>
