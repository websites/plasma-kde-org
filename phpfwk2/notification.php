<?

include_once('Mail.php');

class Notification
{
    var $tableName;
    var $from;
    var $id;
    var $event;
    var $groupID;
    var $recipients;

    function Notification($table, $from)
    {
        $this->tableName = addslashes($table);
        $this->from = $from;
        $this->id = 0;
        $this->event = 0;
        $this->groupID = 0;
        $this->recipients = '';
    }

    function notify($id, $event)
    {
        global $phpfwk_sendmailPath, $phpfwk_sendmailArgs;

        if ($this->id != $id || $this->event != $event)
        {
            $this->createNotification($id, $event);
        }

        if ($this->recipients)
        {
            $headers['From']            =   $this->from;
            $headers['To']              =   $this->recipients;
            $headers['Reply-To']        =   $this->from;
            $headers['Subject']         =   $this->subject();
            $headers['Content-type']    =   "text/html; charset=iso-8859-1";

            $emailMessage =
            "
            <html>
            <body>
            <p> ========== THIS IS AN AUTOMATED MESSAGE DO NOT REPLY ========== </p>
            ";

            $emailMessage .= $this->message();

            $emailMessage .=
            "
            <p> ========== END OF MESSAGE ========== </p>
            </body>
            </html>";

            $sendmail_params['sendmail_path'] = $phpfwk_sendmailPath;
            $sendmail_params['sendmail_args'] = $phpfwk_sendmailArgs;

            $mail_object =& Mail::factory('sendmail', $sendmail_params);
            $mail_object->send($this->recipients, $headers, $emailMessage);
            //print_msg('INFO', "Mail Sent To : {$this->recipients}", $emailMessage);
        }
    }

    function subject()
    {
        return '';
    }

    function message()
    {
        return '';
    }

    function createNotification($id, $event)
    {
        $this->id = intval($id);
        $this->event = intval($event);

        $db = db_connect();

        $event = db_query($db, "SELECT groupID FROM {$this->tableName}
                                WHERE id = {$this->id} AND event = {$this->event};");

        if (db_numRows($event) > 0)
        {
            list($this->groupID) = db_row($event, 0);

            $emails = db_query($db, "SELECT u.email FROM users u
                                     JOIN usergroups ug ON (ug.userid = u.userid)
                                     JOIN groups g ON (g.groupid = ug.groupid AND
                                                       g.groupID = {$this->groupID})
                                     WHERE u.email IS NOT NULL;");
            
            $numEmails = db_numRows($emails);
            $allEmails = array();

            for ($i = 0; $i < $numEmails; $i++)
            {
                list($userEmail) = db_row($emails, $i);

                if ($userEmail)
                {
                    array_push($allEmails, $userEmail);
                }
            }

            $this->recipients = implode(',', $allEmails);
        }
        else
        {
            unset($this->recipients);
        }
    }
}

?>
