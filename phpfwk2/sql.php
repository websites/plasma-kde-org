<?php
/*
 * sql_addToWhereClause -> adds the next conditional to the where clause
 *
 * $clauses : the existing string
 * $boolGlue : the boolean condition (where, and, or, etc)
 * $field : the filed from the DB the condition is on
 * $condition :  the conditional operator (>, ~*, #<, =, etc)
 * $value : the value of the condition
 * $quotes : flag for whether single quotes are to be used for the value
 */

function sql_addToWhereClause(&$clauses, $boolGlue, $field, $condition, $value,
                              $quotes = true, $esc = true)
{
    $clauses .= " $boolGlue $field $condition ";

    if ($quotes)
    {
        if ($esc)
        {
            $clauses .= "'" . addslashes($value) . "'";
        }
        else
        {
            $clauses .= "'$value'";
        }
    }
    else if ($esc)
    {
        $clauses .= addslashes($value);
    }
    else
    {
        $clauses .= $value;
    }
}

/*
 * sql_addAlphaToWhereClause
 * $clauses : the existing string
 */
function sql_addAlphaToWhereClause(&$clauses, $column)
{
    global $alpha;

    if ($alpha != 'All')
    {
        sql_addToWhereClause($clauses, $clauses ? 'AND' : 'WHERE', $column, '~*', "^[" . addslashes($alpha) . "]");
    }
}

function sql_addSortColToOrderBy(&$orderBy, &$orderColIDs)
{
    global $oBy, $oHow;

    if (!is_array($orderColIDs))
    {
        return;
    }

    $orderCol = $orderColIDs[intval($oBy)];
    if ($orderCol)
    {
        $orderCol .= $oHow == 'd' ? ' desc' : ' asc';
    }
    else
    {
        $orderCol = $orderColIDs[0];
    }

    if ($orderCol)
    {
        if ($orderBy)
        {
            $orderBy .= ", $orderCol";
        }
        else
        {
            $orderBy = "ORDER BY $orderCol";
        }
    }
}

/*
 * adding fields and values to inserts
 */
function sql_addRawToInsert(&$fields, &$values, $newField, $newValue)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    if ($newValue)
    {
        $fields .= "$seperator$newField";
        $values .= "$seperator$newValue";
    }
}

function sql_addScalarToInsert(&$fields, &$values, $newField, $newValue)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    if ($newValue)
    {
        $fields .= "$seperator$newField";
        $values .= "$seperator'" . addslashes($newValue) . "'";
    }
}

function sql_addIntToInsert(&$fields, &$values, $newField, $newValue, $forceZero = false)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $newValue = intval($newValue);

    if ($newValue || $forceZero)
    {
        if (!$newValue)
        {
            $newValue = 0;
        }

        $fields .= "$seperator$newField";
        $values .= "$seperator$newValue";
    }
}

function sql_addFloatToInsert(&$fields, &$values, $newField, $newValue, $maxPrecision = 2)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $newValue = floatval($newValue);
    preg_match("/(^[\\-\\+]?[0-9]*\\.?[0-9]{0,$maxPrecision})/", $newValue, $matches);
    $newValue = $matches[1];

    if (!$newValue || $newValue == '.')
    {
        $newValue = '0.0';
    }

    $fields .= "$seperator$newField";
    $values .= "$seperator$newValue";
}

function sql_addArrayToInsert(&$fields, &$values, $newField, &$newValues, $fieldSeperator = ':', $useKeys = true)
{
    if (!is_array($newValues) || count($newValues) < 1)
    {
        return;
    }

    if ($fields != '')
    {
        $seperator = ', ';
    }

    reset($newValues);
    list($key, $value) = each($newValues);
    if ($useKeys)
    {
        $compacted = $key;
    }
    else
    {
        $compacted = $value;
    }

    while (list($key, $value) = each($newValues))
    {
        if ($useKeys)
        {
            $compacted .= $fieldSeperator . $key;
        }
        else
        {
            $compacted .= $fieldSeperator . $value;
        }
    }

    $compacted .= $fieldSeperator;
    $fields .= "$seperator$newField";
    $values .= "$seperator'" . addslashes($compacted) . "'";
}

function sql_addBoolToInsert(&$fields, &$values, $newField, $bool)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $fields .= "$seperator$newField";

    if ($bool)
    {
        $values .= $seperator . 'true';
    }
    else
    {
        $values .= $seperator . 'false';
    }
}

function sql_addTimestampToInsert(&$fields, &$values, $newField, $newTimestamp)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $newDate = date('r', $newTimestamp);

    if ($newDate)
    {
        $fields .= "$seperator$newField";
        $values .= "$seperator'$newDate'::timestamp";
    }
}

function sql_addDateToInsert(&$fields, &$values, $newField, $month, $day, $year, $timestamp = false)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $cast = $timestamp ? 'TIMESTAMP' : 'DATE';
    $month = ereg_replace('[^0-9]', '', $month);
    $day = ereg_replace('[^0-9]', '', $day);
    $year = ereg_replace('[^0-9]', '', $year);

    if (checkdate(intval($month), intval($day), intval($year)))
    {
        $fields .= "$seperator$newField";
        $values .= "$seperator" . "CAST('$month-$day-$year' AS $cast)";
    }
}

function sql_addStrDateToInsert(&$fields, &$values, $newField, $newDate, $timestamp = false)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $cast = $timestamp ? 'TIMESTAMP' : 'DATE';
    $newDate = date('m-d-Y', strtotime($newDate));

    if ($newDate)
    {
        $fields .= "$seperator$newField";
        $values .= "$seperator" . "CAST('$newDate' AS $cast)";
    }
}

/*
 * adding fields and values to updates
 */
function sql_addRawToUpdate(&$fields, $newField, $newValue)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    if ($newValue)
    {
        $fields .= "$seperator$newField = $newValue";
    }
}

function sql_addNullToUpdate(&$fields, $newField)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $fields .= "$seperator$newField = NULL";
}

function sql_addScalarToUpdate(&$fields, $newField, $newValue)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $fields .= "$seperator$newField = '" . addslashes($newValue) . "'";
}

function sql_addIntToUpdate(&$fields, $newField, $newValue)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $newValue = intval($newValue);

    if (!$newValue)
    {
        $newValue = 0;
    }

    $fields .= "$seperator$newField = $newValue";
}

function sql_addFloatToUpdate(&$fields, $newField, $newValue, $maxPrecision = 2)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $newValue = floatval($newValue);
    preg_match("/(^[\\-\\+]?[0-9]*\\.?[0-9]{0,$maxPrecision})/", $newValue, $matches);
    $newValue = $matches[1];

    if (!$newValue || $newValue == '.')
    {
        $newValue = '0.0';
    }

    $fields .= "$seperator$newField = $newValue";
}

function sql_addArrayToUpdate(&$fields, $newField, &$newValues, $fieldSeperator = ':', $useKeys = true)
{
    if (!is_array($newValues))
    {
        $newValues = array();
    }

    if ($fields != '')
    {
        $seperator = ', ';
    }

    reset($newValues);
    list($key, $value) = each($newValues);
    if ($useKeys)
    {
        $compacted = $key;
    }
    else
    {
        $compacted = $value;
    }

    while (list($key, $value) = each($newValues))
    {
        if ($useKeys)
        {
            $compacted .= $fieldSeperator . $key;
        }
        else
        {
            $compacted .= $fieldSeperator . $value;
        }
    }

    $compacted .= $fieldSeperator;
    $fields .= "$seperator$newField = '" . addslashes($compacted) . "'";
}

function sql_addBoolToUpdate(&$fields, $newField, $bool)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    if ($bool)
    {
        $fields .= "$seperator$newField = true";
    }
    else
    {
        $fields .= "$seperator$newField = false";
    }
}

function sql_addTimestampToUpdate(&$fields, $newField, $newTimestamp)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $newDate = date('r', $newTimestamp);

    if ($newDate)
    {
        $fields .= "$seperator$newField = '$newDate'::timestamp";
    }
}

function sql_addDateToUpdate(&$fields, $newField, $month, $day, $year, $timestamp = false)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $cast = $timestamp ? 'TIMESTAMP' : 'DATE';
    $month = ereg_replace('[^0-9]', '', $month);
    $day = ereg_replace('[^0-9]', '', $day);
    $year = ereg_replace('[^0-9]', '', $year);

    if (checkdate(intval($month), intval($day), intval($year)))
    {
        $fields .= "$seperator$newField = CAST('$month-$day-$year' AS $cast)";
    }
}


function sql_addStrDateToUpdate(&$fields, $newField, $newDate, $timestamp = false)
{
    if ($fields != '')
    {
        $seperator = ', ';
    }

    $cast = $timestamp ? 'TIMESTAMP' : 'DATE';
    $newDate = date('m-d-Y', strtotime($newDate));

    if ($newDate)
    {
        $fields .= "$seperator$newField = CAST('$newDate' AS $cast)";
    }
}

?>
