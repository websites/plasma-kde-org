<?php
global $common_baseImagesURL, $common_baseImagesURL, $common_baseURL;
global $cms_TopPageID;

function printTopNavLink($pageID, $title, $last = false, $hasChildren = false, $depth = 0)
{
    static $first = true;
    $url = cms_currentBaseURL(false);
    $ancestry = cms_currentPageAncestry();

    if ($depth > 0)
    {
        return;
    }

    $padding = $first ? '2px' : '2px';
    $mouseOvers = "onMouseOver=\"style.backgroundColor='#00346A';\" onMouseOut=\"style.backgroundColor='#737FB9';\"";
    $currentPageID = cms_currentPageID();
    if ($currentPageID != cms_defaultHomepageID() &&
        ($currentPageID == $pageID || $ancestry[1] == $pageID ))
    {
        $background = 'background: #02188F'; //black';
        unset($mouseOvers);
    }

    $title = strtolower($title);

    if ($pageID == cms_defaultHomePageID())
    {
        print "<td class='navStripe' $mouseOvers nowrap style=\"padding-right: 8px; padding-left: $padding; $background;\" >&bull;<a class='nav' style=\"text-decoration: none; \" href=\"$url\">$title</a></td>";
    }
    else
    {
        print "<td class='navStripe'$mouseOvers nowrap style=\"padding-right: 8px; padding-left: $padding; $background;\" >&bull;<a class='nav' style=\"text-decoration: none; \" href=\"$url" . cms_renderer::link($pageID) . "\">$title</a></td>";
    }

    $first = false;
}


$isHomepage = (cms_currentPageID() == cms_defaultHomepageID());

print '<div align="center">';
print "<table border=0 cellpadding=0 cellspacing=0 width=780 bgcolor=\"white\"
        style=\"border: solid 1px #00346 A; border-right: solid 1px #00346A;\" height=\"99%\">";

print "<tr><td valign=top>";

// the header logo, nav stripe, etc... nav stripe
print '<table cellpadding="0" cellspacing="0" border="0" width="780px" bgcolor="white" height="100%">';
print '<tr><td align="left" valign="top" style="background-color: #02188F;" width="780" height="100">';
print "<a href=\"$common_baseURL\">$common_bizName</a>";
print '</td></tr>';
print '<tr><td valign="top" height="22" width="780px">';

// the navigation
print '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
print '<tr>';
cms_printNav('printTopNavLink', $cms_TopPageID, '', false);
print '<td class="navStripe"><img src="' . $common_baseImagesURL . '/spacer.gif" width="11" height="22" alt="" border="0"></td>';
print '<td class="navStripe" width="100%">&nbsp;</td></tr>';
print '</table>';
print '</td></tr>';

print '</table>';

print '</td></tr>';


print "<tr><td height=100% valign=top>";

?>
