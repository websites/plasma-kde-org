<?php

include_once("phpfwk_config.php");
include_once("$phpfwkIncludePath/db.php");
include_once("$phpfwkIncludePath/util.php");
include_once("$phpfwkIncludePath/cmsCommon.php");
include_once("$phpfwkIncludePath/html-ui.php");

/*
 * print_header
 *
 * $title -> The page title.
 * $cmsPage -> boolean, true if this is a cmspage, false otherwise
 * $noCache -> pass in true to ensure the browser does not cache this page
 * $bodyClass -> the class from the css
 * $refresh -> autorefresh after N seconds, or never if 0
 */

function print_adminHeader($title = '', $cmsPage = true, $noCache = false, $bodyClass = 'main', $refresh = 0)
{
    global $common_siteName, $common_baseURL, $common_bizName, $common_styleSheet, $common_baseImagesURL;
    global $auth_userID, $auth_username;
    static $alreadyBeenHere = 0;

    if ($alreadyBeenHere)
    {
        return;
    }

    $alreadyBeenHere = 1;


    print "<html><head><title>$common_siteName";

    if ($title)
    {
        print ": $title";
    }

    print "</title>";

    include_once($common_styleSheet);

    if ($noCache)
    {
        print '<meta http-equiv="Pragma" content="no-cache">';
    }

    if ($refresh)
    {
        print "<meta http-equiv=\"refresh\" content=\"$refresh\">";
    }

    print "</head><body class=\"$bodyClass\">";

    if ($auth_userID > -1)
    {
        $currentDate = date('h:ma o\n d M, Y');
        print "<div class=\"adminHeader\">$currentDate &bull; Logged in as $auth_username. <a href=\"?logOut=1\">Log out</a></div>";

        print '<table border=0 style="margin: 0px;" cellpadding=0px height="100%" cellspacing=0px>';
        print '<tr><td class="adminNav" valign=top nowrap height="100%">';
        print "<a href=\"$common_baseURL/\">$common_bizName&nbsp;Admin</a><br><br>";
        print "<a href=\"$common_baseURL/cms.php\">Web&nbsp;Pages</a><br>";
        print "<a href=\"$common_baseURL/dms.php\">Documents</a><br><br>";
        print "<a href=\"$common_baseURL/shopping_catalog.php\">Catalogs</a><br>";
        print "<a href=\"$common_baseURL/shopping_stores.php\">Stores</a><br>";
        print "<a href=\"$common_baseURL/shopping_orders.php\">Orders</a><br>";
        print "<a href=\"$common_baseURL/shopping_greetings.php\">Greetings</a><br><br>";
        print "<a href=\"$common_baseURL/albums.php\">Photo Albums</a><br><br>";
        print "<a href=\"$common_baseURL/userAdmin.php\">Admin&nbsp;Users</a><br>";
        print "<a href=\"$common_baseURL/groupAdmin.php\">Admin&nbsp;Groups</a>";
        print '</td><td valign=top>';
    }
}

/*
 * print_footer
 */
function print_adminFooter()
{
    global $auth_userID;
    if ($auth_userID > -1)
    {
        print '</td></tr></table>';
    }

    print '</body></html>';
}

define(ICON_NEWPAGE, 1);
define(ICON_EDITED, 2);
define(ICON_UNPUBLISHED, 3);
define(ICON_OK, 4);
define(ICON_CANCEL, 5);
define(ICON_TREECLOSED, 6);
define(ICON_TREEOPEN, 7);
define(ICON_REMOVE, 8);
define(ICON_EXPLORE, 9);
define(ICON_MAIL, 10);
define(ICON_NEWFOLDER, 11);
define(ICON_WEBPAGE, 12);

function iconURL($icon)
{
    global $common_baseImagesURL;
    switch (strtolower($icon))
    {
        case ICON_NEWPAGE:
            return $common_baseImagesURL . '/newpage.gif';
            break;
        case ICON_EDITED:
            return $common_baseImagesURL . '/edited.gif';
            break;
        case ICON_UNPUBLISHED:
            return $common_baseImagesURL . '/unpublished.gif';
            break;
        case ICON_OK:
            return $common_baseImagesURL . '/ok.gif';
            break;
        case ICON_CANCEL:
            return $common_baseImagesURL . '/cancel.gif';
            break;
        case ICON_TREECLOSED:
            return $common_baseImagesURL . '/tree_closed.gif';
            break;
        case ICON_TREEOPEN:
            return $common_baseImagesURL . '/tree_open.gif';
            break;
        case ICON_REMOVE:
            return $common_baseImagesURL . '/remove.gif';
            break;
        case ICON_EXPLORE:
            return $common_baseImagesURL . '/explore.gif';
            break;
        case ICON_MAIL:
            return $common_baseImagesURL . '/envelope.gif';
            break;
        case ICON_NEWFOLDER:
            return $common_baseImagesURL . '/newpage.gif';
            break;
        case ICON_WEBPAGE:
            return $common_baseImagesURL . '/explore.gif';
            break;
        default:
            return '';
            break;
    }
}

include_once("include/auth_common.php");

?>
