<?php

/*
 *  NOTE:
 *
 * This file is to be used as a template for a web section that
 * requires authentication prior to use.
 *
 * This is a general set of routines used to aquire the login
 * information, and validate a "user".
 *
 * These routines require database interaction with the tables
 * stated in auth_tmplate.sql.  These tables are only the bare
 * minimum.  Additions can be made, but then you are on your own.
 */

global $auth_maxPasswordFailures, $auth_tokenTimeOut, $auth_host;
global $auth_path, $auth_baseURL, $auth_baseURL, $auth_strictURL;
global $auth_mandatory, $auth_hashPasswords;
global $auth_tokenTable, $auth_groupTable, $auth_userTable, $auth_userGroupsTable, $auth_cookieName;

$auth_maxPasswordFailures = 5;
$auth_tokenTimeOut = 3660;
$auth_host = "http://";
$auth_path = "";
$auth_baseURL = $auth_host . $auth_path;
$auth_strictURL = false;
$auth_mandatory = true;
$auth_hashPasswords = true;
$auth_quietOnGroupError = true;

$auth_tokenTable = 'userTokens';
$auth_groupTable = 'groups';
$auth_userTable = 'users';
$auth_userGroupsTable = 'userGroups';
$auth_cookieName = 'adminAuthToken';
$auth_cookieExpire = 3600;

$auth_groupLogin = false;
$auth_caseSensitive = true;

function print_authHeader()
{
    print_adminHeader();
}

function print_authFooter()
{
    print_adminFooter();
}

function auth_printLoginForm()
{
    global $bizName, $auth_baseURL;

    print '<div align=center style="margin: 10px; padding: 10px;">';
    print '<table cellpadding=3 cellspacing=0 class="box">';
    print '<tr> <th class="largeColoredHeader" colspan=2 align=left>Log In</th> </tr>';
    print '<tr> <td>Username</td> <td>';
    print_textbox('authUser', '');
    print '</tr>';
    print '<tr> <td>Password</td> <td>';
    print_passwordbox('authPassword', '');
    print '</tr>';
    print '<tr> <td></td> <td>';
    print_submit('login', 'Log In');
    print '</td> </tr>';
    print '</table> </div>';
}

function auth_postLoginSuccess($mandatory, $errors, $onFailureCallback)
{
}

include_once("$phpfwkIncludePath/authCommon.php");

?>
