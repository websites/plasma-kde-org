<html>
<head>

<title>Sample Dialog</title>
<script LANGUAGE="JavaScript">
var Nav4 = ((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 4))

// Close the dialog
function closeme() {
	window.close()
}

// Handle click of OK button
function handleOK() {
	if (opener && !opener.closed) {
		top.dlogBody.transferData()
		opener.dialogWin.returnFunc()
	} else {
		alert("You have closed the main window.\n\nNo action will be taken on the choices in this dialog box.")
	}
	closeme()
	return false
}

// Handle click of Cancel button
function handleCancel() {
	closeme()
	return false
}

</SCRIPT>

</head>
<frameset FRAMEBORDER=no FRAMESPACING=0 ROWS="*,50" BORDER=0 onLoad="if (opener) opener.blockEvents()" onUnload="if (opener) opener.unblockEvents()">
	<frame NAME="dlogBody" SRC="insert_image.php" FRAMEBORDER=no>
	<frame NAME="dlogButtons" SRC="dlogButtons.html" FRAMEBORDER=no SCROLLING=no>
</frameset>

</html>