<?php

include_once('phpfwk_config.php');
include_once("$phpfwkIncludePath/db.php");
include_once("$phpfwkIncludePath/util.php");
include_once("$phpfwkIncludePath/cmsCommon.php");
include_once("$phpfwkIncludePath/html-ui.php");

global $common_pageHeaderFile, $common_pageFooterFile, $common_pageTitle;
$common_pageHeaderFile = $common_header;
$common_pageFooterFile = $common_footer;
$common_pageTitle = '';

/*
 * print_header
 *
 * $title -> The page title.
 * $cmsPage -> boolean, true if this is a cmspage, false otherwise
 * $noCache -> pass in true to ensure the browser does not cache this page
 * $bodyClass -> the class from the css
 * $refresh -> autorefresh after N seconds, or never if 0
 */

function print_header($title = '', $cmsPage = true, $noCache = false, $bodyClass = 'main', $refresh = 0)
{
    global $common_siteName, $common_baseURL, $common_bizName;
    global $common_templates, $common_header, $common_baseImagesURL;
    global $common_pageHeaderFile, $common_pageFooterFile, $cms_homepageID;
    global $common_bizName, $common_title;
    global $common_pageTitle;
    global $common_printStyleSheet, $common_styleSheet, $pf; // pf == printer friendly

    $common_pageTitle = $title;
    static $alreadyBeenHere = 0;
    if ($alreadyBeenHere)
    {
        return;
    }

    $alreadyBeenHere = 1;

    if ($cmsPage)
    {
        $cmsPage = cms_init();
        if ($cmsPage)
        {
            ob_start();
        }
    }

    print '<html><head><title>';
    if ($common_pageTitle)
    {
        print "$common_pageTitle: ";
    }
    print "$common_siteName</title>";

    include_once($pf ? $common_printStyleSheet : $common_styleSheet);

    if ($noCache)
    {
        print '<meta http-equiv="Pragma" content="no-cache">';
    }

    if ($refresh)
    {
        print "<meta http-equiv=\"refresh\" content=\"$refresh\">";
    }

    print "</head><body class=\"$bodyClass\">";
    if ($common_pageHeaderFile && is_readable("$common_templates/$common_pageHeaderFile"))
    {
        include_once("$common_templates/$common_pageHeaderFile");
    }
    else
    {
        if ($pf)
        {
            print "<table border=0 cellpadding=0px cellspacing=0px bgcolor=\"white\"  height=\"98%\">";
        }
        else
        {
            print '<div align="center">';
            print "<table border=0 cellpadding=0 cellspacing=0 width=780 bgcolor=\"white\" style=\"border: solid 1px #00346A; border-right: solid 1px #00346A;\" height=\"99%\">";
        }

        print "<tr><td height=100% valign=top>";
    }

    if ($cmsPage)
    {
        cms_printPage(cms_currentPageID());
        ob_end_flush();
    }
    else
    {
        print '&nbsp;';
    }
}

/*
 * print_footer
 */
function print_footer()
{
    global $common_pageFooterFile, $common_templates, $pf;

    if ($common_pageFooterFile && is_readable("$common_templates/$common_pageFooterFile"))
    {
        include_once("$common_templates/$common_pageFooterFile");
    }
    else
    {
        print '</td></tr></table>';
        if (!$pf)
        {
            print '</div>';
        }
    }

    print '</body></html>';
}

?>
