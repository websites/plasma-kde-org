<?php

include_once('notification.php');

define('EVENT_DOC_ADDED', 1);
define('EVENT_DOC_MODIFIED', 2);
define('EVENT_DOC_REMOVED', 3);

$dmsEvents = array(1 => 'Document Added',
                   2 => 'Document Modified',
                   3 => 'Document Removed');

class dms_notification extends Notification
{
    var $documentID;
    var $documentURL;
    var $documentTitle;

    function dms_notification($table, $from, $documentID)
    {
        Notification::Notification($table, $from);
        $this->folderID = 0;
        $this->documentID = intval($documentID);
        $this->documentURL = '';
        $this->documentTitle = '';
    }

    function subject()
    {
        if (! $this->documentTitle)
        {
            $db = db_connect();

            $docInfo = db_query($db, "SELECT title, filename FROM dmsDocuments
                                      WHERE documentID = {$this->documentID};");

            if (db_numRows($docInfo) >0)
            {
                list($title, $filename) = db_row($docInfo, 0);
                $this->documentTitle = $filename;
                $this->documentURL = dmsFileURL($this->id, $this->documentID, $filename);
            }
        }

        switch ($this->event)
        {
            case EVENT_DOC_ADDED:
                return "NEW DOCUMENT ADDED : {$this->documentTitle}";
                break;
            case EVENT_DOC_MODIFIED:
                return "DOCUMENT HAS BEEN MODIFIED : {$this->documentTitle}";
                break;
            case EVENT_DOC_REMOVED:
                return "DOCUMENT WAS REMOVED : {$this->documentTitle}";
                break;
            default:
                return "UNKNOWN DOCUMENT EVENT";
                break;
        }
    }

    function message()
    {
        switch ($this->event)
        {
            case EVENT_DOC_ADDED:
                $msg .= 'This document was just added to the Document Management System.';
                break;
            case EVENT_DOC_MODIFIED:
                $msg .= 'This document was modified in the Document Management System.';
                break;
            case EVENT_DOC_REMOVED:
                $msg .= 'This document was removed from the Document Management System.  It is no longer be available.  Please remove all links to this document you may have.';
                break;
            default:
                break;
        }

        $msg .= "<br>Link to this file : <a href=\"{$this->documentURL}\">{$this->documentTitle}</a><br>";

        return $msg;
    }

    function createNotification($id, $event)
    {
        Notification::createNotification($id, $event);

        $db = db_connect();

        $docInfo = db_query($db, "SELECT title, filename FROM dmsDocuments
                                  WHERE documentID = {$this->documentID};");

        if (db_numRows($docInfo) >0)
        {
            list($title, $filename) = db_row($docInfo, 0);
            $this->documentTitle = $filename;
            $this->documentURL = dmsFileURL($this->id, $this->documentID, $filename);
        }
    }
}

$dms_external = array('txt' => '$command = "/bin/cat $file";',
                      'pdf' => '$command = "/usr/bin/pdftotext $file -";',
                      'doc' => '$command = "/usr/bin/wvWare --config=/usr/share/wv/wvText.xml $file";');


function dmsFolderPath($folderID)
{
    global $dmsFilesystemRoot;
    $filename = str_replace(array('/',' '), '_', $filename);
    return "$dmsFilesystemRoot/" . intval($folderID);
}

function dmsFolderURL($folderID)
{
    global $dms_baseURL;
    $filename = str_replace(array('/',' '), '_', $filename);
    return "$dms_baseURL/" . intval($folderID);
}

function dmsFolderID($folderName, $parentFolderID = 0)
{
    $db = db_connect();
    unset($where);
    sql_addToWhereClause($where, 'where', 'name', '=', $folderName);
    if ($parentFolder > 0)
    {
        sql_addToWhereClause($where, 'and', 'parent', '=', $parentFolderID);
    }

    $folder = db_query($db, "select folderID from dmsFolders $where;");

    if (db_numRows($folder) < 1)
    {
        return -1;
    }

    list($folder) = db_row($folder, 0);
    return $folder;
}


function dmsFileName($folderID, $documentID, $filename = '')
{
    global $dmsFilesystemRoot;
    $ext = str_replace(array('/',' '), '_', $filename);
    return "$dmsFilesystemRoot/" . intval($folderID) . '/' . intval($documentID) . '_' . $filename;
}

function dmsFileURL($folderID, $documentID, $filename = '')
{
    global $dms_baseURL;
    $filename = str_replace(array('/',' '), '_', $filename);
    return "$dms_baseURL/" . intval($folderID) . '/' . intval($documentID) . '_' . $filename;
}

function &dmsFolderFamilyTree($folderID, $byname = false)
{
    $id = intval($folderID);

    $db = db_connect();
    $tree = array();

    if (!$byname)
    {
        array_push($tree, $id);
    }

    $top = false;
    $numRows = 1;

    while (!$top && $numRows)
    {
        $folderInfo = db_query($db, "SELECT parentFolderID, name, (parentFolderID IS NULL) AS top
                                     FROM dmsFolders WHERE folderID = $id;");
        $numRows = db_numRows($folderInfo);

        if ($numRows > 0)
        {
            list($id, $name, $top) = db_row($folderInfo, 0);
            $top = db_boolean($top);

            if ($byname)
            {
                array_push($tree, $name);
            }
            else
            {
                if ($id)
                {
                    array_push($tree, $id);
                }
            }
        }
    }

    return $tree;
}

/*
 * addDmsFoldersToList : Create an array (key / value) of the folders,
 *                       From a given starting point (NULL for top level)
 *
 *                       Watch the recursion ;-)
 *
 * &$foldersArray -> the array (by reference) to hold the list
 * $parentID -> the starting parent to go down from
 * $parentPath -> the parents path ('grandparent's name / parent's name')
*/
function addDmsFoldersToList(&$foldersArray, $parentID, $parentPath)
{
    $db = db_connect();

    unset($where);

    if (!$parentID)
    {
        sql_addToWhereClause($where, '', 'parentFolderID', 'IS', 'NULL', false, false);
    }
    else
    {
        sql_addToWhereClause($where, '', 'parentFolderID', '=', $parentID);
    }

    $folders = db_query($db, "SELECT folderID, name FROM dmsFolders WHERE $where ORDER BY name;");

    $numFolders = db_numRows($folders);

    for ($i = 0; $i < $numFolders; $i++)
    {
        list($id, $name) = db_row($folders, $i);

        if ($parentPath)
        {
            $pathToAdd = "$parentPath / " . rtrim($name);
        }
        else
        {
            $pathToAdd = $name;
        }

        $foldersArray[$id] = $pathToAdd;

        addDmsFoldersToList($foldersArray, $id, $foldersArray[$id]);
    }
}

function dmsFileNameByID($documentID, $url = true)
{
    $documentID = intval($documentID);

    if (!$documentID)
    {
        return false;
    }

    $db = db_connect();

    $document = db_query($db, "SELECT docs.folderID, docs.documentID, docs.filename
                               FROM dmsDocuments docs
                               JOIN dmsFolders f ON (docs.folderID = f.folderID)
                               WHERE documentID = $documentID");

    if (db_numRows($document) < 1)
    {
        return false;
    }

    list($folderID, $documentID, $filename) = db_row($document, 0);

    return $url ? dmsFileURL($folderID, $documentID, $filename)
                : dmsFileName($folderID, $documentID, $filename);
}

function dmsRandomFile($folderID)
{
    $folderID = intval($folderID);

    if (!$folderID)
    {
        return false;
    }

    $db = db_connect();

    $document = db_query($db, "SELECT docs.documentID
                               FROM dmsDocuments docs
                               WHERE docs.folderID = $folderID 
                               ORDER BY random() limit 1;");

    if (db_numRows($document) < 1)
    {
        return false;
    }

    list($documentID) = db_row($document, 0);
    return $documentID;
}


// return an array containing the file URL, file name, project name and library name
function dmsFileInfo($documentID)
{
    $documentID = intval($documentID);

    if (!$documentID)
    {
        return false;
    }

    $db = db_connect();

    $document = db_query($db, "SELECT f.folderID, f.name,
                                      d.documentID, d.filename, d.title, d.description
                               FROM dmsDocuments d
                               JOIN dmsFolders f ON (d.folderID = f.folderID)
                               WHERE documentID = $documentID");

    if (db_numRows($document) < 1)
    {
        return false;
    }

    list($folderID, $folderName, $documentID, $filename, $title, $description) = db_row($document, 0);

    $rv = array('title' => $title,
                'description' => $description,
                'filename' => $filename,
                'URL' => dmsFileURL($folderID, $documentID, $filename),
                'path' => dmsFileName($folderID, $documentID, $filename),
                'folder' => $folderID,
                'folderName' => $projectName);

    return $rv;
}

function dmsMkdir($dir, $printErrors = false, $errorPreFunc = '')
{
    if (!@mkdir($dir, 0700))
    {
        if ($printErrors)
        {
            print_msg('ERROR', 'Fatal Document Copy Failure',
                      "The directory $dir could not be created!", 'dmsc/1', true, $errorPreFunc);
        }

        return false;
    }

    return true;
}

function dmsCopy($docSource, $folderID, $documentID, $filename,
                 $printErrors = false, $errorCode = '', $errorPreFunc = '')
{
    global $dmsFilesystemRoot, $dms_external;

    $file = "$dmsFilesystemRoot/$folderID";

    $stat = @stat($file);

    if (!$stat && !dmsMkdir($file, $printErrors, $errorPreFunc))
    {
        return false;
    }

    $file = "$file/{$documentID}_" . str_replace(array('/',' ','#','"','\''), '_', $filename);

    if (!copy($docSource, $file))
    {
        if ($printErrors)
        {
           print_msg('ERROR', 'File upload error',
                "The file could not be copied!<br>
                Please contact the <A HREF=\"mailto:$techSupportEmail\">technical staff</A>
                about this.", $errorCode ? $errorCode : 'dmsc/2', false, 'print_leftSide');
        }

        return false;
    }

    // TODO: add full text searching
    /* load the file into FTS
    $ext = array_pop(explode('.', $file));
    $command = $dms_external[$ext];
    if ($command)
    {
        eval($dms_external[$ext]);
        $pd = popen($command, "r");

        if ($pd)
        {
            $contents = fread($pd, filesize($file));
            $contents = preg_replace('/\s+/', ' ', $contents);
            if ($contents)
            {
                $sql = "insert into dmsdocumentsftistage (documentID, searchtext)
                               values ('$documentID', txt2txtidx('default', '" . addslashes($contents) . "'));";

                $db = db_connect();
                db_query($db, $sql);
            }
            pclose($pd);
        }
     }*/

   return $file;
}

function dmsUpdateFile($docSourceFile, $docFile, $documentID)
{
    $documentID = intval($documentID);

    if ($documentID < 1)
    {
        return false;
    }

    $db = db_connect();

    $document = db_query($db, "SELECT projs.library, docs.project, docs.documentID, docs.filename
                               FROM dmsDocuments docs
                               JOIN dmsProjects projs ON (docs.project = projs.projectID)
                               WHERE documentID = $documentID");

    if (db_numRows($document) < 1)
    {
        return false;
    }

    list($folderID, $documentID, $currentDoc) = db_row($document, 0);

    if ($currentDoc)
    {
        @unlink(dmsFileName($libraryID, $folderID, $documentID, $currentDoc));
    }

    $docFileName = dmsCopy($docSourceFile, $folderID, $documentID, $docFile, true);

    if ($docFileName)
    {
        chmod($docFileName, 0644);

        unset($fields);
        sql_addScalarToUpdate($fields, 'filename', $docFile);
        db_query($db, "UPDATE dmsDocuments SET $fields WHERE documentID = $documentID;");

        return true;
    }
    else
    {
        print_msg('ERROR', 'Document Creation Failure',
                  'No document was uploaded! Aborting update!', 'dmsc/3', false, 'print_dmsHeader');
        return false;
    }

    return false;
}

function dmsSimpleInsert($docSourceFile, $docFile, $folderID, $docTitle, $docDescription = '')
{
    $folderID = intval($folderID);

    if ($folderID < 1)
    {
        return false;
    }

    $db = db_connect();

    $libraryID = db_query($db, "SELECT library FROM dmsProjects WHERE projectID = $folderID;");

    if (db_numRows($libraryID) < 1)
    {
        return false;
    }

    list($libraryID) = db_row($libraryID, 0);

    db_startTransaction($db);

    unset($fields);
    unset($values);

    sql_addIntToInsert($fields, $values, 'project', $folderID);
    sql_addScalarToInsert($fields, $values, 'filename', $docFile);
    sql_addScalarToInsert($fields, $values, 'title', $docTitle);
    sql_addScalarToInsert($fields, $values, 'description', $docDescription);
    sql_addBoolToInsert($fields, $values, 'approved', true); // auto-approve the attachment

    $displayOrder = db_query($db, "SELECT max(displayOrder)
                                   FROM dmsDocuments
                                   WHERE project = $folderID;");

    if (db_numRows($displayOrder) > 0)
    {
        list($displayOrder) = db_row($displayOrder, 0);
        sql_addIntToInsert($fields, $values, 'displayOrder', $displayOrder + 1);
    }

    db_insert($db, 'dmsDocuments',  $fields, $values);
    $documentID = db_seqCurrentVal($db, 'seq_dmsIDs');

    if ($docFile &&
        $docFileName = dmsCopy($docSourceFile, $folderID, $documentID, $docFile, true))
    {
        chmod($docFileName, 0644);
    }
    else
    {
        db_abortTransaction($db);
        return false;
    }

    db_endTransaction($db);
    return $documentID;
}

function dmsDelete($documentID, $printErrors = true, $preErrorCallback = '')
{
    $documentID = intval($documentID);

    if ($documentID < 1)
    {
        return false;
    }

    $db = db_connect();

    $fileInfo = db_query($db, "SELECT f.folderID, filename
                               FROM dmsDocuments docs
                               JOIN dmsFolders f ON (docs.folderID = f.folderID)
                               WHERE docs.documentID = $documentID;");

    if (db_numRows($fileInfo) < 1)
    {
        print_msg('ERROR', 'Document Entry Missing',
                  'The requested document could not be referenced for deletion.
                   This probably means it has already been deleted.',
                  'dmsc/4', false, $preErrorCallback);
        return false;
    }

    list($folderID, $filename) = db_row($fileInfo, 0);
    $filename = dmsFileName($folderID, $documentID, $filename);

    if (!@unlink($filename) && $printErrors)
    {
        print_msg('INFORMATION', 'Document Missing',
                  "The file associated with this document:
                  <p>
                  &nbsp;&nbsp;&nbsp;$filename
                  <p>
                  could not be found on disk.
                  However, the document record was removed successfully from the database.",
                  'dmsc/5', false, $preErrorCallback);
    }

    db_delete($db, 'dmsDocuments', "documentID = $documentID");

    return true;
}

/*
NOTE : This is now recursive, so wrap the call to this in a transaction block
       until nested transactions are handled withing the Postgres stable branch
       we may at some point restrict support for previous PG versions. 
*/
function dmsDeleteFolder($folderID, $printErrors = true, $preErrorCallback = '')
{
    $folderID = intval($folderID);

    if ($folderID < 1)
    {
        return false;
    }

    $db = db_connect();

    $documents = db_query($db, "SELECT documentID FROM dmsDocuments WHERE folderID = $folderID;");
    $numDocuments = db_numRows($documents, $printErrors);

    for ($i = 0; $i < $numDocuments; ++$i)
    {
        list($documentID) = db_row($documents, $i);
        dmsDelete($documentID, $preErrorCallback);
    }

    $subFolders = db_query($db, "SELECT folderID FROM dmsFolders WHERE parentFolderID = $folderID;");
    $numFolders = db_numRows($subFolders, $printErrors);

    for ($i = 0; $i < $numFolders; ++$i)
    {
        list($subFolderID) = db_row($subFolders, $i);
        dmsDeleteFolder($subFolderID, $preErrorCallback);
    }

    $folderPath = dmsFolderPath($folderID);
    @rmdir($folderPath);

    db_delete($db, 'dmsFolders',  "folderID = $folderID");

    return true;
}

function dmsPageAttributes($pageID)
{
    $db = db_connect();

    $library = db_query($db, "SELECT cms.library, cms.showProjects, cms.archives, libs.name
                              FROM cmsDocLib cms
                              JOIN dmsLibraries libs ON (cms.library = libs.libraryID)
                              WHERE pageID = $pageID;");

    if (db_numRows($library) < 1)
    {
        return array(false);
    }

    list($library, $showProjectTitles, $showArchives, $libraryName) = db_row($library, 0);

    $showArchives = db_boolean($showArchives);
    $showProjectTitles = db_boolean($showProjectTitles);

    return array($library, $showProjectTitles, $showArchives, $libraryName);
}

?>
