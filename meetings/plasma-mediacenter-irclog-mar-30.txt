[23:20] == Shaan7_ [~ca81d102@gateway/web/freenode/x-hyxriglmclnbqcvw] has joined #plasma-mediacenter
[23:22] == Shaan7 [~ca81d102@gateway/web/freenode/x-irpwqcncdvnrkgku] has quit [Ping timeout: 252 seconds]
[23:22] == Shaan7_ has changed nick to Shaan7
[23:22] -NickServ- This nickname is registered. Please choose a different nickname, or identify via /msg NickServ identify <password>.
[23:22] *NickServ* identify freenodepassword
[23:22] -NickServ- You are now identified for Shaan7.
[23:40] == notmart [~diau@kde/mart] has joined #plasma-mediacenter
[23:41] <Shaan7> notmart: hi, we got guests :)
[23:41] <notmart> Shaan7: ehia
[00:08] <aseigo> Shaan7: hm.. i need to find where on the website it is.. :P i suppose you submitted it directly to the gsoc website, yes?
[00:08] <Shaan7> aseigo: well, i haven't submitted it yet, thats why :P here's the link http://docs.google.com/Doc?docid=0AW18pBu48E64ZGNiMzVqNzNfMTgzY2Y4NTZqdg&hl=en
[00:09] <aseigo> Shaan7: oh.
[00:09] <aseigo> yes, that would explain it
[00:09] <aseigo> Shaan7: hm. i really don't think PMC is about touchscreens, tbh.
[00:10] <aseigo> under use cases, you may want to add one about using PMC from a laptop
[00:11]  * aseigo notes that this is one feature of macos he really likes and knows the free desktops are sorely msising
[00:11] <aseigo> er, missing
[00:11] <Shaan7> aseigo: but it can be used like that, it'll be nice to have, say a 19" touchscreen and viewing pictures
[00:11] <Shaan7> aseigo: yes, I'll add that
[00:11] <aseigo> hm.. that's kind of like saying that plasma netbook can be used with a t.v. remote control ;)
[00:12] <aseigo> yes, it can be, but it's not designed with that as a primary mechanism in mind.
[00:12] <Shaan7> aseigo: yeah, i never said primary ;)
[00:12]  * notmart thinks while not the primary focus, -could- be made touchscreen friendly with very little effort
[00:12] <aseigo> rather than an emphasis (of any sort) on touch compatibility, i'd recommend (and it's only that: a recommendation) not putting touch so prominently in the submission
[00:12] <aseigo> notmart: yes
[00:13] <Shaan7> aseigo: ok, noted.
[00:13] <notmart> maybe not immediately and would be as usable as something designer -for. that
[00:13] <aseigo> Shaan7: "The design agreed upon after an IRC meeting" <-- you may want to mention when that meeting was held so it is clear that it's a meeting you've already had. others may read that and think it's an irc meeting you will hold as part of the GSoC project
[00:14] <Shaan7> though I'd like to include TV and long viewing distance compatibility :)
[00:14] <notmart> but one could be surprisedtring it there
[00:14] <aseigo> "Saving playlists to the popular M3U format and a custom XML format will be supported." <-- why a custom xml format?
[00:14] <aseigo> Shaan7: yes, tv and "10 foot interface" concepts are key
[00:15] <notmart> getting back at the canola example, canola won't work on a tv just for some details that are missing, that are details but effectively make it useless there
[00:16] <Shaan7> aseigo: well I thought maybe we'd like to add some more info to a playlist item, say a custom cover which the user might've set.
[00:16] <Shaan7> aseigo: but can be nicely dropped if an overhead, was just a thought
[00:16] <notmart> i think playlists should be reeeally simple
[00:17]  * aseigo agrees
[00:17] <aseigo> we're not rewriting amark here :)
[00:17] <aseigo> er, amarok
[00:17] <notmart> i wasn't even conviced that they had to be there at the beginning :)
[00:17] <Shaan7> aseigo: hehe yes, my wrong :)
[00:17] <Shaan7> s/wrong/mistake
[00:17] <notmart> but for music yes, it could be useful
[00:18] <aseigo> Shaan7: ok, so while i really would like to see a PMC GSoC project, what i'm missing from your proposal is a real idea of what the deliverables will be in terms of how it meets the goal of providing something that is shippable
[00:18] <aseigo> right now when i fire up PMC i note three things are missing:
[00:18] <notmart> while i still think the main use is "search for a movie title, watch that single movie, turn it off" :p
[00:18] <aseigo> * a coherent navigation system
[00:18] <aseigo> notmart: yes
[00:19] <aseigo> * a compelling browsing screen
[00:19] <aseigo> * a good video experience
[00:20] <aseigo> i really want to avoid what happened with the first gsoc project for pmc. which was to lay down a bunch of useful code, but with little of it being put together in a way that produced something we could ship as a product
[00:20] <aseigo> it doesn't have to be finished or perfect, but the code should result in something shippable imho
[00:20] <aseigo> it seems that the coherent navigation system is being worked on; that r-b patch, if it was written as a QStateMachine, could provide that
[00:20] <notmart> i think wht there is now is a good fundation, doesn't have to start from zero, so shoul dbe easier
[00:20] <Shaan7> aseigo: thats what even I've in mind as a goal, though as you said, I might need to think on it even more
[00:21] <aseigo> i still don't see where the browsing screen is going to come from, however; in the proposal you talk about adding on to the file browser.
[00:21] <aseigo> notmart: yes
[00:21] <Shaan7> aseigo: it does come when you start the media center shell right now
[00:22] <aseigo> Shaan7: personally, i'd take "video plugin for file browser" and "music plugin for file browser" and turn those into a "consolidated media browser interface"
[00:22] <Shaan7> aseigo: currently only containing the local files plugin
[00:22] <aseigo> yes. but it really isn't that great of a UI
[00:22] <aseigo> it actually pretty much fails as a media browser.
[00:22] <Shaan7> aseigo: well the music plugin won't be actually for browsing files, it'll be a organized collection of the media the user has in the collection
[00:23] <notmart> a grid view would be better i think
[00:23] <aseigo> it's more like dolphin than gwenview :)
[00:23] <Shaan7> notmart: grid view is there already ..
[00:23] <aseigo> Shaan7: have you used any of those computerized jukeboxes they have in some bars/pubs?
[00:24] <Shaan7> aseigo: nup, just some sample music stations at music stores
[00:24] <aseigo> hm... let me do some sketches and then scan them in. damn i wish we had shared whiteboards :)
[00:24] <notmart> Shaan7: yep i know
[00:24] <Shaan7> aseigo: the blackboard applet shared on the Internet ;)
[00:24] <notmart> maybe with horizontal scrolling but this is just my inexplicable fetish with that wrong concept of horizontal grid views :p
[00:25]  * notmart bbiab
[00:34] <Shaan7> brb
[00:44] <Shaan7> back
[00:51] <notmart> recent konversations should have a dcc whiteboard, never tried it tough
[00:54] <Shaan7> aseigo: what actually I thought was concentrating first on getting the things working, and then stress on the UI. For example, there is no organization of Music, and you can't preview videos etc. While I think the File Browser will let you *use* the MC (when the things are done), it'd be nice if you'd elaborate why it isn't so.
[00:55] <Shaan7> aseigo: and about combining music and video, its not good to do so because music, as I said can be categorized according to artists,albums etc, while videos will be browsed as files.
[01:02] <aseigo> Shaan7: http://bddf.ca/~aseigo/pmc_browser_concept.png
[01:02] <Shaan7> about the navigation system, Christophe is doing it (we had lots of coordinating efforts to distribute work amonst us), so I can't mention it in the proposal as its not me who is doing it. Can't take the credit for someone else's work.
[01:02]  * Shaan7 looks
[01:02] <aseigo> let me know when you've got that loaded :)
[01:03] <notmart> uhm, well, basically that's it..
[01:03] <Shaan7> aseigo: loaded
[01:07] <Shaan7> Well, on the whole I see the same thing I had in my mind, so atleast I'm on the right track :)
[01:07] <Shaan7> though this means I was bad at putting it in words
[01:08]  * notmart tought that too
[01:08] <aseigo> perhaps :)
[01:08] <notmart> and the main menu yes, forcing it on a single line with zooming of the actie icon would be nice
[01:08] <aseigo> the key points there from my perspective is that the browsing is actually identical no matter what kind of media is being browsed
[01:09] <aseigo> you have a main screen where you pick what you'd like to be doing
[01:09] <aseigo> and that loads one of the browse modes
[01:09] <aseigo> but the modes really are just differences in data
[01:09] <aseigo> the browse view can be identical
[01:09] <Shaan7> aseigo: ok, actually with all media centers i've used, and from the feedback i got from my friends, people tend to use music organized by the ID3 tags (for MP3 as example), not as files
[01:10] <notmart> doesn't change that much does it?
[01:10] <aseigo> e.g. a grid view of images with title beneath ... perhaps a list of shortcuts (A, B, C, D, etc.. for alpha, e.g.) to jump to that section of the overall listing... categorizations at the top
[01:10] <Shaan7> notmart: the view doesn't change, yes
[01:11] <Shaan7> aseigo: i'll show a small, very ugly mock i had made ...
[01:13] == daitheflu [~daitheflu@ill67-3-88-164-129-128.fbx.proxad.net] has joined #plasma-mediacenter
[01:13] <aseigo> Shaan7: that's fine. look at the music listing there .. at the top there is: album, date, artist, etc
[01:14] <aseigo> if you pick "album" it shows the albums, you select an album and it shows the tracks in that album
[01:14] <aseigo> pick artist and it shows the artists, select an aritst and it shows the tracks by that artist
[01:14] <aseigo> iow it would be organized by id3 tags (or whatever) .. but that's a data issue
[01:14] <aseigo> how it is graphically presented is what seems to be not very well defined atm :)
[01:14] <aseigo> personally i'd love to see it such that whether i'm browsing my photos, my music, my videos or my apps .. the browsing is all identica
[01:14] <aseigo> er, identical
[01:14] <Shaan7> aseigo: yes, it will be. only the categorization changes, as you noticed
[01:15] == aseigo [~aseigo@kde/aseigo] has quit [Remote host closed the connection]
[01:15] <Shaan7> oops
[01:15] <notmart> you killed him
[01:15] == aseigo [~aseigo@66.183.103.183] has joined #plasma-mediacenter
[01:15] == aseigo [~aseigo@66.183.103.183] has quit [Changing host]
[01:15] == aseigo [~aseigo@kde/aseigo] has joined #plasma-mediacenter
[01:15] <aseigo> wah
[01:15] <Shaan7> notmart: hehe
[01:15] <aseigo> what was the last thing you saw?
[01:15] <Shaan7> aseigo: er, identical
[01:15] <aseigo> ah, ok.. didn't miss anything then. whew!
[01:15] <aseigo> what would change is the categorization possibilities (e.g. album / artist / genre / etc for music, album / date for photos, title / genre for movies, etc)
[01:16] <aseigo> and of course the viewer when i finally pick what i want to look at
[01:16] <Shaan7> aseigo: yes, so the plugin offers the data accordingly
[01:16] <notmart> aseigo: that categorization things on top is an horizontal list like a tabbar?
[01:16] <Shaan7> aseigo: pic frame for pictures/slideshows, media player for videos :)
[01:17] <aseigo> notmart: maybe not a tabbar exactly, but a category selector, sure...
[01:17] <notmart> thinking how it sould be used with just a remote and without pain..
[01:17] <aseigo> Shaan7: so what i'm missing here is how "File Browser" gets transformed into "Media Browser"
[01:17] <notmart> bah, will require a different hardware key..
[01:18] <aseigo> Shaan7: "File Browser" is certainly the wrong name, and the current implementation is, quite honestly, not usable
[01:18] <Shaan7> aseigo: Name is wrong, implementation, well ...
[01:18]  * Shaan7 thinks
[01:18] <aseigo> notmart: if just using the arrow keys: pressing "up" from an item at the top would move focus into the selector
[01:19] <notmart> uhm yeah
[01:19] <notmart> would require quite a lot of tapping...
[01:20] <Shaan7> aseigo: beat me if you'd like to, but I still don't get it why the current implementation cannot be used for the things we discussed right now?
[01:20] <notmart> so, let's exactly define what the current implementation misses to become not a pin..
[01:20] <notmart> *pain
[01:21]  * Shaan7 nods
[01:21] <Shaan7> though I agree the name File Browser is misleading
[01:21] <notmart> yes, that's an implementation detail
[01:21] <Shaan7> its just a slave of whatever data the plugin provides, isn't it notmart ?
[01:22] <Shaan7> notmart: as in it just shows what the plugin tells it to show ..
[01:23] <notmart> model or view wise?
[01:24] <notmart> model wise, let's keep it just a fiole browser for now, other plugins will come afterwarrds
[01:24] <notmart> you could try to take that view ad make it something sanely interagible :)
[01:24] <aseigo> let's talk about the view
[01:25] <aseigo> detail level: the hover indicator needs to be redone. it's really not good
[01:25] <aseigo> why the view is touching private members of ViewItem is also a mystery
[01:25] <notmart> is an hover indicator precedent to the libplasma one by months i think
[01:26] <aseigo> yes; it needs to be moved over. it should be a Plasma::ItemBackground subclass i think
[01:26]  * notmart was about to repeat the mantra how sucky is the fact there are no official qgv itemviews, but i'm not since that's a given :)
[01:26] <aseigo> erf.. MediaBrowser::m_viewType is a string
[01:27] <notmart> funningly enough when i wrote itembackground i stared from the code of that one
[01:27] <notmart> that's why there is alediaferia copyright in itembackground :)
[01:27] <aseigo> huh
[01:27] <aseigo> anyways ... defaulting to a list is silly
[01:27] <notmart> but is almost 100% different by now
[01:28] <aseigo> meh.. let's start one step back here
[01:28] <aseigo> plasmediacenter (plasma-mediacenter?) should start full screen
[01:28] <Shaan7> aseigo: yes, agreed
[01:28] <aseigo> then it becomes really apparent how silly the list is.. so switch the default to grid
[01:29] <Shaan7> aseigo: make grid item size a bit flexible
[01:29] <Shaan7> so that it appears larger on large screens
[01:29] <notmart> would be the list ever used?
[01:29] <aseigo> notmart: i think it's useful as a "make this a generalized item view" class
[01:30] <aseigo> and the list might be used ... i'd hold off on removing it just yet
[01:30] <Shaan7> maybe an example would be when i'd only like to use the remote's up down keys instead of having to move in all four directions
[01:31] <notmart> because i'm thinking about the netbook grid view, if it wasn't that has only the grid mode, is pretty generic and i did put a lot of attention on making it work with just arrow keys...
[01:31] <aseigo> in grid mode, there are waaaay too many items (item size is too small)
[01:31] <notmart> the size should depend probably from the font size used
[01:32] <aseigo> notmart: yes, that's a real option
[01:32] <notmart> that would be default as ginormous
[01:32] <Shaan7> notmart: not from  the screen size ?
[01:32] <aseigo> mm... not sure about either of those ideas
[01:32] <Shaan7> notmart: like if I have a TV and viewing it from a large distance, i need the items to be bigger
[01:32] <notmart> Shaan7: screen size is not a really meaningful concept
[01:32] <aseigo> in a media center, the visuals are going to be as or more important than the text
[01:33] <aseigo> so the text may always be disproportionately small compared to the images
[01:33] <aseigo> think of viewing photos, for example
[01:33] <notmart> yeah
[01:33] <aseigo> ah, that's another point. it needs to be able to not show text under the images
[01:33] <notmart> but for movies or song being able to clearly see the title is important too
[01:33] <Shaan7> aseigo: another config option? ;)
[01:34] <aseigo> Shaan7: no, this is something that the data provider will know
[01:34] <notmart> exactly
[01:34] <Shaan7> aseigo: i mean always no text for images?
[01:34] <aseigo> notmart: and this is where i think the list may come into use
[01:34] <aseigo> ok, let's say i'm viewing my music by artist
[01:34] <aseigo> i go into Pearl Jam
[01:34] <notmart> yes
[01:34] <aseigo> in there i see all the albums
[01:35] <aseigo> i click on an album and now i have, finally, the tracks
[01:35] <notmart> or also when you browse a podcast probably a list would be useful
[01:35] <aseigo> those tracks probably should be in a vertical list so i can see the full title
[01:35] <aseigo> for photos, i probably always want the grid view
[01:35] <aseigo> so the view should probably have:
[01:35] <Shaan7> aseigo: even albums might have long names
[01:36] <aseigo> Shaan7: that's why i have cover art
[01:36] <notmart> hmm, now that i think about two implementations for grid and list aren't even needed
[01:36] <aseigo> Shaan7: honestly, this actually works quite well. i had a friend who worked for a company that made computerized jukeboxes for bars / pubs. which is why i asked if you'd ever played with them before.
[01:36] <Shaan7> aseigo: hmm ok
[01:36] <notmart> if the number of columns of the grid is one .. :)
[01:36] <aseigo> Shaan7: they worked exceptionally well and followed this same grid/list system
[01:37] <notmart> it will change the delegate for sure
[01:37] <aseigo> yes, the list could be a grid of column size 1 ...
[01:37] <aseigo> but it should probably paint the text beside the icon, not under it
[01:37] <Shaan7> aseigo: yes
[01:38] <aseigo> which means also having code for detecting when there is only one group (or other such edge cases) which would look like "one column" and not switching into list mode at that point
[01:38] <aseigo> honestly, far easier to just say "go into listing mode"
[01:38] <aseigo> and even if the implementation uses the grid with one column, that's an internal implementation detail
[01:38] <notmart> yes, just a single api method after all :)
[01:38] <Shaan7> notmart: oh, and I still didn't get it, why isn't screen size important in deciding item size for grid?
[01:39] <Shaan7> taking my TV screen example ...
[01:39] <aseigo> Shaan7: because you don't know how far away the screen is
[01:39] <notmart> Shaan7: it is, but is not the only factor
[01:39] <notmart> Shaan7: what is important is to know what will be the perceived real size
[01:39] <aseigo> a 5" screen that's 10" from my face is "way bigger" than a 10' screen that is 8' from my face
[01:39] <Shaan7> notmart: which only the user knows
[01:39] <aseigo> notmart: exactly
[01:39] <aseigo> no, we just "cheat"
[01:40] <aseigo> and we say "we never show more than N items at a time in a grid"
[01:40] <aseigo> we can offer zoom controls if we wish
[01:40] <Shaan7> aseigo: thats what I wanted
[01:40] <aseigo> but offer a sensible default. e.g. 3 rows
[01:40] <Shaan7> rather scared of cluttering the ui though
[01:40] <notmart> there could be an "initial setup" ui
[01:40] <aseigo> well, isn't this what hte control bar is for?
[01:41] <notmart> like press + or - until you can clearly see this from where you are
[01:41] <aseigo> i don't need the "play, stop" etc buttons when i'm viewing my media
[01:41] <aseigo> er, browsing my media
[01:41] <Shaan7> aseigo: might want to pause the music playin in background
[01:41] <aseigo> replace those with a title text and a way to zoom
[01:41] <Shaan7> while you're viewing pictures
[01:41] <notmart> the top and bottom bars should be there just when is playing and probably mutually exclusive with the inner stuff
[01:42] <aseigo> Shaan7: two options; have a "pause/play" button whenever media is playing in that bar or have a "jump to playing media" button
[01:42] <Shaan7> aseigo: first one is better
[01:42] <aseigo> notmart: i think the top bar could be used quite nicely when browsing
[01:42] <notmart> could it have the categories?
[01:43] <aseigo> Shaan7: you'll still want a way to jump to the media player :)
[01:43] <Shaan7> aseigo: we have mode switching on the top bar
[01:43] <aseigo> Shaan7: providing a way to do that will be required i think ... otherwise it gets annoying to figure out how to "get back" to the audi player
[01:43] <Shaan7> i don't want to goto home everytime i want to switch the mode
[01:44] <aseigo> but it also doesn't work to have every item shown on the home page up there
[01:44] <Shaan7> aseigo: I wish you'd been here before, we discussed about the mode switching :)
[01:44] <aseigo> personally, i'd recommend something a lot simpler: a home button, and a button for each actively playing module
[01:44] <Shaan7> aseigo: good point
[01:44] <aseigo> so if i'm playing music and go to view images i get: Home | Audio
[01:45] <aseigo> if i'm just viewing photo albums i get: Home
[01:45] <aseigo> if i have a video playing, i get: Home | Video
[01:45] <Shaan7> got it
[01:45] <aseigo> when i go to the audio player and i've got a photo album open, i get: Home | Audio Photo
[01:46] <aseigo> this will allow it to do tripple duty: show what's currently running, shortcut jumps to those items, a way to go back to home
[01:46] <aseigo> the Pause button would apply to all open players
[01:46] <aseigo> that means players need a "stop for a moment" slot
[01:46] <aseigo> when i'm in the home screen, there are no bars
[01:46] <aseigo> top or bottom
[01:46] <aseigo> when i select an item, i get the grid browser
[01:48] <Shaan7> aseigo: I am playing music, goto home screen, suddenly got a phone call so have to pause music, i need the top bar in home in this case
[01:48] <aseigo> categorization options listed along the top above the grid (Albums, Artists, Years....), what i'm currently viewing perhaps as a centered title ("Audio By Album") in the top bar, home + jump shortcuts to the left, pause (if anything is playing) to the right along with a zoom?
[01:48] <aseigo> Shaan7: if the music is playing, pressing music takes you to the player
[01:49] <aseigo> Shaan7: it doesn't take you back to browsing (imho anyways )
[01:49] <Shaan7> aseigo: extra effort
[01:49] <aseigo> but also usually what one wants
[01:49] <aseigo> if i'm already playing music and i've gone out to do something else, if i go back into music .. take me back to where i was
[01:49] <aseigo> e.g. playing music
[01:50] <aseigo> if i want to browse i can either stop the music or go to the "browse" jump shortcut (which would appear on the top bar ... look like a "grid view" icon)
[01:50] <aseigo> and this only matters when i've left the music player window to do something else
[01:50] <aseigo> if i'm still in the music player or in the browser, pause is one step
[01:50] <aseigo> this keeps the home page beautiful and simple
[01:51] <aseigo> and if i have a nice remote control, it has a pause button on it anyways
[01:51] <Shaan7> thats better :)
[01:51] <aseigo> so a global pause/play action is obviously necessary
[01:52] <Shaan7> aseigo: pause all, and play currently active
[01:52] <aseigo> as is a set of "jump icons" representing home, browser and active players (if any)
[01:53] <aseigo> Shaan7: i don't think it needs to be that clever. otherwise it will get clumsy for the user to interact with (do i want to pause all, just this one, just that one?)
[01:53] <aseigo> Shaan7: let the user manage what is playing and what is not with a shortcut for "pause whatever is playing"
[01:53] <Shaan7> aseigo: maximum playing things will be slideshow and music together
[01:53] <aseigo> if they want to just stop the music but keep the video going, they can "jump" to the music player and hit pause/stop there
[01:53] <Shaan7> simultaneoulsy, that is
[01:54] <Shaan7> aseigo: ok
[01:54] <aseigo> not particularly true
[01:54] <aseigo> if i want to watch the Wizard of Oz while listening to Dark Side of the Moon (classic one, google it if you haven't heard about that combo) i should be able to
[01:54] <aseigo> or if i want to listen to music while checking the weather
[01:54] <Shaan7> aseigo: whoa, looks like a very unusual use case to me
[01:54] <Shaan7> video+music
[01:55] <Shaan7> DJ center
[01:55] <aseigo> or have the football game running  (so i'm listening to the commentary) while looking at the news quickly, i shoudl be able to
[01:55] <aseigo> the idea is to let the user play something, jump somewhere else, and come back quickly
[01:55] <aseigo> so, play X, jump to Y, go back to X
[01:55] <aseigo> browse X, play X, browse more X, play a different X
[01:55] <notmart> hmm, Wizard of Oz and Dark Side of the Moon... just while eating my favourite colorful mushrooms :)
[01:56] <aseigo> browse X, play X, browse Y, pause X, play Y
[01:56] <aseigo> those are the paths that should be short and most importantly _clear_
[01:56] <aseigo> which is why i like the jump icons in the top bar, a home page, a global pause button
[01:56] <aseigo> it handles -all- of those use cases
[01:57] <aseigo> without presenting more than i need at any moment
[01:57] <Shaan7> aseigo: and what do you expect the "global" pause button to do ?
[01:57] <aseigo> the worst case scenario is "play X, jump to Y" which implies going home
[01:57] <aseigo> Shaan7: to pause anything that's currently playing, and then unpause it when i click on it again
[01:57] <Shaan7> aseigo: that is, pause all
[01:57] <aseigo> but only unpause what was previously playing
[01:58] <aseigo> don't unpause things that were paused when pressed the global pause ;)
[01:58] <Shaan7> whoa
[01:58] <aseigo> (which is why there's need for a global pause setting)
[01:58] <aseigo> it's not that hard
[01:58] <Shaan7> yeah, just hard to read ;)
[01:58] <aseigo> :)
[01:58] <Shaan7> so many pauses
[01:58] <aseigo> paws ;P
[01:59] <aseigo> anyways..
[01:59] <aseigo> so from that we get something really, really clear IMHO as to what the UI should be working towards
[01:59] <aseigo> i don't see any of that in the GSoC proposal
[02:00] <aseigo> and to me, that's the most important thing here: what is the goal.
[02:00] <Shaan7> aseigo: ok, I thought including sooo much detail will make in unecessarily lengthy
[02:00] <aseigo> what is the design that meets that goal.
[02:01] <aseigo> Accessibility is not even very useful here either, btw. in fact, for a media center "Accessibility" doesn't mean keyboard shortcuts, it means reading out what's currently selected on the screen :)
[02:01] <aseigo> in any case... yes, MORE detail is BETTER not worse
[02:01] <aseigo> and a very, very clear game plan will be critical to getting this accepted
[02:01] <Shaan7> aseigo: ok, maybe I chose the wrong word for "the key presses should work correctly"
[02:01] <aseigo> and if the use cases all revolve around "using plasma on a t.v." it will be next to impossible to get it approved because of limited #s of spots and how little KDE workspaces are currently used on T.V.s
[02:02] <aseigo> absolutely mention the laptop connection (and how laptops are used as portable media players as well as to plug into large screens)
[02:03] <Shaan7> aseigo: ok, and I should go into all the detail? I've no problem, just thinking if people like to read loads of detail in the proposal, which you've a better idea, obviously
[02:03] <aseigo> provide some graphical mockups (even if they are as crappy as the one i just did ;) to show the point
[02:03] <Shaan7> aseigo: ok, even I'm not an artist ;)
[02:03] <aseigo> i'm not either
[02:04] <aseigo> i'm sure you can tell that clearly from the picture i posted ;)
[02:04] <Shaan7> aseigo: and from your presentation in which you crossed trolltech to make it nokia :P
[02:04] <aseigo> haha. yes
[02:04] <aseigo> done the morning it was announced, actually :P
[02:05] <Shaan7> oh ok
[02:05] <aseigo> but yeah, with some  graphics showing wireframe sketches of the target deliverables (hand drawn, done in gimp/krita, whatever)
[02:05] <aseigo> clearly noting the use cases
[02:05] <Shaan7> aseigo: ok, noted
[02:06] <aseigo> and go into some detail about the interaction your project will result in (e.g. top bar navigation? grid view improvements?)
[02:06] <aseigo> because then it's very concrete, the other kde peopel will know you put a lot of though tinto it are this is SERIOUS and we can measure deliverables very easily
[02:06] <aseigo> GSoC spots are heavily competed for
[02:06] <Shaan7> pretty obvious
[02:07] <aseigo> and if we can get a good PMC GSoC so that we can ship a version of it this year, then plasma will have our four shells going very well and we'll have made it one huge step towards achieving our goals :)
[02:07] <Shaan7> :)
[02:07] <aseigo> pocketable, mobility (tablet, netbook), desktop, media center
[02:08] <Shaan7> aseigo: oh, one more thing, i want to keep this log on the techbase page on PMC, how do i store it in the techbase and link to it?
[02:08] <Shaan7> it might be useful for others who are working :)